//
//  AppDelegate.swift
//  HSP
//
//  Created by Keyur Ashra on 15/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import CoreData
import FBSDKLoginKit
import GGLSignIn
import GoogleSignIn
import FacebookCore
import FacebookLogin
import IQKeyboardManagerSwift
import GooglePlaces
import UserNotifications
import Firebase
import FirebaseCore


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var mAuthorizationManager: AuthorizationManager?
    var mStudentListingData = [StudentListingData]()
    var mFriendList = [GetFriendListData]()
    var mCollaborateListingData = [ChatListingData]()

    var notificationData: String!
    
    var appdelSocket = SocketClient()
   // "AIzaSyC1C6Gjkb8TP9nnQh57ehaTAOd3t6IzRDQ"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Override point for customization after application launch.
        self.createJPMenuView()
        
        //KeyboardMethods
        IQKeyboardManager.sharedManager().enable = true
        
        //Google Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        //Facebook Initialize Sign-in
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //AuthorizationManagerForRefreshToken
        mAuthorizationManager = AuthorizationManager()
        
        //GooglePlacesAPIKEY
        GMSPlacesClient.provideAPIKey(GOOGLE_PLACES_KEY)
        //GMSServices.provideAPIKey(GOOGLE_PLACES_KEY)
        
        //Socket Connect
        appdelSocket.connect()
        
        
        //Push Notifiction
        
        FIRApp.configure()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification), name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {  
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
        return true
    }
    
    /* //Facebook Integration
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        //return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }*/
    
    //MARK:- Open Social Media
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        //Facebook Integration
         SDKApplicationDelegate.shared.application(app, open: url, options: options)
        
        //Google Integration
         GIDSignIn.sharedInstance().handle(url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation])
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
         FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "HSP")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    //Root To CreateJPMenuView
    public func createJPMenuView() {
        if Utility.getUserLogin() == false{
            // create viewController code...
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let mainViewController = storyboard.instantiateViewController(withIdentifier: "SocialSignUpViewController") as! SocialSignUpViewController
            let leftViewController = storyboard.instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
            let rightViewController = storyboard.instantiateViewController(withIdentifier: "CollaborateInviteViewController") as! CollaborateInviteViewController
            
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            
            //SetNavigation Color
            let navigationBarAppearace = UINavigationBar.appearance()
            navigationBarAppearace.tintColor = UIColor(hex: 0xffffff)
            navigationBarAppearace.barTintColor = UIColor(hex: 0x252A32)
            //252A32 - COLOR_PIMARY_DARK
            //2E343E - COLOR_TOOLBAR
            
            // change navigation item title color
            navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            navigationBarAppearace.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Nunito-Regular", size: 17)!,NSForegroundColorAttributeName: UIColor.white]
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
            
            let drawerMenuController = JPDrawerViewController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
            drawerMenuController.automaticallyAdjustsScrollViewInsets = true
            drawerMenuController.delegate = mainViewController
            self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
            self.window?.rootViewController = drawerMenuController
            self.window?.makeKeyAndVisible()
        } else {
            // create viewController code...
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let mainViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            let leftViewController = storyboard.instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
            let rightViewController = storyboard.instantiateViewController(withIdentifier: "CollaborateInviteViewController") as! CollaborateInviteViewController
            
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            
            //SetNavigation Color
            let navigationBarAppearace = UINavigationBar.appearance()
            navigationBarAppearace.tintColor = UIColor(hex: 0xffffff)
            navigationBarAppearace.barTintColor = UIColor(hex: 0x252A32)
            //252A32 - COLOR_PIMARY_DARK
            //2E343E - COLOR_TOOLBAR
            
            // change navigation item title color
            navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            navigationBarAppearace.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Nunito-Regular", size: 17)!,NSForegroundColorAttributeName: UIColor.white]
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
            
            let drawerMenuController = JPDrawerViewController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
            drawerMenuController.automaticallyAdjustsScrollViewInsets = true
            drawerMenuController.delegate = mainViewController as? DrawerLeftRightDelegate
            self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
            self.window?.rootViewController = drawerMenuController
            self.window?.makeKeyAndVisible()
        }
        
    }
    
    /* SET NAVIGATION HOME */
    func moveHome(screenName: String)  {
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var mainViewController: UIViewController!
        
        if screenName == "userLogout" {
            mainViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        } else if screenName == "gotoHome" {
             mainViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        }
        
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "CollaborateInviteViewController") as! CollaborateInviteViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        //SetNavigation Color
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor(hex: 0xffffff)
        navigationBarAppearace.barTintColor = UIColor(hex: 0x252A32)
        
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        navigationBarAppearace.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Nunito-Regular", size: 17)!,NSForegroundColorAttributeName: UIColor.white]
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        
        let drawerMenuController = JPDrawerViewController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        drawerMenuController.automaticallyAdjustsScrollViewInsets = true
        drawerMenuController.delegate = mainViewController as? DrawerLeftRightDelegate
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = drawerMenuController
        self.window?.makeKeyAndVisible()
    }
    
    func getManager() -> AuthorizationManager {
        return mAuthorizationManager!
    }
    
    
    func setStudentList(data: [StudentListingData]) {
        self.mStudentListingData = data
    }

    func getStudentList() -> [StudentListingData] {
        return mStudentListingData
    }
    
    func setFriendList(data: [GetFriendListData]) {
        self.mFriendList = data
    }
    
    func getFriendList() -> [GetFriendListData] {
        return mFriendList
    }
    
    func setCollaborateList(data: [ChatListingData]) {
        self.mCollaborateListingData = data
    }
    
    func getCollaborateList() -> [ChatListingData] {
        return mCollaborateListingData
    }
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        print("FCMToke: \(String(describing: FIRInstanceID.instanceID().token()))")
        Utility.setFcmToken(data: FIRInstanceID.instanceID().token())


        
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    


    
//    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        
//        print("userInfo :- \(userInfo)")
//        
//        if (application.applicationState == .active) {
//            print("state :- \(application.applicationState)")
//        }
//        else {
//            Utility.setPushNotification(data: true)
//            print("userInfo :- \(userInfo)")
//
//            
//        }
//    }
    
 
    func application(_ application: UIApplication,didReceiveRemoteNotification userInfo: [AnyHashable : Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void){
        
        if ( application.applicationState == UIApplicationState.active){
            print("Active")
            // App is foreground and notification is recieved,
            // Show a alert.
        }else if( application.applicationState == UIApplicationState.background){
            print("Background")
            // App is in background and notification is received,
            // You can fetch required data here don't do anything with UI.
        }else if( application.applicationState == UIApplicationState.inactive){
            print("Inactive")
            // App came in foreground by used clicking on notification,
            // Use userinfo for redirecting to specific view controller.
            print("Recived: \(userInfo)")

        }
        completionHandler(.newData)
        
    }

    func applicationReceivedRemoteMessage(remoteMessage: FIRMessagingRemoteMessage){
        print(remoteMessage)
    }
    
    func tokenRefreshNotification(notification: NSNotification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        connectToFcm()
    }
    
    func connectToFcm() {
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        FIRMessaging.messaging().disconnect()
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                Utility.setFcmToken(data: FIRInstanceID.instanceID().token())
                print("FCMToke: \(Utility.getFcmToken())")
            }
        }
    }
    
    
    
    
    
}

