//
//  CollaborateSearchAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 15/05/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateSearchAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    //MARK:- Variable Declaration
    private var mCollaborateSearchVC: CollaborateSearchViewContoller!
    public var mList = [SearchData]()
    
    // MARK: - Constructor
    init (viewController: CollaborateSearchViewContoller, data : [SearchData]) {
        mCollaborateSearchVC = viewController
        mList = data
//        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
//        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
//        let serverResponse = StudentListingResponse(dictionary: userDictionary! as NSDictionary)
//        mList = (serverResponse?.getData())!
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (mList.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateSearchTableViewCell", for: indexPath) as! CollaborateSearchTableViewCell
        
        UIStyle.setAppButtonStyleRounded(view: cell.imgUser)
        
        let iconName = mList[indexPath.row].getImageUrl()
        cell.imgUser.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        cell.imgUser.sd_setShowActivityIndicatorView(true)
        cell.imgUser.sd_setIndicatorStyle(.gray)
        
        let fullname = mList[indexPath.row].getFirstName() + " " + mList[indexPath.row].getLastName()
        cell.lblName.text = fullname
        cell.lblCityName.text = mList[indexPath.row].getCity()
        cell.btnSendRequest.tag = indexPath.row
        cell.btnSendRequest.addTarget(self, action: #selector(openRequestPopUp), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func openRequestPopUp(sender: UIButton) {
        print("GETID = \(mList[sender.tag].getId())")
        mCollaborateSearchVC.mFriendId = mList[sender.tag].getId()
        mCollaborateSearchVC.sendRequestPopupView.isHidden = false
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }


}
