//
//  CollaborateSearchViewContoller.swift
//  HSP
//
//  Created by Keyur Ashra on 15/05/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateSearchViewContoller: UIViewController, BaseView, UITextViewDelegate {
    
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var sendRequestPopupView: UIView!
    @IBOutlet weak var txtSendReqText: UITextView!
    @IBOutlet weak var btnSendRequestOl: UIButton!

    //MARK:- Variable Declaration
    private var mCollaborateSearchAdapter: CollaborateSearchAdapter!
    private var TAG : String = "CollaborateSearchViewContoller"
    private var mProgressIndicator: ProgressIndicator?
    private var mList = [SearchData]()
    public var mFriendId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = "Search Friends"
    }
    
    //TextViewDelegate ->Placeholder Text
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter Message" {
            textView.text = ""
            textView.textColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Enter Message"
            textView.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }

    
    //MARK: BAseViewMethod
    func initialise() {
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        //SETUI
        self.tblSearch.tableFooterView = UIView()
        txtSearch.placeholder = "Search"
        txtSendReqText.placeholderText = "Enter Message"
        UIStyle.setAppButtonStyleRounded(view: btnSearch)
        UIStyle.setCustomStyle(view: sendRequestPopupView, cornerRadius: 3.0, boderColor: COLOR_SEPARATOR, borderWidth: 0.5)
        btnSendRequestOl.borders(for: [.top])
        
        txtSendReqText.delegate = self
        txtSendReqText.textColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_REGULAR, alpha: 1.0)
        txtSendReqText.text = "Enter Message"
        txtSendReqText.placeholderText = "Enter Message"
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    func refreshTableView() {
        self.tblSearch.delegate = self.mCollaborateSearchAdapter
        self.tblSearch.dataSource = self.mCollaborateSearchAdapter
        self.tblSearch.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
        self.tblSearch.tableFooterView = UIView()
        self.tblSearch.reloadData()
    }
    
    @IBAction func btnSendRequestOnClick(_ sender: Any) {
        if txtSendReqText.text.characters.count == 0 {
            showAlert(msg: "Please Enter Message")
        } else {
            self.sendRequestPopupView.isHidden = true
            callSendRequestAPI()
        }
    }
    
    @IBAction func btnSearchOnClick(_ sender: Any) {
        if txtSearch.text?.characters.count == 0 {
            showAlert(msg: "Enter Name")
        } else {
            callSearchAPI()
        }
    }

    //MARK: API Calling Methods
    func callSearchAPI() {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            
            let searchRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)friends/search?q=\(txtSearch.text!)")
            AppLog.debug(tag: self.TAG, msg: "URL = \(searchRequest)")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: searchRequest, success: { (data) -> Void in
                self.hideProgress()
                AppLog.debug(tag: self.TAG, msg: "ResponseSessionManager: \(String(describing: data))")
                self.setSearchList(data: data as! Data)
            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("DATA = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setSearchList(data: Data) {
        //hideProgress()
        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
        let serverResponse = SearchResponse(dictionary: userDictionary! as NSDictionary)
        AppLog.debug(tag: TAG, msg: "SearchListResponse: \(serverResponse!)")
        
        mList = (serverResponse?.getData())!
        self.tblSearch.tableFooterView = nil
        self.tblSearch.backgroundView = nil
        
        if mList.count == 0 {
            UIStyle.showEmptyTableView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self.tblSearch)
        } else {
            mCollaborateSearchAdapter = CollaborateSearchAdapter(viewController: self, data: mList)
            refreshTableView()
        }
    }

    //CallSendFriendRequest
    func callSendRequestAPI() {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            
            var sendFriendRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)friends/add")
            
            let getFListData = GetFriendListData()
            getFListData.setUserId(userId: String(mFriendId))
            getFListData.setInviteMessage(inviteMessage: self.txtSendReqText.text!)
            
            let serverParam = GetFriendListResponse()
            serverParam.setFriend(friend: getFListData)
            
            let paramObject = serverParam.toGetFrndRequestDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            sendFriendRequest.httpBody = jsonData
            
            //let paramObject:[String : Any] = ["friend": ["userId": "\(Utility.getMyUserId())"]]
            //studentListingRequest.httpBody = try! JSONSerialization.data(withJSONObject: params)
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(sendFriendRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: sendFriendRequest, success: { (data) -> Void in
                self.hideProgress()
                //AppLog.debug(tag: self.TAG, msg: "ResponseSessionManager: \(String(describing: data))")
                self.setSendRequestData(data: data as! Data)
            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("DATA = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setSendRequestData(data: Data) {
        //hideProgress()
        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
        let serverResponse = SendFriendRequestResponse(dictionary: userDictionary! as NSDictionary)
        AppLog.debug(tag: TAG, msg: "SendFriendRequestResponse: \(serverResponse!)")
        self.navigationController?.popViewController(animated: true)
    }
    
    

  

}
