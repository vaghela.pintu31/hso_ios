//
//  CollaborateSearchTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 15/05/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateSearchTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var btnSendRequest: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
