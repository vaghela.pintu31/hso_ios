//
//  TodoLandingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 13/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class LessonPlanDetailViewController: UIViewController, BaseView,UITextFieldDelegate {
    
    @IBOutlet weak var scrollViewDetail: UIScrollView!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblWeekDetail: UILabel!
    @IBOutlet weak var txtResult: UITextField!
    @IBOutlet weak var btnPresent: UIButton!
    @IBOutlet weak var btnAbsent: UIButton!
    @IBOutlet weak var lblAssignmentText: UILabel!
    @IBOutlet weak var imgDetailImage: UIImageView!
    @IBOutlet weak var lblNoteText: UILabel!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var lblResultTitle: UILabel!
    
    //MARK:- Variable Declaration
    private var TAG : String = "LessonPlanDetailViewController"
    private var mProgressIndicator:ProgressIndicator?
    var mEntityId: Int!
    var navTitle: String!
    var mStudentAssignmentDetails: ViewStudentAssignmentResponse?
    var studentImageUrl: String!
    var studentFullName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIStyle.setAppButtonStyleRounded(view: btnUpdate)
        UIStyle.setCustomStyle(view: txtResult, cornerRadius: 5.0, boderColor: COLOR_SEPARATOR, borderWidth: 0.5)
        
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = navTitle
        UIStyle.setAppButtonStyleRounded(view: imgProfilePic)
        imgProfilePic.sd_setImage(with: URL(string: studentImageUrl), placeholderImage: UIImage(named: noImageFound))
        imgProfilePic.sd_setShowActivityIndicatorView(true)
        imgProfilePic.sd_setIndicatorStyle(.gray)
        
        
        //  self.setNavigationBarItem()
        
        AppLog.debug(tag: self.TAG, msg: "LessonPlanListing-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        callStudentAssignmentDetailsAPI()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: BaseView Method
    func initialise() {
        lblAssignmentText.text = mStudentAssignmentDetails?.getData().getStudentAssignment().getTitle()
        lblNoteText.text = mStudentAssignmentDetails?.getData().getStudentAssignment().getNoteTitle()
        
        
        if mStudentAssignmentDetails?.getData().getAttendance() == "A" {
            btnAbsent.isSelected = true
            btnPresent.isSelected = false
        } else if mStudentAssignmentDetails?.getData().getAttendance() == "P" {
            btnAbsent.isSelected = false
            btnPresent.isSelected = true
        } else {
            btnAbsent.isSelected = false
            btnPresent.isSelected = false
        }
        
        if mStudentAssignmentDetails?.getData().getStudentAssignment().getCategory() == "test"{
            txtResult.isHidden = false
            lblResultTitle.isHidden = false
            btnUpdate.isHidden = false
            self.txtResult.text = mStudentAssignmentDetails?.getData().getScore()
        }else {
            txtResult.isHidden = true
            lblResultTitle.isHidden = true
            btnUpdate.isHidden = true
        }
        
        lblName.text = studentFullName
        lblWeekDetail.text = "Week " + "\(mStudentAssignmentDetails!.getData().getLesson().getWeeks())" + " Day " + "\(mStudentAssignmentDetails!.getData().getLesson().getDays())"
        let iconName = mStudentAssignmentDetails?.getData().getImageUrl()
        imgDetailImage.sd_setImage(with: URL(string: iconName!), placeholderImage: UIImage(named: noImageFound))
        imgDetailImage.sd_setShowActivityIndicatorView(true)
        imgDetailImage.sd_setIndicatorStyle(.gray)
        txtResult.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtResult.resignFirstResponder()
        return true
    }
    
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }

    
    //MARK: OnClickEvents
    @IBAction func btnOnCClickeAbsent(_ sender: Any) {
        if !btnAbsent.isSelected{
            callMarkPresenceAPI(attType: 1)
        }
    }
    
    @IBAction func btnOnClickePresent(_ sender: Any) {
        if !btnPresent.isSelected{
            callMarkPresenceAPI(attType: 0)
        }
    }
    
    
    @IBAction func btnOnClickeUpdate(_ sender: Any) {
        if txtResult.text?.characters.count != 0 {
            callGradeStudentAPI(grade: txtResult.text!)
        }
    }
    
    //API CALL
    func callStudentAssignmentDetailsAPI () {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let studentAssignmentDetails = ServerRequest.authenticateGetRequest(url: "\(mainURL)lessonplans/viewStudentAssignment/\(mEntityId!)")
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentAssignmentDetails)")
            Alamofire.request(studentAssignmentDetails).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    //debugPrint(response)
                    let serverResponse = ViewStudentAssignmentResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setStudentAssignmentDetails(data: value as! NSDictionary)
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        // UIStyle.showEmptyView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self)
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "SocialApi_Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //setMarkPresenceAPIData
    func setStudentAssignmentDetails(data: NSDictionary) {
        mStudentAssignmentDetails = ViewStudentAssignmentResponse(dictionary: data)!
        initialise()
    }
    
    
    
    //MARK: CALL API MARK PRESENCE
    func callMarkPresenceAPI (attType: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var markPresenceRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/markPresence")
            
            let attendanceType: String!
            if attType == 0 {
                attendanceType = "P"
            } else {
                attendanceType = "A"
            }
            
            
            let serverParam = ServerRequestParam()
            serverParam.setStudentAssignmentId(studentAssignmentId: mEntityId)
            serverParam.setAttendance(attendance: attendanceType)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            markPresenceRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(markPresenceRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(paramObject)")
            
            Alamofire.request(markPresenceRequest).responseJSON { response in
                let statusCode = response.response!.statusCode
                AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                
                switch response.result {
                case .success:
                    //debugPrint(response)
                    let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setMarkPresenceAPIData(data: value as! NSDictionary, type: attType)
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "SocialApi_Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //setMarkPresenceAPIData
    func setMarkPresenceAPIData(data: NSDictionary, type: Int) {
        //let markPresenceResponse = ServerResponse(dictionary: data)
        //Utility.showTost(strMsg: (markPresenceResponse?.getMessage())!, view: self.view)
        
        if type == 0 {
            btnPresent.isSelected = true
            btnAbsent.isSelected = false
        } else {
            btnPresent.isSelected = false
            btnAbsent.isSelected = true
        }
        
        
    }
    
    //CAllGradeStudentAPI
    func callGradeStudentAPI (grade: String) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var gradeStudentRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/scoreStudent")
            
            let serverParam = ServerRequestParam()
            serverParam.setStudentAssignmentId(studentAssignmentId:  mEntityId)
            serverParam.setScore(score: grade)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            gradeStudentRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(gradeStudentRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(paramObject)")
            
            Alamofire.request(gradeStudentRequest).responseJSON { response in
                let statusCode = response.response!.statusCode
                AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                
                switch response.result {
                case .success:
                    debugPrint(response)
                    let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            //let markPresenceResponse = ServerResponse(dictionary: value)
                            //Utility.showTost(strMsg: (markPresenceResponse?.getMessage())!, view: self.view)
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "SocialApi_Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
}
