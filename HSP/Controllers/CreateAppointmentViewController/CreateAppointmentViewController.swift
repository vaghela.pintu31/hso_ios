//
//  CreateAppointmentViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 26/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire
import GooglePlaces

class CreateAppointmentViewController: UIViewController, BaseView, UITextFieldDelegate {
    
    @IBOutlet weak var txtAppointmentTitle: MKTextField!
    @IBOutlet weak var DescriptionTextView: UITextView!
    @IBOutlet weak var txtLocation: MKTextField!
    @IBOutlet weak var txtStartDate: MKTextField!
    @IBOutlet weak var txtEndDate: MKTextField!
    @IBOutlet weak var txtStartTime: MKTextField!
    @IBOutlet weak var txtEndTime: MKTextField!
    @IBOutlet weak var imgCalSdate: UIImageView!
    @IBOutlet weak var imgCalEdate: UIImageView!
    @IBOutlet weak var descHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnCreateOl: UIButton!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var tblPopUp: UITableView!
    
    
    //MARK:- Variable Declaration
    var TAG : String = "CreateAppointmentViewController"
    private var mProgressIndicator:ProgressIndicator?
    public var mFabClick: Bool?
    public var mSelectedAppId: Int?
    public var mViewAppointment: Bool?
    
    var placesClient: GMSPlacesClient!
    var fetcher: GMSAutocompleteFetcher?
    var autoCompletePossibilities = [AutoComplete]()
    
    private var mLatitude = 0.0
    private var mLongitude = 0.0
    
    var appointmentParam = Appointment()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        placesClient = GMSPlacesClient.shared()
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func placeAutocomplete(query: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        placesClient.autocompleteQuery(query, bounds: nil, filter: nil, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            
            if let results = results {
                self.autoCompletePossibilities.removeAll()
                let regularFont = UIFont.systemFont(ofSize: UIFont.labelFontSize)
                let boldFont = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
                
                for result in results {
                    let autoComplete = AutoComplete()
                    let bolded = result.attributedPrimaryText.mutableCopy() as! NSMutableAttributedString
                    bolded.enumerateAttribute(kGMSAutocompleteMatchAttribute, in: NSMakeRange(0, bolded.length), options: []) {
                        (value, range: NSRange, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                        let font = (value == nil) ? regularFont : boldFont
                        bolded.addAttribute(NSFontAttributeName, value: font, range: range)
                    }
                    
                    let regularText = result.attributedSecondaryText!.mutableCopy() as! NSMutableAttributedString
                    regularText.enumerateAttribute(kGMSAutocompleteMatchAttribute, in: NSMakeRange(0, regularText.length), options: []) {
                        (value, range: NSRange, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                        let font = (value == nil) ? regularFont : regularFont
                        regularText.addAttribute(NSFontAttributeName, value: font, range: range)
                    }
                    
                    autoComplete.setPlaceId(placeId: result.placeID!)
                    autoComplete.setPrimaryText(primaryText: bolded)
                    autoComplete.setSecondaryText(secondaryText: regularText)
                    autoComplete.setFullText(fullText: result.attributedFullText)
                    self.autoCompletePossibilities.append(autoComplete)
                }
                self.tblPopUp.reloadData()
            }
        })
    }
    
    
//    func textViewDidChange(_ textView: UITextView) {
//        let size = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
//        if size.height != descHeightConstraint.constant && size.height > textView.frame.size.height{
//            descHeightConstraint.constant = size.height
//            textView.setContentOffset(CGPoint.zero, animated: false)
//        }
//    }
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtAppointmentTitle.resignFirstResponder()
        txtLocation.resignFirstResponder()
        txtStartTime.resignFirstResponder()
        txtEndTime.resignFirstResponder()
        txtStartDate.resignFirstResponder()
        txtEndDate.resignFirstResponder()
        DescriptionTextView.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtStartDate {
            txtStartDateOnClick(textField as! MKTextField)
        } else if textField == txtEndDate {
            txtEndDateOnClick(textField as! MKTextField)
        } else if textField == txtStartTime {
            txtStartTimeOnClick(textField as! MKTextField)
        } else if textField == txtEndTime {
            txtEndTimeOnClick(textField as! MKTextField)
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
        fetcher?.sourceTextHasChanged(textField.text!)
        
        if (textField.text?.characters.count)! > 3 {
            placeAutocomplete(query: textField.text!)
            self.popUpView.isHidden = false
            self.popUpView.fadeIn()
        }
    }
    
    //TextViewDelegate ->Placeholder Text
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter Description" {
            textView.text = ""
            textView.textColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Enter Description"
            textView.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
  
    //MARK: BaseView Method
    /* Initialize View */
    func initialise() {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("create_appointment", comment: "")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        txtAppointmentTitle.layer.borderColor = UIColor.clear.cgColor
        txtLocation.layer.borderColor = UIColor.clear.cgColor
        txtStartDate.layer.borderColor = UIColor.clear.cgColor
        txtEndDate.layer.borderColor = UIColor.clear.cgColor
        txtStartTime.layer.borderColor = UIColor.clear.cgColor
        txtEndTime.layer.borderColor = UIColor.clear.cgColor
        DescriptionTextView.layer.borderColor = UIColor.clear.cgColor
        
        //setDelegate
        txtLocation.delegate = self
        tblPopUp.delegate = self
        tblPopUp.dataSource = self
        UIStyle.setAppButtonStyle(view: popUpView)
        
        //SetPlaceHolderText
        txtAppointmentTitle.placeholder = NSLocalizedString("appointment_title", comment: "")
        txtLocation.placeholder = NSLocalizedString("location", comment: "")
        txtStartDate.placeholder = NSLocalizedString("start_date", comment: "")
        txtEndDate.placeholder = NSLocalizedString("end_date", comment: "")
        txtStartTime.placeholder = NSLocalizedString("start_time", comment: "")
        txtEndTime.placeholder = NSLocalizedString("end_time", comment: "")
        
        //UISetup
        UIStyle.setAppButtonStyleRounded(view: btnCreateOl)
        UIStyle.setCustomStyle(view: DescriptionTextView, cornerRadius: 3.0, boderColor: COLOR_SEPARATOR, borderWidth: 1.0)
        DescriptionTextView.delegate = self
        DescriptionTextView.textColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_REGULAR, alpha: 1.0)
        DescriptionTextView.text = NSLocalizedString("enter_description", comment: "")
        DescriptionTextView.placeholderText = "Enter Description"
        
        if mFabClick != true {
            self.navigationItem.title = NSLocalizedString("update_appointment", comment: "")
            btnCreateOl.setTitle(NSLocalizedString("update", comment: ""), for: .normal)
            callViewAppointmentAPI(id: mSelectedAppId!)
        }
 
        
        txtLocation?.autoresizingMask = .flexibleWidth
        txtLocation?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                               for: .editingChanged)
    }
    
    func editAppointment() {
        txtAppointmentTitle.text = appointmentParam.getTitle()
        DescriptionTextView.text = appointmentParam.getDescription()
        txtLocation.text = appointmentParam.getLocation()
        
        let stdate = Utility.convertDBDateToString(dateString: (appointmentParam.getStartDate()))
        txtStartDate.text = stdate
        
        let endate = Utility.convertDBDateToString(dateString: (appointmentParam.getEndDate()))
        txtEndDate.text = endate
        
        txtStartTime.text = appointmentParam.getStartTime()
        txtEndTime.text = appointmentParam.getEndTime()
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //MARK: OnClickEvents
    @IBAction func txtStartDateOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.startDateValueChanged), for: UIControlEvents.valueChanged)
    }
    
    //DatePicker
    func startDateValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: sender.date)
        self.txtStartDate.text = strDate
    }
    
    
    @IBAction func txtEndDateOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.endDateValueChanged), for: UIControlEvents.valueChanged)
        
    }
    
    func endDateValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: sender.date)
        self.txtEndDate.text = strDate
    }
    
    @IBAction func txtStartTimeOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.startTimeValueChanged), for: UIControlEvents.valueChanged)
        
    }
    
    func startTimeValueChanged(sender:UIDatePicker) {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        timeFormatter.dateFormat = "hh:mm a"
        let strDate = timeFormatter.string(from: sender.date)
        self.txtStartTime.text = strDate
    }
    
    
    @IBAction func txtEndTimeOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.endTimeValueChanged), for: UIControlEvents.valueChanged)
    }
    
    func endTimeValueChanged(sender:UIDatePicker) {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        timeFormatter.dateFormat = "hh:mm a"
        let strDate = timeFormatter.string(from: sender.date)
        self.txtEndTime.text = strDate
    }
    
    @IBAction func btnCreateOnClick(_ sender: Any) {
        DescriptionTextView.resignFirstResponder()
        if (self.txtAppointmentTitle.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_title", comment: ""))
        } else if (self.DescriptionTextView.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_description", comment: ""))
        } else if (self.txtLocation.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_location", comment: ""))
        } else if (self.txtStartDate.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_start_date", comment: ""))
        } else if (self.txtStartTime.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_start_time", comment: ""))
        } else if (self.txtEndDate.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_end_date", comment: ""))
        } else if (self.txtEndTime.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_end_time", comment: ""))
        } else if checkDateCompare() == false  {
            showAlert(msg: NSLocalizedString("compare_date_error", comment: ""))
        } else {
            if mFabClick != true {
                callEditAppointmentAPI(id: mSelectedAppId!)
            } else {
                callCreateAppointmentAPI()

            }
        }
    }
    
    func checkDateCompare() -> Bool{
        let stTemDate = ((txtStartDate.text)! + " " + (txtStartTime.text)!)
        let enTemDate = ((txtEndDate.text! + " " + txtEndTime.text!))
        print("Date = \(stTemDate)\n\(enTemDate)")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        let stDate = dateFormatter.date(from: stTemDate)
        let enDate = dateFormatter.date(from: enTemDate)
        
        if stDate! > enDate! {
            return false
        } else {
            return true
        }
    }
    
    //MARK: CALLING API Methods
    //CALL_CREATE_APPOINTMENT_API
    func callCreateAppointmentAPI () {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            var createAppointmentRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)appointments/add")
            
            let appointmentParam = Appointment()
            appointmentParam.setTitle(title: (txtAppointmentTitle.text)!)
            appointmentParam.setDescription(description: (DescriptionTextView.text)!)
            appointmentParam.setStartDate(startDate: (Utility.convertLocalDateToServerDate(string: txtStartDate.text!)))
            appointmentParam.setStartTime(startTime: (Utility.convertLocalTimeToServerTime(dateString: txtStartTime.text!)))
            appointmentParam.setEndDate(endDate: (Utility.convertLocalDateToServerDate(string: txtEndDate.text!)))
            appointmentParam.setEndTime(endTime: (Utility.convertLocalTimeToServerTime(dateString: txtEndTime.text!)))
            appointmentParam.setLocation(location: (txtLocation.text)!)
            appointmentParam.setLatitude(latitude: mLatitude)
            appointmentParam.setLongitude(longitude: mLongitude)
            
            let serverParam = AppointmentData()
            serverParam.setAppointment(Appointment: appointmentParam)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            createAppointmentRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(createAppointmentRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            Alamofire.request(createAppointmentRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let appointmentResponse = AppointmentResponse(dictionary: response.result.value as! NSDictionary)
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setCreateAppointmentData(data: value as! NSDictionary)
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (appointmentResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (appointmentResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setCreateAppointmentData(data: NSDictionary) {
        hideProgress()
        let appointmentResponse = AppointmentResponse(dictionary: data)
        AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: appointmentResponse?.getMessage(), action: { (CancelButton) -> Void in
            self.navigationController?.popViewController(animated: true)
        })
    }
    //
    
    func getPlaceLatLng(placeId: String) {
        placesClient.lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                self.mLatitude = 0.0
                self.mLongitude = 0.0
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeId)")
                self.mLatitude = 0.0
                self.mLongitude = 0.0
                return
            }
            
            print("Place name \(place.name)")
            self.mLatitude = place.coordinate.latitude
            self.mLongitude = place.coordinate.longitude
            
            print("Place Lat \(self.mLatitude)")
            print("Place Lng \(self.mLongitude)")
            
        })
    }
    
    
    //CALL_EDIT_APPOINTMENT_API
    func callEditAppointmentAPI(id: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            print("id = \(id)")
            let myUrl = "\(mainURL)appointments/edit/\(id)"//static
            //Dynammic
            //let myUrl = "\(mainURL)appointments/view/\(self.mList[id].getId())"
            
            var editAppointmentRequest = ServerRequest.authenticatePostRequest(url: myUrl)
            
            appointmentParam.setTitle(title: (txtAppointmentTitle.text)!)
            appointmentParam.setDescription(description: (DescriptionTextView.text)!)
            appointmentParam.setStartDate(startDate: (Utility.convertLocalDateToServerDate(string: txtStartDate.text!)))
            appointmentParam.setStartTime(startTime: (Utility.convertLocalTimeToServerTime(dateString: txtStartTime.text!)))
            appointmentParam.setEndDate(endDate: (Utility.convertLocalDateToServerDate(string: txtEndDate.text!)))
            appointmentParam.setEndTime(endTime: (Utility.convertLocalTimeToServerTime(dateString: txtEndTime.text!)))
            appointmentParam.setLocation(location: (txtLocation.text)!)
            appointmentParam.setLatitude(latitude: mLatitude)
            appointmentParam.setLongitude(longitude: mLongitude)
            
            let serverParam = AppointmentData()
            serverParam.setAppointment(Appointment: appointmentParam)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            editAppointmentRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(editAppointmentRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")

            Alamofire.request(editAppointmentRequest).responseJSON { response in
                //debugPrint(response)
                
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    
                    let deleteServerResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setEditAppointmentData(data: value as! NSDictionary, id: id)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setEditAppointmentData(data: NSDictionary, id: Int) {
        hideProgress()
        AppLog.debug(tag: TAG, msg: "ResponseAppointmentResponse = \(data)")
        //let serverResponse = AppointmentResponse(dictionary: data)
        //Utility.showTost(strMsg: (serverResponse?.getMessage())!, view: self.view)
    }
    
    //CALL_VIEW_APPOINTMENT_API
    func callViewAppointmentAPI(id: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let myUrl = "\(mainURL)appointments/view/\(id)"
            //Dynammic
            //let myUrl = "\(mainURL)appointments/view/\(self.mList[id].getId())"
            
            let editAppointmentRequest = ServerRequest.authenticateGetRequest(url: myUrl)
            AppLog.debug(tag: self.TAG, msg: "URL = \(editAppointmentRequest)")
            AppLog.debug(tag: self.TAG, msg: "Header = \(String(describing: editAppointmentRequest.allHTTPHeaderFields))")
            
            Alamofire.request(editAppointmentRequest).responseJSON { response in
                //debugPrint(response)
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let appointmentResponse = AppointmentResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            self.setViewAppointmentData(data: appointmentResponse!, id: id)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (appointmentResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (appointmentResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setViewAppointmentData(data: AppointmentResponse, id: Int) {
        hideProgress()
        appointmentParam = data.getData().getAppointment()
        mFabClick = false
        editAppointment()
        
        //Only_View_Appointment
        if mViewAppointment == true {
            btnCreateOl.isHidden = true
            self.view.isUserInteractionEnabled = false
        }
    }
    
    
}

extension CreateAppointmentViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //print("Content Height \(self.DescriptionTextView.contentSize.height)")
        if (self.DescriptionTextView.contentSize.height < self.descHeightConstraint.constant) {
            self.DescriptionTextView.isScrollEnabled = false
        } else {
            self.DescriptionTextView.isScrollEnabled = true
        }
        return true
    }
}

extension CreateAppointmentViewController: GMSAutocompleteFetcherDelegate {
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        let resultsStr = NSMutableString()
        for prediction in predictions {
            resultsStr.appendFormat("%@\n", prediction.attributedPrimaryText)
        }
        Utility.showTost(strMsg: resultsStr as String, view: self.view)
       // resultText?.text = resultsStr as String
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        Utility.showTost(strMsg: "\(error.localizedDescription)", view: self.view)
      //  resultText?.text = error.localizedDescription
    }
}

extension CreateAppointmentViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autoCompletePossibilities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        let index = indexPath.row as Int
        let primaryText = cell.viewWithTag(501) as! UILabel
        primaryText.attributedText = autoCompletePossibilities[index].getPrimaryText()
        let secondaryText = cell.viewWithTag(502) as! UILabel
        secondaryText.attributedText = autoCompletePossibilities[index].getSecondaryText()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtLocation.text = self.autoCompletePossibilities[indexPath.row].getFullText().string
        print("PlaceId = \(self.autoCompletePossibilities[indexPath.row].getPlaceId())")
        self.popUpView.isHidden = true
        getPlaceLatLng(placeId: self.autoCompletePossibilities[indexPath.row].getPlaceId())
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 57
    }
}

