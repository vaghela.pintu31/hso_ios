//
//  LessonPlanCreateNotesViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 10/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire


class LessonPlanCreateNotesViewController: UIViewController, BaseView ,UITextFieldDelegate{
    
    
    @IBOutlet weak var lblMainWeek: UILabel!
    @IBOutlet weak var txtDay1: UITextField!
    @IBOutlet weak var txtDay2: UITextField!
    @IBOutlet weak var txtDay3: UITextField!
    @IBOutlet weak var txtDay4: UITextField!
    @IBOutlet weak var txtDay5: UITextField!
    @IBOutlet weak var txtDay6: UITextField!
    @IBOutlet weak var txtDay7: UITextField!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG : String = "LessonPlanCreateAssignmentsViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var arrayOfButton = ["Assignment", "Test"]
    private var mLessonPlanList = [LessonPlan]()
    private var mainVC: LessonPlanCreateAssignmentsNotesVC!
    private var weekAdd = 0
    
    //TabSetup
    private var mPageIndex : Int = 1
    private var mPagerView: UIPageViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if mainVC.getSubmitFlag() == 0 {
            mainVC.setWeekNotes(week: weekAdd)
            if mainVC!.getLessonPlan().count != 0 {
                let mLPCreateData = mainVC!.getLessonPlan()[0]
                var newFlag = true
                
                if mLPCreateData.getWeeks() == 0 {
                    newFlag = false
                } else {
                    if mLPCreateData.getDays() == 0 {
                        newFlag = false
                    } else {
                        if mLPCreateData.getHours() == 0 {
                            newFlag = false
                        } else {
                            if mLPCreateData.getName() == "" {
                                newFlag = false
                            }
                        }
                    }
                }
                
                if newFlag {
                    setAssignment(flag: false, indexOfWeek: weekAdd)
                }
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    
    func loadView(pageIndex : Int,pagerView: UIPageViewController!, data: [LessonPlan], mainVC: LessonPlanCreateAssignmentsNotesVC) -> LessonPlanCreateNotesViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LessonPlanCreateNotesViewController") as! LessonPlanCreateNotesViewController
        vc.mPageIndex = mPageIndex
        vc.mPagerView = mPagerView
        vc.mainVC = mainVC
        vc.mLessonPlanList = data
        return vc
    }
    
    //MARK: Getter Setter Methods
    public func getPageIndex() -> Int {
        return mPageIndex
    }
    
    
    //MARK: BaseView Method
    func initialise() {
        weekAdd = mainVC.getWeekNotes()
        lblMainWeek.text = "Week \(weekAdd + 1)"
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** Set Data **/
        setData()
        setupView()
    }
    
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtDay1.resignFirstResponder()
        txtDay2.resignFirstResponder()
        txtDay3.resignFirstResponder()
        txtDay4.resignFirstResponder()
        txtDay5.resignFirstResponder()
        txtDay6.resignFirstResponder()
        txtDay7.resignFirstResponder()
        return true
    }
    
    func textFieldDelegateSet() {
        txtDay1.delegate = self
        txtDay2.delegate = self
        txtDay3.delegate = self
        txtDay4.delegate = self
        txtDay5.delegate = self
        txtDay6.delegate = self
        txtDay7.delegate = self
    }
    
    func setData() {
        UIStyle.setAppButtonStyleRounded(view: btnSubmit)
        UIStyle.setAppButtonStyleRounded(view: btnNext)
        UIStyle.setCustomStyle(view: btnPrevious, cornerRadius: btnPrevious.frame.height/2, boderColor: COLOR_SEPARATOR, borderWidth: 1.0)
        
        //TextField_Delegate_Method_Set
        textFieldDelegateSet()
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //SetUpView
    func setupView() {
        btnSubmit.addTarget(self, action: #selector(self.btnSubmitOnClick), for: .touchUpInside)
        btnNext.addTarget(self, action: #selector(self.btnNextOnClick), for: .touchUpInside)
        btnPrevious.addTarget(self, action: #selector(self.btnPreviousOnClick), for: .touchUpInside)
        btnPrevious.isHidden = true
        
        if mainVC.getLessonPlan().count == 0 || mainVC.getLessonPlan()[0].getWeeks() == 0 {
            UIStyle.showEmptyView(message: NSLocalizedString("notes_select_week", comment: ""), viewController: self)
        } else {
            let weekCount = mainVC.getLessonPlan()[0].getWeeks()
            
            if weekCount == 1 {
                btnNext.isHidden = true
                btnPrevious.isHidden = true
                btnSubmit.isHidden = false
            } else if weekAdd == weekCount - 1 {
                btnNext.isHidden = true
                btnPrevious.isHidden = false
                btnSubmit.isHidden = false
            } else if weekAdd == 0 {
                btnPrevious.isHidden = true
                btnSubmit.isHidden = true
                btnNext.isHidden = false
            } else {
                btnNext.isHidden = false
                btnPrevious.isHidden = false
                btnSubmit.isHidden = true
            }
            
            //VisibleDaysInputAdd
            visibleDaysInput()
            displayData()
        }
    }
    
    func btnSubmitOnClick() {
        let mLPCreateData = mainVC!.getLessonPlan()[0]
        if mLPCreateData.getWeeks() == 0 {
            showAlert(msg: "Please Select Week")
        } else {
            if mLPCreateData.getDays() == 0 {
                showAlert(msg: "Please Select Day")
            } else {
                if mLPCreateData.getHours() == 0 {
                    showAlert(msg: "Please Select Hour")
                } else {
                    if mLPCreateData.getName() == "" {
                        showAlert(msg: "Please Enter Name")
                    } else {
                        setAssignment(flag: true, indexOfWeek: weekAdd)
                    }
                }
            }
        }
    }
    
    func btnNextOnClick() {
        let oldWeek = weekAdd
        weekAdd += 1
        
        if weekAdd == mainVC.getLessonPlan()[0].getWeeks() - 1 {
            btnNext.isHidden = true
            btnPrevious.isHidden = false
            btnSubmit.isHidden = false
        } else {
            btnNext.isHidden = false
            btnPrevious.isHidden = false
            btnSubmit.isHidden = true
        }
        
        
        lblMainWeek.text = "Week \(weekAdd + 1)"
        clearTextData(indexForWeek: oldWeek)
        mainVC.setWeekNotes(week: weekAdd)

    }
    
    func btnPreviousOnClick() {
        let oldWeek = weekAdd
        weekAdd -= 1
        
        if weekAdd == 0 {
            btnPrevious.isHidden = true
            btnSubmit.isHidden = true
            btnNext.isHidden = false
        } else {
            btnPrevious.isHidden = false
            btnNext.isHidden = false
            btnSubmit.isHidden = true
        }
        
        lblMainWeek.text = "Week \(weekAdd + 1)"
        clearTextData(indexForWeek: oldWeek)
        mainVC.setWeekNotes(week: weekAdd)
    }
    
    func clearTextData(indexForWeek: Int) {
        setAssignment(flag: false, indexOfWeek: indexForWeek)
    }
    
    
    func visibleDaysInput() {
        //Days["1","2","3","4","5","6","7"]
        if mainVC.getLessonPlan()[0].getDays() == 0 {
            print("No Days Selected")
        } else {
            let daysIs:Int = mainVC.getLessonPlan()[0].getDays()
            switch daysIs{
            case 1:
                txtDay2.isHidden = true
                txtDay3.isHidden = true
                txtDay4.isHidden = true
                txtDay5.isHidden = true
                txtDay6.isHidden = true
                txtDay7.isHidden = true
                
            case 2:
                txtDay3.isHidden = true
                txtDay4.isHidden = true
                txtDay5.isHidden = true
                txtDay6.isHidden = true
                txtDay7.isHidden = true
                
            case 3:
                txtDay4.isHidden = true
                txtDay5.isHidden = true
                txtDay6.isHidden = true
                txtDay7.isHidden = true
                
            case 4:
                txtDay5.isHidden = true
                txtDay6.isHidden = true
                txtDay7.isHidden = true
                
            case 5:
                txtDay6.isHidden = true
                txtDay7.isHidden = true
                
            case 6:
                txtDay7.isHidden = true
                
            case 7:
                AppLog.debug(tag: self.TAG, msg: "All are visible")
            default:
                break
            }
        }
    }
    
    /*
     */
    func setAssignment(flag: Bool, indexOfWeek: Int) {
        let daysCount:Int = mainVC.getLessonPlan()[0].getDays()
        let dayWeek = indexOfWeek + 1
        
        var lAssignmentArrayData = [LessonAssignment]()
        lAssignmentArrayData.removeAll()
        //print("iNDEX_OF_WEEK = \(indexOfWeek)")
        for indexDay in 0..<daysCount{
            //print("Tite = \(mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[indexDay].getTitle())")
            //print("getNoteTitle = \(mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[indexDay].getNoteTitle())")
            
            switch indexDay {
            case 0:
                let lassignment = mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[0]
                lassignment.setNoteTitle(noteTitle: txtDay1.text!)
                lAssignmentArrayData.append(lassignment)
                break
                
            case 1:
                let lassignment2 = mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[1]
                lassignment2.setNoteTitle(noteTitle: txtDay2.text!)
                lAssignmentArrayData.append(lassignment2)
                break
                
            case 2:
                let lassignment3 = mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[2]
                lassignment3.setNoteTitle(noteTitle: txtDay3.text!)
                lAssignmentArrayData.append(lassignment3)
                break
                
            case 3:
                let lassignment4 = mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[3]
                lassignment4.setNoteTitle(noteTitle: txtDay4.text!)
                lAssignmentArrayData.append(lassignment4)
                break
                
            case 4:
                let lassignment5 = mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[4]
                lassignment5.setNoteTitle(noteTitle: txtDay5.text!)
                lAssignmentArrayData.append(lassignment5)
                break
                
            case 5:
                let lassignment6 = mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[5]
                lassignment6.setNoteTitle(noteTitle: txtDay6.text!)
                lAssignmentArrayData.append(lassignment6)
                break
                
            case 6:
                let lassignment7 = mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()[6]
                lassignment7.setNoteTitle(noteTitle: txtDay7.text!)
                lAssignmentArrayData.append(lassignment7)
                break
                
            default:
                break
            }
        }
        
        var laarray = mainVC.getLessonPlan()[indexOfWeek].getLessonAssignment()
        laarray.removeAll()
        laarray = lAssignmentArrayData
        mainVC.getLessonPlan()[indexOfWeek].setLessonAssignment(lessonAssignment: laarray)
        
        /*
         self.txtDay1.text = ""
         self.txtDay2.text = ""
         self.txtDay3.text = ""
         self.txtDay4.text = ""
         self.txtDay5.text = ""
         self.txtDay6.text = ""
         self.txtDay7.text = ""
         */
        
        
        if flag {
            callLessonPlanCreateAPI()
        } else {
            //print("PositionWeekAdd :- \(weekAdd)")
            let cloneLessonPlan = mainVC.getLessonPlan()[weekAdd]
            
            if cloneLessonPlan != nil {
                let notes = cloneLessonPlan.getLessonAssignment()
                
                for indexDay in 0..<notes.count {
                    //print("nTite = \(notes[indexDay].getTitle())")
                    //print("ngetNoteTitle = \(notes[indexDay].getNoteTitle())")
                    

                    switch indexDay {
                    case 0:
                        txtDay1.text = notes[indexDay].getNoteTitle()
                        break
                        
                    case 1:
                        txtDay2.text = notes[indexDay].getNoteTitle()
                        break
                        
                    case 2:
                        txtDay3.text = notes[indexDay].getNoteTitle()
                        break
                        
                    case 3:
                        txtDay4.text = notes[indexDay].getNoteTitle()
                        break
                        
                    case 4:
                        txtDay5.text = notes[indexDay].getNoteTitle()
                        break
                        
                    case 5:
                        txtDay6.text = notes[indexDay].getNoteTitle()
                        break
                        
                    case 6:
                        txtDay7.text = notes[indexDay].getNoteTitle()
                        break
                        
                    default:
                        break
                    }
                }
            }
        }
    }
    
    func checkFinalValidation() -> Bool {
        let cloneArray = mainVC.getLessonPlan()
        let counter = cloneArray.count
        for i in 0..<counter {
            var lessonAssignmentArray = cloneArray[i].getLessonAssignment()
            let countTitle = lessonAssignmentArray.count
            
            if lessonAssignmentArray.count == 0 {
                return false
            } else {
                for k in 0..<countTitle {
                    if lessonAssignmentArray[k].getTitle().characters.count == 0{
                        return false
                    }
                }
            }
        }
        return true
    }
    
    
    
    func callLessonPlanCreateAPI() {
        if checkFinalValidation() {
            
            if Reachability.isConnectedToNetwork() == true {
                showProgress()
                
                //URLRequest
                var LessonPlanCreateRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/add")
                
                let cloneArray = mainVC.getLessonPlan()
                let counter = cloneArray.count
                //print("Count = \(counter)")
                
                let finalLessonPlan = LessonPlan()
                finalLessonPlan.setName(name: cloneArray[0].getName())
                finalLessonPlan.setWeeks(weeks: cloneArray[0].getWeeks())
                finalLessonPlan.setDays(days: cloneArray[0].getDays())
                finalLessonPlan.setHours(hours: cloneArray[0].getHours())
                
                var lessonAssignmentArray = finalLessonPlan.getLessonAssignment()
                //var lessonNotesArray = finalLessonPlan.getLessonNote()
                
                for i in 0..<counter {
                    let laArray = cloneArray[i].getLessonAssignment()
                    //let notesArray = cloneArray[i].getLessonNote()
                    
                    for k in 0..<laArray.count {
                        lessonAssignmentArray.append(laArray[k])
                    }
                    
                    /*
                     for j in 0..<notesArray.count {
                     lessonNotesArray.append(notesArray[j])
                     }*/
                }
                
                finalLessonPlan.setLessonAssignment(lessonAssignment: lessonAssignmentArray)
                //finalLessonPlan.setLessonNote(lessonNote: lessonNotesArray)
                
                // mLPCreateData.setLessonAssignment(lessonAssignment: lAssignmentArrayData)
                
                let serverParam = LessonPlanCreateResponse()
                serverParam.setLessonPlan(lessonPlan: finalLessonPlan)
                
                let paramObject = serverParam.toDictionary()
                let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
                LessonPlanCreateRequest.httpBody = jsonData
                
                AppLog.debug(tag: self.TAG, msg: "URL = \(LessonPlanCreateRequest)")
                AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
                
                Alamofire.request(LessonPlanCreateRequest).responseJSON { response in
                    switch response.result {
                    case .success:
                        let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                        AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                        if (statusCode == 200) {
                            if let value = response.result.value {
                                self.hideProgress()
                                self.setLessonCreateData(data: value as! NSDictionary)
                            }else{
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.hideProgress()
                                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            }
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    case .failure(let encodingError):
                        AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                        self.hideProgress()
                        self.showAlert(msg: serverDown)
                    }
                }
            } else {
                showAlert(msg: noInternet)
            }
        }else {
            showAlert(msg: "Please fill the all value for Assignment.")
        }
    }
    
    func setLessonCreateData(data: NSDictionary) {
        hideProgress()
        AppLog.debug(tag: TAG, msg: "ResponseLessonCreate = \(data)")
        let serverResponse = ServerResponse(dictionary: data)
        AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: serverResponse?.getMessage(), action: { (CancelButton) -> Void in
            self.mainVC.setSubmitFlag(submitFlag: 1)
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func displayData() {
        //print("WEEK ADD = \(weekAdd)")
        let cloneLessonPlan = mainVC.getLessonPlan()[weekAdd]
        if cloneLessonPlan != nil {
            let notes = cloneLessonPlan.getLessonAssignment()
            for indexDay in 0..<notes.count {
                //print("Tite = \(notes[indexDay].getTitle())")
                //print("getNoteTitle = \(notes[indexDay].getNoteTitle())")
                switch indexDay {
                case 0:
                    txtDay1.text = notes[indexDay].getNoteTitle()
                    break
                    
                case 1:
                    txtDay2.text = notes[indexDay].getNoteTitle()
                    break
                    
                case 2:
                    txtDay3.text = notes[indexDay].getNoteTitle()
                    break
                    
                case 3:
                    txtDay4.text = notes[indexDay].getNoteTitle()
                    break
                    
                case 4:
                    txtDay5.text = notes[indexDay].getNoteTitle()
                    break
                    
                case 5:
                    txtDay6.text = notes[indexDay].getNoteTitle()
                    break
                    
                case 6:
                    txtDay7.text = notes[indexDay].getNoteTitle()
                    break
                    
                default:
                    break
                }
            }
        }
    }
}
