//
//  LessonPlanCreateAssignmentsNotesVC.swift
//  HSP
//
//  Created by Keyur Ashra on 10/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanCreateAssignmentsNotesVC: UIViewController, UITextFieldDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var txtLessonName: UITextField!
    @IBOutlet weak var txtSelectLesson: UITextField!
    @IBOutlet weak var txtWeeks: UITextField!
    @IBOutlet weak var txtDays: UITextField!
    @IBOutlet weak var txtHours: UITextField!
    @IBOutlet weak var collectionViewLpCreateAssiNotes: UICollectionView!
    
    /*** PageSwipParm ***/
    var pagerView: UIPageViewController!
    var tutorialViewController = [UIViewController]()
    var mSelectedIndex: Int?
    var nextPageIndex : Int = 0
    var currentPageIndex : Int = 0 {
        didSet {
            currentPageIndex = cap(pageIndex: currentPageIndex)
        }
    }
    
    var arrayOfHeader = [NSLocalizedString("assignments", comment: ""),
                         NSLocalizedString("notes", comment: "")]
    
    //LPCreateData
    private var arrayOfLesson = [String]()
    private var arrayOfWeeks = [String]()
    private var arrayOfDays = [String]()
    //private var arrayOFHours = [String]()
    var stRange: Int?
    var enRange: Int?
    
    var mLessonPlanList = [LessonPlan]()
    var weekNotesAdd = 0
    var weekAssignment = 0
    private var submitFlag = 0
    
    public func setSubmitFlag(submitFlag: Int) {
        self.submitFlag = submitFlag
    }
    
    public func getSubmitFlag() -> Int {
        return submitFlag
    }
    
    public func setWeekNotes(week: Int) {
        self.weekNotesAdd = week
    }
    
    public func getWeekNotes() -> Int {
        return weekNotesAdd
    }
    
    public func setWeekAssignment(week: Int) {
        self.weekAssignment = week
    }
    
    public func getWeekAssignment() -> Int {
        return weekAssignment
    }
    
    public func getLessonPlan() -> [LessonPlan] {
        return mLessonPlanList
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("create_lesson_plan", comment: "")
        self.navigationItem.hidesBackButton = true
        let backImage   = UIImage(named: "ic_back")!
        let newBackButton: UIBarButtonItem = UIBarButtonItem(image: backImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        //For SelectLesson
        arrayOfLesson = ["Lesson", "Chapter", "Page"]
        
        //For Weeks
        for i in 1..<55 {
            arrayOfWeeks.append("\(i)")
        }
        
        //For Days
        for i in 1..<8 {
            arrayOfDays.append("\(i)")
        }
        
//        //for Hours
//        for i in 1..<25 {
//            arrayOFHours.append("\(i)")
//        }
        
        mLessonPlanList.removeAll()
        //mLessonPlanList.append(lessonPlan)
        //lessonPlan.setName(name: txtLessonName.text!)
        
        mSelectedIndex = 0
        collectionViewLpCreateAssiNotes.delegate = self
        collectionViewLpCreateAssiNotes.dataSource = self
        collectionViewLpCreateAssiNotes.reloadData()
        pageViewSetup()
    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        submitFlag = 1
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtLessonName.resignFirstResponder()
        txtWeeks.resignFirstResponder()
        txtDays.resignFirstResponder()
        txtHours.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        //if textField == txtLessonName {
        //  lessonPlan.setName(name: txtLessonName.text!)
        //}
        return true
    }
    
    func textFieldDelegateSet() {
        txtLessonName.delegate = self
        txtWeeks.delegate = self
        txtDays.delegate = self
        txtHours.delegate = self
    }
    
    func closeKeyboard(){
        txtLessonName.resignFirstResponder()
        txtWeeks.resignFirstResponder()
        txtDays.resignFirstResponder()
        txtHours.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case txtDays:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 4
        default:
            return true
        }
    }
    
    func chageCollectionCell(index: Int){
        for cell in collectionViewLpCreateAssiNotes.visibleCells as! [LessonPlanCreateAssignmentsAndNotesCollectionViewCell] {
            cell.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)
            cell.lblChange.isHidden = true
            cell.btnHeader.titleLabel?.textColor = UIColor.white.withAlphaComponent(0.5)
        }
        
        let indexPath = NSIndexPath(row: index, section: 0)
        let cell = collectionViewLpCreateAssiNotes.cellForItem(at: indexPath as IndexPath) as! LessonPlanCreateAssignmentsAndNotesCollectionViewCell
        cell.lblChange.isHidden = false
        cell.btnHeader.titleLabel?.textColor = UIColor.white.withAlphaComponent(1.0)
        
        UIView.animate(withDuration: 1.0, animations: {
            let animation: CATransition = CATransition()
            animation.duration = 0.6
            animation.type = kCATransitionFromRight
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            cell.lblChange.layer.add(animation, forKey: "changeTextTransition")
        }, completion: nil)
        collectionViewLpCreateAssiNotes.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
    }
    
    //MARK: OnClickEvents
    @IBAction func btnSelectLessons(_ sender: Any) {
        closeKeyboard()
        selectLessons()
    }
    
    @IBAction func btnSelectWeeksOnClick(_ sender: Any) {
        closeKeyboard()
        selectWeeks()
//        if txtSelectLesson.text?.characters.count == 0 || enRange == nil {
//            Utility.showTost(strMsg: "Enter Lesson/Range", view: self.view)
//        } else {
//            closeKeyboard()
//            selectWeeks()
//        }
    }
    
    @IBAction func btnSelectDaysOnClick(_ sender: Any) {
        closeKeyboard()
        selectDays()
//        if txtSelectLesson.text?.characters.count == 0 || enRange == nil {
//            Utility.showTost(strMsg: "Enter Lesson/Range", view: self.view)
//        } else {
//            closeKeyboard()
//            selectDays()
//        }
        
    }
    
    @IBAction func btnSelectHoursOnClick(_ sender: Any) {
//        if txtSelectLesson.text?.characters.count == 0 || enRange == nil {
//            Utility.showTost(strMsg: "Enter Lesson/Range", view: self.view)
//        } else {
//            closeKeyboard()
//            selectHours()
//        }
    }
    
    //Dropdown Function
    //Select_Lessons
    func selectLessons(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Select Lessons", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = arrayOfLesson.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(self.arrayOfLesson[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtSelectLesson.text = self.arrayOfLesson[index]
                    //"Lesson", "Chapter", "Page"
                    if self.txtSelectLesson.text == "Page" {
                        self.selectRange()
                    } else {
                        self.selectSingleRange()
                    }
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func selectSingleRange(){
        let alertController = UIAlertController(title: "Select Range", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
            alert -> Void in
            
            let txtRange = alertController.textFields![0] as UITextField
            self.stRange = 0
            self.enRange = Int(txtRange.text!)
            
            if self.enRange != nil && self.enRange != 0 {
                if self.enRange! <= 1 && self.enRange! >= 999 {
                    Utility.showTost(strMsg: "Range must be 1 to 999", view: self.view)
                } else {
                    print("StartRange \(self.stRange!), EndRange \(self.enRange!)")
                    self.txtWeeks.text = self.arrayOfWeeks[0]
                    self.txtDays.text = self.arrayOfDays[0]
                    self.createLessonPlan()
                }
            } else {
                Utility.showTost(strMsg: "Enter Range", view: self.view)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Range"
            textField.keyboardType = UIKeyboardType.numberPad
            
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func selectRange(){
        let alertController = UIAlertController(title: "Select Range", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
            alert -> Void in
            
            let txtStartRange = alertController.textFields![0] as UITextField
            let txtEndRange = alertController.textFields![1] as UITextField
            
            
            self.stRange = Int(txtStartRange.text!)
            self.enRange = Int(txtEndRange.text!)
            
            if self.enRange != nil && self.enRange! != 0 && self.stRange != nil {
                if self.enRange! < self.stRange! {
                    Utility.showTost(strMsg: "End Range Must be Greater then Start Range", view: self.view)
                } else {
                    print("StartRange \(txtStartRange.text!), EndRange \(txtEndRange.text!)")
                    self.txtWeeks.text = self.arrayOfWeeks[0]
                    self.txtDays.text = self.arrayOfDays[0]
                    self.createLessonPlan()
                }
            }else {
                Utility.showTost(strMsg: "Enter Range", view: self.view)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Start Range"
            textField.keyboardType = UIKeyboardType.numberPad
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter End Range"
            textField.keyboardType = UIKeyboardType.numberPad
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
       
        self.present(alertController, animated: true, completion: nil)
        

    }
    
    //Select_Weeks
    func selectWeeks(){
        let actionSheetController = UIAlertController(title: "", message: "Number of Weeks", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = arrayOfWeeks.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(self.arrayOfWeeks[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtWeeks.text = self.arrayOfWeeks[index]
                    
                    //                    if self.mLessonPlanList.count > 0 {
                    //                        self.mLessonPlanList[0].setWeeks(weeks: Int(self.arrayOfWeeks[index])!)
                    //                    } else {
                    //                        let dummLessonPlan = LessonPlan()
                    //                        dummLessonPlan.setWeeks(weeks: Int(self.arrayOfWeeks[index])!)
                    //                        self.mLessonPlanList.append(dummLessonPlan)
                    //                    }
                    //
                    self.weekAssignment = 0
                    self.weekNotesAdd = 0
                    self.createLessonPlan()
                }
            }
            
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //Select_Days
    func selectDays(){
        let actionSheetController = UIAlertController(title: "", message: "Number of Days", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = arrayOfDays.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(self.arrayOfDays[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtDays.text = self.arrayOfDays[index]
                    
                    self.weekAssignment = 0
                    self.weekNotesAdd = 0
                    self.createLessonPlan()
                    
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    /*
    //Select_Hours
    func selectHours(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Select Hours", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = arrayOFHours.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(self.arrayOFHours[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtHours.text = self.arrayOFHours[index]
                    
                    if self.mLessonPlanList.count > 0 {
                        self.mLessonPlanList[0].setHours(hours: Int(self.arrayOFHours[index])!)
                    } else {
                        let dummLessonPlan = LessonPlan()
                        dummLessonPlan.setHours(hours: Int(self.arrayOFHours[index])!)
                        self.mLessonPlanList.append(dummLessonPlan)
                    }
                    
                    self.pageViewSetup()
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
     */
}


extension LessonPlanCreateAssignmentsNotesVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "LessonPlanCreateAssignmentsAndNotesCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! LessonPlanCreateAssignmentsAndNotesCollectionViewCell
        cell.btnHeader.tag = indexPath.row
        cell.btnHeader.addTarget(self, action: #selector(LessonPlanCreateAssignmentsNotesVC.buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        cell.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)
        if indexPath.row == 0 {
            cell.btnHeader.setTitle("\(arrayOfHeader[indexPath.row])", for: UIControlState.normal)
            cell.lblChange.isHidden = false
            cell.btnHeader.titleLabel!.textColor = UIColor.white.withAlphaComponent(1.0)
        }else {
            cell.btnHeader.setTitle("\(arrayOfHeader[indexPath.row])", for: UIControlState.normal)
            cell.lblChange.isHidden = true
            cell.lblChange.textColor = UIColor.lightText
            cell.btnHeader.titleLabel!.textColor = UIColor.white.withAlphaComponent(0.5)
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width: CGFloat((screenWidth / 2)), height: CGFloat(40))
    }
    
    //HeaderButtonClick
    func buttonClicked(sender: UIButton) {
        if currentPageIndex != sender.tag {
            if currentPageIndex < sender.tag {
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                collectionViewLpCreateAssiNotes.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.right, animated: true)
                pagerView.setViewControllers([pageForindex(pageIndex: sender.tag)!], direction: .forward, animated: true, completion: nil)
            } else if currentPageIndex > sender.tag {
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                collectionViewLpCreateAssiNotes.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.left, animated: true)
                pagerView.setViewControllers([pageForindex(pageIndex: sender.tag)!], direction: .reverse, animated: true, completion: nil)
            }
            currentPageIndex = sender.tag
            chageCollectionCell(index: currentPageIndex)
        }
    }
    
    func cap(pageIndex : Int) -> Int {
        return pageIndex
    }
    
    func carrouselJump() {
        currentPageIndex += 1
        pagerView.setViewControllers([self.pageForindex(pageIndex: currentPageIndex)!], direction: .forward, animated: true, completion: nil)
    }
    
    func pageForindex(pageIndex : Int) -> UIViewController? {
        guard (pageIndex < tutorialViewController.count) && (pageIndex>=0) else {
            return nil
        }
        
        if pageIndex == 0 {
            let currentViewController = storyboard!.instantiateViewController(withIdentifier: "LessonPlanCreateAssignmentsViewController") as! LessonPlanCreateAssignmentsViewController
            mSelectedIndex = pageIndex
            return currentViewController.loadView(pageIndex: pageIndex,pagerView: pagerView!, data: mLessonPlanList, mainVC: self)
        } else if pageIndex == 1{
            let currentViewController = storyboard!.instantiateViewController(withIdentifier: "LessonPlanCreateNotesViewController") as! LessonPlanCreateNotesViewController
            mSelectedIndex = pageIndex
            return currentViewController.loadView(pageIndex: pageIndex,pagerView: pagerView!, data: mLessonPlanList, mainVC: self)
        } else {
            return nil
        }
    }
    
    func indexForPage(vc : UIViewController) -> Int {
        if let vc = vc as? LessonPlanCreateAssignmentsViewController {
            return vc.getPageIndex()
        } else if let vc = vc as? LessonPlanCreateNotesViewController {
            return vc.getPageIndex()
        } else {
            preconditionFailure("VCPagImageSlidesTutorial page is not a VCTutorialImagePage")
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,viewControllerAfter viewController: UIViewController) -> UIViewController?{
        return pageForindex(pageIndex: cap(pageIndex: indexForPage(vc: viewController)+1))
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        return pageForindex(pageIndex: cap(pageIndex: indexForPage(vc: viewController)-1))
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        nextPageIndex = indexForPage(vc: pendingViewControllers.first!)
        chageCollectionCell(index: nextPageIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !finished { return }
        currentPageIndex = nextPageIndex
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func pageViewSetup() {
        let studentProfileViewController = storyboard!.instantiateViewController(withIdentifier: "LessonPlanCreateAssignmentsViewController") as! LessonPlanCreateAssignmentsViewController
        
        let vcObjectProfile = studentProfileViewController.loadView(pageIndex: 0, pagerView: UIPageViewController(), data: mLessonPlanList, mainVC: self) as UIViewController
        tutorialViewController.append(vcObjectProfile)
        
        
        let studentAttendenceViewController = storyboard!.instantiateViewController(withIdentifier: "LessonPlanCreateNotesViewController") as! LessonPlanCreateNotesViewController
        let vcObjectAttendence = studentAttendenceViewController.loadView(pageIndex: 1,pagerView: UIPageViewController(), data: mLessonPlanList, mainVC: self) as UIViewController
        tutorialViewController.append(vcObjectAttendence)
        
        pagerView = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pagerView.view.backgroundColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        pagerView.view.frame = CGRect(x: CGFloat(0), y: CGFloat(203), width: CGFloat(screenWidth), height: CGFloat(screenHeight - 203))
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.setViewControllers([pageForindex(pageIndex: mSelectedIndex!)!], direction: .forward, animated: false, completion: nil)
        addChildViewController(pagerView)
        self.view.addSubview(pagerView!.view)
        pagerView.didMove(toParentViewController: self)
    }
    
    //ADD PREFIX ALGO
    func addPrifix(prifix: String,rangeIs: Int,week: Int,day: Int){
        var range:Int?
        
        if (stRange != 0){
            range = (enRange! - (stRange! - 1))
        } else {
            range = enRange!
        }
        
        var counter = stRange
        if (stRange! >= 1) {
            counter = (stRange! - 1)
        }
        
        var dummyWeekCount = 0
        var dummyDayCount = 0
        
        let totalDay = (day * week)
        let ratio:Double = Double(range! / totalDay)
        var roundedRatio =  ratio.rounded()
        var autoFillText: String?
        var reminder = range! % totalDay
        
        //print("Range: \(range!)\nTotalDay: \(totalDay)\nRatio: \(ratio)\nRoundedRatio: \(roundedRatio)\n")
        //print("RemainigCount: \(reminder)")
        //print("counter = \(counter!)")
        
        if (roundedRatio > ratio) {
            roundedRatio = roundedRatio - 1
        }
        
        for _ in 0..<totalDay {
            if (range! <= totalDay) {
                if (counter! < enRange!) {
                    //print("counter = \(counter!)")
                    counter = counter! + 1
                    autoFillText = prifix + " " + "\(counter!)"
                    if (dummyDayCount == day) {
                        dummyDayCount = 0
                        dummyWeekCount = dummyWeekCount + 1
                    }
                    
                    setDayDefaultData(weekCount: dummyWeekCount, dayCount: dummyDayCount, text: autoFillText!)
                    dummyDayCount = dummyDayCount + 1
                } else {
                    break
                }
            } else {
                counter = counter! + 1
                var autoFillText: String = prifix + " " + "\(counter!)"
                if (roundedRatio != 1.0) {
                    for _ in 0..<Int(roundedRatio - 1) {
                        counter = counter! + 1
                        autoFillText = autoFillText + "," + "\(counter!)"
                        /*
                         if (j == Int(roundedRatio - 2) && reminder == 0) {
                         autoFillText?.append("-\(counter)")
                         }*/
                    }
                }
                
                if (reminder != 0) {
                    if (reminder < totalDay){
                        reminder = reminder - 1
                        counter = counter! + 1
                        autoFillText =  autoFillText + "," + "\(counter!)"
                    }
                }
                if (dummyDayCount == day) {
                    dummyDayCount = 0;
                    dummyWeekCount = dummyWeekCount + 1
                }
                
                setDayDefaultData(weekCount: dummyWeekCount, dayCount: dummyDayCount, text: autoFillText)
                dummyDayCount = dummyDayCount + 1
            }
        }
        
    }
    
    func setDayDefaultData(weekCount: Int,dayCount: Int,text: String){
        self.mLessonPlanList[weekCount].getLessonAssignment()[dayCount].setTitle(title: text)
    }
    
    /*
     */
    func createLessonPlan() {
        var counter = 0
        self.mLessonPlanList.removeAll()
        
        let weeks = self.txtWeeks.text
        let days = self.txtDays.text
        let hours = self.txtHours.text
        
        if weeks != nil && weeks != "" && days != nil && days != "" {
           
            
            for i in 0..<Int(weeks!)! {
                
                let dummyLessonPlan = LessonPlan()
                dummyLessonPlan.setName(name: self.txtLessonName.text!)
                
                if weeks != nil &&  weeks != "" {
                    dummyLessonPlan.setWeeks(weeks: Int(weeks!)!)
                }
                
                if days != nil &&  days != "" {
                    dummyLessonPlan.setDays(days: Int(days!)!)
                }
                
                if hours != nil &&  hours != "" {
                    dummyLessonPlan.setHours(hours: Double(hours!)!)
                }
                for indexDay in 0..<Int(days!)! {
                    let lassignment = LessonAssignment()
                    counter += 1
                    
                    lassignment.setTitle(title: "")
                    lassignment.setCategory(category: "assignment")
                    lassignment.setWeek(week: i)
                    lassignment.setDay(day: indexDay)
                    
                    var lessonAssignmentList = dummyLessonPlan.getLessonAssignment()
                    lessonAssignmentList.append(lassignment)
                    dummyLessonPlan.setLessonAssignment(lessonAssignment: lessonAssignmentList)
                }
                self.mLessonPlanList.append(dummyLessonPlan)
            }
            if enRange != nil && enRange != 0 {
                addPrifix(prifix: txtSelectLesson.text!, rangeIs: enRange!, week: Int(weeks!)!, day: Int(days!)!)
            }
            
        }
        self.pageViewSetup()
    }
}
