//
//  LessonPlanCreateAssignmentsAndNotesCollectionViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 10/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanCreateAssignmentsAndNotesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnHeader: UIButton!
    @IBOutlet weak var lblChange: UILabel!
}
