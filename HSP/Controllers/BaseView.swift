//
//  BaseView.swift
//  HSP
//
//  Created by Keyur Ashra on 23/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

protocol BaseView {
    func initialise()
    func showProgress()
    func hideProgress()
    func showAlert(msg: String)
}
