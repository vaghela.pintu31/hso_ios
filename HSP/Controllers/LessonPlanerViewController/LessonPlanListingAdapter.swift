//
//  LessonPlanListingAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanListingAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    private var mLessonPlanListingVC: LessonPlanListingViewController!
    public var mList = [LessonPlanListingData]()
    public var mSelectedLP: Int?

    // MARK: - Constructor
    init (viewController: LessonPlanListingViewController, data: NSDictionary) {
        mLessonPlanListingVC = viewController
        let serverResponse = LessonPlanListingResponse(dictionary: data)
        mList = (serverResponse?.getData())!
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LessonPlanListingTableViewCell", for: indexPath) as! LessonPlanListingTableViewCell
        cell.lblSubjectName.text = mList[indexPath.row].getName()
        cell.lblWeekandDays.text = "\(mList[indexPath.row].getWeeks()) weeks - \(mList[indexPath.row].getDays()) days/week"
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(self.btnEditOnClick), for: .touchUpInside)
        
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(self.btnShareOnClick), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(self.btnDeleteOnClick), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lpListingVC = mLessonPlanListingVC.storyboard!.instantiateViewController(withIdentifier: "LessonPlanDetailTabController") as! LessonPlanDetailTabController
        lpListingVC.mList = mList
        lpListingVC.mSelectedLP = indexPath.row
        mLessonPlanListingVC.navigationController!.pushViewController(lpListingVC, animated: true)
    }
    
    //MARK: OnClickEvents
    
    //DeleteOnClick
    func btnDeleteOnClick(sender: UIButton) {
        AlertDialog().showAlertWithYesNoButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: "Are you sure you want to delete this item", buttonTitle: NSLocalizedString("alertCancel", comment: ""), otherButtonTitle: NSLocalizedString("alertbtnYes", comment: ""), action: {(OtherButton) -> Void in
            print("IndexPath.row = \(sender.tag)")
            self.mLessonPlanListingVC.callLessonPlanDeleteAPI(id: sender.tag)
        })
    }
    
    //ShareOnClick
    func btnShareOnClick(sender: UIButton) {
        let lpListingVC = mLessonPlanListingVC.storyboard!.instantiateViewController(withIdentifier: "LessonPlanAssignAndNewTabController") as! LessonPlanAssignAndNewTabController
        lpListingVC.mList = mList
        lpListingVC.mSelectedLP = sender.tag
        mLessonPlanListingVC.navigationController!.pushViewController(lpListingVC, animated: true)
    }

    //EditOnClick
    func btnEditOnClick(sender: UIButton) {
        let sLessonPlanEditAssignmentsNotesVC = mLessonPlanListingVC.storyboard!.instantiateViewController(withIdentifier: "LessonPlanEditAssignmentsNotesVC") as! LessonPlanEditAssignmentsNotesVC
        sLessonPlanEditAssignmentsNotesVC.mlistData = mList[sender.tag]
        mLessonPlanListingVC.navigationController!.pushViewController(sLessonPlanEditAssignmentsNotesVC, animated: true)
    }


}
