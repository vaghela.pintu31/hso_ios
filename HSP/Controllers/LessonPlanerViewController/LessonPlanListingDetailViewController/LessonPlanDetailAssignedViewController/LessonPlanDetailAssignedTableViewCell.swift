//
//  LessonPlanAssignedTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanDetailAssignedTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblWeekNumber: UILabel!
    @IBOutlet weak var lblDayNumber: UILabel!
    @IBOutlet weak var lblDecription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews()    {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
        self.lblDecription.preferredMaxLayoutWidth = self.lblDecription.frame.size.width
    }
}
