//
//  LessonPlanAssignedAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanDetailAssignedAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    private var mLessonDetailPlanAssignedVC: LessonPlanDetailAssignedViewController!
    //var mList = [LessonPlanListingData]()
    private var arrayDisplayDetailsAssignment = [LessonAssignmentView]()
    
    // MARK: - Constructor
    init (viewController: LessonPlanDetailAssignedViewController, data : [LessonAssignmentView]) {
        mLessonDetailPlanAssignedVC = viewController
        arrayDisplayDetailsAssignment = data
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayDisplayDetailsAssignment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LessonPlanDetailAssignedTableViewCell", for: indexPath) as! LessonPlanDetailAssignedTableViewCell
        
        if indexPath.row == 0{
            cell.lblWeekNumber.isHidden = false
            cell.lblWeekNumber.text = "Week " + "\(arrayDisplayDetailsAssignment[indexPath.row].getWeek())"
            cell.lblDayNumber.text = "Day " + "\(arrayDisplayDetailsAssignment[indexPath.row].getDay())"
            cell.lblDecription.text = arrayDisplayDetailsAssignment[indexPath.row].getTitle()
        }else {
            if arrayDisplayDetailsAssignment[indexPath.row].getWeek() == arrayDisplayDetailsAssignment[indexPath.row - 1].getWeek(){
                cell.lblWeekNumber.isHidden = true
                cell.lblDayNumber.text = "Day " + "\(arrayDisplayDetailsAssignment[indexPath.row].getDay())"
                cell.lblDecription.text = arrayDisplayDetailsAssignment[indexPath.row].getTitle()
            }else {
                cell.lblWeekNumber.isHidden = false
                cell.lblWeekNumber.text = "Week " + "\(arrayDisplayDetailsAssignment[indexPath.row].getWeek())"
                cell.lblDayNumber.text = "Day " + "\(arrayDisplayDetailsAssignment[indexPath.row].getDay())"
                cell.lblDecription.text = arrayDisplayDetailsAssignment[indexPath.row].getTitle()
            }
        }
        
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}
