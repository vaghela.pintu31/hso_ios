//
//  LessonPlanAssignedViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class LessonPlanDetailAssignedViewController: UIViewController {
    
    @IBOutlet weak var tblLessonDetailPlanAssigned: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
    //MARK:- Variable Declaration
    private var mLessonDetailPlanAssignedAdapter: LessonPlanDetailAssignedAdapter!
    private var mLessonAssignmentList = [LessonAssignmentView]()
    public var mList = [LessonPlanListingData]()    
    public var mSelectedLP: Int?
    
    //TabSetup
    private var mPageIndex : Int = 0
    private var mPagerView: UIPageViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialise()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadView(pageIndex : Int,pagerView: UIPageViewController!) -> LessonPlanDetailAssignedViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LessonPlanDetailAssignedViewController") as! LessonPlanDetailAssignedViewController
        vc.mPageIndex = mPageIndex
        vc.mPagerView = mPagerView
        vc.mSelectedLP = mSelectedLP
        vc.mList = mList
        return vc
    }
    
    //MARK: Getter Setter Methods
    public func getPageIndex() -> Int {
        return mPageIndex
    }
    
    //MARK: BaseView Method
    func initialise() {
        /** Set Tableview Data **/
        setData()
    }
    
    /** Set Tableview Data **/
    func setData() {
        
        let weekCount = mList[mSelectedLP!].getLessonAssignment().count
        if weekCount == 0{
            noDataView.isHidden = false
        }else{
            noDataView.isHidden = true
            var arrayDisplayDetailsAssignment = [LessonAssignmentView]()
            arrayDisplayDetailsAssignment = mList[mSelectedLP!].getLessonAssignment().sorted(by: { $0.getWeek() < $1.getWeek() })
            
            mLessonDetailPlanAssignedAdapter = LessonPlanDetailAssignedAdapter(viewController: self, data: arrayDisplayDetailsAssignment)
            self.tblLessonDetailPlanAssigned.delegate = self.mLessonDetailPlanAssignedAdapter
            self.tblLessonDetailPlanAssigned.dataSource = self.mLessonDetailPlanAssignedAdapter
            self.tblLessonDetailPlanAssigned.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            tblLessonDetailPlanAssigned.reloadData()
            self.tblLessonDetailPlanAssigned.tableFooterView = UIView()
        }
    }
    
}
