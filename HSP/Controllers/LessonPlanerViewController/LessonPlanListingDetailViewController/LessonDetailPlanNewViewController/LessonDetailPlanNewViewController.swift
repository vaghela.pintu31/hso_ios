
//
//  LessonPlanViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class LessonDetailPlanNewViewController: UIViewController {
    
    @IBOutlet weak var tblLessonDetailPlanNew: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
    //MARK:- Variable Declaration
    private var TAG : String = "LessonPlanNewViewController"
    private var mLessonDetailPlanNewAdapter: LessonPlanDetailNewAdapter!
    private var mLessonNoteList = [LessonAssignmentView]()
    public var mList = [LessonPlanListingData]()
    public var mSelectedLP: Int?
    
    
    
    //TabSetup
    private var mPageIndex : Int = 1
    private var mPagerView: UIPageViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadView(pageIndex : Int,pagerView: UIPageViewController!) -> LessonDetailPlanNewViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LessonDetailPlanNewViewController") as! LessonDetailPlanNewViewController
        vc.mPageIndex = mPageIndex
        vc.mPagerView = mPagerView
        vc.mList = mList
        vc.mSelectedLP = mSelectedLP
        return vc
    }
    
    //MARK: Getter Setter Methods
    public func getPageIndex() -> Int {
        return mPageIndex
    }
    
    //MARK: BaseView Method
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "LessonPlanNewVC-Init")
        
        
       /** Set Tableview Data **/
        setData()
    }
    
    func setData() {
        
        let noteCount = mList[mSelectedLP!].getLessonAssignment().count
        if noteCount == 0 {
            noDataView.isHidden = false
        }else {
            noDataView.isHidden = true
            var arrayDisplayDetailsAssignment = [LessonAssignmentView]()
            arrayDisplayDetailsAssignment = mList[mSelectedLP!].getLessonAssignment().sorted(by: { $0.getWeek() < $1.getWeek() })
            
            mLessonDetailPlanNewAdapter = LessonPlanDetailNewAdapter(viewController: self, data: arrayDisplayDetailsAssignment)
            self.tblLessonDetailPlanNew.delegate = self.mLessonDetailPlanNewAdapter
            self.tblLessonDetailPlanNew.dataSource = self.mLessonDetailPlanNewAdapter
            self.tblLessonDetailPlanNew.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblLessonDetailPlanNew.tableFooterView = UIView()
        }
        
    }
    
}
