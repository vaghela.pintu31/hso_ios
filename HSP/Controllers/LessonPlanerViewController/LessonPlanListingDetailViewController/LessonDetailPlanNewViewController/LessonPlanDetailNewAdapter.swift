//  LessonPlanNewAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanDetailNewAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK:- Variable Declaration
    private var mLessonDetailPlanNewVC: LessonDetailPlanNewViewController!
    private var arrayDisplayDetailsAssignment = [LessonAssignmentView]()
    
    // MARK: - Constructor
    init (viewController: LessonDetailPlanNewViewController, data : [LessonAssignmentView]) {
        mLessonDetailPlanNewVC = viewController
        arrayDisplayDetailsAssignment = data
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayDisplayDetailsAssignment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LessonPlanDetailNewTableViewCell", for: indexPath) as! LessonPlanDetailNewTableViewCell
        if indexPath.row == 0{
            cell.labNoteNumber.isHidden = false
            cell.labNoteNumber.text = "Week " + "\(arrayDisplayDetailsAssignment[indexPath.row].getWeek())"
            cell.lblDayNumber.text = "Day " + "\(arrayDisplayDetailsAssignment[indexPath.row].getDay())"
            cell.lblDayDescription.text = arrayDisplayDetailsAssignment[indexPath.row].getNoteTitle()
        }else {
            if arrayDisplayDetailsAssignment[indexPath.row].getWeek() == arrayDisplayDetailsAssignment[indexPath.row - 1].getWeek(){
                cell.labNoteNumber.isHidden = true
                cell.lblDayNumber.text = "Day " + "\(arrayDisplayDetailsAssignment[indexPath.row].getDay())"
                cell.lblDayDescription.text = arrayDisplayDetailsAssignment[indexPath.row].getNoteTitle()
            }else {
                cell.labNoteNumber.isHidden = false
                cell.labNoteNumber.text = "Week " + "\(arrayDisplayDetailsAssignment[indexPath.row].getWeek())"
                cell.lblDayNumber.text = "Day " + "\(arrayDisplayDetailsAssignment[indexPath.row].getDay())"
                cell.lblDayDescription.text = arrayDisplayDetailsAssignment[indexPath.row].getNoteTitle()
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
