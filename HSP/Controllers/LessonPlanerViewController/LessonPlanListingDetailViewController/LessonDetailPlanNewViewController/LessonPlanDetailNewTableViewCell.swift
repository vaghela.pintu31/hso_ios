//
//  LessonPlanNewTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanDetailNewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labNoteNumber: UILabel!
    @IBOutlet weak var lblDayNumber: UILabel!
    @IBOutlet weak var lblDayDescription: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    override func layoutSubviews()    {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
        self.lblDayDescription.preferredMaxLayoutWidth = self.lblDayDescription.frame.size.width
    }
}
