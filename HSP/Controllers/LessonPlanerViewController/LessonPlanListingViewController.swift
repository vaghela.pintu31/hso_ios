//
//  LessonPlanListingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class LessonPlanListingViewController: UIViewController, BaseView {
    
    @IBOutlet weak var tblLessonPlan: UITableView!
    @IBOutlet weak var btnFab: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG : String = "LessonPlanListingViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mLessonPlanListingAdapter: LessonPlanListingAdapter!
    private var mList = [LessonPlanListingData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialise()
    }
    
    //MARK: BaseView Method
    func initialise() {
        UIStyle.setAppButtonStyleRounded(view: btnFab)
        
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("my_lesson_plan", comment: "")
        self.setNavigationBarItem()
        
        AppLog.debug(tag: self.TAG, msg: "LessonPlanListing-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        //CallListingAPI
        callLessonPlanListingAPI()
    }
    
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //MARK:- OnClickEvents
    @IBAction func btnFabOnClick(_ sender: Any) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "LessonPlanCreateAssignmentsNotesVC") as! LessonPlanCreateAssignmentsNotesVC
        self.navigationController!.pushViewController(homeViewController, animated: true)
    }
    
    //MARK: API Calling Methods
    func callLessonPlanListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let lessonPlanListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)lessonplans/index")
            AppLog.debug(tag: self.TAG, msg: "URL = \(lessonPlanListingRequest)")
            
            
            Alamofire.request(lessonPlanListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let lPlanData = LessonPlanListingResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setLessonPlanListing(data: value as! NSDictionary)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (lPlanData?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (lPlanData?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setLessonPlanListing(data: NSDictionary) {
        hideProgress()
        let serverResponse = LessonPlanListingResponse(dictionary: data)
        mList = (serverResponse?.getData())!
        self.tblLessonPlan.tableFooterView = nil
        self.tblLessonPlan.backgroundView = nil
        
        if mList.count == 0 {
            UIStyle.showEmptyTableView(message: NSLocalizedString("lesson_not_found", comment: ""), viewController: self.tblLessonPlan)
        } else {
            mLessonPlanListingAdapter = LessonPlanListingAdapter(viewController: self, data: data)
            self.tblLessonPlan.delegate = self.mLessonPlanListingAdapter
            self.tblLessonPlan.dataSource = self.mLessonPlanListingAdapter
            self.tblLessonPlan.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblLessonPlan.tableFooterView = UIView()
        }
    }
    
    //CALL_LessonAssignDeleteAPI
    //CALL_STUDENT_DELETEAPI
    func callLessonPlanDeleteAPI(id: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            print("id = \(id)")
            let myUrl = "\(mainURL)lessonplans/delete/\(self.mList[id].getId())"
            print("myUrl = \(myUrl)")
            let studentDeleteRequest = ServerRequest.authenticatePostRequest(url: myUrl)
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentDeleteRequest)")
            AppLog.debug(tag: self.TAG, msg: "Header = \(String(describing: studentDeleteRequest.allHTTPHeaderFields))")
            
            Alamofire.request(studentDeleteRequest).responseJSON { response in
                debugPrint(response)
                
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let deleteServerResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setLADeleteData(data: value as! NSDictionary, id: id)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    
    func setLADeleteData(data: NSDictionary, id: Int) {
        hideProgress()
        AppLog.debug(tag: TAG, msg: "ResponseDeleteApi = \(data)")
        //let serverResponse = LessonPlanListingResponse(dictionary: data)
        //Utility.showTost(strMsg: (serverResponse?.getMessage())!, view: self.view)
        self.mList.remove(at: id)
        //self.tblLessonPlan.reloadData()
        initialise()
    }
}
