//
//  CollaborateInviteTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 25/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateInviteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cardViewOl: UIView!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var lblCIName: UILabel!
    @IBOutlet weak var lblCICityName: UILabel!
    @IBOutlet weak var lblCIDescription: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
        
        self.lblCIDescription.preferredMaxLayoutWidth = self.lblCIDescription.frame.size.width
    }

}
