//
//  CollaborateInviteAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 25/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateInviteAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Variable Declaration
    private var mCollaborateInviteVC: CollaborateInviteViewController!
    private var mList = [User]()
    
    // MARK: - Constructor
    init (viewController: CollaborateInviteViewController, data : [User]) {
        mCollaborateInviteVC = viewController
        mList = data
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateInviteTableViewCell", for: indexPath) as! CollaborateInviteTableViewCell
        UIStyle.setAppButtonStyle(view: cell.cardViewOl)
        UIStyle.setAppButtonStyleRounded(view: cell.imgProfilePic)
       
        let iconName = mList[indexPath.row].getImageUrl()
        cell.imgProfilePic.image = UIImage(named: iconName)
        cell.lblCIName.text = mList[indexPath.row].getName()
        //cell.lblCICityName.text = mList[indexPath.row].getGrade()
        cell.lblCIDescription.text = mList[indexPath.row].getLongDesc()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Get Cell Label
        // let indexPath = tableView.indexPathForSelectedRow
        // let currentCell = tableView.cellForRow(at: indexPath!) as! BudgetLandingTVC
    }
}

