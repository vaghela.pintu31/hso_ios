//
//  CollaborateInviteViewController.swift
//  
//
//  Created by Keyur Ashra on 25/03/17.
//
//

import UIKit

class CollaborateInviteViewController: UIViewController, BaseView {

    @IBOutlet weak var tblCollaborateInvite: UITableView!
    
    //MARK:- Variable Declaration
    private var TAG : String = "CollaborateInviteViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mCollaborateInviteAdapter: CollaborateInviteAdapter!
    private var mList = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: BaseView Method
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "CollaborateInviteVC-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** Set Tableview Data **/
        setData()
    }
    
    /** Set Tableview Data **/
    func setData() {
        mList = User().setData()
        mCollaborateInviteAdapter = CollaborateInviteAdapter(viewController: self, data: mList)
        self.tblCollaborateInvite.delegate = self.mCollaborateInviteAdapter
        self.tblCollaborateInvite.dataSource = self.mCollaborateInviteAdapter
        self.tblCollaborateInvite.separatorStyle = .none
        self.tblCollaborateInvite.tableFooterView = UIView()
        self.tblCollaborateInvite.setNeedsLayout()
        self.tblCollaborateInvite.layoutIfNeeded()
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }

}
