//
//  CreateSingleLessonPlanViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 06/05/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CreateSingleLessonPlanViewController: UIViewController, BaseView, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var txtTitle: MKTextField!
    @IBOutlet weak var txtSelectStudent: MKTextField!
    @IBOutlet weak var btnSelectStudent: UIButton!
    @IBOutlet weak var assignmentTextView: UITextView!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var txtStartDate: MKTextField!
    @IBOutlet weak var txtEndDate: MKTextField!
    @IBOutlet weak var txtStartTime: MKTextField!
    @IBOutlet weak var txtEndTime: MKTextField!
    @IBOutlet weak var btnCreate: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG : String = "CreateSingleLessonPlanViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mBudgetLandingAdapter: BudgetLandingAdapter!
    private var arrayOfStudent = [String]()
    private var arrayOfStudentId = [Int]()
    private var selectedStudentIdIs = 0
    private var strStartDateTime: String?
    private var strEndDateTime: String?
    public var edEntityId: Int?
    public var edAssignmentId: Int?
    public var edNoteId: Int?
    public var mFabClick: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtTitle.resignFirstResponder()
        txtSelectStudent.resignFirstResponder()
        assignmentTextView.resignFirstResponder()
        noteTextView.resignFirstResponder()
        txtStartDate.resignFirstResponder()
        txtEndDate.resignFirstResponder()
        txtStartTime.resignFirstResponder()
        txtEndTime.resignFirstResponder()
        return true
    }
    
    func closeKeyboard() {
        txtTitle.resignFirstResponder()
        txtSelectStudent.resignFirstResponder()
        assignmentTextView.resignFirstResponder()
        noteTextView.resignFirstResponder()
        txtStartDate.resignFirstResponder()
        txtEndDate.resignFirstResponder()
        txtStartTime.resignFirstResponder()
        txtEndTime.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtStartDate {
            txtStartDateOnClick(textField as! MKTextField)
        } else if textField == txtEndDate {
            txtEndDateOnClick(textField as! MKTextField)
        } else if textField == txtStartTime {
            txtStartTimeOnClick(textField as! MKTextField)
        } else if textField == txtEndTime {
            txtEndTimeOnClick(textField as! MKTextField)
        }
    }
   
    
    //TextViewDelegate ->Placeholder Text
    func textViewDidBeginEditing(_ textView: UITextView) {
        if assignmentTextView.text == "Enter Assignment" {
            assignmentTextView.text = ""
            assignmentTextView.textColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
        }
        //assignmentTextView.becomeFirstResponder()
        if noteTextView.text == "Enter Notes" {
            noteTextView.text = ""
            noteTextView.textColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
        }
       // noteTextView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if assignmentTextView.text == "" {
            assignmentTextView.text = "Enter Assignment"
            assignmentTextView.textColor = UIColor.lightGray
        }
        //assignmentTextView.resignFirstResponder()

        if noteTextView.text == "" {
            noteTextView.text = "Enter Notes"
            noteTextView.textColor = UIColor.lightGray
        }
        //noteTextView.resignFirstResponder()
    }
    
    //Dropdown Function
    //Select_Lessons
    func selectStudent(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Select Student", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = arrayOfStudent.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(self.arrayOfStudent[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtSelectStudent.text = self.arrayOfStudent[index]
                    self.selectedStudentIdIs = self.arrayOfStudentId[index]
                    //let strSelectStudentIs = self.arrayOfStudent[index]
                    //self.btnSelectStudent.setTitle(strSelectStudentIs, for: .normal)
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK: BaseView Method
    func initialise() {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("create_lesson_plan", comment: "")
        if mFabClick == true {
            self.navigationItem.title = NSLocalizedString("edit_lesson_plan", comment: "")
            self.btnCreate.setTitle("EDIT", for: .normal)
        }
        
        AppLog.debug(tag: self.TAG, msg: "CreateSingleLP-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        txtTitle.delegate = self
        txtSelectStudent.delegate = self
        txtStartDate.delegate = self
        txtEndDate.delegate = self
        txtStartTime.delegate = self
        txtEndTime.delegate = self
        
        //SET UI
        txtTitle.layer.borderColor = UIColor.clear.cgColor
        txtSelectStudent.layer.borderColor = UIColor.clear.cgColor
        txtStartDate.layer.borderColor = UIColor.clear.cgColor
        txtEndDate.layer.borderColor = UIColor.clear.cgColor
        txtStartTime.layer.borderColor = UIColor.clear.cgColor
        txtEndTime.layer.borderColor = UIColor.clear.cgColor
        UIStyle.setAppButtonStyleRounded(view: btnCreate)
        
        //Assignment
        UIStyle.setCustomStyle(view: assignmentTextView, cornerRadius: 3.0, boderColor: COLOR_SEPARATOR, borderWidth: 1.0)
        assignmentTextView.delegate = self
        assignmentTextView.textColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_REGULAR, alpha: 1.0)
        assignmentTextView.text = NSLocalizedString("enter_assignment", comment: "")
        assignmentTextView.placeholderText = "Enter Assignment"
        //Notes
        UIStyle.setCustomStyle(view: noteTextView, cornerRadius: 3.0, boderColor: COLOR_SEPARATOR, borderWidth: 1.0)
        noteTextView.delegate = self
        noteTextView.textColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_REGULAR, alpha: 1.0)
        noteTextView.text = NSLocalizedString("enter_notes", comment: "")
        noteTextView.placeholderText = "Enter Notes"
        
        //For SelectStudent
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        for i in 0..<appDelegate.getStudentList().count {
            arrayOfStudent = [appDelegate.getStudentList()[i].getFirstName() + " " + appDelegate.getStudentList()[i].getLastName()]
            arrayOfStudentId = [appDelegate.getStudentList()[i].getId()]
            break
        }
        
        //CallViewStudentAssignmentAPI
        if mFabClick == true {
            callViewStudentAssignmentAPI(id: edEntityId!)
        }
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //MARK: OnClickEvents
    @IBAction func btnSelectStudentOnClick(_ sender: Any) {
        closeKeyboard()
        selectStudent()
    }
    
    //OnClickEvents
    @IBAction func txtStartDateOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.startDateValueChanged), for: UIControlEvents.valueChanged)
    }
    
    //DatePicker
    func startDateValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: sender.date)
        self.txtStartDate.text = strDate
    }
    
    
    @IBAction func txtEndDateOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.endDateValueChanged), for: UIControlEvents.valueChanged)
        
    }
    
    func endDateValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"//yyyy-MM-dd
        let strDate = dateFormatter.string(from: sender.date)
        self.txtEndDate.text = strDate
    }
    
    @IBAction func txtStartTimeOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.startTimeValueChanged), for: UIControlEvents.valueChanged)
        
    }
    
    func startTimeValueChanged(sender:UIDatePicker) {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        timeFormatter.dateFormat = "hh:mm a"
        let strTime = timeFormatter.string(from: sender.date)
        self.txtStartTime.text = strTime
    }
    
    @IBAction func txtEndTimeOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.endTimeValueChanged), for: UIControlEvents.valueChanged)
    }
    
    func endTimeValueChanged(sender:UIDatePicker) {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        timeFormatter.dateFormat = "hh:mm a"
        let strTime = timeFormatter.string(from: sender.date)
        self.txtEndTime.text = strTime
    }
    
    func checkDateCompare() -> Bool{
        let stTemDate = ((txtStartDate.text)! + " " + (txtStartTime.text)!)
        let enTemDate = ((txtEndDate.text! + " " + txtEndTime.text!))
        //print("Date = \(stTemDate)\n\(enTemDate)")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        let stDate = dateFormatter.date(from: stTemDate)
        let enDate = dateFormatter.date(from: enTemDate)
        //print("stDate = \(stDate)\nenDate = \(enDate)")
        strStartDateTime = Utility.convertLocalDateTimeToDB(dateString: stTemDate)
        strEndDateTime = Utility.convertLocalDateTimeToDB(dateString: enTemDate)
        //print("strStartDate = \(strEndDate)\nenDate = \(strEndDate)")
        
        if stDate! > enDate! {
            return false
        } else {
            return true
        }
    }
    
    @IBAction func btnCreateOnClick(_ sender: Any) {
        if (self.txtTitle.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_title", comment: ""))
        } else if (self.txtSelectStudent.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_student", comment: ""))
        } else if assignmentTextView.text == "Enter Assignment" {
            showAlert(msg: NSLocalizedString("enter_assignment", comment: ""))
        }
//        else if noteTextView.text == "Enter Notes" {
//            showAlert(msg: NSLocalizedString("enter_notes", comment: ""))
//        }
        else if (self.txtStartDate.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_start_date", comment: ""))
        } else if (self.txtStartTime.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_start_time", comment: ""))
        } else if (self.txtEndDate.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_end_date", comment: ""))
        }  else if (self.txtEndTime.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_end_time", comment: ""))
        } else if checkDateCompare() == false  {
            showAlert(msg: NSLocalizedString("compare_date_error", comment: ""))
        } else {
            if mFabClick == true {
                callEditStudentAssignmentAPI()
            } else {
                callSaveStudentAssignmentAPI()
            }
        }
    }
    
    //MARK: API Calling Methods
    func callSaveStudentAssignmentAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var saveStudentAssignmentRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/saveStudentAssignment")
            AppLog.debug(tag: self.TAG, msg: "URL = \(saveStudentAssignmentRequest)")
            
            let saveStudentAssignment = SaveStudentAssignment()
            saveStudentAssignment.setTitle(title: assignmentTextView.text!)
            saveStudentAssignment.setCategory(category: "Assignment")
            saveStudentAssignment.setStartDate(startDate: strStartDateTime!)
            saveStudentAssignment.setEndDate(endDate: strEndDateTime!)
            saveStudentAssignment.setStudentId(studentId: selectedStudentIdIs)
            
            let saveStudentNote = SaveStudentNote()
            saveStudentNote.setTitle(title: noteTextView.text!)
            saveStudentNote.setStartDate(startDate: strStartDateTime!)
            saveStudentNote.setEndDate(endDate: strEndDateTime!)
            saveStudentNote.setStudentId(studentId: selectedStudentIdIs)
            
            let serverParam = SaveStudentAssignmentData()
            serverParam.setStudentAssignment(studentAssignment: saveStudentAssignment)
            serverParam.setStudentNote(studentNote: saveStudentNote)

            let paramObject = serverParam.toSaveStudentAssignmentDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            saveStudentAssignmentRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(saveStudentAssignmentRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: saveStudentAssignmentRequest, success: { (data) -> Void in
                AppLog.debug(tag: self.TAG, msg: "RSManager: \(String(describing: data))")
                self.setSaveStudentAssignment(data: data as! Data)
            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("RSManagerFailure = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setSaveStudentAssignment(data: Data) {
        hideProgress()
        print("RSData = \(data)")
        self.navigationController?.popViewController(animated: true)
//        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
//        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
//        let serverResponse = SaveStudentAssignmentResponse(dictionary: userDictionary! as NSDictionary)
//        print("serverResponse = \(serverResponse)")
    }
    
    func callEditStudentAssignmentAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var saveStudentAssignmentRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/saveStudentAssignment")
            AppLog.debug(tag: self.TAG, msg: "URL = \(saveStudentAssignmentRequest)")
            
            let saveStudentAssignment = SaveStudentAssignment()
            saveStudentAssignment.setTitle(title: assignmentTextView.text!)
            saveStudentAssignment.setCategory(category: "Assignment")
            saveStudentAssignment.setStartDate(startDate: strStartDateTime!)
            saveStudentAssignment.setEndDate(endDate: strEndDateTime!)
            saveStudentAssignment.setStudentId(studentId: selectedStudentIdIs)
            saveStudentAssignment.setId(id: edAssignmentId!)
            
            let saveStudentNote = SaveStudentNote()
            saveStudentNote.setTitle(title: noteTextView.text!)
            saveStudentNote.setStartDate(startDate: strStartDateTime!)
            saveStudentNote.setEndDate(endDate: strEndDateTime!)
            saveStudentNote.setStudentId(studentId: selectedStudentIdIs)
            saveStudentNote.setId(id: edNoteId!)
            
            let serverParam = SaveStudentAssignmentData()
            serverParam.setStudentAssignment(studentAssignment: saveStudentAssignment)
            serverParam.setStudentNote(studentNote: saveStudentNote)
            
            let paramObject = serverParam.toSaveStudentAssignmentDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            saveStudentAssignmentRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(saveStudentAssignmentRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: saveStudentAssignmentRequest, success: { (data) -> Void in
                AppLog.debug(tag: self.TAG, msg: "RSManager: \(String(describing: data))")
                self.setEditStudentAssignment(data: data as! Data)
            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("RSManagerFailure = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setEditStudentAssignment(data: Data) {
        hideProgress()
        print("RSData = \(data)")
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:VIEW_STUDENT_ASSIGNMENT
    func callViewStudentAssignmentAPI(id: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let viewStudentAssignmentRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)lessonplans/viewStudentAssignment/\(id)")
            AppLog.debug(tag: self.TAG, msg: "URL = \(viewStudentAssignmentRequest)")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: viewStudentAssignmentRequest, success: { (data) -> Void in
                AppLog.debug(tag: self.TAG, msg: "RSManager: \(String(describing: data))")
                self.setViewStudentAssignment(data: data as! Data)
            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("RSManagerFailure = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setViewStudentAssignment(data: Data) {
        hideProgress()
        print("RSData = \(data)")
        
        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
        let serverResponse = ViewStudentAssignmentResponse(dictionary: userDictionary! as NSDictionary)
        print("ViewStudentAssignmentResponse = \(sResponceString!)")
        
        if serverResponse?.getMessage() == "Student Assignment Not Found." {
            Utility.showTost(strMsg: (serverResponse?.getMessage())!, view: self.view)
        } else {
            self.edAssignmentId = serverResponse?.getData().getStudentAssignment().getId()
            self.edNoteId = serverResponse?.getData().getLessonId()
            self.txtTitle.text = serverResponse?.getData().getLesson().getName()
            let indexOfSA = arrayOfStudentId.index(of: (serverResponse?.getData().getStudentId())!)
            self.txtSelectStudent.text = arrayOfStudent[indexOfSA!]
            self.assignmentTextView.text = serverResponse?.getData().getStudentAssignment().getTitle()
            self.noteTextView.text = serverResponse?.getData().getStudentAssignment().getNoteTitle()
            self.txtStartDate.text = Utility.convertDBDateToString(dateString: (serverResponse?.getData().getStartDate())!)
            self.txtEndDate.text = Utility.convertDBDateToString(dateString: (serverResponse?.getData().getEndDate())!)
            self.txtStartTime.text = Utility.convertDBDateToStringTime(dateString: (serverResponse?.getData().getStartDate())!)
            self.txtEndTime.text = Utility.convertDBDateToStringTime(dateString: (serverResponse?.getData().getEndDate())!)
            self.selectedStudentIdIs = (serverResponse?.getData().getStudentId())!

        }
        
        
        
    }

    
    
    
    
}
