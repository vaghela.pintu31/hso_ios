//
//  CollaborateCreateGroupViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class CollaborateGroupProfileViewController: UIViewController, BaseView, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var lblCreateByName: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var tblGroupUser: UITableView!
    public var mChatListingData = ChatListingData()
    private var groupInfo = ChatGroupViewResponse()
    private var mProgressIndicator:ProgressIndicator?
    
    var chatId: String!
    private var TAG =  "CollaborateGroupProfileViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: BaseView Method
    func initialise() {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        self.tblGroupUser.tableFooterView = UIView()
        showProgress()
        // callGroupViewAPI()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.appdelSocket.getGroupInfo(chatId: chatId!) { (data, statusCode) in
            self.groupInfo = data!
            if (statusCode == 200) {
                self.setData()
            }else{
                self.hideProgress()
                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (data!.getMessage()), statusCode: statusCode))
            }
        }
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    
    //    //MARK: API Calling Methods
    //    func callGroupViewAPI() {
    //        if Reachability.isConnectedToNetwork() == true {
    //            showProgress()
    //            let assignStudentRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)chats/view/\(chatId!)")
    //            AppLog.debug(tag: self.TAG, msg: "URL = \(assignStudentRequest)")
    //            Alamofire.request(assignStudentRequest).responseJSON { response in
    //                switch response.result {
    //                case .success:
    //                    let statusCode = response.response!.statusCode
    //                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
    //                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
    //                    if (statusCode == 200) {
    //                        if response.result.value != nil {
    //                            self.hideProgress()
    //                            let userDictionary = Utility.convertStringToDictionary(text: response.result.value! as! NSString)
    //                            self.groupInfo = ChatGroupViewResponse(dictionary: userDictionary! as NSDictionary)!
    //
    //                        }else{
    //                            let value = response.result.value
    //                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
    //                            self.hideProgress()
    //                            //                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (lpAssignStudentResponse?.getMessage())!, statusCode: statusCode))
    //
    //                        }
    //                    }else{
    //                        let value = response.result.value
    //                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
    //                        self.hideProgress()
    //                        //                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (lpAssignStudentResponse?.getMessage())!, statusCode: statusCode))
    //                    }
    //                case .failure(let encodingError):
    //                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
    //                    self.hideProgress()
    //                    self.showAlert(msg: serverDown)
    //
    //                }
    //            }
    //        } else {
    //            showAlert(msg: noInternet)
    //        }
    //    }
    
    
    func setData(){
        lblDate.text = Utility.convertDBDateToString(dateString: groupInfo.getData().getCreatedAt())
        for i in 0..<groupInfo.getData().getUserGroupInformation().count{
            if groupInfo.getData().getCreatorId() == groupInfo.getData().getUserGroupInformation()[i].getId(){
                lblCreateByName.text = groupInfo.getData().getUserGroupInformation()[i].getName()
                print("CretaeName :- \(groupInfo.getData().getUserGroupInformation()[i].getName())")
                break
            }
        }
        
        //        for i in 0..<groupInfo.getData().getUserGroupInformation().count{
        //            if Utility.getMyUserId() == groupInfo.getData().getUserGroupInformation()[i].getId(){
        //                lblCreateByName.text = "You"
        //                break
        //            }
        //        }
        self.navigationController?.navigationBar.topItem?.title = groupInfo.getData().getName()
        self.tblGroupUser.delegate = self
        self.tblGroupUser.dataSource = self
        self.tblGroupUser.reloadData()
        hideProgress()
    }
    
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupInfo.getData().getUserGroupInformation().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupUserDetailTableViewCell", for: indexPath) as! GroupUserDetailTableViewCell
        
        UIStyle.setAppButtonStyleRounded(view: cell.imgUser)
        if Utility.getMyUserId() == groupInfo.getData().getUserGroupInformation()[indexPath.row].getId(){
            cell.lblUserName.text = "You"
        }else {
            cell.lblUserName.text = groupInfo.getData().getUserGroupInformation()[indexPath.row].getName()
        }
        print("name :- \(groupInfo.getData().getUserGroupInformation()[indexPath.row].getName())")
        print("image :- \(groupInfo.getData().getUserGroupInformation()[indexPath.row].getImageUrl())")
        
        let iconName = groupInfo.getData().getUserGroupInformation()[indexPath.row].getImageUrl()
        cell.imgUser.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        cell.imgUser.sd_setShowActivityIndicatorView(true)
        cell.imgUser.sd_setIndicatorStyle(.gray)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userProfileViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        userProfileViewController.flage = false
        userProfileViewController.sUserGroupInformation = groupInfo.getData().getUserGroupInformation()[indexPath.row]
        if Utility.getMyUserId() == groupInfo.getData().getUserGroupInformation()[indexPath.row].getId(){
            userProfileViewController.currentUser = true
        }
        self.navigationController!.pushViewController(userProfileViewController, animated: true)
    }
}


