//
//  MainViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 17/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import os


class HomeViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblMainView: UITableView!
    
    var screeName = ["CollaborateListing","BudgetListing","Lesson-PlanListing"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tblMainView.delegate = self
        self.tblMainView.dataSource = self
       // let someLogger = OSLog(subsystem: "com.SomeName.yourApplication", category: "general")
       // os_log("some log message", log: someLogger, type: .error)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBarItem()
        
    }
   
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return screeName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1")
        cell?.textLabel?.text = screeName[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        switch index {
        case 0:
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "CollaborateLandingViewController") as! CollaborateLandingViewController
            self.navigationController!.pushViewController(homeViewController, animated: true)
        case 1:
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "BudgetLandingViewController") as! BudgetLandingViewController
            self.navigationController!.pushViewController(homeViewController, animated: true)
        case 2:
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "LessonPlanListingViewController") as! LessonPlanListingViewController
            self.navigationController!.pushViewController(homeViewController, animated: true)
        default:
            break
        }
    }
}

extension HomeViewController : DrawerLeftRightDelegate {
    func leftWillOpen() {
        //print("DrawerLeftRightDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        //print("DrawerLeftRightDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        //print("DrawerLeftRightDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        //print("DrawerLeftRightDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        //print("DrawerLeftRightDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        //print("DrawerLeftRightDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        //print("DrawerLeftRightDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        //print("DrawerLeftRightDelegate: rightDidClose")
    }
}


