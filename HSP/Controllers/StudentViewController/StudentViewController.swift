//
//  PlaceViewSlideVC.swift
//  TripoZon
//
//  Created by Admin on 30/06/16.
//  Copyright © 2016 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class StudentViewController: UIViewController{
    
    
    @IBOutlet weak var tabColletionView: UICollectionView!
    
    /*** PageSwipParm ***/
    var pagerView: UIPageViewController!
    var tutorialViewController = [UIViewController]()
    var selectedIndex = 0
    var nextPageIndex : Int = 0
    var currentPageIndex : Int = 0 {
        didSet {
            currentPageIndex = cap(pageIndex: currentPageIndex)
        }
    }
    public var mList = [StudentListingData]()
    public var mSelectedId: Int?
    
    var arrayHeader = [NSLocalizedString("profile", comment: ""),
                       NSLocalizedString("attendence", comment: "")]
    
    //MARK: View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initalize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initalize(){
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = mList[mSelectedId!].getFirstName() + " " + mList[mSelectedId!].getLastName()
        
//        if selectedIndex == 0 {
//            //AddRightBarButton
//            let drawerImage = UIImage(named: "ic_colorpicker")
//            navigationItem.rightBarButtonItem = UIBarButtonItem(image: drawerImage, style: .plain, target: self, action: #selector(self.EditProfileOnClick))
//        }
        
        tabColletionView.delegate = self
        tabColletionView.dataSource = self
        tabColletionView.reloadData()
        pageViewSetup()
    }
    
    func EditProfileOnClick() {
        print("EditProfileOnClick")
    }
    
    func chageCollectionCell(index: Int){
        for cell in tabColletionView.visibleCells as! [StudentCollectionViewCell] {
            cell.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)
            cell.lblChange.isHidden = true
            cell.btnHeader.titleLabel?.textColor = UIColor.white.withAlphaComponent(0.5)
        }
        
        let indexPath = NSIndexPath(row: index, section: 0)
        let cell = tabColletionView.cellForItem(at: indexPath as IndexPath) as! StudentCollectionViewCell
        cell.lblChange.isHidden = false
        cell.btnHeader.titleLabel?.textColor = UIColor.white.withAlphaComponent(1.0)
        
        UIView.animate(withDuration: 1.0, animations: {
            let animation: CATransition = CATransition()
            animation.duration = 0.6
            animation.type = kCATransitionFromRight
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            cell.lblChange.layer.add(animation, forKey: "changeTextTransition")
        }, completion: nil)
        tabColletionView.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
    }
    
}

extension StudentViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "StudentCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! StudentCollectionViewCell
        cell.btnHeader.tag = indexPath.row
        cell.btnHeader.addTarget(self, action: #selector(StudentViewController.buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        cell.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)
        if indexPath.row == 0 {
            cell.btnHeader.setTitle("\(arrayHeader[indexPath.row])", for: UIControlState.normal)
            cell.lblChange.isHidden = false
            cell.btnHeader.titleLabel!.textColor = UIColor.white.withAlphaComponent(1.0)
        }else {
            cell.btnHeader.setTitle("\(arrayHeader[indexPath.row])", for: UIControlState.normal)
            cell.lblChange.isHidden = true
            cell.lblChange.textColor = UIColor.lightText
            cell.btnHeader.titleLabel!.textColor = UIColor.white.withAlphaComponent(0.5)
        }
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width: CGFloat((screenWidth / 2)), height: CGFloat(40))
    }
    
    //HeaderButtonClick
    func buttonClicked(sender: UIButton) {
        if currentPageIndex != sender.tag {
            if currentPageIndex < sender.tag {
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                tabColletionView.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.right, animated: true)
                pagerView.setViewControllers([pageForindex(pageIndex: sender.tag)!], direction: .forward, animated: true, completion: nil)
            } else if currentPageIndex > sender.tag {
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                tabColletionView.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.left, animated: true)
                pagerView.setViewControllers([pageForindex(pageIndex: sender.tag)!], direction: .reverse, animated: true, completion: nil)
            }
            currentPageIndex = sender.tag
            chageCollectionCell(index: currentPageIndex)
        }
    }
}


extension StudentViewController : UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    
    func cap(pageIndex : Int) -> Int {
        return pageIndex
    }
    
    func carrouselJump() {
        currentPageIndex += 1
        pagerView.setViewControllers([self.pageForindex(pageIndex: currentPageIndex)!], direction: .forward, animated: true, completion: nil)
    }
    
    func pageForindex(pageIndex : Int) -> UIViewController? {
        guard (pageIndex < tutorialViewController.count) && (pageIndex>=0) else {
            return nil
        }
        
        //print("pageIndex :- \(pageIndex)")
        if pageIndex == 0 {
            let currentViewController = storyboard!.instantiateViewController(withIdentifier: "AddStudentViewController") as! AddStudentViewController
            selectedIndex = pageIndex
            currentViewController.mList = mList
            currentViewController.mSelectedId = mSelectedId
            return currentViewController.loadView(pageIndex: pageIndex,pagerView: pagerView!)
        }else if pageIndex == 1 {
            let currentViewController = storyboard!.instantiateViewController(withIdentifier: "StudentAttendenceViewController") as! StudentAttendenceViewController
            selectedIndex = pageIndex
            return currentViewController.loadView(pageIndex: pageIndex,pagerView: pagerView!)
        } else {
            return nil
        }
    }
    
    func indexForPage(vc : UIViewController) -> Int {
        if let vc = vc as? StudentAttendenceViewController {
            return vc.getPageIndex()
        } else if let vc = vc as? AddStudentViewController {
            return vc.getPageIndex()
        } else {
            preconditionFailure("VCPagImageSlidesTutorial page is not a VCTutorialImagePage")
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,viewControllerAfter viewController: UIViewController) -> UIViewController?{
        return pageForindex(pageIndex: cap(pageIndex: indexForPage(vc: viewController)+1))
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        return pageForindex(pageIndex: cap(pageIndex: indexForPage(vc: viewController)-1))
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        nextPageIndex = indexForPage(vc: pendingViewControllers.first!)
        //print("NPI = \(nextPageIndex)\nCPI: \(currentPageIndex)\nSPI: \(selectedIndex)")
        chageCollectionCell(index: nextPageIndex)//nextPageIndex
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !finished { return }
        currentPageIndex = nextPageIndex
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0 
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func pageViewSetup() {
        let studentProfileViewController = storyboard!.instantiateViewController(withIdentifier: "AddStudentViewController") as! AddStudentViewController
        let vcObjectProfile = studentProfileViewController.loadView(pageIndex: 0,pagerView: UIPageViewController()) as UIViewController
        tutorialViewController.append(vcObjectProfile)
        
        let studentAttendenceViewController = storyboard!.instantiateViewController(withIdentifier: "StudentAttendenceViewController") as! StudentAttendenceViewController
        let vcObjectAttendence = studentAttendenceViewController.loadView(pageIndex: 1,pagerView: UIPageViewController()) as UIViewController
        tutorialViewController.append(vcObjectAttendence)
        
        pagerView = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pagerView.view.backgroundColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        pagerView.view.frame = CGRect(x: CGFloat(0), y: CGFloat(104), width: CGFloat(screenWidth), height: CGFloat(screenHeight - 104))
        pagerView.dataSource=self
        pagerView.delegate=self
        print("pageViewSetup: selectedIndex = \(selectedIndex)")
        //self.chageCollectionCell(index: selectedIndex)
        pagerView.setViewControllers([pageForindex(pageIndex: selectedIndex)!], direction: .forward, animated: false, completion: nil)
        addChildViewController(pagerView)
        self.view.addSubview(pagerView!.view)
        pagerView.didMove(toParentViewController: self)
    }
}





