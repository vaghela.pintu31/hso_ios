//
//  LessonPlanListingAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
class StudentAttendenceAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    var mList = [ListMonthlyData]()
    var arrayColor = ["#ff6666", "#9aca40", "#4f8ef3", "#9900ff", "#ff9900"]

    // MARK: - Constructor
    init (data: [ListMonthlyData]) {
        mList = data
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentAttendenceTableViewCell", for: indexPath) as! StudentAttendenceTableViewCell
        cell.lblSubjectName.text = mList[indexPath.row].getSubject()
        
        let index = Int(arc4random_uniform(UInt32(arrayColor.count)))

        cell.imgDate.image = cell.imgDate.image!.withRenderingMode(.alwaysTemplate)
        cell.imgDate.tintColor = Utility.hexStringToUIColor(hex: arrayColor[index] , alpha: 1.0)        
        
        if mList[indexPath.row].getIsHiden() == true{
            cell.lblDay.text = cell.convertDateFormateDay(date: mList[indexPath.row].getDate())
            cell.lblMonthYear.text = cell.convertDateFormateMonth(date: mList[indexPath.row].getDate())
            cell.imgDateView.isHidden = false
            cell.imgDate.isHidden = false
            cell.lblLine.isHidden = true
        }else {
            cell.imgDateView.isHidden = true
            cell.imgDate.isHidden = true
            cell.lblLine.isHidden = false
        }
        
        if mList[indexPath.row].getAttendance() == true {
            cell.imgPresent.image = UIImage(named: strPresent)
            cell.imgAbsent.image = UIImage(named: strLightAbsent)
        }else {
            cell.imgPresent.image = UIImage(named: strLightPresent)            
            cell.imgAbsent.image = UIImage(named: strAbsent)
        }
        cell.selectionStyle = .none
        return cell
    }

  
 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }


}
