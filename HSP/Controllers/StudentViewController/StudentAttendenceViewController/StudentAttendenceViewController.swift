//
//  LessonPlanListingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class StudentAttendenceViewController: UIViewController {
    
    @IBOutlet weak var tblSudentAttendence: UITableView!
    
    //MARK:- Variable Declaration
    private var mStudentAttendenceAdapter: StudentAttendenceAdapter!
    private var mMonthlyData: MonthlyData!
    private var mPageIndex : Int = 1
    private var mPagerView: UIPageViewController!
    private var mainVC: StudentViewController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        var mMontlyData = MonthlyDataTemp().setData()
        var mListMonthlyData = [ListMonthlyData]()        
        
        for mIndex in 0..<mMontlyData.count{
            for dIndex in 0..<mMontlyData[mIndex].getData().count{
                let temp = ListMonthlyData()
                temp.setDate(date: mMontlyData[mIndex].getDate())
                temp.setAttendance(attendance: mMontlyData[mIndex].getData()[dIndex].getAttendance())
                temp.setSubject(subject: mMontlyData[mIndex].getData()[dIndex].getSubject())
                if dIndex == 0{
                    temp.setIsHiden(isHiden: true)
                }else {
                    temp.setIsHiden(isHiden: false)
                }
                mListMonthlyData.append(temp)
            }
        }
        
        mStudentAttendenceAdapter = StudentAttendenceAdapter(data: mListMonthlyData)
        self.tblSudentAttendence.delegate = self.mStudentAttendenceAdapter
        self.tblSudentAttendence.dataSource = self.mStudentAttendenceAdapter
        self.tblSudentAttendence.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
        self.tblSudentAttendence.tableFooterView = UIView()
    }
    
    func loadView(pageIndex : Int,pagerView: UIPageViewController!) -> StudentAttendenceViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "StudentAttendenceViewController") as! StudentAttendenceViewController
        vc.mPageIndex = mPageIndex
        vc.mPagerView = mPagerView
        return vc
    }
    
    //MARK: Getter Setter Methods
    public func getPageIndex() -> Int {
        return mPageIndex
    }
    
    
}
