//
//  AddStudentViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 22/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class AddStudentViewController: UIViewController, BaseView, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var curveView: UIView!
    @IBOutlet weak var txtFirstName: MKTextField!
    @IBOutlet weak var txtLastName: MKTextField!
    @IBOutlet weak var txtEmail: MKTextField!
    @IBOutlet weak var txtPhoneNo: MKTextField!
    @IBOutlet weak var txtGender: MKTextField!
    @IBOutlet weak var txtDateofBirth: MKTextField!
    @IBOutlet weak var txtSchoolInfo: MKTextField!
    @IBOutlet weak var txtAcadamicYearFrom: MKTextField!
    @IBOutlet weak var txtAcadamicYearTo: MKTextField!
    @IBOutlet weak var txtGrade: MKTextField!
    @IBOutlet weak var txtPinCode: MKTextField!
    @IBOutlet weak var txtParentFirstName: MKTextField!
    @IBOutlet weak var txtParentLastName: MKTextField!
    @IBOutlet weak var txtParentEmail: MKTextField!
    @IBOutlet weak var txtParentPhoneNo: MKTextField!
    
    @IBOutlet weak var lblParentInfoOl: UILabel!
    @IBOutlet weak var btnAddOl: UIButton!
    @IBOutlet weak var imgCalender: UIImageView!
    @IBOutlet weak var imgAYFCalender: UIImageView!
    @IBOutlet weak var imgAYTCalender: UIImageView!
    
    //MARK:- Variable Declaration
    private var TAG : String = "AddStudentViewController"
    private var mHttpCodes = DataManager.getHttpCodeData()
    private var mProgressIndicator:ProgressIndicator?
    private var mPageIndex : Int = 0
    private var mPagerView: UIPageViewController!
    private var mSelectedImage: UIImage?
    public var mList = [StudentListingData]()
    public var mFabClick: Bool?
    public var mSelectedId: Int?
    private var mainVC: StudentViewController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("add_student", comment: "")
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadView(pageIndex : Int,pagerView: UIPageViewController!) -> AddStudentViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddStudentViewController") as! AddStudentViewController
        vc.mPageIndex = mPageIndex
        vc.mPagerView = mPagerView
        vc.mSelectedId = mSelectedId
        vc.mList = mList
        return vc
    }
    
    //MARK: Getter Setter Methods
    public func getPageIndex() -> Int {
        return mPageIndex
    }
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtEmail.resignFirstResponder()
        txtPhoneNo.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtDateofBirth.resignFirstResponder()
        txtSchoolInfo.resignFirstResponder()
        txtAcadamicYearFrom.resignFirstResponder()
        txtAcadamicYearTo.resignFirstResponder()
        txtGrade.resignFirstResponder()
        txtPinCode.resignFirstResponder()
        txtParentFirstName.resignFirstResponder()
        txtParentLastName.resignFirstResponder()
        txtParentEmail.resignFirstResponder()
        txtParentPhoneNo.resignFirstResponder()
        return true
    }
    
    func closeKeyboard() {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtEmail.resignFirstResponder()
        txtPhoneNo.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtDateofBirth.resignFirstResponder()
        txtSchoolInfo.resignFirstResponder()
        txtAcadamicYearFrom.resignFirstResponder()
        txtAcadamicYearTo.resignFirstResponder()
        txtGrade.resignFirstResponder()
        txtPinCode.resignFirstResponder()
        txtParentFirstName.resignFirstResponder()
        txtParentLastName.resignFirstResponder()
        txtParentEmail.resignFirstResponder()
        txtParentPhoneNo.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtGender {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
            selectGender()
        } else if textField == txtDateofBirth {
            txtDateOfBirthOnClick(textField as! MKTextField)
        } else if textField == txtAcadamicYearFrom {
            txtAcadamicFromYearOnClick(textField as! MKTextField)
        } else if textField == txtAcadamicYearTo {
            txtAcadamicToYearOnClick(textField as! MKTextField)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case txtFirstName:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 32
        case txtLastName:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 32
        case txtParentFirstName:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 32
        case txtParentLastName:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 32
        case txtPhoneNo:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10
        case txtParentPhoneNo:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10
        case txtPinCode:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 6
        default:
            return true
        }
    }
    
    
    @IBAction func btnSelectGenderOnClick(_ sender: Any) {
        self.closeKeyboard()
        self.selectGender()
    }
    
    @IBAction func btnSelectGradeOnClick(_ sender: Any) {
        self.closeKeyboard()
        self.selectGrade()
    }
    
    @IBAction func txtAcadamicFromYearOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(AddStudentViewController.datePickerValueChangedFromYear), for: UIControlEvents.valueChanged)
    }
    
    //DatePickerFromYear
    func datePickerValueChangedFromYear(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: sender.date)
        self.txtAcadamicYearFrom.text = strDate
    }
    
    @IBAction func txtAcadamicToYearOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(AddStudentViewController.datePickerValueChangedToYear), for: UIControlEvents.valueChanged)
    }
    
    //DatePickerToYear
    func datePickerValueChangedToYear(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: sender.date)
        self.txtAcadamicYearTo.text = strDate
    }
    
    //MARK: OnClickEvents
    @IBAction func txtDateOfBirthOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(AddStudentViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    //DatePicker
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: sender.date)
        self.txtDateofBirth.text = strDate
    }
    
    //MARK: UIImagePickerDelegate
    @IBAction func btnChangePictureOnClick(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: UIImagePickerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo  info: [String : Any]) {
        
        mSelectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgProfilePic.backgroundColor = UIColor.clear
        imgProfilePic.contentMode = UIViewContentMode.scaleAspectFit
        imgProfilePic.image = mSelectedImage
        AppLog.debug(tag: TAG, msg: "IMAGE_FOUND: \(mSelectedImage!)")

        //UploadImageApi()
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddOnClick(_ sender: Any) {
        if (self.txtFirstName.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_fname", comment: ""))
        } else if (self.txtLastName.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_lname", comment: ""))
        }
//        else if (self.txtEmail.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_email", comment: ""))
//        } else if (self.txtPhoneNo.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_phoneno", comment: ""))
//        }
//        else if (self.txtGender.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_gender", comment: ""))
//        }
//        else if (self.txtDateofBirth.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_dob", comment: ""))
//        } else if (self.txtSchoolInfo.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_school_info", comment: ""))
//        }
        else if (self.txtAcadamicYearFrom.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_acadamic_year_from", comment: ""))
        } else if (self.txtAcadamicYearTo.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_acadamic_year_to", comment: ""))
        } else if (self.txtGrade.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_grade", comment: ""))
        }
//        else if (self.txtPinCode.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_zipcode", comment: ""))
//        }
//        else if (self.txtParentFirstName.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_pfname", comment: ""))
//        } else if (self.txtParentLastName.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_lfname", comment: ""))
//        }
//        else if (self.txtParentEmail.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_pemail", comment: ""))
//        } else if (self.txtParentPhoneNo.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_pphoneno", comment: ""))
//        } else if Utility.isValidEmail(urlString: txtEmail.text!) == false || Utility.isValidEmail(urlString: txtParentEmail.text!) == false {
//            showAlert(msg: NSLocalizedString("enter_valid_email", comment: ""))
//        }
        else {
            if mFabClick != true {
                AppLog.debug(tag: self.TAG, msg: "SelectedID = \(mSelectedId!)")
                AppLog.debug(tag: self.TAG, msg: "edID = \(mList[mSelectedId!].getId())")
                callStudentEditAPI(edId: mList[mSelectedId!].getId())
            } else {
                callStudentAddAPI()
            }
        }
    }
    
    //MARK: BaseView Method
    /* Initialize View */
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "AddStudent-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        //SetView
        UIStyle.setCustomStyle(view: imgProfilePic, cornerRadius: imgProfilePic.frame.height / 2, boderColor: COLOR_BACKGROUND, borderWidth: 1.0)
        
        imgCalender.image = imgCalender.image!.withRenderingMode(.alwaysTemplate)
        imgCalender.tintColor = UIColor.black
        imgAYFCalender.image = imgAYFCalender.image!.withRenderingMode(.alwaysTemplate)
        imgAYFCalender.tintColor = UIColor.black
        imgAYTCalender.image = imgAYTCalender.image!.withRenderingMode(.alwaysTemplate)
        imgAYTCalender.tintColor = UIColor.black
        
        UIStyle.setAppButtonStyleRounded(view: curveView)
        UIStyle.setAppButtonStyleRounded(view: imgProfilePic)
        UIStyle.setAppButtonStyleRounded(view: btnAddOl)
        
        txtFirstName.layer.borderColor = UIColor.clear.cgColor
        txtLastName.layer.borderColor = UIColor.clear.cgColor
        txtEmail.layer.borderColor = UIColor.clear.cgColor
        txtPhoneNo.layer.borderColor = UIColor.clear.cgColor
        txtGender.layer.borderColor = UIColor.clear.cgColor
        txtDateofBirth.layer.borderColor = UIColor.clear.cgColor
        txtSchoolInfo.layer.borderColor = UIColor.clear.cgColor
        txtAcadamicYearFrom.layer.borderColor = UIColor.clear.cgColor
        txtAcadamicYearTo.layer.borderColor = UIColor.clear.cgColor
        txtGrade.layer.borderColor = UIColor.clear.cgColor
        //txtPinCode.layer.borderColor = UIColor.clear.cgColor
        txtParentFirstName.layer.borderColor = UIColor.clear.cgColor
        txtParentLastName.layer.borderColor = UIColor.clear.cgColor
        txtParentEmail.layer.borderColor = UIColor.clear.cgColor
        txtParentPhoneNo.layer.borderColor = UIColor.clear.cgColor
        
        //SetPlaceHolderText
        txtFirstName.placeholder = NSLocalizedString("first_name", comment: "")
        txtLastName.placeholder = NSLocalizedString("last_name", comment: "")
        txtEmail.placeholder = NSLocalizedString("email", comment: "")
        txtPhoneNo.placeholder = NSLocalizedString("phone_number", comment: "")
        txtGender.placeholder = NSLocalizedString("gender", comment: "")
        txtDateofBirth.placeholder = NSLocalizedString("dob", comment: "")
        txtSchoolInfo.placeholder = NSLocalizedString("school_info", comment: "")
        txtAcadamicYearFrom.placeholder = NSLocalizedString("acadamic_year_from", comment: "")
        txtAcadamicYearTo.placeholder = NSLocalizedString("acadamic_year_to", comment: "")
        txtGrade.placeholder = NSLocalizedString("grade", comment: "")
        //txtPinCode.placeholder = NSLocalizedString("zipcode", comment: "")
        
        lblParentInfoOl.text = NSLocalizedString("parents_info", comment: "")
        txtParentFirstName.placeholder = NSLocalizedString("parents_fname", comment: "")
        txtParentLastName.placeholder = NSLocalizedString("parents_lname", comment: "")
        txtParentEmail.placeholder = NSLocalizedString("email", comment: "")
        txtParentPhoneNo.placeholder = NSLocalizedString("phone_number", comment: "")
        
        btnAddOl.setTitle(NSLocalizedString("add", comment: ""), for: .normal)
        
        if mFabClick != true {
            editProfile()
        }
    }
    
    func editProfile() {
        txtFirstName.text = mList[mSelectedId!].getFirstName()
        txtLastName.text = mList[mSelectedId!].getLastName()
        txtEmail.text = mList[mSelectedId!].getEmail()
        txtPhoneNo.text = mList[mSelectedId!].getContactNumber()
        txtGender.text = mList[mSelectedId!].getGender()
        txtDateofBirth.text = Utility.convertOldDateToNewDate(string: mList[mSelectedId!].getDob())
        txtSchoolInfo.text = mList[mSelectedId!].getSchool()
        txtAcadamicYearFrom.text = Utility.convertOldDateToNewDate(string: mList[mSelectedId!].getAcademicYearStart())
        txtAcadamicYearTo.text = Utility.convertOldDateToNewDate(string: mList[mSelectedId!].getAcademicYearEnd())
        txtGrade.text = mList[mSelectedId!].getGrade()
        //txtPinCode.text = "MissingPincode"
        
        txtParentFirstName.text = mList[mSelectedId!].getParentFirstName()
        txtParentLastName.text = mList[mSelectedId!].getParentLastName()
        txtParentEmail.text = mList[mSelectedId!].getParentEmail()
        txtParentPhoneNo.text = mList[mSelectedId!].getParentContactNumber()
        
        let iconName = mList[mSelectedId!].getImageUrl()
        imgProfilePic.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        imgProfilePic.sd_setShowActivityIndicatorView(true)
        imgProfilePic.sd_setIndicatorStyle(.white)
        
        btnAddOl.setTitle("SUBMIT", for: .normal)//EDIT
    }
    
    
    func showProgress() {
        mProgressIndicator?.start()
        //  self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        //  self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //MARK: Dropdown Function
    //Gender
    func selectGender() {
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: NSLocalizedString("selectGender", comment: ""), preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = DataManager.genderArray.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(DataManager.genderArray[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    let firstChar = DataManager.genderArray.initialsFirstChar
                    self.txtGender.text = firstChar[index]
                    self.txtGender.resignFirstResponder()
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //Grade
    func selectGrade() {
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: NSLocalizedString("selectGrade", comment: ""), preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = DataManager.studetGradeArray.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(DataManager.studetGradeArray[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtGrade.text = DataManager.studetGradeArray[index]
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK: API Calling Methods
    func callStudentAddAPI () {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            var studentAddRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)students/add")
            
            let studentAddData = StudentAddData()
            studentAddData.setFirstName(firstName: (txtFirstName.text)!)
            studentAddData.setLastName(lastName: (txtLastName.text)!)
            studentAddData.setEmail(email: (txtEmail.text)!)
            studentAddData.setContactNumber(contactNumber: (txtPhoneNo.text!))
            studentAddData.setGender(gender: (txtGender.text)!)
            studentAddData.setDob(dob: Utility.convertLocalDateToServerDate(string: txtDateofBirth.text!))
            studentAddData.setSchool(school: (txtSchoolInfo.text)!)
            studentAddData.setAcademicYearStart(academicYearStart: Utility.convertLocalDateToServerDate(string: txtAcadamicYearFrom.text!))
            studentAddData.setAcademicYearEnd(academicYearEnd: Utility.convertLocalDateToServerDate(string: txtAcadamicYearTo.text!))
            studentAddData.setGrade(grade: (txtGrade.text)!)
            //pincode
            studentAddData.setParentFirstName(parentFirstName: (txtParentFirstName.text)!)
            studentAddData.setParentLastName(parentLastName: (txtParentLastName.text)!)
            studentAddData.setParentEmail(parentEmail: (txtParentEmail.text)!)
            studentAddData.setParentContactNumber(parentContactNumber: (txtParentPhoneNo.text)!)
            
            let serverParam = StudentAddResponse()
            serverParam.setStudentData(student: studentAddData)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            studentAddRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentAddRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            Alamofire.request(studentAddRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let studentAddResponse = StudentAddResponse(dictionary: response.result.value as! NSDictionary)
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setStudentAddData(data: value as! NSDictionary)
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentAddResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentAddResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setStudentAddData(data: NSDictionary) {
        hideProgress()
        //let studentAddResponse = StudentAddResponse(dictionary: data)
        //print("VALUES = \(data.allKeys)")
        let temp = data.value(forKeyPath: "data") as! NSDictionary
        //print("mSelectedId = \(temp.value(forKeyPath: "Student"))")
        let temp1 = temp.value(forKeyPath: "Student") as! NSDictionary
        let studentId = temp1.value(forKeyPath: "id")
        
        if mSelectedImage == nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            //print("mSelectedImage: \(self.mSelectedImage!)")
            //print("mSelectedId: \(studentId)")
            mSelectedId = studentId as? Int
            UploadImageApi()
        }
    }
    //
    func UploadImageApi() {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            let headers: HTTPHeaders = ["X-Access-Token": Utility.getXAccessToken()]
            let uploadImageRequest = try! URLRequest(url: "\(mainURL)upload", method: .post, headers: headers)
            AppLog.debug(tag: self.TAG, msg: "URL = \(uploadImageRequest)")
            AppLog.debug(tag: self.TAG, msg: "headers = \(headers)")
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("student".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "model")
                AppLog.debug(tag: self.TAG, msg: "SelectedId = \(self.mSelectedId!)")
                multipartFormData.append("\(self.mSelectedId!)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "entityId")
                multipartFormData.append(UIImagePNGRepresentation(self.mSelectedImage!)!, withName: "media", fileName: "picture.png", mimeType: "image/png")
            }, with: uploadImageRequest, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint("SUCCESS RESPONSE: \(response)")
                        // let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                        //AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                        
                        if (statusCode == 200) {
                            let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                            if response.result.value != nil {
                                self.hideProgress()
                                AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: serverResponse?.getMessage(), action: { (CancelButton) -> Void in
                                    self.navigationController?.popViewController(animated: true)
                                })
                                //Utility.showTost(strMsg: (serverResponse?.getMessage())!, view: self.view)
                            }else{
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.hideProgress()
                                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            }
                        }else{
                            let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                        
                    }
                case .failure(let encodingError):
                    self.hideProgress()
                    AppLog.debug(tag: self.TAG, msg: "ERROR RESPONSE: \(encodingError)")
                    self.showAlert(msg: serverDown)
                }
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    //
    
    func callStudentEditAPI(edId: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            var studentEditRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)students/edit/\(edId)")
            let studentAddData = StudentAddData()
            studentAddData.setFirstName(firstName: (txtFirstName.text)!)
            studentAddData.setLastName(lastName: (txtLastName.text)!)
            studentAddData.setEmail(email: (txtEmail.text)!)
            studentAddData.setContactNumber(contactNumber: (txtPhoneNo.text)!)
            let firstChar = txtGender.text?[(txtGender.text?.startIndex)!]
            studentAddData.setGender(gender: String(firstChar!))
            studentAddData.setDob(dob: Utility.convertLocalDateToServerDate(string: txtDateofBirth.text!))
            studentAddData.setSchool(school: (txtSchoolInfo.text)!)
            studentAddData.setAcademicYearStart(academicYearStart: Utility.convertLocalDateToServerDate(string: txtAcadamicYearFrom.text!))
            studentAddData.setAcademicYearEnd(academicYearEnd: Utility.convertLocalDateToServerDate(string: txtAcadamicYearTo.text!))
            studentAddData.setGrade(grade: (txtGrade.text)!)
            //pincode
            studentAddData.setParentFirstName(parentFirstName: (txtParentFirstName.text)!)
            studentAddData.setParentLastName(parentLastName: (txtParentLastName.text)!)
            studentAddData.setParentEmail(parentEmail: (txtParentEmail.text)!)
            studentAddData.setParentContactNumber(parentContactNumber: (txtParentPhoneNo.text)!)

            let serverParam = StudentAddResponse()
            serverParam.setStudentData(student: studentAddData)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            studentEditRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentEditRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            Alamofire.request(studentEditRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let studentAddResponse = StudentAddResponse(dictionary: response.result.value as! NSDictionary)
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setStudentEditData(data: value as! NSDictionary)
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentAddResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentAddResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setStudentEditData(data: NSDictionary) {
        hideProgress()
      
        if mSelectedImage != nil {
            let studentAddResponse = StudentAddResponse(dictionary: data)
            AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: studentAddResponse?.getMessage(), action: { (CancelButton) -> Void in
                self.navigationController?.popViewController(animated: true)
            })
        } else {
            print("mSelectedImage: \(self.mSelectedImage!)")
            print("mSelectedId: \(self.mSelectedId!)")
            print("ID: \(self.mList[self.mSelectedId!].getId())")
            mSelectedId = self.mList[mSelectedId!].getId()
            UploadImageApi()
        }
    }
    //
    
}


