//
//  DashboardViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 07/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Charts
import Alamofire

class DashboardTableViewController: NSObject, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    
    var mDashboardViewController: DashboardViewController!
    var strSendNote = ""
    
    // MARK: - Constructor
    init (viewController: DashboardViewController) {
        mDashboardViewController = viewController
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            let todoCount = mDashboardViewController.mTodoListingResponce.getData().count
            if todoCount == 0{
                return 2
            }else {
                if todoCount >= 3{
                    return 4
                }else{
                    return todoCount + 1
                }
            }
            
        case 1:
            let agendaCount = mDashboardViewController.mCalenderListing.count
            if agendaCount == 0{
                return 2
            }else {
                if agendaCount >= 3{
                    return 4
                }else{
                    return agendaCount + 1
                }
            }
        case 2:
            return 1
        default:
            return 3
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0: //TODO_CELL
            
            if mDashboardViewController.mTodoListingResponce.getData().count == 0{
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TodoFirstCell", for: indexPath) as! TodoFirstCell
                    cell.txtNote.text = ""
                    cell.txtNote.delegate = self
                    cell.btnSentNote.tag = indexPath.row
                    cell.btnSentNote.addTarget(self, action: #selector(btnSendNote), for: .touchUpInside)
                    cell.selectionStyle = .none
                    return cell
                } else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetCell", for: indexPath) as! BudgetCell
                    cell.pieChartView.isHidden = true
                    cell.selectionStyle = .none
                    return cell
                }
            }else {

                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TodoFirstCell", for: indexPath) as! TodoFirstCell
                    UIStyle.setAppButtonStyle(view: cell.btnSentNote)
                    cell.txtNote.text = ""
                    cell.txtNote.delegate = self
                    cell.btnSentNote.tag = indexPath.row
                    cell.btnSentNote.addTarget(self, action: #selector(btnSendNote), for: .touchUpInside)
                    cell.selectionStyle = .none
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as! TodoCell
                    let index = indexPath.row - 1
                    cell.lblTitle.text = mDashboardViewController.mTodoListingResponce.getData()[index].getTitle()
                    cell.lblDate.text =  cell.convertDateString(dateString: mDashboardViewController.mTodoListingResponce.getData()[index].getCreatedAt())
                    cell.btnCheckBox.tag = index
                    cell.btnCheckBox.isSelected = mDashboardViewController.mTodoListingResponce.getData()[index].getCompleted()
                    cell.btnCheckBox.addTarget(self, action: #selector(btnCheckBox), for: .touchUpInside)
                    
                    
                    cell.btnEditTodo.tag = index
                    cell.btnEditTodo.addTarget(self, action: #selector(btnEditOnClick), for: .touchUpInside)
                    cell.selectionStyle = .none
                    return cell
                }
            }
            
        case 1: //AGENDA_CELL
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TodoDateCell", for: indexPath) as! TodoDateCell
                
                if Utility.convertDateToString(date: self.mDashboardViewController.todayDate) == Utility.convertDateToString(date: Date()){
                    cell.lblDate.text = "TODAY"
                }else {
                    cell.lblDate.text = Utility.convertDateToString(date: self.mDashboardViewController.todayDate)
                }
                
                
                cell.tappedNext = { [unowned self] (selectedCell) -> Void in
                    let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to:  self.mDashboardViewController.todayDate)
                    self.mDashboardViewController.callCalenderListingAPI(startDate: tomorrow!)
                }
                
                cell.tappedPrevious = { [unowned self] (selectedCell) -> Void in
                    let date = Calendar.current.date(byAdding: .day, value: -1, to:  self.mDashboardViewController.todayDate)
                    self.mDashboardViewController.callCalenderListingAPI(startDate: date!)
                }
                
                cell.selectionStyle = .none
                return cell
                
            }else {
                if mDashboardViewController.mCalenderListing.count == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetCell", for: indexPath) as! BudgetCell
                    cell.pieChartView.isHidden = true
                    cell.imgNoDataAvailable.image = UIImage(named: "ic_lesson_not_found")

                    cell.selectionStyle = .none
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateCell", for: indexPath) as! CollaborateCell
                    
                    UIStyle.setAppButtonStyleRounded(view: cell.imgProfilePic)

                    var imgURL = ""
                    var agendaTitle = ""
                    let index = indexPath.row - 1
                    if mDashboardViewController.mCalenderListing[index].getModel() == "appointment" {
                        cell.imgProfilePic.image = UIImage(named: ic_appointment)
                        cell.lblName.text = mDashboardViewController.mCalenderListing[index].getTitle()
                        cell.lblMessage.text = mDashboardViewController.mCalenderListing[index].getmetaData().getDescription()

                    } else {
                        for i in 0..<mDashboardViewController.mStudentListing.count {
                            if mDashboardViewController.mCalenderListing[index].getmetaData().getStudentId() == mDashboardViewController.mStudentListing[i].getId()
                            {
                                imgURL = mDashboardViewController.mStudentListing[i].getImageUrl()
                                agendaTitle = mDashboardViewController.mStudentListing[i].getFirstName() + " " + mDashboardViewController.mStudentListing[i].getLastName()
                            }
                        }
                        cell.imgProfilePic.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: noImageFound))
                        cell.lblName.text = agendaTitle
                        cell.lblMessage.text = mDashboardViewController.mCalenderListing[index].getmetaData().getTitle()
                    }
                    
                    cell.lblDate.text = Utility.convertDBDateToString(dateString: mDashboardViewController.mCalenderListing[indexPath.row - 1].getStartDate())
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case 2: //CHART
            let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetCell", for: indexPath) as! BudgetCell
            
            cell.pieChartView.isHidden = false
            cell.setChart()
            //callChart(dir: "default")

            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipeGesture(gesture:)))
            swipeLeft.direction = UISwipeGestureRecognizerDirection.left
            mDashboardViewController.view.addGestureRecognizer(swipeLeft)
            
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipeGesture(gesture:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            mDashboardViewController.view.addGestureRecognizer(swipeRight)
            
            cell.selectionStyle = .none
            return cell
        default: //COLLABORATE
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateCell", for: indexPath) as! CollaborateCell
            UIStyle.setAppButtonStyleRounded(view: cell.imgProfilePic)
            
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            
//            for i in 0..<appDelegate.getCollaborateList().count {
//                if appDelegate.getCollaborateList()[i].getType() == "group" {
//                    cell.lblName.text = appDelegate.getCollaborateList()[i].getName()
//                } else {
//                    if appDelegate.getCollaborateList()[i].getName() == ""{
//                        if appDelegate.getCollaborateList()[i].getUserchats().count != 0 {
//                            for j in 0..<appDelegate.getCollaborateList()[i].getUserchats().count {
//                                let id = appDelegate.getCollaborateList()[i].getUserchats()[j].getUserId()
//                                if id != Utility.getMyUserId() {
//                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                                    for k in 0..<appDelegate.getFriendList().count{
//                                        if appDelegate.getFriendList()[k].getId() == id {
//                                            let fullName = appDelegate.getFriendList()[k].getFirstName() + " " + appDelegate.getFriendList()[k].getLastName()
//                                            appDelegate.getCollaborateList()[i].setName(name: fullName)
//                                            appDelegate.getCollaborateList()[i].setImageUrl(imageUrl: appDelegate.getFriendList()[k].getImageUrl())
//                                            cell.lblName.text = appDelegate.getCollaborateList()[i].getName()
//                                            break
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }else {
//                        cell.lblName.text = appDelegate.getCollaborateList()[i].getName()
//                    }
//                    
//                }
//                
//                //cell.lblUserName.text = mChatListingData[indexPath.row].getName()
//                
//                if appDelegate.getCollaborateList()[i].getMessages().count != 0 {
//                    
//                    cell.lblName.text = appDelegate.getCollaborateList()[i].getName()
//                    let array = appDelegate.getCollaborateList()[i].getMessages()
//                    cell.lblMessage.text = appDelegate.getCollaborateList()[i].getMessages()[array.endIndex - 1].getMessageBody()
//                    cell.lblDate.text = Utility.convertDBDateToString(dateString: appDelegate.getCollaborateList()[i].getMessages()[array.endIndex - 1].getCreatedAt())
//                    let iconName = appDelegate.getCollaborateList()[i].getImageUrl()
//                    cell.imgProfilePic.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
//                    cell.imgProfilePic.sd_setShowActivityIndicatorView(true)
//                    cell.imgProfilePic.sd_setIndicatorStyle(.gray)
//                }
//            }
            
            
            
            cell.lblName.text = "User Name"
            cell.lblMessage.text = "Hi hsp!"
            cell.lblDate.text = "01/02/1993"
            UIStyle.setAppButtonStyleRounded(view: cell.imgProfilePic)
            cell.imgProfilePic.image = UIImage(named: noImageFound)
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if mDashboardViewController.mTodoListingResponce.getData().count == 0{
                if indexPath.row == 0 {
                    return 40
                } else{
                    return 250
                }
            }else {
                return 40
            }
        case 1:
            if mDashboardViewController.mCalenderListing.count == 0{
                if indexPath.row == 0 {
                    return 50
                } else{
                    return 250
                }
            }else {
                if indexPath.row == 0 {
                    return 50
                } else{
                    return 60
                }
            }
        case 2:
            return 284
        default:
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        switch section {
        case 0:
            headerCell.lblHeader.text = "My Tasks"
            
        case 1:
            headerCell.lblHeader.text = "Agenda"
            
        case 2:
            headerCell.lblHeader.text = "Budget"
            
        default:
            headerCell.lblHeader.text = "Panda Messenger"
            
        }
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let  footerCell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as! FooterCell
        
        switch section {
        case 0:
            
            if mDashboardViewController.mTodoListingResponce.getData().count > 3{
                footerCell.btnMoreView.isHidden = false
                //footerCell.btnMoreView.setTitle("+ \(mDashboardViewController.mTodoListingResponce.getData().count - 3) Todo", for: .normal)
                footerCell.btnMoreView.setTitle("See More", for: .normal)
            }else {
                footerCell.btnMoreView.isHidden = true
            }
        case 1:
            if mDashboardViewController.mCalenderListing.count > 3{
                footerCell.btnMoreView.isHidden = false
                //footerCell.btnMoreView.setTitle("+ \(mDashboardViewController.mCalenderListing.count - 3) Agenda", for: .normal)
                footerCell.btnMoreView.setTitle("See More", for: .normal)
            }else {
                footerCell.btnMoreView.isHidden = true
            }
        case 2:
            //footerCell.btnMoreView.setTitle("+ 3 Budget", for: .normal)
            footerCell.btnMoreView.setTitle("See More", for: .normal)
        default:
            //footerCell.btnMoreView.setTitle("+ 3 Collaborate", for: .normal)
            footerCell.btnMoreView.setTitle("See More Messages", for: .normal)
        }
        footerCell.btnMoreView.tag = section
        footerCell.btnMoreView.addTarget(self, action: #selector(btnViewMore), for: .touchUpInside)
        
        return footerCell
    }
    
    func btnViewMore(sender: UIButton) {
        switch sender.tag {
        case 0:
            let dashboardVC = mDashboardViewController.storyboard!.instantiateViewController(withIdentifier: "TodoLandingViewController") as! TodoLandingViewController
            dashboardVC.mListData = mDashboardViewController.mTodoListingResponce
            mDashboardViewController.navigationController!.pushViewController(dashboardVC, animated: true)
        case 1:
            let dashboardVC = mDashboardViewController.storyboard!.instantiateViewController(withIdentifier: "CalenderWeekViewController") as! CalenderWeekViewController
            mDashboardViewController.navigationController!.pushViewController(dashboardVC, animated: true)
        case 2:
            let dashboardVC = mDashboardViewController.storyboard!.instantiateViewController(withIdentifier: "BudgetLandingViewController") as! BudgetLandingViewController
            mDashboardViewController.navigationController!.pushViewController(dashboardVC, animated: true)
        default:
            let dashboardVC = mDashboardViewController.storyboard!.instantiateViewController(withIdentifier: "CollaborateLandingViewController") as! CollaborateLandingViewController
            mDashboardViewController.navigationController!.pushViewController(dashboardVC, animated: true)
        }
    }
    
    func btnSendNote(sender: UIButton) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        callAddToDoAPI()
    }
    
    func btnCheckBox(sender: UIButton) {
        if sender.isSelected == true{
            sender.isSelected = false
            //Utility.showTost(strMsg: "Todo is already Completed.", view: mDashboardViewController.view)
        }else {
            callCompleteToDoAPI(sender: sender)
        }
    }
    
    func btnEditOnClick(sender: UIButton) {
        let mTodoEditingViewController = mDashboardViewController.storyboard!.instantiateViewController(withIdentifier: "TodoEditingViewController") as! TodoEditingViewController
        mTodoEditingViewController.tododata = mDashboardViewController.mTodoListingResponce.getData()[sender.tag]
        mDashboardViewController.navigationController!.pushViewController(mTodoEditingViewController, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        strSendNote = textField.text!
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        strSendNote = textField.text!
    }
    
    //HandleSwipeGesture
    func handleSwipeGesture(gesture: UIGestureRecognizer) {
         let cell = mDashboardViewController.tblDeshbord.dequeueReusableCell(withIdentifier: "BudgetCell") as! BudgetCell
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                cell.totalBudgetChart.isHidden = false
                cell.pieChartView.isHidden = true
                cell.setTotalBudgetChart()

            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                cell.totalBudgetChart.isHidden = true
                cell.pieChartView.isHidden = false
                cell.setChart()
            default:
                break
            }
        }
    }
    
    func callChart(dir: String) {
        let cell = mDashboardViewController.tblDeshbord.dequeueReusableCell(withIdentifier: "BudgetCell") as! BudgetCell
        print("dir = \(dir)")
        if dir == "left" {
            cell.totalBudgetChart.isHidden = true
            cell.pieChartView.isHidden = false
            cell.imgNoDataAvailable.isHidden = true
            cell.setChart()
        } else if dir == "right" {
            cell.totalBudgetChart.isHidden = false
            cell.pieChartView.isHidden = true
            cell.imgNoDataAvailable.isHidden = true
            cell.setTotalBudgetChart()
        } else {
            cell.totalBudgetChart.isHidden = true
            cell.pieChartView.isHidden = false
            cell.imgNoDataAvailable.isHidden = true
            cell.setChart()
        }
    }
    
   

    
    //MARK: API Calling Methods
    func callAddToDoAPI() {
        if strSendNote == ""  {
            mDashboardViewController.showAlert(msg: "Enter Tasks.")
        } else {
            if Reachability.isConnectedToNetwork() == true {
                mDashboardViewController.showProgress()
                
                var todoListingRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)todos/add")
                AppLog.debug(tag: mDashboardViewController.TAG, msg: "URL = \(todoListingRequest)")
                let serverParam = CreateTodo()
                serverParam.getTodo().setTitle(title: strSendNote)
                
                let paramObject = serverParam.toDictionary()
                let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
                todoListingRequest.httpBody = jsonData
                
                AppLog.debug(tag: mDashboardViewController.TAG, msg: "params = \(String(describing: paramObject))")
                
                Alamofire.request(todoListingRequest).responseJSON { response in
                    switch response.result {
                    case .success:
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.mDashboardViewController.TAG, msg: "StatusCode = \(statusCode)")
                        AppLog.debug(tag: self.mDashboardViewController.TAG, msg: "Response = \(String(describing: response.result.value))")
                        let serverResponse = TodoCreateReponce(dictionary: response.result.value as! NSDictionary)
                        if (statusCode == 200) {
                            if let value = response.result.value {
                                self.mDashboardViewController.hideProgress()
                                self.setAddTodo(data: value as! NSDictionary)
                            } else {
                                let value = response.result.value
                                AppLog.debug(tag: self.mDashboardViewController.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.mDashboardViewController.hideProgress()
                                self.mDashboardViewController.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            }
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.mDashboardViewController.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.mDashboardViewController.hideProgress()
                            self.mDashboardViewController.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    case .failure(let encodingError):
                        AppLog.debug(tag: self.mDashboardViewController.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                        self.mDashboardViewController.hideProgress()
                        self.mDashboardViewController.showAlert(msg: serverDown)
                    }
                }
            } else {
                self.mDashboardViewController.showAlert(msg: noInternet)
            }
        }
    }
    
    func setAddTodo(data: NSDictionary) {
        self.mDashboardViewController.hideProgress()
        let serverResponse = TodoCreateReponce(dictionary: data)
        let createTodo = TodoData()
        createTodo.setTitle(title: (serverResponse?.getData().getTitle())!)
        createTodo.setId(id: (serverResponse?.getData().getId())!)
        createTodo.setUserId(userId: (serverResponse?.getData().getUserId())!)
        createTodo.setCompleted(completed: (serverResponse?.getData().getCompleted())!)
        createTodo.setCreatedAt(createdAt: (serverResponse?.getData().getCreatedAt())!)
        createTodo.setUpdatedAt(updatedAt: (serverResponse?.getData().getUpdatedAt())!)
        
        let todoList = mDashboardViewController.mTodoListingResponce
        var array = todoList?.getData()
        array?.insert(createTodo, at: 0)
        todoList?.setData(data: array!)
        mDashboardViewController.tblDeshbord.reloadData()
        strSendNote = ""
    }
    
    //MARK: API Calling Methods
    func callCompleteToDoAPI(sender: UIButton) {
        if Reachability.isConnectedToNetwork() == true {
            mDashboardViewController.showProgress()
            let todoDeleteRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)todos/markComplete/\(mDashboardViewController.mTodoListingResponce.getData()[sender.tag].getId())")
            AppLog.debug(tag: self.mDashboardViewController.TAG, msg: "URL = \(todoDeleteRequest)")
            Alamofire.request(todoDeleteRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    print("response.result.value :- \(String(describing: response.result.value))")
                    let serverResponse = TodoListingResponce(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.mDashboardViewController.hideProgress()
                            var array = self.mDashboardViewController.mTodoListingResponce.getData()
                            array[sender.tag] = (serverResponse?.getData()[0])!
                            self.mDashboardViewController.mTodoListingResponce.setData(data: array)
                            sender.isSelected = true
                        } else {
                            self.mDashboardViewController.hideProgress()
                            self.mDashboardViewController.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        AppLog.debug(tag: self.mDashboardViewController.TAG, msg: "URL = \(todoDeleteRequest)")
                        self.mDashboardViewController.hideProgress()
                        self.mDashboardViewController.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure( _):
                    self.mDashboardViewController.hideProgress()
                    self.mDashboardViewController.showAlert(msg: serverDown)
                }
            }
        } else {
            mDashboardViewController.showAlert(msg: noInternet)
        }
    }
    
}
