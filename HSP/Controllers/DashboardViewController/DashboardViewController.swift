//
//  DashboardViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 07/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Charts
import Alamofire

class DashboardViewController: UIViewController, BaseView {
    
    @IBOutlet weak var tblDeshbord: UITableView!
    private var mProgressIndicator:ProgressIndicator?
    var TAG : String = "DashboardTableViewController"
    var mStudentListing = [StudentListingData]()
    var mFriendList = [GetFriendListData]()
    var mCollaborateListingData = [ChatListingData]()

    //DashboradAPIList
    var mTodoListingResponce: TodoListingResponce!
    var mDashboardTableViewController: DashboardTableViewController!
    var mCalenderListing = [ViewAppointment]()
    var todayDate: Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        todayDate = Date()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationItem.title = NSLocalizedString("dashboard", comment: "")
        let drawerImage = UIImage(named: "ic_menu_black_24dp")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: drawerImage, style: .plain, target: self, action: #selector(self.toggleLeft))
        
        initialise()
    }
    
    //MARK: BaseView Method
    func initialise() {
        
        //ProgressBar-Init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        tblDeshbord.separatorStyle = .none
        
        
        if mStudentListing.count == 0 {
            callStudentListingAPI()
        }
        
        if mFriendList.count == 0 {
            callFriendsListingAPI()
        }
        
        //Call_TODO_Listing_API
        callToDoListingAPI()
    }
    
    func showProgress() {
        mProgressIndicator?.start()

        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    
    //MARK: API Calling Methods
    func callToDoListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let todoListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)todos/index")
            AppLog.debug(tag: self.TAG, msg: "URL = \(todoListingRequest)")
            
            Alamofire.request(todoListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    self.mTodoListingResponce = TodoListingResponce(dictionary: response.result.value as! NSDictionary)!
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            self.setTableViewDeleget()
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (self.mTodoListingResponce.getMessage()), statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (self.mTodoListingResponce.getMessage()), statusCode: statusCode))
                    }
                    self.callCalenderListingAPI(startDate: self.todayDate)
                    
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                    self.callCalenderListingAPI(startDate: self.todayDate)
                    
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setTableViewDeleget() {
        mDashboardTableViewController = DashboardTableViewController(viewController: self)
        self.tblDeshbord.delegate = self.mDashboardTableViewController
        self.tblDeshbord.dataSource = self.mDashboardTableViewController
        self.tblDeshbord.reloadData()
    }
    
    func callCalenderListingAPI(startDate: Date) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let endDate = Calendar.current.date(byAdding: .day, value: 1, to: startDate)
            
            let dateStringFormatter = DateFormatter()
            dateStringFormatter.dateFormat = "yyyy-MM-dd"
            dateStringFormatter.locale = Locale(identifier: "en_US_POSIX")
            let strStartDate = dateStringFormatter.string(from: startDate)
            let strEndDate = dateStringFormatter.string(from: endDate!)
            
            
            let calenderListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)calendars/index?startDate=\(strStartDate)&endDate=\(strEndDate)")
            AppLog.debug(tag: self.TAG, msg: "URL = \(calenderListingRequest)")
 
            Alamofire.request(calenderListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    debugPrint(response)
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let studentListData = CalenderListingResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            self.todayDate = startDate
                            self.mCalenderListing = (studentListData?.getData())!
                            self.setTableViewDeleget()
                            
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }

    //StudentListing Store in Appdelegate
    func callStudentListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let studentListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)students/index")
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentListingRequest)")
            
            Alamofire.request(studentListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    //AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    //AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let studentListData = StudentListingResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            let serverResponse = StudentListingResponse(dictionary: value as! NSDictionary)
                            self.mStudentListing = (serverResponse?.getData())!
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.setStudentList(data: self.mStudentListing)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //CallFriendListing
    func callFriendsListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var friendListingRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)friends/index")
            
            let getFListData = GetFriendListData()
            getFListData.setUserId(userId: "\(Utility.getMyUserId())")
            
            let serverParam = GetFriendListResponse()
            serverParam.setFriend(friend: getFListData)
            
            let paramObject = serverParam.toGetFrndDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            friendListingRequest.httpBody = jsonData
            
            //AppLog.debug(tag: self.TAG, msg: "URL = \(friendListingRequest)")
            //AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: friendListingRequest, success: { (data) -> Void in
                self.hideProgress()
                let sResponceString = NSString(data: data as! Data ,encoding: String.Encoding.ascii.rawValue)
                let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
                let friendListResponse = GetFriendListResponse(dictionary: userDictionary! as NSDictionary)
                //AppLog.debug(tag: "FROM APPDELEGATE", msg: "mFriendListResponse: \(friendListResponse!))")
                self.mFriendList = (friendListResponse?.getData())!
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.setFriendList(data: self.mFriendList)

            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("DATA = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //setCollaborateData
    func setCollaborateData() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.appdelSocket.getChats { (data, statusCode) in
            AppLog.debug(tag: self.TAG, msg: "CollaborateLandingResponse: \(data!.getData())")
            AppLog.debug(tag: self.TAG, msg: "CollaborateLandingStatusCode: \(statusCode)")
            AppLog.debug(tag: self.TAG, msg: "CollaborateLandingMessage = \(data!.getMessage())")
            appDelegate.setCollaborateList(data: data!.getData())
        }
    }
    
}

extension DashboardViewController : DrawerLeftRightDelegate {
    func leftWillOpen() {
        //print("DrawerLeftRightDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        //print("DrawerLeftRightDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        //print("DrawerLeftRightDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        //print("DrawerLeftRightDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        //print("DrawerLeftRightDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        //print("DrawerLeftRightDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        //print("DrawerLeftRightDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        //print("DrawerLeftRightDelegate: rightDidClose")
    }
}
