//
//  CollaborateLandingTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateCell: UITableViewCell {
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
