//
//  CollaborateLandingTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Charts

class BudgetCell: UITableViewCell {
  
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var noDataFoundView: UIView!
    @IBOutlet weak var imgNoDataAvailable: UIImageView!
    @IBOutlet weak var totalBudgetChart: PieChartView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setChart() {
        let dataPoints = ["Books", "Sports", "Misc", "Stationary", "Uniform"]
        let values = [10.0, 4.0, 6.0, 3.0, 12.0]
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry1 = PieChartDataEntry(value: values[i], label: dataPoints[i], data:  dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry1)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        pieChartView.noDataText = "No data available"
        // user interaction
        pieChartView.isUserInteractionEnabled = false
        
        let d = Description()
        d.text = "" //"iOSCharts.io"
        pieChartView.chartDescription = d
        pieChartView.centerText = "Expenses"
        pieChartView.holeRadiusPercent = 0.5
        pieChartView.transparentCircleColor = UIColor.clear
        
        pieChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        let colors: [UIColor] = [Utility.hexStringToUIColor(hex: "#339933", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#ff9900", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#3366cc", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#dd3f11", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#990099", alpha: 1.0)]
        
        pieChartDataSet.colors = colors       
        
    }
    
    
    func setTotalBudgetChart() {
        let dataPoints = ["Books", "Sports", "Misc", "Stationary", "Uniform"]
        let values = [10.0, 4.0, 6.0, 3.0, 12.0]
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry1 = PieChartDataEntry(value: values[i], label: dataPoints[i], data:  dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry1)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        pieChartView.noDataText = "No data available"
        // user interaction
        pieChartView.isUserInteractionEnabled = false
        
        let d = Description()
        d.text = "" //"iOSCharts.io"
        pieChartView.chartDescription = d
        pieChartView.centerText = "Total Budget"
        pieChartView.holeRadiusPercent = 0.5
        pieChartView.transparentCircleColor = UIColor.clear
        
        pieChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        let colors: [UIColor] = [Utility.hexStringToUIColor(hex: "#339933", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#ff9900", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#3366cc", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#dd3f11", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#990099", alpha: 1.0)]
        
        pieChartDataSet.colors = colors
        
    }


}
