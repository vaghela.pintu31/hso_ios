//
//  TodoDateCell.swift
//  HSP
//
//  Created by Keyur Ashra on 03/05/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class TodoDateCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    var tappedPrevious: ((TodoDateCell) -> Void)?
    var tappedNext: ((TodoDateCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func btnPreviousOnClick(_ sender: Any) {
        tappedPrevious?(self)
    }
    
    @IBAction func btnNextOnClick(_ sender: Any) {
        tappedNext?(self)
    }

}
