//
//  CollaborateLandingTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class TodoCell: UITableViewCell {
    
    
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnEditTodo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //Convert String To Date
    func convertDateString(dateString: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let returnDate = dateFormatter.date(from: dateString)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "dd MMM"
        let dateString = dateFormatterString.string(from: returnDate!)
        return dateString
        
    }
}
