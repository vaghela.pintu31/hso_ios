//
//  LastViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 17/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LastViewController: UIViewController {

    weak var delegate: LeftMenuProtocol?
    
    @IBOutlet weak var txtOne: MKTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtOne.layer.borderColor = UIColor.clear.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.removeNavigationBarItem()
        setNavigationBarItem()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.drawerMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: LastViewController.self)  {
                self.drawerMenuController()?.removeLeftGestures()
                self.drawerMenuController()?.removeRightGestures()
            }
        })
    }


    @IBAction func LastClick(_ sender: Any) {
        delegate?.changeViewController(LeftMenu.main)
    }
    
}
