//
//  LessonPlanerSelectDaysViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 23/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import FSCalendar
import Alamofire

class LessonPlanerSelectDaysViewController: UIViewController, BaseView, FSCalendarDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var calenderLessonPlannerSd: FSCalendar!
    @IBOutlet weak var btnSubmitOl: UIButton!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var timeTopBorder: UIView!
    @IBOutlet weak var imgTimeCal: UIImageView!
    //MARK:- Variable Declaration
    private var TAG : String = "LessonPlanerSelectDaysViewController"
    private var mProgressIndicator:ProgressIndicator?
    public var mStudentId: Int?
    private var mAssignmentDaysArray = [String]()
    private var mSelectedDate: String?
   // public var mNumberOfDays: Int?
  //  public var mlessonId: Int?

    var mLessonPlanListingData: LessonPlanListingData?
    
    var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        //yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
        formatter.dateFormat = "MM-dd-yyyy"//yyyy-MM-dd
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print(" mNumberOfDays= \(String(describing: mLessonPlanListingData?.getDays()))")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialise()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtTime {
            txtTimeOnClick(textField)
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        AppLog.debug(tag: self.TAG, msg: "NSDate: \(date)")
        mSelectedDate = self.dateFormatter.string(from: date)
        AppLog.debug(tag: self.TAG, msg: "CurrentDate: \(String(describing: mSelectedDate))")
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
    //MARK: BaseView Method
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "LessonPlanerSelectDaysVC-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        UIStyle.setAppButtonStyleRounded(view: btnSubmitOl)
        
        AppLog.debug(tag: self.TAG, msg: "lessonId = \(String(describing: mLessonPlanListingData?.getDays()))")
        AppLog.debug(tag: self.TAG, msg: "studentId = \(mStudentId!)")
        
        timeTopBorder.borders(for: [.top])
        imgTimeCal.image = imgTimeCal.image!.withRenderingMode(.alwaysTemplate)
        imgTimeCal.tintColor = UIColor.black
        calenderLessonPlannerSd.delegate = self
        txtTime.delegate = self
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    @IBAction func txtTimeOnClick(_ sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.startTimeValueChanged), for: UIControlEvents.valueChanged)
        
    }
    
    func startTimeValueChanged(sender:UIDatePicker) {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        timeFormatter.dateFormat = "HH:mm"
        let strTime = timeFormatter.string(from: sender.date)
        self.txtTime.text = strTime
    }
    
    @IBAction func btnSelectDaysOnClick(_ sender: UIButton) {
        switch sender.tag{
        case 51:
            //MonSelected
            if sender.isSelected == true {
                sender.isSelected = false
                let indexOfDay = mAssignmentDaysArray.index(of: "mon")
                mAssignmentDaysArray.remove(at: indexOfDay!)
            } else {
                mAssignmentDaysArray.append("mon")
                sender.isSelected = true
            }
        case 52:
            //TueSelected
            if sender.isSelected == true {
                sender.isSelected = false
                let indexOfDay = mAssignmentDaysArray.index(of: "tue")
                mAssignmentDaysArray.remove(at: indexOfDay!)
            } else {
                mAssignmentDaysArray.append("tue")
                sender.isSelected = true
            }
        case 53:
            //WedSelected
            if sender.isSelected == true {
                sender.isSelected = false
                let indexOfDay = mAssignmentDaysArray.index(of: "wed")
                mAssignmentDaysArray.remove(at: indexOfDay!)
            } else {
                mAssignmentDaysArray.append("wed")
                sender.isSelected = true
            }
        case 54:
            //ThuSelected
            if sender.isSelected == true {
                sender.isSelected = false
                let indexOfDay = mAssignmentDaysArray.index(of: "thu")
                mAssignmentDaysArray.remove(at: indexOfDay!)
            } else {
                mAssignmentDaysArray.append("thu")
                sender.isSelected = true
            }
        case 55:
            //FriSelected
            if sender.isSelected == true {
                sender.isSelected = false
                let indexOfDay = mAssignmentDaysArray.index(of: "fri")
                mAssignmentDaysArray.remove(at: indexOfDay!)
            } else {
                mAssignmentDaysArray.append("fri")
                sender.isSelected = true
            }
        case 56:
            //SatSelected
            if sender.isSelected == true {
                sender.isSelected = false
                let indexOfDay = mAssignmentDaysArray.index(of: "sat")
                mAssignmentDaysArray.remove(at: indexOfDay!)
            } else {
                mAssignmentDaysArray.append("sat")
                sender.isSelected = true
            }
        case 57:
            //SunSelected
            if sender.isSelected == true {
                sender.isSelected = false
                let indexOfDay = mAssignmentDaysArray.index(of: "sun")
                mAssignmentDaysArray.remove(at: indexOfDay!)
            } else {
                mAssignmentDaysArray.append("sun")
                sender.isSelected = true
            }
        default:
            break
        }
        
    }
    
    @IBAction func btnSubmitOnClick(_ sender: Any) {
        print("date = \(String(describing: mSelectedDate))")
        if mSelectedDate == nil {
            showAlert(msg: "Please Select Date")
        } else {
            if mAssignmentDaysArray == [] {
                showAlert(msg: "Please Select Days")
            } else {
                print("mNumberOfDays = \(mLessonPlanListingData?.getDays())")
                print("mNumberOfDays = \(mAssignmentDaysArray.count)")

                if mLessonPlanListingData?.getDays() == mAssignmentDaysArray.count{
                    if (self.txtTime.text?.isEmpty)! {
                        showAlert(msg: "Please Select Time")
                    } else {
                        callAssignStudentAPI()
                    }
                }else {
                    showAlert(msg: "You can select \(String(describing: mLessonPlanListingData?.getDays())) days only.")
                }
            }
        }
    }
    
    //MARK: API Calling Methods
    func callAssignStudentAPI () {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var assignStudentRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/assignToStudent")
            
            let lessonPlan = LPAssignStudentData()
            lessonPlan.setStudentId(studentId: self.mStudentId!)
            lessonPlan.setLessonId(lessonId: (mLessonPlanListingData?.getId())!)
            lessonPlan.setAssignmentDays(assignmentDays: mAssignmentDaysArray)
            lessonPlan.setStartDate(startDate: Utility.convertLocalDateToServerDate(string: mSelectedDate!))
            lessonPlan.setTime(time: Utility.convertLocalTimeToServerTime(dateString: txtTime.text!))
            
            let paramObject = lessonPlan.toDictionary()
            print("paramObject = \(paramObject)")
            
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            
            assignStudentRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(assignStudentRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            Alamofire.request(assignStudentRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let lpAssignStudentResponse = LPAssignStudentResponse(dictionary: response.result.value as! NSDictionary)
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: lpAssignStudentResponse?.getMessage(), action: { (CancelButton) -> Void in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (lpAssignStudentResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (lpAssignStudentResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    
}
