//
//  LessonPlanCreateAssignmentsNotesVC.swift
//  HSP
//
//  Created by Keyur Ashra on 10/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanEditAssignmentsNotesVC: UIViewController, UITextFieldDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var txtLessonName: UITextField!
    @IBOutlet weak var txtWeeks: UITextField!
    @IBOutlet weak var txtDays: UITextField!
    @IBOutlet weak var txtHours: UITextField!
    @IBOutlet weak var txtSelectLesson: UITextField!
    @IBOutlet weak var collectionViewLpCreateAssiNotes: UICollectionView!
    
    /*** PageSwipParm ***/
    var TAG = "LessonPlanEditAssignmentsNotesVC"
    var pagerView: UIPageViewController!
    var tutorialViewController = [UIViewController]()
    var mSelectedIndex: Int?
    var nextPageIndex : Int = 0
    var currentPageIndex : Int = 0 {
        didSet {
            currentPageIndex = cap(pageIndex: currentPageIndex)
        }
    }
    
    var arrayOfHeader = [NSLocalizedString("assignments", comment: ""),
                         NSLocalizedString("notes", comment: "")]
    
    //LPCreateData
    private var arrayOfWeeks = [String]()
    private var arrayOfDays = [String]()
    private var arrayOFHours = [String]()
    
    var mlistData = LessonPlanListingData()
    var mLessonPlanEditList = [LessonPlanEdit]()
    
    var weekNotesAdd = 0
    var weekAssignment = 0
    private var submitFlag = 0
    
    public func setSubmitFlag(submitFlag: Int) {
        self.submitFlag = submitFlag
    }
    
    public func getSubmitFlag() -> Int {
        return submitFlag
    }
    
    public func setWeekNotes(week: Int) {
        self.weekNotesAdd = week
    }
    
    public func getWeekNotes() -> Int {
        return weekNotesAdd
    }
    
    public func setWeekAssignment(week: Int) {
        self.weekAssignment = week
    }
    
    public func getWeekAssignment() -> Int {
        return weekAssignment
    }
    
    public func getLessonPlanEdit() -> [LessonPlanEdit] {
        return mLessonPlanEditList
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("create_lesson_plan", comment: "")
        self.navigationItem.hidesBackButton = true
        
        let backImage   = UIImage(named: "ic_back")!
        let newBackButton: UIBarButtonItem = UIBarButtonItem(image: backImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        mSelectedIndex = 0
        collectionViewLpCreateAssiNotes.delegate = self
        collectionViewLpCreateAssiNotes.dataSource = self
        collectionViewLpCreateAssiNotes.reloadData()
        
        txtLessonName.text = mlistData.getName()
        txtWeeks.text = "\(mlistData.getWeeks())"
        txtDays.text = "\(mlistData.getDays())"
        txtHours.text = "\(mlistData.getHours())"
        
        var arrayLessonPlanWeekData = [LessonPlanWeekData]()
        
        //Set LassionPlanWeekData
        for _ in 0..<mlistData.getWeeks() {
            mLessonPlanEditList.append(LessonPlanEdit())
            arrayLessonPlanWeekData.append(LessonPlanWeekData())
        }
        
        for i in 0..<mlistData.getLessonAssignment().count{
            let lessonAssignment = mlistData.getLessonAssignment()[i]
            let index = lessonAssignment.getWeek() - 1
            
            let data = arrayLessonPlanWeekData[index]
            var array = data.getLessonAssignmentView()
            array.append(lessonAssignment)
            data.setLessonAssignmentView(lessonAssignment: array)
            
            arrayLessonPlanWeekData[index] = data
        }
        
        
        for i in 0..<mlistData.getLessonNote().count{
            let lessonNote = mlistData.getLessonNote()[i]
            let index = lessonNote.getWeek() - 1
            
            let data = arrayLessonPlanWeekData[index]
            var array = data.getLessonNoteView()
            array.append(lessonNote)
            data.setLessonNoteView(lessonNote: array)
            arrayLessonPlanWeekData[index] = data
        }
        
        
        for i in 0..<arrayLessonPlanWeekData.count {
            let object = LessonPlanEdit()
            object.setName(name: mlistData.getName())
            object.setWeeks(weeks: mlistData.getWeeks())
            object.setDays(days: mlistData.getDays())
            object.setHours(hours: mlistData.getHours())
            object.setUserId(userId: mlistData.getUserId())
            object.setCreatedAt(createdAt: mlistData.getCreatedAt())
            object.setUpdatedAt(updatedAt: mlistData.getUpdatedAt())
            
            object.setLessonAssignmentView(lessonAssignment: arrayLessonPlanWeekData[i].getLessonAssignmentView())
             object.setLessonNoteView(lessonNote: arrayLessonPlanWeekData[i].getLessonNoteView())
            mLessonPlanEditList[i] = object
        }
        pageViewSetup()
    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        submitFlag = 1
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtLessonName.resignFirstResponder()
        txtWeeks.resignFirstResponder()
        txtDays.resignFirstResponder()
        txtHours.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == txtLessonName{
            mLessonPlanEditList[0].setName(name: txtLessonName.text!)
        }
        return true
    }
    
    func textFieldDelegateSet() {
        txtLessonName.delegate = self
        txtWeeks.delegate = self
        txtDays.delegate = self
        txtHours.delegate = self
    }
    
    func closeKeyboard(){
        txtLessonName.resignFirstResponder()
        txtWeeks.resignFirstResponder()
        txtDays.resignFirstResponder()
        txtHours.resignFirstResponder()
    }
    
    
    func chageCollectionCell(index: Int){
        for cell in collectionViewLpCreateAssiNotes.visibleCells as! [LessonPlanCreateAssignmentsAndNotesCollectionViewCell] {
            cell.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)
            cell.lblChange.isHidden = true
            cell.btnHeader.titleLabel?.textColor = UIColor.white.withAlphaComponent(0.5)
        }
        
        let indexPath = NSIndexPath(row: index, section: 0)
        let cell = collectionViewLpCreateAssiNotes.cellForItem(at: indexPath as IndexPath) as! LessonPlanCreateAssignmentsAndNotesCollectionViewCell
        cell.lblChange.isHidden = false
        cell.btnHeader.titleLabel?.textColor = UIColor.white.withAlphaComponent(1.0)
        
        UIView.animate(withDuration: 1.0, animations: {
            let animation: CATransition = CATransition()
            animation.duration = 0.6
            animation.type = kCATransitionFromRight
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            cell.lblChange.layer.add(animation, forKey: "changeTextTransition")
        }, completion: nil)
        collectionViewLpCreateAssiNotes.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
    }
}


extension LessonPlanEditAssignmentsNotesVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "LessonPlanCreateAssignmentsAndNotesCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! LessonPlanCreateAssignmentsAndNotesCollectionViewCell
        cell.btnHeader.tag = indexPath.row
        cell.btnHeader.addTarget(self, action: #selector(LessonPlanEditAssignmentsNotesVC.buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        cell.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)
        if indexPath.row == 0 {
            cell.btnHeader.setTitle("\(arrayOfHeader[indexPath.row])", for: UIControlState.normal)
            cell.lblChange.isHidden = false
            cell.btnHeader.titleLabel!.textColor = UIColor.white.withAlphaComponent(1.0)
        }else {
            cell.btnHeader.setTitle("\(arrayOfHeader[indexPath.row])", for: UIControlState.normal)
            cell.lblChange.isHidden = true
            cell.lblChange.textColor = UIColor.lightText
            cell.btnHeader.titleLabel!.textColor = UIColor.white.withAlphaComponent(0.5)
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width: CGFloat((screenWidth / 2)), height: CGFloat(40))
    }
    
    //HeaderButtonClick
    func buttonClicked(sender: UIButton) {
        if currentPageIndex != sender.tag {
            if currentPageIndex < sender.tag {
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                collectionViewLpCreateAssiNotes.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.right, animated: true)
                pagerView.setViewControllers([pageForindex(pageIndex: sender.tag)!], direction: .forward, animated: true, completion: nil)
            } else if currentPageIndex > sender.tag {
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                collectionViewLpCreateAssiNotes.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.left, animated: true)
                pagerView.setViewControllers([pageForindex(pageIndex: sender.tag)!], direction: .reverse, animated: true, completion: nil)
            }
            currentPageIndex = sender.tag
            chageCollectionCell(index: currentPageIndex)
        }
    }
    
    func cap(pageIndex : Int) -> Int {
        return pageIndex
    }
    
    func carrouselJump() {
        currentPageIndex += 1
        pagerView.setViewControllers([self.pageForindex(pageIndex: currentPageIndex)!], direction: .forward, animated: true, completion: nil)
    }
    
    func pageForindex(pageIndex : Int) -> UIViewController? {
        guard (pageIndex < tutorialViewController.count) && (pageIndex>=0) else {
            return nil
        }
        //print("pageIndex :- \(pageIndex)")
        if pageIndex == 0 {
            let currentViewController = storyboard!.instantiateViewController(withIdentifier: "LessonPlanEditAssignmentsViewController") as! LessonPlanEditAssignmentsViewController
            mSelectedIndex = pageIndex
            return currentViewController.loadView(pageIndex: pageIndex,pagerView: pagerView!, data: mLessonPlanEditList, mainVC: self)
        } else if pageIndex == 1{
            let currentViewController = storyboard!.instantiateViewController(withIdentifier: "LessonPlanEditNotesViewController") as! LessonPlanEditNotesViewController
            mSelectedIndex = pageIndex
            return currentViewController.loadView(pageIndex: pageIndex,pagerView: pagerView!, data: mLessonPlanEditList, mainVC: self)
        } else {
            return nil
        }
    }
    
    func indexForPage(vc : UIViewController) -> Int {
        if let vc = vc as? LessonPlanEditAssignmentsViewController {
            return vc.getPageIndex()
        } else if let vc = vc as? LessonPlanEditNotesViewController {
            return vc.getPageIndex()
        } else {
            preconditionFailure("VCPagImageSlidesTutorial page is not a VCTutorialImagePage")
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,viewControllerAfter viewController: UIViewController) -> UIViewController?{
        return pageForindex(pageIndex: cap(pageIndex: indexForPage(vc: viewController)+1))
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        return pageForindex(pageIndex: cap(pageIndex: indexForPage(vc: viewController)-1))
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        nextPageIndex = indexForPage(vc: pendingViewControllers.first!)
        chageCollectionCell(index: nextPageIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !finished { return }
        currentPageIndex = nextPageIndex
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func pageViewSetup() {
        let studentProfileViewController = storyboard!.instantiateViewController(withIdentifier: "LessonPlanEditAssignmentsViewController") as! LessonPlanEditAssignmentsViewController
        
        let vcObjectProfile = studentProfileViewController.loadView(pageIndex: 0, pagerView: UIPageViewController(), data: mLessonPlanEditList, mainVC: self) as UIViewController
        tutorialViewController.append(vcObjectProfile)
        
        
        let studentAttendenceViewController = storyboard!.instantiateViewController(withIdentifier: "LessonPlanEditNotesViewController") as! LessonPlanEditNotesViewController
        let vcObjectAttendence = studentAttendenceViewController.loadView(pageIndex: 1,pagerView: UIPageViewController(), data: mLessonPlanEditList, mainVC: self) as UIViewController
        tutorialViewController.append(vcObjectAttendence)
        
        pagerView = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pagerView.view.backgroundColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        pagerView.view.frame = CGRect(x: CGFloat(0), y: CGFloat(203), width: CGFloat(screenWidth), height: CGFloat(screenHeight - 203))
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.setViewControllers([pageForindex(pageIndex: mSelectedIndex!)!], direction: .forward, animated: false, completion: nil)
        addChildViewController(pagerView)
        self.view.addSubview(pagerView!.view)
        pagerView.didMove(toParentViewController: self)
    }
}



