//
//  LessonPlanCreateAssignmentsViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 10/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class LessonPlanEditAssignmentsViewController: UIViewController, BaseView, UITextFieldDelegate {
    
    @IBOutlet weak var lblMainWeek: UILabel!
    @IBOutlet weak var txtDay1: UITextField!
    @IBOutlet weak var btnDay1: UIButton!
    @IBOutlet weak var txtDay2: UITextField!
    @IBOutlet weak var btnDay2: UIButton!
    @IBOutlet weak var txtDay3: UITextField!
    @IBOutlet weak var btnDay3: UIButton!
    @IBOutlet weak var txtDay4: UITextField!
    @IBOutlet weak var btnDay4: UIButton!
    @IBOutlet weak var txtDay5: UITextField!
    @IBOutlet weak var btnDay5: UIButton!
    @IBOutlet weak var txtDay6: UITextField!
    @IBOutlet weak var btnDay6: UIButton!
    @IBOutlet weak var txtDay7: UITextField!
    @IBOutlet weak var btnDay7: UIButton!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var imgDownArrow1: UIImageView!
    @IBOutlet weak var imgDownArrow2: UIImageView!
    @IBOutlet weak var imgDownArrow3: UIImageView!
    @IBOutlet weak var imgDownArrow4: UIImageView!
    @IBOutlet weak var imgDownArrow5: UIImageView!
    @IBOutlet weak var imgDownArrow6: UIImageView!
    @IBOutlet weak var imgDownArrow7: UIImageView!
    
    //TabSetup
    private var mPageIndex : Int = 0
    private var mPagerView: UIPageViewController!
    
    //MARK:- Variable Declaration
    private var TAG : String = "LessonPlanCreateAssignmentsViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var arrayOfButton = ["Assignment", "Test"]
    private var mLessonPlanEditList = [LessonPlanEdit]()
    private var mainVC: LessonPlanEditAssignmentsNotesVC!
    
    private var weekAdd = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadView(pageIndex : Int,pagerView: UIPageViewController!, data: [LessonPlanEdit], mainVC: LessonPlanEditAssignmentsNotesVC) -> LessonPlanEditAssignmentsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LessonPlanEditAssignmentsViewController") as! LessonPlanEditAssignmentsViewController
        vc.mLessonPlanEditList = data
        vc.mPageIndex = mPageIndex
        vc.mPagerView = mPagerView
        vc.mainVC = mainVC
        return vc
    }
    
    //MARK: Getter Setter Methods
    public func getPageIndex() -> Int {
        return mPageIndex
    }
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtDay1.resignFirstResponder()
        txtDay2.resignFirstResponder()
        txtDay3.resignFirstResponder()
        txtDay4.resignFirstResponder()
        txtDay5.resignFirstResponder()
        txtDay6.resignFirstResponder()
        txtDay7.resignFirstResponder()
        return true
    }
    
    func textFieldDelegateSet() {
        txtDay1.delegate = self
        txtDay2.delegate = self
        txtDay3.delegate = self
        txtDay4.delegate = self
        txtDay5.delegate = self
        txtDay6.delegate = self
        txtDay7.delegate = self
    }
    
    //MARK: BaseView Method
    func initialise() {
        
        weekAdd = mainVC.getWeekAssignment()
        //ProgressIndicator init
        lblMainWeek.text = "Week \(weekAdd + 1)"
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** Set Data **/
        setData()
        setupView()
    }
    
    func setData() {
        UIStyle.setAppButtonStyle(view: btnDay1)
        UIStyle.setAppButtonStyle(view: btnDay2)
        UIStyle.setAppButtonStyle(view: btnDay3)
        UIStyle.setAppButtonStyle(view: btnDay4)
        UIStyle.setAppButtonStyle(view: btnDay5)
        UIStyle.setAppButtonStyle(view: btnDay6)
        UIStyle.setAppButtonStyle(view: btnDay7)
        UIStyle.setAppButtonStyleRounded(view: btnSubmit)
        UIStyle.setAppButtonStyleRounded(view: btnNext)
        UIStyle.setCustomStyle(view: btnPrevious, cornerRadius: btnPrevious.frame.height/2, boderColor: COLOR_SEPARATOR, borderWidth: 1.0)
        
        //TextField_Delegate_Method_Set
        textFieldDelegateSet()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if mainVC.getSubmitFlag() == 0 {
            mainVC.setWeekAssignment(week: weekAdd)
            
            let mLPCreateData = mainVC!.getLessonPlanEdit()[0]
            var newFlag = true
            
            if mLPCreateData.getWeeks() == 0 {
                newFlag = false
            } else {
                if mLPCreateData.getDays() == 0 {
                    newFlag = false
                } else {
                    if mLPCreateData.getHours() == 0 {
                        newFlag = false
                    } else {
                        if mLPCreateData.getName() == "" {
                            newFlag = false
                        }
                    }
                }
            }
            
            if newFlag {
                setAssignment(flag: false, indexOfWeek: weekAdd)
            }
        }
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //Dropdown Function
    func selectAssignment(senderTag: Int){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Select Weeks", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = arrayOfButton.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(self.arrayOfButton[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    switch senderTag {
                    case 71:
                        self.btnDay1.setTitle(self.arrayOfButton[index], for: .normal)
                    case 72:
                        self.btnDay2.setTitle(self.arrayOfButton[index], for: .normal)
                    case 73:
                        self.btnDay3.setTitle(self.arrayOfButton[index], for: .normal)
                    case 74:
                        self.btnDay4.setTitle(self.arrayOfButton[index], for: .normal)
                    case 75:
                        self.btnDay5.setTitle(self.arrayOfButton[index], for: .normal)
                    case 76:
                        self.btnDay6.setTitle(self.arrayOfButton[index], for: .normal)
                    case 77:
                        self.btnDay7.setTitle(self.arrayOfButton[index], for: .normal)
                    default:
                        break
                    }
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    //MARK: OnClickEvents
    @IBAction func btnDayOnClick(_ sender: UIButton) {
        if sender.tag == 71 {
            selectAssignment(senderTag: 71)
        } else if sender.tag == 72 {
            selectAssignment(senderTag: 72)
        } else if sender.tag == 73 {
            selectAssignment(senderTag: 73)
        } else if sender.tag == 74 {
            selectAssignment(senderTag: 74)
        } else if sender.tag == 75 {
            selectAssignment(senderTag: 75)
        } else if sender.tag == 76 {
            selectAssignment(senderTag: 76)
        } else if sender.tag == 77 {
            selectAssignment(senderTag: 77)
        }
    }
    
    func btnSubmitOnClick() {
        if validation() {
            let mLPCreateData = mainVC!.getLessonPlanEdit()[0]
            if mLPCreateData.getWeeks() == 0 {
                showAlert(msg: "Please Select Week")
            } else {
                if mLPCreateData.getDays() == 0 {
                    showAlert(msg: "Please Select Day")
                } else {
                    if mLPCreateData.getHours() == 0 {
                        showAlert(msg: "Please Select Hour")
                    } else {
                        if mLPCreateData.getName() == "" {
                            showAlert(msg: "Please Enter Name")
                        } else {
                            setAssignment(flag: true, indexOfWeek: weekAdd)
                            
                        }
                    }
                }
            }
        } else {
            showAlert(msg: "Please fill the all value for Assignment.")
        }
    }
    
    func btnNextOnClick() {
        if validation() {
            let oldWeek = weekAdd
            weekAdd += 1
            
            if weekAdd == mainVC.getLessonPlanEdit()[0].getWeeks() - 1 {
                btnNext.isHidden = true
                btnPrevious.isHidden = false
                btnSubmit.isHidden = false
            } else {
                btnNext.isHidden = false
                btnPrevious.isHidden = false
                btnSubmit.isHidden = true
            }
            
            lblMainWeek.text = "Week \(weekAdd + 1)"
            clearTextData(indexForWeek: oldWeek)
            
            mainVC.setWeekAssignment(week: weekAdd)
        } else {
            showAlert(msg: "Please fill the all value for Assignment.")
        }
    }
    
    func btnPreviousOnClick() {
        if validation() {
            let oldWeek = weekAdd
            weekAdd -= 1
            
            if weekAdd == 0 {
                btnPrevious.isHidden = true
                btnSubmit.isHidden = true
                btnNext.isHidden = false
            } else {
                btnPrevious.isHidden = false
                btnNext.isHidden = false
                btnSubmit.isHidden = true
            }
            
            lblMainWeek.text = "Week \(weekAdd + 1)"
            AppLog.debug(tag: TAG, msg: "uC: \(oldWeek)")

            clearTextData(indexForWeek: oldWeek)
            AppLog.debug(tag: TAG, msg: "UpC: \(oldWeek)")

            mainVC.setWeekAssignment(week: weekAdd)
        } else {
            showAlert(msg: "Please fill the all value for Assignment.")
        }
    }
    
    func clearTextData(indexForWeek: Int) {
        setAssignment(flag: false, indexOfWeek: indexForWeek)
    }
    
    //SetUpView
    func setupView() {
        btnSubmit.addTarget(self, action: #selector(self.btnSubmitOnClick), for: .touchUpInside)
        btnNext.addTarget(self, action: #selector(self.btnNextOnClick), for: .touchUpInside)
        btnPrevious.addTarget(self, action: #selector(self.btnPreviousOnClick), for: .touchUpInside)
        btnPrevious.isHidden = true
        
        if mainVC.getLessonPlanEdit()[0].getWeeks() == 0 {
            UIStyle.showEmptyView(message: NSLocalizedString("assignment_select_week", comment: ""), viewController: self)
        } else {
            let weekCount:Int = mainVC.getLessonPlanEdit()[0].getWeeks()
            
            if weekCount == 1 {
                btnNext.isHidden = true
                btnPrevious.isHidden = true
                btnSubmit.isHidden = false
            } else if weekAdd == weekCount - 1 {
                btnNext.isHidden = true
                btnPrevious.isHidden = false
                btnSubmit.isHidden = false
            } else if weekAdd == 0 {
                btnPrevious.isHidden = true
                btnSubmit.isHidden = true
                btnNext.isHidden = false
            } else {
                btnNext.isHidden = false
                btnPrevious.isHidden = false
                btnSubmit.isHidden = true
            }
            
            //VisibleDaysInputAdd
            visibleDaysInput()
            displayData()
            
        }
    }
    
    func visibleDaysInput() {
        //Days["1","2","3","4","5","6","7"]
        if mainVC.getLessonPlanEdit()[0].getDays() == 0 {
            print("No Days Selected")
        } else {
            let daysIs:Int = mainVC.getLessonPlanEdit()[0].getDays()
            switch daysIs{
            case 1:
                btnDay2.isHidden = true
                txtDay2.isHidden = true
                imgDownArrow2.isHidden = true
                btnDay3.isHidden = true
                txtDay3.isHidden = true
                imgDownArrow3.isHidden = true
                btnDay4.isHidden = true
                txtDay4.isHidden = true
                imgDownArrow4.isHidden = true
                btnDay5.isHidden = true
                txtDay5.isHidden = true
                imgDownArrow5.isHidden = true
                btnDay6.isHidden = true
                txtDay6.isHidden = true
                imgDownArrow6.isHidden = true
                btnDay7.isHidden = true
                txtDay7.isHidden = true
                imgDownArrow7.isHidden = true
                
            case 2:
                btnDay3.isHidden = true
                txtDay3.isHidden = true
                imgDownArrow3.isHidden = true
                btnDay4.isHidden = true
                txtDay4.isHidden = true
                imgDownArrow4.isHidden = true
                btnDay5.isHidden = true
                txtDay5.isHidden = true
                imgDownArrow5.isHidden = true
                btnDay6.isHidden = true
                txtDay6.isHidden = true
                imgDownArrow6.isHidden = true
                btnDay7.isHidden = true
                txtDay7.isHidden = true
                imgDownArrow7.isHidden = true
                
            case 3:
                btnDay4.isHidden = true
                txtDay4.isHidden = true
                imgDownArrow4.isHidden = true
                btnDay5.isHidden = true
                txtDay5.isHidden = true
                imgDownArrow5.isHidden = true
                btnDay6.isHidden = true
                txtDay6.isHidden = true
                imgDownArrow6.isHidden = true
                btnDay7.isHidden = true
                txtDay7.isHidden = true
                imgDownArrow7.isHidden = true
                
            case 4:
                btnDay5.isHidden = true
                txtDay5.isHidden = true
                imgDownArrow5.isHidden = true
                btnDay6.isHidden = true
                txtDay6.isHidden = true
                imgDownArrow6.isHidden = true
                btnDay7.isHidden = true
                txtDay7.isHidden = true
                imgDownArrow7.isHidden = true
                
            case 5:
                btnDay6.isHidden = true
                txtDay6.isHidden = true
                imgDownArrow6.isHidden = true
                btnDay7.isHidden = true
                txtDay7.isHidden = true
                imgDownArrow7.isHidden = true
                
            case 6:
                btnDay7.isHidden = true
                txtDay7.isHidden = true
                imgDownArrow7.isHidden = true
                
            case 7:
                AppLog.debug(tag: self.TAG, msg: "All are visible")
            default:
                break
            }
        }
    }
    
    /*
     */
    func setAssignment(flag: Bool, indexOfWeek: Int) {
        let mLPCreateData = mainVC!.getLessonPlanEdit()[0]
        let daysCount = mLPCreateData.getDays()
        //print("daysCount :- \(daysCount)")
        
        //let dayWeek = indexOfWeek + 1
        
        var lAssignmentArrayData = [LessonAssignmentView]()
        lAssignmentArrayData.removeAll()
        
        for indexDay in 0..<daysCount{
            switch indexDay {
            case 0:
                let lassignment = mainVC.getLessonPlanEdit()[indexOfWeek].getLessonAssignmentView()[0]
                lassignment.setTitle(title: txtDay1.text!)
                lassignment.setCategory(category: (btnDay1.titleLabel!.text!.lowercased()))
                lAssignmentArrayData.append(lassignment)
                break
                
            case 1:
                let lassignment2 = mainVC.getLessonPlanEdit()[indexOfWeek].getLessonAssignmentView()[1]
                lassignment2.setTitle(title: txtDay2.text!)
                lassignment2.setCategory(category: (btnDay2.titleLabel!.text!.lowercased()))
                lAssignmentArrayData.append(lassignment2)
                break
                
            case 2:
                let lassignment3 = mainVC.getLessonPlanEdit()[indexOfWeek].getLessonAssignmentView()[2]
                lassignment3.setTitle(title: txtDay3.text!)
                lassignment3.setCategory(category: (btnDay3.titleLabel!.text!.lowercased()))
                lAssignmentArrayData.append(lassignment3)
                break
                
            case 3:
                let lassignment4 = mainVC.getLessonPlanEdit()[indexOfWeek].getLessonAssignmentView()[3]
                lassignment4.setTitle(title: txtDay4.text!)
                lassignment4.setCategory(category: (btnDay4.titleLabel!.text!.lowercased()))
                lAssignmentArrayData.append(lassignment4)
                break
                
            case 4:
                let lassignment5 = mainVC.getLessonPlanEdit()[indexOfWeek].getLessonAssignmentView()[4]
                lassignment5.setTitle(title: txtDay5.text!)
                lassignment5.setCategory(category: (btnDay5.titleLabel!.text!.lowercased()))
                lAssignmentArrayData.append(lassignment5)
                break
                
            case 5:
                let lassignment6 = mainVC.getLessonPlanEdit()[indexOfWeek].getLessonAssignmentView()[5]
                lassignment6.setTitle(title: txtDay6.text!)
                lassignment6.setCategory(category: (btnDay6.titleLabel!.text!.lowercased()))
                lAssignmentArrayData.append(lassignment6)
                break
                
            case 6:
                let lassignment7 = mainVC.getLessonPlanEdit()[indexOfWeek].getLessonAssignmentView()[6]
                lassignment7.setTitle(title: txtDay7.text!)
                lassignment7.setCategory(category: (btnDay7.titleLabel!.text!.lowercased()))
                lAssignmentArrayData.append(lassignment7)
                break
                
            default:
                break
            }
        }
        
        
        var laarray = mainVC.getLessonPlanEdit()[indexOfWeek].getLessonAssignmentView()
        laarray.removeAll()
        laarray = lAssignmentArrayData
        
        //print("indexOfWeek :- \(indexOfWeek)")
        
        let lessonPlan = mainVC.getLessonPlanEdit()
        //mainVC.getLessonPlanEdit()[indexOfWeek].setLessonAssignmentView(lessonAssignment: laarray)
        
        lessonPlan[indexOfWeek].setLessonAssignmentView(lessonAssignment: laarray)
        
        self.txtDay1.text = ""
        self.txtDay2.text = ""
        self.txtDay3.text = ""
        self.txtDay4.text = ""
        self.txtDay5.text = ""
        self.txtDay6.text = ""
        self.txtDay7.text = ""
        
        self.btnDay1.setTitle(self.arrayOfButton[0], for: .normal)
        self.btnDay2.setTitle(self.arrayOfButton[0], for: .normal)
        self.btnDay3.setTitle(self.arrayOfButton[0], for: .normal)
        self.btnDay4.setTitle(self.arrayOfButton[0], for: .normal)
        self.btnDay5.setTitle(self.arrayOfButton[0], for: .normal)
        self.btnDay6.setTitle(self.arrayOfButton[0], for: .normal)
        self.btnDay7.setTitle(self.arrayOfButton[0], for: .normal)
        
        if flag {
            callLessonPlanEditAPI()
        } else {
            //print("Position :- \(weekAdd)")
            let cloneLessonPlan = mainVC.getLessonPlanEdit()[weekAdd]
            
            if cloneLessonPlan != nil {
                let assignments = cloneLessonPlan.getLessonAssignmentView()
                
                for indexDay in 0..<assignments.count {
                    print("assignments \(assignments[indexDay])")
                    switch indexDay {
                    case 0:
                        txtDay1.text = assignments[indexDay].getTitle()
                        
                        if assignments[indexDay].getCategory() == "assignment" {
                            self.btnDay1.setTitle(self.arrayOfButton[0], for: .normal)
                        } else {
                            self.btnDay1.setTitle(self.arrayOfButton[1], for: .normal)
                        }
                        break
                        
                    case 1:
                        txtDay2.text = assignments[indexDay].getTitle()
                        
                        if assignments[indexDay].getCategory() == "assignment" {
                            self.btnDay2.setTitle(self.arrayOfButton[0], for: .normal)
                        } else {
                            self.btnDay2.setTitle(self.arrayOfButton[1], for: .normal)
                        }
                        break
                        
                    case 2:
                        txtDay3.text = assignments[indexDay].getTitle()
                        
                        if assignments[indexDay].getCategory() == "assignment" {
                            self.btnDay3.setTitle(self.arrayOfButton[0], for: .normal)
                        } else {
                            self.btnDay3.setTitle(self.arrayOfButton[1], for: .normal)
                        }
                        break
                        
                    case 3:
                        txtDay4.text = assignments[indexDay].getTitle()
                        
                        if assignments[indexDay].getCategory() == "assignment" {
                            self.btnDay4.setTitle(self.arrayOfButton[0], for: .normal)
                        } else {
                            self.btnDay4.setTitle(self.arrayOfButton[1], for: .normal)
                        }
                        break
                        
                    case 4:
                        txtDay5.text = assignments[indexDay].getTitle()
                        
                        if assignments[indexDay].getCategory() == "assignment" {
                            self.btnDay5.setTitle(self.arrayOfButton[0], for: .normal)
                        } else {
                            self.btnDay5.setTitle(self.arrayOfButton[1], for: .normal)
                        }
                        break
                        
                    case 5:
                        txtDay6.text = assignments[indexDay].getTitle()
                        
                        if assignments[indexDay].getCategory() == "assignment" {
                            self.btnDay6.setTitle(self.arrayOfButton[0], for: .normal)
                        } else {
                            self.btnDay6.setTitle(self.arrayOfButton[1], for: .normal)
                        }
                        break
                        
                    case 6:
                        txtDay7.text = assignments[indexDay].getTitle()
                        
                        if assignments[indexDay].getCategory() == "assignment" {
                            self.btnDay7.setTitle(self.arrayOfButton[0], for: .normal)
                        } else {
                            self.btnDay7.setTitle(self.arrayOfButton[1], for: .normal)
                        }
                        break
                        
                    default:
                        break
                    }
                }
            }
        }
    }
    
    
    func checkFinalValidation() -> Bool {
        let cloneArray = mainVC.getLessonPlanEdit()
        let counter = cloneArray.count
        for i in 0..<counter {
            var lessonAssignmentArray = cloneArray[i].getLessonAssignmentView()
            let countTitle = lessonAssignmentArray.count
            
            if lessonAssignmentArray.count == 0 {
                return false
            } else {
                for k in 0..<countTitle {
                    if lessonAssignmentArray[k].getTitle().characters.count == 0{
                        return false
                    }
                }
            }
        }
        return true
    }
    
    
    func callLessonPlanEditAPI() {
        if checkFinalValidation() == true {
            if Reachability.isConnectedToNetwork() == true {
                showProgress()
                
                //URLRequest
                var LessonPlanCreateRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/edit/\(mainVC.mlistData.getId())")
                
                let cloneArray = mainVC.getLessonPlanEdit()
                let counter = cloneArray.count
                
                let finalLessonPlan = LessonPlanEdit()
                finalLessonPlan.setName(name: cloneArray[0].getName())
                finalLessonPlan.setWeeks(weeks: cloneArray[0].getWeeks())
                finalLessonPlan.setDays(days: cloneArray[0].getDays())
                finalLessonPlan.setHours(hours: cloneArray[0].getHours())
                finalLessonPlan.setUpdatedAt(updatedAt: cloneArray[0].getUpdatedAt())
                finalLessonPlan.setCreatedAt(createdAt: cloneArray[0].getCreatedAt())
                finalLessonPlan.setUserId(userId: cloneArray[0].getUserId())
                
                
                var lessonAssignmentArray = finalLessonPlan.getLessonAssignmentView()
                var lessonNotesArray = finalLessonPlan.getLessonNoteView()
                
                for i in 0..<counter {
                    let laArray = cloneArray[i].getLessonAssignmentView()
                    let notesArray = cloneArray[i].getLessonNoteView()
                    
                    for k in 0..<laArray.count {
                        lessonAssignmentArray.append(laArray[k])
                    }
                    
                    for j in 0..<notesArray.count {
                        lessonNotesArray.append(notesArray[j])
                    }
                }
                
                finalLessonPlan.setLessonAssignmentView(lessonAssignment: lessonAssignmentArray)
                finalLessonPlan.setLessonNoteView(lessonNote: lessonNotesArray)
                
                let paramObject = finalLessonPlan.toDictionary()
                let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
                LessonPlanCreateRequest.httpBody = jsonData
                
                AppLog.debug(tag: self.TAG, msg: "URL = \(LessonPlanCreateRequest)")
                AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
                
                Alamofire.request(LessonPlanCreateRequest).responseJSON { response in
                    switch response.result {
                    case .success:
                        let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                        AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                        if (statusCode == 200) {
                            if let value = response.result.value {
                                self.hideProgress()
                                self.setLessonCreateData(data: value as! NSDictionary)
                            }else{
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.hideProgress()
                                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            }
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    case .failure(let encodingError):
                        AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                        self.hideProgress()
                        self.showAlert(msg: serverDown)
                    }
                }
            } else {
                showAlert(msg: noInternet)
            }
        }else {
            showAlert(msg: "Please fill the all value for Assignment.")
        }
    }
    
    func setLessonCreateData(data: NSDictionary) {
        hideProgress()
        mainVC.setSubmitFlag(submitFlag: 1)
        AppLog.debug(tag: TAG, msg: "ResponseLessonCreate = \(data)")
        let serverResponse = ServerResponse(dictionary: data)
        AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: serverResponse?.getMessage(), action: { (CancelButton) -> Void in
            self.mainVC.setSubmitFlag(submitFlag: 1)
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    
    func displayData() {
        let cloneLessonPlan = mainVC.getLessonPlanEdit()[weekAdd]
        if cloneLessonPlan != nil {
            let assignments = cloneLessonPlan.getLessonAssignmentView()
            AppLog.debug(tag: TAG, msg: "\(assignments)")

            for indexDay in 0..<assignments.count {
                switch indexDay {
                case 0:
                    txtDay1.text = assignments[indexDay].getTitle()
                    
                    if assignments[indexDay].getCategory() == "assignment" {
                        self.btnDay1.setTitle(self.arrayOfButton[0], for: .normal)
                    } else {
                        self.btnDay1.setTitle(self.arrayOfButton[1], for: .normal)
                    }
                    break
                    
                case 1:
                    txtDay2.text = assignments[indexDay].getTitle()
                    
                    if assignments[indexDay].getCategory() == "assignment" {
                        self.btnDay2.setTitle(self.arrayOfButton[0], for: .normal)
                    } else {
                        self.btnDay2.setTitle(self.arrayOfButton[1], for: .normal)
                    }
                    break
                    
                case 2:
                    txtDay3.text = assignments[indexDay].getTitle()
                    
                    if assignments[indexDay].getCategory() == "assignment" {
                        self.btnDay3.setTitle(self.arrayOfButton[0], for: .normal)
                    } else {
                        self.btnDay3.setTitle(self.arrayOfButton[1], for: .normal)
                    }
                    break
                    
                case 3:
                    txtDay4.text = assignments[indexDay].getTitle()
                    
                    if assignments[indexDay].getCategory() == "assignment" {
                        self.btnDay4.setTitle(self.arrayOfButton[0], for: .normal)
                    } else {
                        self.btnDay4.setTitle(self.arrayOfButton[1], for: .normal)
                    }
                    break
                    
                case 4:
                    txtDay5.text = assignments[indexDay].getTitle()
                    
                    if assignments[indexDay].getCategory() == "assignment" {
                        self.btnDay5.setTitle(self.arrayOfButton[0], for: .normal)
                    } else {
                        self.btnDay5.setTitle(self.arrayOfButton[1], for: .normal)
                    }
                    break
                    
                case 5:
                    txtDay6.text = assignments[indexDay].getTitle()
                    
                    if assignments[indexDay].getCategory() == "assignment" {
                        self.btnDay6.setTitle(self.arrayOfButton[0], for: .normal)
                    } else {
                        self.btnDay6.setTitle(self.arrayOfButton[1], for: .normal)
                    }
                    break
                    
                case 6:
                    txtDay7.text = assignments[indexDay].getTitle()
                    
                    if assignments[indexDay].getCategory() == "assignment" {
                        self.btnDay7.setTitle(self.arrayOfButton[0], for: .normal)
                    } else {
                        self.btnDay7.setTitle(self.arrayOfButton[1], for: .normal)
                    }
                    break
                    
                default:
                    break
                }
            }
        }
    }
    
    
    func validation() -> Bool {
        let dayCount = mainVC.getLessonPlanEdit()[0].getDays()
        for i in 0..<dayCount {
            switch i{
            case 0:
                if(txtDay1.text?.characters.count == 0) {
                    return false
                }
                
            case 1:
                if(txtDay2.text?.characters.count == 0) {
                    return false
                }
                
            case 2:
                if(txtDay3.text?.characters.count == 0) {
                    return false
                }
                
            case 3:
                if(txtDay4.text?.characters.count == 0) {
                    return false
                }
                
            case 4:
                if(txtDay5.text?.characters.count == 0) {
                    return false
                }
                
            case 5:
                if(txtDay6.text?.characters.count == 0) {
                    return false
                }
                
            case 6:
                if(txtDay7.text?.characters.count == 0) {
                    return false
                }
            default:
                return false
            }
        }
        return true
    }
    
}
