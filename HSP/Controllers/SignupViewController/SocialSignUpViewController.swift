//
//  SocialSignUpViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 30/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import Alamofire

class SocialSignUpViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate, BaseView {
    
    @IBOutlet weak var btnFacebookOl: UIButton!
    @IBOutlet weak var btnGoogleOl: UIButton!
    @IBOutlet weak var btnSignUpOl: UIButton!
    @IBOutlet weak var btnLoginOl: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG : String = "SocialSignUpViewController"
    private var logsEnable: Bool!
    private var mHttpCodes = DataManager.getHttpCodeData()
    private var mProgressIndicator:ProgressIndicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    @IBAction func btnSignUpOnClick(_ sender: Any) {
        let socialSignUpVC = self.storyboard!.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController!.pushViewController(socialSignUpVC, animated: true)
    }
    
    @IBAction func btnLoginOnClick(_ sender: Any) {
        let socialSignUpVC = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController!.pushViewController(socialSignUpVC, animated: true)
    }
    
    //MARK: FACEBOOK INTEGRATION
    @IBAction func btnFacebookOnClick(_ sender: Any) {
        if Reachability.isConnectedToNetwork() == true {
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    
                    if fbloginresult.grantedPermissions != nil {
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            let accessToken = FBSDKAccessToken.current()
                            if(accessToken != nil) //should be != nil
                            {
                                AppLog.debug(tag: self.TAG, msg: "accessToken :- \(String(describing: accessToken?.tokenString))")
                                
                                //callSocialSignUpAPI
                                self.callSocialSignUpAPI(token: (accessToken?.tokenString)!, type: 0)
                                
                            }
                            fbLoginManager.logOut()
                        }
                    }
                } else   {
                    AppLog.debug(tag: self.TAG, msg: "Error in SignInAppDel-> \(String(describing: error?.localizedDescription))")
                    let message = self.mHttpCodes[002]!
                    self.showAlert(msg: message)
                }
            }
        } else {
            let message = mHttpCodes[001]!
            self.showAlert(msg: message)
        }
    }
    
    //MARK: GOOGLE INTEGRATION
    @IBAction func btnGoogleOnClick(_ sender: Any) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().signIn()
        } else {
            hideProgress()
            self.showAlert(msg: mHttpCodes[001]!)
        }
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,dismiss viewController: UIViewController!) {
        hideProgress()
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        //self.indicator?.start()
        if (error == nil)        {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let name = user.profile.name
            let email = user.profile.email
            var pic: NSURL!
            if user.profile.hasImage  {
                pic = user.profile.imageURL(withDimension: 100)! as NSURL
            }
            AppLog.debug(tag: self.TAG, msg: "GoogleSignInData  = \(userId!)\n\(idToken!)\(name!)\n\(email!)\n\(pic!)")
            
            //Call GoogleAPI
            callSocialSignUpAPI(token: idToken!, type: 1)
            
        }else   {
            self.hideProgress()
            //print("Error in SignInAppDel-> \(error.localizedDescription)")
            let message = mHttpCodes[001]!
            self.showAlert(msg: message)
        }
    }
    
    func myFileString(theFile: NSURL) -> String {
        return theFile.absoluteString!
    }
    
    //MARK: BaseView Method
    func initialise() {
        UIStyle.setCustomStyle(view: btnFacebookOl, cornerRadius: 20, boderColor: COLOR_WHITE, borderWidth: 1)
        UIStyle.setCustomStyle(view: btnGoogleOl, cornerRadius: 20, boderColor: COLOR_WHITE, borderWidth: 1)
        UIStyle.setCustomStyle(view: btnSignUpOl, cornerRadius: 20, boderColor: COLOR_WHITE, borderWidth: 1)
        UIStyle.setAppButtonStyleRounded(view: btnLoginOl)
        
        AppLog.debug(tag: self.TAG, msg: "SocialSignUp-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    func printLogs (msg :String) {
        logsEnable = true
        if(logsEnable){
            print("\(TAG) + \(msg)")
        }
    }
    
    //MARK: CALLAPIS
    func callSocialSignUpAPI (token: String,type: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let socialType: String!
            if type == 0 {
                socialType = "facebook"
            } else {
                socialType = "google"
            }
            
            var socialLoginRequest = ServerRequest.getPostRequest(url: "\(mainURL)users/add")
            let serverParam = ServerRequestParam()
            serverParam.setToken(token: token)
            serverParam.setType(type: socialType)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            socialLoginRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(socialLoginRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(paramObject)")
            
            Alamofire.request(socialLoginRequest).responseJSON { response in
                    switch response.result {
                    case .success:
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                        AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                        let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                        if (statusCode == 200) {
                           if let value = response.result.value {
                                self.hideProgress()
                                self.setSocialSignUpAPIData(data: value as! NSDictionary)
                            }else{
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.hideProgress()
                                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            }
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    case .failure(let encodingError):
                        AppLog.debug(tag: self.TAG, msg: "SocialApi_Failure:- \(encodingError.localizedDescription)")
                        self.hideProgress()
                        self.showAlert(msg: serverDown)
                    }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //SetSocialSignUpAPIData
    func setSocialSignUpAPIData(data: NSDictionary) {
        let signUpResponse = SignUpResponse(dictionary: data)
        let socialSignUpVC = self.storyboard!.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        
        let Fullname = (signUpResponse?.getData().getUser().getFirstName())! + " " + (signUpResponse?.getData().getUser().getLastName())!
        Utility.setUserLogin(isLogin: true)
        Utility.setUserName(username: Fullname)
        Utility.setUserEmail(EmailId: signUpResponse?.getData().getUser().getEmail())
        Utility.setUserImageUrl(ImageUrl: signUpResponse?.getData().getUser().getImageUrl())
        Utility.setXAccessToken(token: signUpResponse?.getData().getAuthToken())
        Utility.setMyUserId(myuserId: signUpResponse?.getData().getUser().getId())
        
        //StoreProfileData
        Utility.setUserGender(gender: signUpResponse?.getData().getUser().getGender())
        Utility.setUserDob(dob: signUpResponse?.getData().getUser().getDob())
        Utility.setUserCity(city: signUpResponse?.getData().getUser().getCity())
        Utility.setUserState(state: signUpResponse?.getData().getUser().getState())
        
        self.navigationController!.pushViewController(socialSignUpVC, animated: true)
    }
}

extension SocialSignUpViewController : DrawerLeftRightDelegate {
    func leftWillOpen() {
        //print("DrawerLeftRightDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        //print("DrawerLeftRightDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        //print("DrawerLeftRightDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        //print("DrawerLeftRightDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        //print("DrawerLeftRightDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        //print("DrawerLeftRightDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        //print("DrawerLeftRightDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        //print("DrawerLeftRightDelegate: rightDidClose")
    }
}
