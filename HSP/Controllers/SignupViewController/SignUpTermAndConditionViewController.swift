//
//  SignUpTermAndConditionViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 01/05/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class SignUpTermAndConditionViewController: UIViewController, UIWebViewDelegate {

    var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        openWebView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openWebView(){
        webView = UIWebView(frame: UIScreen.main.bounds)
        webView.delegate = self
        view.addSubview(webView)
        if let url = URL(string: "http://apple.com") {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
}
