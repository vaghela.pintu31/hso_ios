//
//  SignUpViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 17/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class SignUpViewController: UIViewController,BaseView, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var curveView: UIView!
    @IBOutlet weak var txtFirstName: MKTextField!
    @IBOutlet weak var txtLastName: MKTextField!
    @IBOutlet weak var txtEmailId: MKTextField!
    @IBOutlet weak var txtPassword: MKTextField!
    @IBOutlet weak var txtGender: MKTextField!
    @IBOutlet weak var txtDob: MKTextField!
    @IBOutlet weak var txtPhoneNo: MKTextField!
    @IBOutlet weak var txtStreet: MKTextField!
    @IBOutlet weak var txtCity: MKTextField!
    @IBOutlet weak var txtCountryCode: MKTextField!
    @IBOutlet weak var txtCountry: MKTextField!
    @IBOutlet weak var txtPincode: MKTextField!
    @IBOutlet weak var btnContinueOl: UIButton!
    @IBOutlet weak var btnTermsAndCondCheckBoxOL: UIButton!
    @IBOutlet weak var btnTermsAndCondLabelOl: UIButton!

    //MARK:- Variable Declaration
    private var TAG : String = "SignUpViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mHttpCodes = DataManager.getHttpCodeData()
    private var mSelectedId = 0
    private var mSelectedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        /* Initialize View */
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UIImagePickerControllerDelegate
    @IBAction func openGallary(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: UIImagePickerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo  info: [String : Any]) {
        mSelectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgProfilePic.backgroundColor = UIColor.clear
        imgProfilePic.contentMode = UIViewContentMode.scaleAspectFit
        imgProfilePic.image = mSelectedImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnPasswordHideShowOnClick(_ sender: UIButton) {
        if sender.isSelected == true {
            sender.isSelected = false
            txtPassword.isSecureTextEntry = true
        } else {
            sender.isSelected = true
            txtPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func btnTermandConditionCheckboxOnClick(_ sender: UIButton) {
        if btnTermsAndCondCheckBoxOL.isSelected == true{
            btnTermsAndCondCheckBoxOL.isSelected = false
        }else{
            btnTermsAndCondCheckBoxOL.isSelected = true
        }
    }
    
    @IBAction func btnTermsAndConditionLabelOnClick(_ sender: UIButton) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "SignUpTermAndConditionViewController") as! SignUpTermAndConditionViewController
        self.navigationController!.pushViewController(homeViewController, animated: true)
    }
    
    
    
    @IBAction func btnContinueOnClick(_ sender: Any) {
        if (self.txtFirstName.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_fname", comment: ""))
        } else if (self.txtLastName.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_lname", comment: ""))
        } else if (self.txtEmailId.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_email", comment: ""))
        } else if Utility.isValidEmail(urlString: txtEmailId.text!) == false {
            showAlert(msg: NSLocalizedString("enter_valid_email", comment: ""))
        } else if (self.txtPassword.text?.isEmpty)! {
            showAlert(msg: NSLocalizedString("enter_password", comment: ""))
        } else if (self.txtPassword.text?.characters.count)! < 5 || (self.txtPassword.text?.characters.count)! > 30 {
            showAlert(msg: NSLocalizedString("strPasswordMinimumLength", comment: ""))
        }
//        else if (self.txtPhoneNo.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_phoneno", comment: ""))
//        } else if (self.txtGender.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_gender", comment: ""))
//        } else if (self.txtDob.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_dob", comment: ""))
//        } else if (self.txtStreet.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_street", comment: ""))
//        } else if (self.txtPincode.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_pincode", comment: ""))
//        } else if (self.txtCity.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_city", comment: ""))
//        } else if (self.txtCountry.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_country", comment: ""))
//        }else if (self.txtCountryCode.text?.isEmpty)! {
//            showAlert(msg: NSLocalizedString("enter_countrycode", comment: ""))
//        }
        else if btnTermsAndCondCheckBoxOL.isSelected == false {
            showAlert(msg: NSLocalizedString("please_tickmark_term&con", comment: ""))
        } else {
            callSignUpAPI()
        }
    }
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtEmailId.resignFirstResponder()
        txtPassword.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtPhoneNo.resignFirstResponder()
        txtStreet.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtPincode.resignFirstResponder()
        txtCountryCode.resignFirstResponder()
        txtDob.resignFirstResponder()
        return true
    }
    
    
    @IBAction func btnSelectGender(_ sender: Any) {
        self.closeKeyboard()
        selectGender()
    }
    
    @IBAction func btnSelectCountryCode(_ sender: Any) {
        self.closeKeyboard()
        showProgress()
        //selectCountryCode()
        selectCountryName()
    }
    
    @IBAction func btnSelectCountry(_ sender: Any) {
        self.closeKeyboard()
        showProgress()
        selectCountryName()
    }
    
    
    @IBAction func btnDobOnClick(_ sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    //DatePicker
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: sender.date)
        self.txtDob.text = strDate
    }
    
    func closeKeyboard() {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtEmailId.resignFirstResponder()
        txtPassword.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtPhoneNo.resignFirstResponder()
        txtStreet.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtPincode.resignFirstResponder()
        txtCountryCode.resignFirstResponder()
        txtDob.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case txtFirstName:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 32
        case txtLastName:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 32
        case txtPhoneNo:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10
        case txtPassword:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 15
        case txtPincode:
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 6
        default:
             return true
        }
    }
    
    @IBAction func txtGenderOnClick(_ sender: Any) {
        self.closeKeyboard()
        selectGender()
    }
    
    //MARK: BaseView Method
    /* Initialize View */
    func initialise() {
        //NavigationTitle
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationItem.title = NSLocalizedString("signup", comment: "")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        //UISetup
        UIStyle.setCustomStyle(view: imgProfilePic, cornerRadius: imgProfilePic.frame.height/2, boderColor: COLOR_BACKGROUND, borderWidth: 2)
        UIStyle.setAppButtonStyleRounded(view: curveView)
        UIStyle.setAppButtonStyleRounded(view: btnContinueOl)
        
        txtFirstName.layer.borderColor = UIColor.clear.cgColor
        txtLastName.layer.borderColor = UIColor.clear.cgColor
        txtEmailId.layer.borderColor = UIColor.clear.cgColor
        txtPassword.layer.borderColor = UIColor.clear.cgColor
        txtPhoneNo.layer.borderColor = UIColor.clear.cgColor
        txtGender.layer.borderColor = UIColor.clear.cgColor
        txtStreet.layer.borderColor = UIColor.clear.cgColor
        txtCity.layer.borderColor = UIColor.clear.cgColor
        txtCountry.layer.borderColor = UIColor.clear.cgColor
        txtPincode.layer.borderColor = UIColor.clear.cgColor
        //
        txtDob.layer.borderColor = UIColor.clear.cgColor
        txtCountryCode.layer.borderColor = UIColor.clear.cgColor

        //SetPlaceHolderText
        txtFirstName.placeholder = NSLocalizedString("first_name", comment: "")
        txtLastName.placeholder = NSLocalizedString("last_name", comment: "")
        txtEmailId.placeholder = NSLocalizedString("email", comment: "")
        txtPassword.placeholder = NSLocalizedString("password", comment: "")
        txtGender.placeholder = NSLocalizedString("gender", comment: "")
        txtPhoneNo.placeholder = NSLocalizedString("phone_number", comment: "")
        txtStreet.placeholder = NSLocalizedString("street", comment: "")
        txtCity.placeholder = NSLocalizedString("city", comment: "")
        txtCountry.placeholder = NSLocalizedString("country", comment: "")
        txtPincode.placeholder = NSLocalizedString("zipcode", comment: "")
        btnContinueOl.setTitle(NSLocalizedString("continue_str", comment: ""), for: .normal)
        txtDob.placeholder = NSLocalizedString("dob", comment: "")
        txtCountryCode.placeholder = NSLocalizedString("country_code", comment: "")
        AppLog.debug(tag: self.TAG, msg: "SignUp-Init")
        
        //1
        let normalFont = UIFont(name: "Nunito-Regular", size: 15)!
        let boldFont = UIFont(name: "Nunito-Regular", size: 15)!
        let string = "By creating account you agree to our Terms & Conditions." as NSString
        let attributedString = NSMutableAttributedString(string: string as String)
        
        // 2
        let firstAttributes = [NSForegroundColorAttributeName: Utility.hexStringToUIColor(hex: COLOR_PRIMARY_LIGHT, alpha: 1.0 ), NSFontAttributeName: normalFont]
        let secondAttributes = [NSForegroundColorAttributeName: Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0), NSFontAttributeName: boldFont]
        
        // 3
        attributedString.addAttributes(firstAttributes, range: string.range(of: "By creating account you agree to our"))
        attributedString.addAttributes(secondAttributes, range: string.range(of: " Terms & Conditions."))
        
        //4
        btnTermsAndCondLabelOl.setAttributedTitle(attributedString, for: .normal)
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //Dropdown Function
    //Gender
    func selectGender(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: NSLocalizedString("selectGender", comment: ""), preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = DataManager.genderArray.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(DataManager.genderArray[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtGender.text = DataManager.genderArray[index]
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //CountryCode
    func selectCountryCode(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: NSLocalizedString("Select Country Code", comment: ""), preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let count = DataManager.countryCode.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(DataManager.countryCode[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtCountryCode.text = DataManager.countryCode[index]
                    self.txtCountry.text = DataManager.countryName[index]
                    self.hideProgress()
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in
            self.hideProgress()
        }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //Country
    func selectCountryName(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Select Country Name", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = DataManager.countryName.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(DataManager.countryName[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtCountry.text = DataManager.countryName[index]
                    self.txtCountryCode.text = DataManager.countryCode[index]
                    self.hideProgress()
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in
            self.hideProgress()
        }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }

    
    //MARK: API Calling Methods
    func callSignUpAPI () {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var signUpRequest = ServerRequest.getPostRequest(url: "\(mainURL)users/add")
            
            let signUpUser = SignUpUser()
            signUpUser?.setFirstName(firstName: (txtFirstName.text)!)
            signUpUser?.setLastName(lastName: (txtLastName.text)!)
            signUpUser?.setEmail(email: txtEmailId.text!)
            signUpUser?.setPassword(password: txtPassword.text!)
//            signUpUser?.setContactNumber(contactNumber: txtPhoneNo.text!)
//            signUpUser?.setSex(sex: txtGender.text!)
//            signUpUser?.setDob(dob: txtDob.text!)
//            signUpUser?.setAddress(address: txtStreet.text!)
//            signUpUser?.setZipCode(zipCode: txtPincode.text!)
//            signUpUser?.setCity(city: txtCity.text!)
//            signUpUser?.setCountryCode(countryCode: txtCountryCode.text!)
            
            let serverParam = ServerRequestParam()
            serverParam.setUser(user: signUpUser!)
            serverParam.setType(type: "password")
            
            let paramObject = serverParam.toSignupDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            signUpRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(signUpRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(paramObject)")
            
            Alamofire.request(signUpRequest).responseJSON { response in
                    switch response.result {
                    case .success:
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "Status Code = \(statusCode)")
                        let signUpResponse = SignUpResponse(dictionary: response.result.value as! NSDictionary)
                        if (statusCode == 200) {
                            if let value = response.result.value {
                                AppLog.debug(tag: self.TAG, msg: "Response = \(value)")
                                self.hideProgress()
                                self.setSignupData(data: value as! NSDictionary)
                            }else{
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.hideProgress()
                                 self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (signUpResponse?.getMessage())!, statusCode: statusCode))
                            }
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (signUpResponse?.getMessage())!, statusCode: statusCode))
                        }
                    case .failure(let encodingError):
                        AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                        self.hideProgress()
                        self.showAlert(msg: serverDown)
                    }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setSignupData(data: NSDictionary) {
        hideProgress()
        
        let signUpResponse = SignUpResponse(dictionary: data)
        
        if signUpResponse?.getMessage() == "User created successfully."{
            let Fullname = (signUpResponse?.getData().getUser().getFirstName())! + " " + (signUpResponse?.getData().getUser().getLastName())!
            Utility.setUserLogin(isLogin: true)
            Utility.setUserName(username: Fullname)
            Utility.setUserEmail(EmailId: signUpResponse?.getData().getUser().getEmail())
            Utility.setUserImageUrl(ImageUrl: signUpResponse?.getData().getUser().getImageUrl())
            Utility.setXAccessToken(token: signUpResponse?.getData().getAuthToken())
            Utility.setMyUserId(myuserId: signUpResponse?.getData().getUser().getId())
            
            //StoreProfileData
            Utility.setUserGender(gender: signUpResponse?.getData().getUser().getGender())
            Utility.setUserDob(dob: signUpResponse?.getData().getUser().getDob())
            Utility.setUserCity(city: signUpResponse?.getData().getUser().getCity())
            Utility.setUserState(state: signUpResponse?.getData().getUser().getState())
            
            AppLog.debug(tag: self.TAG, msg: "Id = \(String(describing: signUpResponse?.getData().getUser().getId()))")
            self.mSelectedId = (signUpResponse?.getData().getUser().getId())!
            print("SelectedID = \(self.mSelectedId)")
            if (self.mSelectedImage == nil){
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.moveHome(screenName: "gotoHome")
            } else {
                self.UploadImageApi()
            }
        }else {
            AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: "\(signUpResponse!.getMessage())", action: { (CancelButton) -> Void in
            })
        }
    }
    //
    func UploadImageApi() {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            
            let headers: HTTPHeaders = ["X-Access-Token": Utility.getXAccessToken()]
            let uploadImageRequest = try! URLRequest(url: "\(mainURL)upload", method: .post, headers: headers)
            AppLog.debug(tag: self.TAG, msg: "URL = \(uploadImageRequest)")
            AppLog.debug(tag: self.TAG, msg: "headers = \(headers)")
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("user".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "model")
                AppLog.debug(tag: self.TAG, msg: "SelectedId = \(self.mSelectedId)")
                multipartFormData.append("\(self.mSelectedId)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "entityId")
                multipartFormData.append(UIImagePNGRepresentation(self.mSelectedImage!)!, withName: "media", fileName: "picture.png", mimeType: "image/png")
            }, with: uploadImageRequest, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint("SUCCESS RESPONSE: \(response)")
                        let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                        AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                        
                        if (statusCode == 200) {
                            if response.result.value != nil {
                                self.hideProgress()
                                //Utility.showTost(strMsg: (serverResponse?.getMessage())!, view: self.view)
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.moveHome(screenName: "gotoHome")
                            }else{
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.hideProgress()
                                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.moveHome(screenName: "gotoHome")
                            }
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.moveHome(screenName: "gotoHome")
                        }
                        
                    }
                case .failure(let encodingError):
                    self.hideProgress()
                    AppLog.debug(tag: self.TAG, msg: "ERROR RESPONSE: \(encodingError)")
                    self.showAlert(msg: serverDown)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.moveHome(screenName: "gotoHome")
                }
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    //
}
