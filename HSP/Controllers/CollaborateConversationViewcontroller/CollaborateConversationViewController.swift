//
//  CollaborateConversationViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 27/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class CollaborateConversationViewController: UIViewController, BaseView, UITextViewDelegate {
    
    @IBOutlet weak var tblCollaborateConversation: UITableView!
    @IBOutlet weak var txtSendText: UITextView!
    @IBOutlet weak var btnSendOl: UIButton!
    @IBOutlet weak var BorderView: UIView!
    @IBOutlet weak var txtSendConstraint: NSLayoutConstraint!
    
    //MARK:- Variable Declaration
    private var TAG : String = "CollaborateConversationViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mCollaborateConversationAdapter: CollaborateConversationAdapter!
    public var mChatListingData = ChatListingData()
    var comeFromCSContact = false
    var userNameHeader: String!
    private var currentUserChatId: String!
    var delegate: writeValueBackDelegate?
    var chageFlage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //Single Tap
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap(_:)))
        singleTap.numberOfTapsRequired = 1
        self.tblCollaborateConversation.addGestureRecognizer(singleTap)
        self.tblCollaborateConversation.separatorStyle = .none
        
        /** InitialiseView **/
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.writeValueBack(sChatListingData:mChatListingData , updateFlage: chageFlage);
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    func handleSingleTap(_ sender: UITapGestureRecognizer) {
        txtSendText.endEditing(true)
    }
    
    
    //Placeholder Text
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type a message.." {
            textView.text = ""
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Type a message.."
        }
        textView.resignFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (self.txtSendText.contentSize.height < self.txtSendConstraint.constant) {
            self.txtSendText.isScrollEnabled = false
            print("false")
        } else {
            self.txtSendText.isScrollEnabled = true
            print("true")
        }
        return true
    }
    
    @IBAction func btnEmojiOnClick(_ sender: Any) {
    }
    
    
    @IBAction func btnSendMessageOnClick(_ sender: Any) {
        
        if txtSendText.text == "Type a message.." {
            AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle:"Enter mesaage...", action: { (CancelButton) -> Void in
            })
        }else {
            if !validate(textView: txtSendText){
                txtSendText.endEditing(true)
                AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle:"Enter mesaage...", action: { (CancelButton) -> Void in
                })
            }else {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let sMessage = Message()
                sMessage.setMessageBody(messageBody: txtSendText.text)
                sMessage.setChatId(chatId: mChatListingData.getUserchats()[0].getChatId())
                appDelegate.appdelSocket.createMessage(sMessage, ack: { (data) in
                    AppLog.debug(tag: "chamipion", msg: "MYRESPO: \(data)")
                    let json  = data[0] as! Dictionary<String, AnyObject>
                    let sBody = json["body"] as! Dictionary<String, AnyObject>
                    let sData = sBody["data"] as! Dictionary<String, AnyObject>
                    let sMessage = sData["Message"] as! Dictionary<String, AnyObject>
                    
                    
                    let teamp = ChatListingMessages(dictionary: sMessage as NSDictionary)
                    
                    var array = self.mChatListingData.getMessages()
                    array.append(teamp!)
                    self.mChatListingData.setMessages(messages: array)
                    
                    self.tblCollaborateConversation.beginUpdates()
                    let rowIndex = self.mChatListingData.getMessages().count - 1
                    self.tblCollaborateConversation.insertRows(at: [IndexPath(row: rowIndex, section: 0)], with: .automatic)
                    self.tblCollaborateConversation.endUpdates()
                    self.tblCollaborateConversation.scrollToBottom(animated: true)
                    self.txtSendText.resignFirstResponder()
                    self.txtSendText.text = " "
                    self.chageFlage = true
                })
            }
        }
    }
    
    func validate(textView: UITextView) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
                // this will be reached if the text is nil (unlikely)
                // or if the text only contains white spaces
                // or no text at all
                return false
        }
        
        return true
    }
    
    /** BaseView Methods **/
    func initialise() {
        self.navigationController?.navigationBar.topItem?.title = " "
        BorderView.layer.borderColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0).cgColor
        BorderView.layer.borderWidth = 1
        
        UIStyle.setAppButtonStyleRounded(view: btnSendOl)
        
        txtSendText.delegate = self
        txtSendText.textColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_REGULAR, alpha: 1.0)
        txtSendText.text = NSLocalizedString("message_placeholder", comment: "")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** Set Tableview Data **/
        setData()
        setNavigationBar()
    }
    
    func setNavigationBar()  {
        
        /*** Custom Back Button ***/
        self.navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .plain, target: self, action: #selector(self.moveToPreviousScreen))
        self.navigationItem.leftBarButtonItem = backButton
        
        /*** Custom TitleView ***/
        screenWidth = screenSize.width
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth - 50,height: 34))
        
        let imageView = UIImageView()
        
        let iconName = "http://www.thatpetplace.com/c.1043140/site/img/photo_na.jpg"
        imageView.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        imageView.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        imageView.layer.cornerRadius = imageView.frame.size.height/2
        imageView.layer.masksToBounds = true
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.displayToolTipDetails))
        customView.isUserInteractionEnabled = true
        customView.addGestureRecognizer(tapRecognizer)
        
        let longTitleLabel = UILabel(frame: CGRect(x: 40, y: 0, width: 100, height: 16))
        if comeFromCSContact != true {
            userNameHeader = mChatListingData.getName()
        }
        longTitleLabel.text = userNameHeader
        longTitleLabel.textColor = UIColor.white
        longTitleLabel.sizeToFit()
        longTitleLabel.font = longTitleLabel.font.withSize(16)
        customView.addSubview(longTitleLabel)
        
        let titleUserStatus = UILabel(frame: CGRect(x: 40, y: 16, width: 100, height: 14))
        titleUserStatus.text =  "online"
        titleUserStatus.textColor = UIColor.white
        titleUserStatus.sizeToFit()
        titleUserStatus.font = titleUserStatus.font.withSize(12)
        customView.addSubview(titleUserStatus)
        
        customView.addSubview(imageView)
        self.navigationItem.titleView = customView
    }
    
    
    func displayToolTipDetails(_ sender : UITapGestureRecognizer) {
        if mChatListingData.getType() == "group" {
            let sCollaborateGroupProfileViewController = self.storyboard!.instantiateViewController(withIdentifier: "CollaborateGroupProfileViewController") as! CollaborateGroupProfileViewController
            sCollaborateGroupProfileViewController.chatId = self.mChatListingData.getMessages()[0].getChatId()
            self.navigationController!.pushViewController(sCollaborateGroupProfileViewController, animated: true)
        }else {
            self.showProgress()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.appdelSocket.getGroupInfo(chatId: self.mChatListingData.getMessages()[0].getChatId()) { (data, statusCode) in
                let groupInfo = data!
                for i in 0..<groupInfo.getData().getUserGroupInformation().count{
                    if Utility.getMyUserId() != groupInfo.getData().getUserGroupInformation()[i].getId(){
                        let userProfileViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
                        userProfileViewController.flage = false
                        userProfileViewController.sUserGroupInformation = groupInfo.getData().getUserGroupInformation()[i]
                        self.navigationController!.pushViewController(userProfileViewController, animated: true)
                        
                        break
                    }
                }
            }
        }
    }
    
    func setData() {
        //mList = MessageData().setData()
        //mChatListingData =
        print("MAINCOUNT= \(mChatListingData)")
        mCollaborateConversationAdapter = CollaborateConversationAdapter(viewController: self, data: mChatListingData)
        self.tblCollaborateConversation.delegate = self.mCollaborateConversationAdapter
        self.tblCollaborateConversation.dataSource = self.mCollaborateConversationAdapter
        self.tblCollaborateConversation.separatorStyle = .none
        self.tblCollaborateConversation.tableFooterView = UIView()
        self.tblCollaborateConversation.setNeedsLayout()
        self.tblCollaborateConversation.layoutIfNeeded()
        tableViewScrollToBottom(animated: true)
    }
    
    
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //OnClickEvents
    func moveToPreviousScreen(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.tblCollaborateConversation.numberOfSections
            let numberOfRows = self.tblCollaborateConversation.numberOfRows(inSection: numberOfSections-1)
            
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.tblCollaborateConversation.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
        }
    }
}

extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        self.scrollToRow(at: IndexPath(row: rows - 1, section: sections - 1), at: .bottom, animated: true)
    }
}
