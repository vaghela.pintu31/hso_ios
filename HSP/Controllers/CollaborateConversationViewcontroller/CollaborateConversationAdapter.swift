//
//  CollaborateConversationAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 27/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateConversationAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    private var mCollaborateConversationVC: CollaborateConversationViewController!
    //private var mList = [MessageData]()
    private var mChatListingData = ChatListingData()
    
    
    private var scrollFlage: Bool = false
    
    // MARK: - Constructor
    init (viewController: CollaborateConversationViewController,data : ChatListingData) {
        mCollaborateConversationVC = viewController
        mChatListingData = data
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mChatListingData.getMessages().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            if Utility.getMyUserId() == mChatListingData.getMessages()[indexPath.row].getUserId(){
                let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateConversationWithHeaderTableViewCell", for: indexPath) as! CollaborateConversationTableViewCell
                
                cell.flageHeader = true
                UIStyle.setAppButtonStyleRounded(view: cell.imgSendUser)
                if mChatListingData.getMessages()[indexPath.row].getTextHight() == 0{
                    let str = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                    mChatListingData.getMessages()[indexPath.row].setTextHight(textHight: str.widthOfString(usingFont: cell.lblMessageSend.font))
                }
                cell.lblHeader.text = self.compareDate(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                cell.lblWidth = mChatListingData.getMessages()[indexPath.row].getTextHight()
                cell.lblSendTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                cell.lblMessageSend.text = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                cell.lblSendTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                
                //let iconName = mList[mCollaborateConversationVC.mSelectedRow!].getName()
                //cell1.imgSendUser.image = UIImage(named: iconName)
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateConversationWithHeaderResiverTableViewCell", for: indexPath) as! CollaborateConversationResiverTableViewCell
                UIStyle.setAppButtonStyleRounded(view: cell.imgReceiveUser)
                if mChatListingData.getMessages()[indexPath.row].getTextHight() == 0{
                    let str = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                    mChatListingData.getMessages()[indexPath.row].setTextHight(textHight: str.widthOfString(usingFont: cell.lblMessageReceive.font))
                }
                cell.flageHeader = true
                
                cell.lblWidth = mChatListingData.getMessages()[indexPath.row].getTextHight()
                cell.lblMessageReceive.text = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                cell.lblReceiveTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                
                //let iconName = mList[mCollaborateConversationVC.mSelectedRow!].getImageReceiveUrl()
                //cell2.imgReceiveUser.image = UIImage(named: iconName)
                cell.selectionStyle = .none
                return cell
            }
        }else {
            if Utility.getMyUserId() == mChatListingData.getMessages()[indexPath.row].getUserId(){
                
                let flag = self.compareToDate(firstdateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt(), secondDateString: mChatListingData.getMessages()[indexPath.row - 1].getCreatedAt())
                
                if flag{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateConversationWithHeaderTableViewCell", for: indexPath) as! CollaborateConversationTableViewCell
                    
                    cell.flageHeader = true
                    cell.lblHeader.text = self.compareDate(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                    UIStyle.setAppButtonStyleRounded(view: cell.imgSendUser)
                    if mChatListingData.getMessages()[indexPath.row].getTextHight() == 0{
                        let str = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                        mChatListingData.getMessages()[indexPath.row].setTextHight(textHight: str.widthOfString(usingFont: cell.lblMessageSend.font))
                    }
                    
                    cell.lblWidth = mChatListingData.getMessages()[indexPath.row].getTextHight()
                    cell.lblSendTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                    cell.lblMessageSend.text = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                    cell.lblSendTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                    
                    //let iconName = mList[mCollaborateConversationVC.mSelectedRow!].getName()
                    //cell1.imgSendUser.image = UIImage(named: iconName)
                    cell.selectionStyle = .none
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateConversationTableViewCell", for: indexPath) as! CollaborateConversationTableViewCell
                    UIStyle.setAppButtonStyleRounded(view: cell.imgSendUser)
                    cell.flageHeader = false
                    
                    if mChatListingData.getMessages()[indexPath.row].getTextHight() == 0{
                        let str = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                        mChatListingData.getMessages()[indexPath.row].setTextHight(textHight: str.widthOfString(usingFont: cell.lblMessageSend.font))
                    }
                    
                    cell.lblWidth = mChatListingData.getMessages()[indexPath.row].getTextHight()
                    cell.lblSendTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                    cell.lblMessageSend.text = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                    cell.lblSendTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                    
                    //let iconName = mList[mCollaborateConversationVC.mSelectedRow!].getName()
                    //cell1.imgSendUser.image = UIImage(named: iconName)
                    cell.selectionStyle = .none
                    return cell
                }
                
                
            } else {
                
                
                let flag = self.compareToDate(firstdateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt(), secondDateString: mChatListingData.getMessages()[indexPath.row - 1].getCreatedAt())
                
                if flag{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateConversationWithHeaderResiverTableViewCell", for: indexPath) as! CollaborateConversationResiverTableViewCell
                    UIStyle.setAppButtonStyleRounded(view: cell.imgReceiveUser)
                    if mChatListingData.getMessages()[indexPath.row].getTextHight() == 0{
                        let str = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                        mChatListingData.getMessages()[indexPath.row].setTextHight(textHight: str.widthOfString(usingFont: cell.lblMessageReceive.font))
                    }
                    cell.flageHeader = true
                    
                    cell.lblWidth = mChatListingData.getMessages()[indexPath.row].getTextHight()
                    cell.lblMessageReceive.text = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                    cell.lblReceiveTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                    
                    //let iconName = mList[mCollaborateConversationVC.mSelectedRow!].getImageReceiveUrl()
                    //cell2.imgReceiveUser.image = UIImage(named: iconName)
                    cell.selectionStyle = .none
                    return cell
                    
                }else {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateConversationResiverTableViewCell", for: indexPath) as! CollaborateConversationResiverTableViewCell
                    UIStyle.setAppButtonStyleRounded(view: cell.imgReceiveUser)
                    if mChatListingData.getMessages()[indexPath.row].getTextHight() == 0{
                        let str = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                        mChatListingData.getMessages()[indexPath.row].setTextHight(textHight: str.widthOfString(usingFont: cell.lblMessageReceive.font))
                    }
                    cell.flageHeader = false
                    cell.lblWidth = mChatListingData.getMessages()[indexPath.row].getTextHight()
                    cell.lblMessageReceive.text = mChatListingData.getMessages()[indexPath.row].getMessageBody()
                    cell.lblReceiveTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData.getMessages()[indexPath.row].getCreatedAt())
                    
                    //let iconName = mList[mCollaborateConversationVC.mSelectedRow!].getImageReceiveUrl()
                    //cell2.imgReceiveUser.image = UIImage(named: iconName)
                    cell.selectionStyle = .none
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //// Get Cell Label
        //let indexPath = tableView.indexPathForSelectedRow
        //let currentCell = tableView.cellForRow(at: indexPath!) as! BudgetLandingTVC
    }
    
    func compareDate(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        //print(dateString)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let returnDate = dateFormatter.date(from: dateString)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatterString.string(from: returnDate!)
        let currentDateString = dateFormatterString.string(from: Date())
        
        let yesterdayDate = Calendar.current.date(byAdding: .day, value: -1, to:  Date())
        let yesterdayString = dateFormatterString.string(from: yesterdayDate!)
        
        if dateString == currentDateString {
            return "TODAY"
        }else if dateString == yesterdayString {
            return "YEATERDAY"
        }else{
            return dateString
        }
    }
    
    
    func compareToDate(firstdateString: String, secondDateString: String) -> Bool {
        let dateFormatter = DateFormatter()
        //print(dateString)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let firstDate = dateFormatter.date(from: firstdateString)
        let secondDate = dateFormatter.date(from: secondDateString)
        
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "yyyy-MM-dd"
        let firstString = dateFormatterString.string(from: firstDate!)
        let secondString = dateFormatterString.string(from: secondDate!)
        
        if firstString == secondString {
            return false
        }else {
            return true
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if !scrollFlage{
            let indexPath = IndexPath(row: 0, section: 0)
            let cellRect = mCollaborateConversationVC.tblCollaborateConversation.rectForRow(at: indexPath)
            let completelyVisible = mCollaborateConversationVC.tblCollaborateConversation.bounds.contains(cellRect)
            if completelyVisible{
                setData()
            }
        }
    }
    
    /** Set Tableview Data **/
    func setData() {
        scrollFlage = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.appdelSocket.getMessages(chatId: mCollaborateConversationVC.mChatListingData.getMessages()[0].getChatId(), limit: 20, skip: mChatListingData.getMessages().count) { (data, statusCode) in
            print("MessageResponse :- \(data!)")
            print("MessageResponse :- \(statusCode)")
//            let json  = data[0] as! Dictionary<String, AnyObject>
//            let statusCode = json["statusCode"] as! Int
//            if (statusCode == 200)  {
//                print("Date Count :- \(data!.getData().count)")
//                print("MessageResponse :- \(data!.getData()[0].getMessages().count)")
//                print("MessageResponse :- \(data!.getData()[0].getMessages()[0].getChatId())")
//                self.scrollFlage = false
//            } else {
//                Utility.showTost(strMsg: "StatusCode: \(statusCode)", view: self.mCollaborateConversationVC.view)
//            }
            

        }
        
    }

}

extension String {
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        let size = self.size(attributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        let size = self.size(attributes: fontAttributes)
        return size.height
    }
}



