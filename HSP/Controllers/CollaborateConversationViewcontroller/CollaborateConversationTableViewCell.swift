//
//  CollaborateConversationTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 27/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateConversationTableViewCell: UITableViewCell {
    
    var lblWidth: CGFloat!
    var flageHeader: Bool = false
    
    //SendCell
    @IBOutlet weak var imgSendUser: UIImageView!
    @IBOutlet weak var lblMessageSend: UILabel!
    @IBOutlet weak var lblSendTime: UILabel!
    
    @IBOutlet weak var lblHeader: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews(){
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
        self.lblMessageSend.preferredMaxLayoutWidth = self.lblMessageSend.frame.size.width
    }
    
 
    
    override func draw(_ rect: CGRect) {
        var bubbleSpace = CGRect(x: 0, y: 0, width: 0, height: 0)
        let comparWidth = self.bounds.width - 30
     //   let startX = self.bounds.width - lblWidth - 55
        let startX: CGFloat = 95

        var startY = self.bounds.origin.y + 5
        var subHight: CGFloat = 10
        if flageHeader {
            startY = startY + 20
            subHight = subHight + 20
        }
        
         bubbleSpace = CGRect(x:startX, y: startY, width: self.bounds.width - 160,height: self.bounds.height - subHight)
        
//            if lblWidth > comparWidth{
//                bubbleSpace = CGRect(x:100.0, y: startY, width: self.bounds.width - 115,height: self.bounds.height - subHight)
//            }else {
//                if lblWidth > self.bounds.width - 150{
//                    bubbleSpace = CGRect(x: startX , y: startY, width: self.bounds.width - 115, height: self.bounds.height - subHight)
//                }else {
//                    bubbleSpace = CGRect(x: startX - 30, y: startY, width: lblWidth + 20, height: self.bounds.height - subHight)
//                }
//            }
            let bubblePath = UIBezierPath(roundedRect: bubbleSpace, cornerRadius: 5.0)
            let colorStroke = Utility.hexStringToUIColor(hex: "e7f8c7", alpha: 1.0)
            colorStroke.setStroke()
            colorStroke.setFill()
            bubblePath.stroke()
            bubblePath.fill()
            let trianglePath = UIBezierPath()
            let startPoint = CGPoint(x: startX, y: self.bounds.height - 30)
            let endPoint = CGPoint(x: startX, y: self.bounds.height - 20)
            trianglePath.move(to: startPoint)
            trianglePath.addLine(to: endPoint)
            trianglePath.close()
            colorStroke.setStroke()
            colorStroke.setFill()
            trianglePath.stroke()
            trianglePath.fill()
            
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
}
