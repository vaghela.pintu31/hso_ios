//
//  StudentLandingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Charts
import Alamofire

class BudgetLandingViewController: UIViewController, BaseView {
    
    
    @IBOutlet weak var tblBudgetLanding: UITableView!
    @IBOutlet weak var budgetChatView: PieChartView!
    @IBOutlet weak var lblAmountSpend: UILabel!
    @IBOutlet weak var lblAmountRemaining: UILabel!
    
    //MARK:- Variable Declaration
    private var TAG : String = "BudgetLandingViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mBudgetLandingAdapter: BudgetLandingAdapter!
    private var sBudgetLandingResponce = BudgetLandingResponce()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("budget", comment: "")
        self.setNavigationBarItem()
        
        callBudgetsIndexAPI()
        
        //RightSide Two Button in Navigation Set
        let budget = UIImage(named: "ic_budget")!
    
        
        let budgetButton: UIBarButtonItem = UIBarButtonItem(image: budget, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.didTapBudgetButton(sender:)))
        navigationItem.rightBarButtonItems = [budgetButton]

    }
    
    
    func didTapBudgetButton(sender: AnyObject){
        let sBudgetSetupViewController = self.storyboard!.instantiateViewController(withIdentifier: "BudgetSetupViewController") as! BudgetSetupViewController
        sBudgetSetupViewController.sTotalBudget = sBudgetLandingResponce.getData().getTotalBudget()
        self.navigationController!.pushViewController(sBudgetSetupViewController, animated: true)
    }
    
    //MARK: BaseView Method
    func initialise() {      
        
        AppLog.debug(tag: self.TAG, msg: "BudgetLanding-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
    }
    
    
        
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    func setChart() {
        let arrayCategory = sBudgetLandingResponce.getData().getTotalBudget().getCategories()
        var dataPoints = [String]()
        var values = [Int]()
        
        for i in 0..<arrayCategory.count{
            if dataPoints.contains(arrayCategory[i].getName()){
                let indexOfA = dataPoints.index(of: arrayCategory[i].getName())
                var amount: Int!
                if arrayCategory[i].getRemainingAmount() > 0{
                    amount = values[indexOfA!] + (arrayCategory[i].getRemainingAmount())
                }else {
                     amount = values[indexOfA!] + (-1 * arrayCategory[i].getRemainingAmount())
                }
                
                values.append(amount)
            }else {
                var amount: Int!
                if arrayCategory[i].getRemainingAmount() > 0{
                    amount = arrayCategory[i].getRemainingAmount()
                }else {
                    amount = (-1 * arrayCategory[i].getRemainingAmount())
                }                
                
                values.append(amount)
                dataPoints.append(arrayCategory[i].getName())
            }
        }
        
        
        print("dataPoints :- \(dataPoints)")
        print("values :- \(values)")

        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry1 = PieChartDataEntry(value: Double(values[i]), label: dataPoints[i], data:  dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry1)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        budgetChatView.data = pieChartData
        budgetChatView.noDataText = "No data available"
        // user interaction
        budgetChatView.isUserInteractionEnabled = false
        
        let d = Description()
        d.text = "" //"iOSCharts.io"
        budgetChatView.chartDescription = d
        budgetChatView.centerText = "Expenses"
        budgetChatView.holeRadiusPercent = 0.5
        budgetChatView.transparentCircleColor = UIColor.clear
        
        budgetChatView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        let colors: [UIColor] = [Utility.hexStringToUIColor(hex: "#339933", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#ff9900", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#3366cc", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#dd3f11", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#990099", alpha: 1.0)]
        
        pieChartDataSet.colors = colors
        
    }
    
    
    func setTotalChatData(remainingAmount: Int, spentAmount: Int)  {
        let totalBudget = spentAmount + (-1 * remainingAmount)
        let remainingPercentage = ((-1 * remainingAmount) * 100) / totalBudget
        let spentPercentage = (spentAmount * 100) / totalBudget
        let dataPoints = ["Remaining Amount", "Spent Amount"]
        let values = [remainingPercentage,spentPercentage]
     
        var dataEntries: [ChartDataEntry] = []

        for i in 0..<dataPoints.count {
            let dataEntry1 = PieChartDataEntry(value: Double(values[i]), label: dataPoints[i], data:  dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry1)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        budgetChatView.data = pieChartData
        budgetChatView.noDataText = "No data available"
        // user interaction
        budgetChatView.isUserInteractionEnabled = false
        
        let d = Description()
        d.text = "" //"iOSCharts.io"
        budgetChatView.chartDescription = d
        budgetChatView.centerText = "Total Budget"
        budgetChatView.holeRadiusPercent = 0.5
        budgetChatView.transparentCircleColor = UIColor.clear
        
        budgetChatView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        let colors: [UIColor] = [Utility.hexStringToUIColor(hex: "#339933", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#ff9900", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#3366cc", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#dd3f11", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#990099", alpha: 1.0)]
        
        pieChartDataSet.colors = colors

    }
    
    
    //MARK: API Calling Methods
    func callBudgetsIndexAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let todoListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)budgets/index")
            AppLog.debug(tag: self.TAG, msg: "URL = \(todoListingRequest)")
            
            
            Alamofire.request(todoListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let sBudgetLandingResponce = BudgetLandingResponce(dictionary: response.result.value as! NSDictionary)
                    
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setTodoListing(data: value as! NSDictionary)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (sBudgetLandingResponce?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (sBudgetLandingResponce?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setTodoListing(data: NSDictionary) {
        hideProgress()
        sBudgetLandingResponce = BudgetLandingResponce(dictionary: data)!
        
        // Set Total Budget Chat
        // self.setTotalChatData(remainingAmount: sBudgetLandingResponce.getData().getOverallBudget().getRemainingAmount(), spentAmount: sBudgetLandingResponce.getData().getOverallBudget().getSpentAmount())
        
        //Set Spend Amount
        setChart()
        
        lblAmountSpend.text = "$" + "\(String(describing: sBudgetLandingResponce.getData().getOverallBudget().getSpentAmount()))"
        lblAmountRemaining.text = "$" + "\(String(describing: sBudgetLandingResponce.getData().getOverallBudget().getRemainingAmount()))"
        
        if sBudgetLandingResponce.getData().getStudentBudgets().count == 0 {
            UIStyle.showEmptyTableView(message: "No Data Found!", viewController: self.tblBudgetLanding)
        } else {
            mBudgetLandingAdapter = BudgetLandingAdapter(viewController: self, data: (sBudgetLandingResponce.getData().getStudentBudgets()))
            self.tblBudgetLanding.delegate = self.mBudgetLandingAdapter
            self.tblBudgetLanding.dataSource = self.mBudgetLandingAdapter
            self.tblBudgetLanding.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblBudgetLanding.tableFooterView = UIView()
            
            
        }
    }
}
