//
//  StudentLandingAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class BudgetLandingAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    private var mBudgetLandingVC: BudgetLandingViewController!
    private var mStudentBudgets  = [StudentBudgets]()
    
    // MARK: - Constructor
    init (viewController: BudgetLandingViewController,data : [StudentBudgets] ) {
        mBudgetLandingVC = viewController
        mStudentBudgets = data
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mStudentBudgets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetLandingTableViewCell", for: indexPath) as! BudgetLandingTableViewCell
        
        if mStudentBudgets[indexPath.row].getRemainingAmount() <= 0 {
            cell.lblBudgetAmount.textColor = Utility.hexStringToUIColor(hex: COLOR_ABSENT, alpha: 1.0)
        } else {
            cell.lblBudgetAmount.textColor = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)
        }

        
        cell.imgBudget.layer.cornerRadius = cell.imgBudget.frame.width/2
        cell.imgBudget.clipsToBounds = true
        let iconName = mStudentBudgets[indexPath.row].getImageUrl()
        cell.imgBudget.image = UIImage(named: iconName)
        cell.lblStudentName.textColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
        cell.lblStudentGrade.textColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_REGULAR, alpha: 1.0)
        cell.lblStudentName.text = mStudentBudgets[indexPath.row].getStudentId().getFirstName() + "  " + mStudentBudgets[indexPath.row].getStudentId().getLastName()
        cell.lblStudentGrade.text = "No Idea"
        cell.lblBudgetAmount.text = "\(mStudentBudgets[indexPath.row].getRemainingAmount())"
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Get Cell Label
        let nextViewController = mBudgetLandingVC.storyboard!.instantiateViewController(withIdentifier: "RemainingBudgetViewController") as! RemainingBudgetViewController
        nextViewController.userId = mStudentBudgets[indexPath.row].getId()
        mBudgetLandingVC.navigationController!.pushViewController(nextViewController, animated: true)
    }
}
