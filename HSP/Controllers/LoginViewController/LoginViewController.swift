//
//  ViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 15/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import Alamofire

class LoginViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate, BaseView {
    @IBOutlet weak var btnLoginOl: UIButton!
    @IBOutlet weak var googleViewOl: UIView!
    @IBOutlet weak var facebookViewOl: UIView!
    @IBOutlet weak var txtUserName: MKTextField!
    @IBOutlet weak var txtPassword: MKTextField!
    @IBOutlet weak var btnSignupOl: UIButton!
    @IBOutlet weak var btnForgotPasswordOl: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG : String = "LoginViewController"
    private var mProgressIndicator:ProgressIndicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* Initialize View */
    func initialise() {
        //Title
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationItem.title = NSLocalizedString("login", comment: "")
        AppLog.debug(tag: self.TAG, msg: "LogIn-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        UIStyle.setAppButtonStyleRounded(view: googleViewOl)
        UIStyle.setAppButtonStyleRounded(view: facebookViewOl)
        UIStyle.setAppButtonStyleRounded(view: btnLoginOl)
        
        txtUserName.layer.borderColor = UIColor.clear.cgColor
        txtPassword.layer.borderColor = UIColor.clear.cgColor
        
        //SetPlaceHolderText
        txtUserName.placeholder = NSLocalizedString("email", comment: "")
        txtPassword.placeholder = NSLocalizedString("password", comment: "")
        btnLoginOl.setTitle(NSLocalizedString("login", comment: ""), for: .normal)
        btnSignupOl.setTitle(NSLocalizedString("sign_up", comment: ""), for: .normal)
        btnForgotPasswordOl.setTitle(NSLocalizedString("forgot_password", comment: ""), for: .normal)
    }
    
    @IBAction func btnPasswordHideShowOnClick(_ sender: UIButton) {
        if sender.isSelected == true {
            sender.isSelected = false
            txtPassword.isSecureTextEntry = true
        } else {
            sender.isSelected = true
            txtPassword.isSecureTextEntry = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UITextField.connectFields(fields: [txtUserName, txtPassword])
        if textField == txtPassword {
            btnLoginOnClick(textField)
        }
        return true
    }
    
    
    @IBAction func btnSignUpOnClick(_ sender: Any) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "SocialSignUpViewController") as! SocialSignUpViewController
        self.navigationController!.pushViewController(homeViewController, animated: true)
    }
    
    
    @IBAction func btnForgotPasswordOnClick(_ sender: Any) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "SocialSignUpViewController") as! SocialSignUpViewController
        self.navigationController!.pushViewController(homeViewController, animated: true)
    }
    
    @IBAction func btnLoginOnClick(_ sender: Any) {
        if txtUserName.text?.characters.count == 0 {
            showAlert(msg: NSLocalizedString("enter_valid_email", comment: ""))
        } else {
            if txtPassword.text?.characters.count == 0 {
                showAlert(msg: NSLocalizedString("enter_valid_password", comment: ""))
            } else {
                if (txtPassword.text?.characters.count)! < 6 || (txtPassword.text?.characters.count)! >= 15 {
                    showAlert(msg: NSLocalizedString("strPasswordMinimumLength", comment: ""))
                } else {
                    if Utility.isValidEmail(urlString: txtUserName.text!) == true{
                        callLoginAPI()
                    }else {
                        showAlert(msg: NSLocalizedString("enter_valid_email", comment: ""))
                    }
                }
            }
        }
    }
    
    @IBAction func btnFBLogInOnClick(_ sender: Any) {
        if Reachability.isConnectedToNetwork() == true {
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    
                    if fbloginresult.grantedPermissions != nil {
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            let accessToken = FBSDKAccessToken.current()
                            if(accessToken != nil) //should be != nil
                            {
                                print("\(self.TAG)" + "accessToken :- \(String(describing: accessToken?.tokenString))")
                                
                                //   AppLog.error(tag: self.TAG, msg:((accessToken) as? StaticString) )
                                //callapi Func
                                self.callApi(token: (accessToken?.tokenString)!, type: 0)
                                
                            }
                            fbLoginManager.logOut()
                        }
                    }
                } else   {
                    AppLog.debug(tag: self.TAG, msg: "FB_Failure:- \(String(describing: error?.localizedDescription))")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            self.showAlert(msg: noInternet)
        }
        
    }
    
    //MARK: - GOOGLE INTEGRATION
    @IBAction func btnGoogleLogInOnClick(_ sender: Any) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().signIn()
        } else {
            self.showAlert(msg: noInternet)
        }
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,dismiss viewController: UIViewController!) {
        hideProgress()
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        //self.indicator?.start()
        if (error == nil)        {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let name = user.profile.name
            let email = user.profile.email
            var pic: NSURL!
            if user.profile.hasImage  {
                pic = user.profile.imageURL(withDimension: 100)! as NSURL
            }
            print("GoogleSignInData  = \(userId!)\n\(idToken!)\(name!)\n\(email!)\n\(pic!)")
            
            //Call GoogleAPI
            callApi(token: idToken!, type: 1)
            
            //callAPISocialUser(user.profile.name, email: user.profile.email, imageUrl: myFileString(pic))
        }else   {
            AppLog.debug(tag: self.TAG, msg: "Google_Failure:- \(String(describing: error?.localizedDescription))")
            self.hideProgress()
            self.showAlert(msg: serverDown)
        }
    }
    
    func myFileString(theFile: NSURL) -> String {
        return theFile.absoluteString!
    }
    
    func callApi (token: String,type: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let socialType: String!
            if type == 0 {
                socialType = "facebook"
            } else {
                socialType = "google"
            }
            
            var socialLoginRequest = ServerRequest.getPostRequest(url: "\(mainURL)users/add")
            let params:[String : Any] = ["token": "\(token)","type": "\(socialType!)"]
            socialLoginRequest.httpBody = try! JSONSerialization.data(withJSONObject: params)
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(socialLoginRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(params)")
            
            Alamofire.request(socialLoginRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            //setData()
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "SocialApi_Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //MARK: BaseView Method
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //MARK: API Calling Methods
    func callLoginAPI () {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var loginRequest = ServerRequest.getPostRequest(url: "\(mainURL)users/login")
            
            let signUpUser = SignUpUser()
            signUpUser?.setEmail(email: txtUserName.text!)
            signUpUser?.setPassword(password: txtPassword.text!)
            
            let serverParam = ServerRequestParam()
            serverParam.setUser(user: signUpUser!)
            serverParam.setType(type: "password")
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            loginRequest.httpBody = jsonData
            
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(loginRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(paramObject)")
            AppLog.debug(tag: self.TAG, msg: "header = \(String(describing: loginRequest.allHTTPHeaderFields))")
            
            Alamofire.request(loginRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "Status Code = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let loginResponse = SignUpResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            let Fullname = (loginResponse?.getData().getUser().getFirstName())! + " " + (loginResponse?.getData().getUser().getLastName())!
                            Utility.setUserLogin(isLogin: true)
                            Utility.setUserName(username: Fullname)
                            Utility.setUserEmail(EmailId: loginResponse?.getData().getUser().getEmail())
                            Utility.setUserImageUrl(ImageUrl: loginResponse?.getData().getUser().getImageUrl())
                            Utility.setXAccessToken(token: loginResponse?.getData().getAuthToken())
                            Utility.setMyUserId(myuserId: loginResponse?.getData().getUser().getId())
                            //StoreProfileData
                            Utility.setUserGender(gender: loginResponse?.getData().getUser().getGender())
                            Utility.setUserDob(dob: loginResponse?.getData().getUser().getDob())
                            Utility.setUserCity(city: loginResponse?.getData().getUser().getCity())
                            Utility.setUserState(state: loginResponse?.getData().getUser().getState())
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.moveHome(screenName: "gotoHome")
                            //self.setLoginData(data: value as! NSDictionary)
                        } else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (loginResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (loginResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    //
}

extension LoginViewController : DrawerLeftRightDelegate {
    func leftWillOpen() {
        //print("DrawerLeftRightDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        //print("DrawerLeftRightDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        //print("DrawerLeftRightDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        //print("DrawerLeftRightDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        //print("DrawerLeftRightDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        //print("DrawerLeftRightDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        //print("DrawerLeftRightDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        //print("DrawerLeftRightDelegate: rightDidClose")
    }
}

