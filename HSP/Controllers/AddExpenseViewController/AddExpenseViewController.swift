//
//  AddExpenseViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 23/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import FSCalendar

class AddExpenseViewController: UIViewController, BaseView, UITextFieldDelegate{

    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var txtCategory: MKTextField!
    @IBOutlet weak var txtNotes: MKTextField!
    @IBOutlet weak var txtAmount: MKTextField!
    @IBOutlet weak var btnSubmitOl: UIButton!
    
    @IBAction func btnSelectCategory(_ sender: Any) {
        closeKeyboard()
        selectCategory()
    }
    @IBAction func btnOnclickeSubmit(_ sender: Any) {
        if txtCategory.text?.characters.count == 0{
            Utility.showTost(strMsg: "Select Category!", view: self.view)
        }else{
            if txtNotes.text?.characters.count == 0{
                Utility.showTost(strMsg: "Enter Note!", view: self.view)
            }else {
                if txtAmount.text?.characters.count == 0{
                    Utility.showTost(strMsg: "Enter Amount!", view: self.view)
                }else {
                    
                    callTransactionsAddAPI()
                }
            }
        }
        
        
    }
    //MARK:- Variable Declaration
    private var TAG : String = "AddExpenseViewController"
    private var mProgressIndicator:ProgressIndicator?
    
    var addFlage: Bool = false
    var categoryId = 1
    var sTransactions = Transactions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
        
        txtCategory.delegate = self
        txtNotes.delegate = self
        txtAmount.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("add_expense", comment: "")
        
        if addFlage{
            btnSubmitOl.setTitle("Add", for: .normal)
        }else{
            btnSubmitOl.setTitle("Update", for: .normal)
            txtCategory.text = sTransactions.getBudgetCategoryId().getCategoryId().getName()
            categoryId = sTransactions.getBudgetCategoryId().getCategoryId().getId()
            txtNotes.text = sTransactions.getNote()
            txtAmount.text = String(sTransactions.getAmount())
            
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCategory{
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
            selectCategory()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtCategory.resignFirstResponder()
        txtNotes.resignFirstResponder()
        txtAmount.resignFirstResponder()
        return true
    }
    
    //MARK: BaseView Method
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "LessonPlanListing-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        //SetView
        setData()
    }
    
    func setData() {
        UIStyle.setAppButtonStyleRounded(view: btnSubmitOl)
    
        txtCategory.layer.borderColor = UIColor.clear.cgColor
        txtNotes.layer.borderColor = UIColor.clear.cgColor
        txtAmount.layer.borderColor = UIColor.clear.cgColor
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    func closeKeyboard() {
        txtAmount.resignFirstResponder()
        txtCategory.resignFirstResponder()
        txtNotes.resignFirstResponder()

    }
    
    //Country
    func selectCategory(){
        
        let array = [["Book",1],["Games",2],["Curriculum",3],["Educational",4],["Sports",5],["Uniform",6],["Stationery",7]]
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Select Country Name", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = array.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(array[index][0])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtCategory.text = array[index][0] as? String
                    self.categoryId = array[index][1] as! Int
                    self.hideProgress()
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in
            self.hideProgress()
        }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }

    
    //MARK: API Calling Methods
    func callTransactionsAddAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var urlString = ""
            if addFlage{
                urlString = "\(mainURL)transactions/add"
            }else{
                urlString = "\(mainURL)transactions/edit/1"
            }
            
            var transactionsAdd = ServerRequest.authenticatePostRequest(url: urlString)
            
            
            let sTransection = Transaction()
            sTransection.setNote(note: txtNotes.text!)
            sTransection.setAmount(amount: Int(txtAmount.text!)!)
            sTransection.setBudgetCategoryId(budgetCategoryId: categoryId)
            
            let sTransactionsRequest = TransactionsRequest()
            sTransactionsRequest.setTransaction(transaction: sTransection)
            
            
            let paramObject = sTransactionsRequest.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            transactionsAdd.httpBody = jsonData
   
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(transactionsAdd)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: transactionsAdd, success: { (data) -> Void in
                self.hideProgress()
                
                self.setData(data: data as! Data)
            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("DATA = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setData(data: Data) {
        hideProgress()
        
        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
//        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
//        let serverResponse = GetFriendListResponse(dictionary: userDictionary! as NSDictionary)
        print("sResponceString = \(sResponceString!)")
        if addFlage{
            Utility.showTost(strMsg: "Transaction Created Successfuly and Budget Updated", view: self.view)
        }else{
            Utility.showTost(strMsg: "Transaction Updated Successfuly", view: self.view)
        }

    }

    


}
