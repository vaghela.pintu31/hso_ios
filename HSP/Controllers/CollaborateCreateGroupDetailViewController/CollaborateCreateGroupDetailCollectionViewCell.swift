//
//  CollaborateCreateGroupDetailCollectionViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateCreateGroupDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgMember: UIImageView!
    @IBOutlet weak var lblMemberName: UILabel!
}
