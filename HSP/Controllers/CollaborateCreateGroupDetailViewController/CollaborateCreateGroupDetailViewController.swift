//
//  CollaborateCreateGroupDetailViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateCreateGroupDetailViewController: UIViewController, BaseView {
    
    @IBOutlet weak var imgGroupIcon: UIImageView!
    @IBOutlet weak var txtGroupName: MKTextField!
    @IBOutlet weak var lblTotalParticipants: UILabel!
    @IBOutlet weak var collectionViewCCGDetail: UICollectionView!
    
    //MARK:- Variable Declaration
    private var TAG : String = "CollaborateCreateGroupDetailViewController"
    private var mProgressIndicator:ProgressIndicator?
    public var mSelectedList = [GetFriendListData]()
    var userChatArray = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCreateGroupOnClick(_ sender: Any) {
        createGroupChat()
    }
    
    func createGroupChat() {
        let newChat = NewChat()
        newChat.setName(name: txtGroupName.text!)
        newChat.setType(type: "group")
        newChat.setUserchats(userchats: userChatArray)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.appdelSocket.add(newChat) { (data) in
            AppLog.debug(tag: self.TAG, msg: "ChatListingResponse: \(data)")
            let json  = data[0] as! Dictionary<String, AnyObject>
            let statusCode = json["statusCode"] as! Int
            if statusCode == 200 || statusCode == 201 {
                let controllers = self.navigationController?.viewControllers
                for vc in controllers! {
                    if vc is CollaborateLandingViewController {
                        _ = self.navigationController?.popToViewController(vc as! CollaborateLandingViewController, animated: true)
                    }
                }
            } else {
                Utility.showTost(strMsg: "StatusCode: \(statusCode)", view: self.view)
            }
            
            
            
        }
    }

    
    
    //MARK: BaseView Method
    func initialise() {
        /** Set Tableview Data **/
        setData()
        
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("new_group", comment: "")
        
        AppLog.debug(tag: self.TAG, msg: "CollaborateCreateGroupDetail-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
    }
    
    /** Set Tableview Data **/
    func setData() {
        self.imgGroupIcon.image = UIImage(named: "ic_camera_circle")
        self.lblTotalParticipants.text = "Participants: \(mSelectedList.count)/25"
        collectionViewCCGDetail.delegate = self
        collectionViewCCGDetail.dataSource = self
        txtGroupName.layer.borderColor = UIColor.clear.cgColor
        txtGroupName.placeholder = NSLocalizedString("type_group_name_here", comment: "")
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }

}


extension CollaborateCreateGroupDetailViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mSelectedList.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "CollaborateCreateGroupDetailCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! CollaborateCreateGroupDetailCollectionViewCell

        //RoundedImageSet
        UIStyle.setAppButtonStyleRounded(view: cell.imgMember)
        
        let iconName = mSelectedList[indexPath.row].getImageUrl()
        cell.imgMember.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        cell.imgMember.sd_setShowActivityIndicatorView(true)
        cell.imgMember.sd_setIndicatorStyle(.gray)
        let fullname = mSelectedList[indexPath.row].getFirstName() + " " + mSelectedList[indexPath.row].getLastName()
        cell.lblMemberName.text = fullname
        userChatArray.append(mSelectedList[indexPath.row].getId())
        
//        cell.imgMember.layer.cornerRadius = cell.imgMember.frame.width/2
//        cell.imgMember.clipsToBounds = true
//        
//        let iconName = mSelectedList[indexPath.row].getImageUrl()
//        cell.imgMember.image = UIImage(named: iconName)
//        cell.lblMemberName.text = mSelectedList[indexPath.row].getName()
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        let screenSize: CGRect = UIScreen.main.bounds
//        let screenWidth = screenSize.width
//        return CGSize(width: CGFloat((screenWidth / 4)), height: CGFloat(87))
//    }
}
