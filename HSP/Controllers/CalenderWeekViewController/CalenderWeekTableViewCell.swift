//
//  CalenderWeekTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 13/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CalenderWeekTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgUserPic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserSubject: UILabel!
    @IBOutlet weak var imgAttachment: UIImageView!
    @IBOutlet weak var txtGrade: UITextField!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnAbsent: UIButton!
    @IBOutlet weak var btnPresent: UIButton!
    @IBOutlet weak var BorderView: UIView!
    @IBOutlet weak var cardView: UIView!
    
    //AppointmentListingView
    @IBOutlet weak var cardAppView: UIView!
    @IBOutlet weak var imgAppIcon: UIImageView!
    @IBOutlet weak var lblAppName: UILabel!
    @IBOutlet weak var lblAppDesc: UILabel!
    @IBOutlet weak var btnAppDelete: UIButton!
    @IBOutlet weak var btnAppEdit: UIButton!
    
    private var mCalenderWeekVC: CalenderWeekViewController!

    var tappedAbsent: ((CalenderWeekTableViewCell) -> Void)?
    var tappedPresent: ((CalenderWeekTableViewCell) -> Void)?
    var tappedEditAssignment: ((CalenderWeekTableViewCell) -> Void)?
    var tappedEdit: ((CalenderWeekTableViewCell) -> Void)?
    var tappedDelete: ((CalenderWeekTableViewCell) -> Void)?
    var tappedUploadAttachment: ((CalenderWeekTableViewCell) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func btnAbsentOnClick(_ sender: Any) {
        tappedAbsent?(self)
    }
    
    @IBAction func btnPresentOnClick(_ sender: Any) {
        tappedPresent?(self)
    }
    
    @IBAction func btnEditAssignmentOnClick(_ sender: Any) {
        tappedEditAssignment?(self)
    }
    
    
    @IBAction func btnEditOnClick(_ sender: Any) {
        tappedEdit?(self)
    }
    
    @IBAction func btnDeleteOnClick(_ sender: Any) {
        tappedDelete?(self)
    }
    
    @IBAction func uploadAttachmentOnClick(_ sender: Any) {
        tappedUploadAttachment?(self)
    }
    
    
    
//    //MarkAbsent
//    func btnAbsentOnClick(sender: UIButton!) {
//        print("Id = \(sender.tag)")
//        self.mCalenderWeekVC.callMarkPresenceAPI(attType: 1)
//    }
//    
//    //MarkPresent
//    func btnPresentOnClick(sender: UIButton!) {
//        self.mCalenderWeekVC.callMarkPresenceAPI(attType: 0)
//    }
//    //DeleteAppointment
//    func btnDeleteOnClick(sender: UIButton!) {
//        AlertDialog().showAlertWithYesNoButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: "Are you sure you want to delete this item", buttonTitle: NSLocalizedString("alertCancel", comment: ""), otherButtonTitle: NSLocalizedString("alertbtnYes", comment: ""), action: {(OtherButton) -> Void in
//            print("IndexPath.row = \(sender.tag)")
//            //self.mCalenderWeekVC.callLessonPlanDeleteAPI(id: sender.tag)
//            self.mCalenderWeekVC.callAppointmentDeleteAPI(id: 22)
//        })
//    }
//    
//    //EditAppointment
//    func btnEditOnClick(sender: UIButton!) {
//        self.mCalenderWeekVC.mFabClick = false
//        self.mCalenderWeekVC.callEditAppointmentAPI(id: 22)
//    }

    
}
