//
//  CalenderWeekAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 13/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CalenderWeekAdapter: NSObject, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {
    
    //MARK:- Variable Declaration
    private var mCalenderWeekVC: CalenderWeekViewController!
    private var mCalenderListing = [CalenderListingResponse]()
    var setGrade = ""
    var imgURL = ""
    var agendaTitle = ""
    var selectedMenuItem: String?
    var arrayOfMenu = ["Edit", "Reschedule"]
    //    // MARK: - Constructor
    //    init (viewController: CalenderWeekViewController,data : NSDictionary) {
    //        mCalenderWeekVC = viewController
    //        let serverResponse = CalenderListingResponse(dictionary: data)
    //        mCalenderListing = (serverResponse?.getData())!
    //        print("Title = \(String(describing: serverResponse?.getData()[0].getTitle()))")
    //    }
    
    // MARK: - Constructor
    init (viewController: CalenderWeekViewController,data : [CalenderListingResponse]) {
        mCalenderWeekVC = viewController
        //let serverResponse = CalenderListingResponse(dictionary: data)
        mCalenderListing = data
        //print("Title = \(String(describing: serverResponse?.getData()[0].getTitle()))")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        setGrade = textField.text!
        textField.resignFirstResponder()
        callGradeStudentAPI(strGrade: setGrade, index: textField.tag)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        setGrade = textField.text!
    }
    
    func callGradeStudentAPI(strGrade: String, index: Int) {
        self.mCalenderWeekVC.callGradeStudentAPI(grade: strGrade, index: index)
    }

    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if mCalenderListing.count > 0 {
            return mCalenderListing[mCalenderWeekVC.selectDay].getData().count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getModel() != "appointment" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalenderWeekTableViewCell", for: indexPath) as! CalenderWeekTableViewCell
            
            UIStyle.setAppButtonStyle(view: cell.cardView)
            cell.BorderView.borders(for: [.top,.bottom])
            
            cell.imgUserPic.layer.cornerRadius = cell.imgUserPic.frame.width/2
            cell.imgUserPic.clipsToBounds = true
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            for i in 0..<appDelegate.getStudentList().count {
                if mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getmetaData().getStudentId() == appDelegate.getStudentList()[i].getId() {
                    imgURL = appDelegate.getStudentList()[i].getImageUrl()
                    agendaTitle = appDelegate.getStudentList()[i].getFirstName() + " " + appDelegate.getStudentList()[i].getLastName()
                    break
                }
            }
            
            cell.imgUserPic.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: noImageFound))
            cell.imgUserPic.sd_setShowActivityIndicatorView(true)
            cell.imgUserPic.sd_setIndicatorStyle(.gray)
            
            
            let iconName = mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getmetaData().getImageUrl()
            UIStyle.setAppButtonStyleRounded(view: cell.imgAttachment)
            cell.imgAttachment.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: "ic_upload"))
            cell.imgAttachment.sd_setShowActivityIndicatorView(true)
            cell.imgAttachment.sd_setIndicatorStyle(.gray)

            cell.lblUserName.text = agendaTitle
            cell.lblUserSubject.text = mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getTitle()
            
            cell.txtGrade.tag = indexPath.row
            
            if mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getmetaData().getAttendance() == "A" {
                cell.btnAbsent.isSelected = true
                cell.btnPresent.isSelected = false
            } else if mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getmetaData().getAttendance() == "P" {
                cell.btnAbsent.isSelected = false
                cell.btnPresent.isSelected = true
            } else {
                cell.btnAbsent.isSelected = false
                cell.btnPresent.isSelected = false
            }
            
            
//            if mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getmetaData().getCategory() == "test"{
//                cell.txtGrade.isHidden = false
//                cell.txtGrade.text = mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getmetaData().getScore()
//                cell.txtGrade.delegate = self
//            } else {
//                cell.txtGrade.delegate = nil
//                cell.txtGrade.isHidden = true
//            }
            
            cell.tappedAbsent = { [unowned self] (selectedCell) -> Void in
                let path = tableView.indexPathForRow(at: selectedCell.center)!
                let selectedItem = path.row
                //print("tappedAbsentIndex = \(selectedItem)")
                self.mCalenderWeekVC.callMarkPresenceAPI(attType: 1, index: selectedItem)
            }
            
            cell.tappedPresent = { [unowned self] (selectedCell) -> Void in
                let path = tableView.indexPathForRow(at: selectedCell.center)!
                let selectedItem = path.row
                //print("tappedPresentIndex = \(selectedItem)")
                self.mCalenderWeekVC.callMarkPresenceAPI(attType: 0, index: selectedItem)
            }
            
            cell.tappedUploadAttachment = { [unowned self] (selectedCell) -> Void in
                let path = tableView.indexPathForRow(at: selectedCell.center)!
                let selectedItem = path.row
                //print("tappedUploadAttachmentIndex = \(selectedItem)")
                self.mCalenderWeekVC.callUploadImage(index: selectedItem)
            }
            
            cell.tappedEditAssignment = { [unowned self] (selectedCell) -> Void in
                let path = tableView.indexPathForRow(at: selectedCell.center)!
                let selectedItem = path.row
                self.openMenu(index: selectedItem)
            }
            
            cell.selectionStyle = .none
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "appointmentView", for: indexPath) as! CalenderWeekTableViewCell
            
            UIStyle.setAppButtonStyle(view: cell.cardAppView)
            cell.BorderView.borders(for: [.top,.bottom])
            
            cell.imgAppIcon.layer.cornerRadius = cell.imgAppIcon.frame.height/2
            cell.imgAppIcon.clipsToBounds = true
            cell.imgAppIcon.image = UIImage(named: ic_appointment)
            cell.lblAppName.text = mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getTitle()
            cell.lblAppDesc.text = mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getmetaData().getDescription()
            
            cell.tappedEdit = { [unowned self] (selectedCell) -> Void in
                _ = tableView.indexPathForRow(at: selectedCell.center)!
                self.mCalenderWeekVC.mFabClick = false
                self.mCalenderWeekVC.callEditAppointmentAPI(id: self.mCalenderListing[self.mCalenderWeekVC.selectDay].getData()[indexPath.row].getEntityId())
            }
            
            cell.tappedDelete = { [unowned self] (selectedCell) -> Void in
                let path = tableView.indexPathForRow(at: selectedCell.center)!
                let selectedItem = path.row
                print("INDEXPATHPresent = \(selectedItem)")
                AlertDialog().showAlertWithYesNoButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: "Are you sure you want to delete this item?", buttonTitle: NSLocalizedString("alertCancel", comment: ""), otherButtonTitle: NSLocalizedString("alertbtnYes", comment: ""), action: {(OtherButton) -> Void in
                    self.mCalenderWeekVC.callAppointmentDeleteAPI(index: indexPath.row)
                })
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getModel() != "appointment" {
            return 133
        } else {
            return 104
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getModel() != "appointment" {
            let calenderWeekVC = mCalenderWeekVC.storyboard!.instantiateViewController(withIdentifier: "LessonPlanDetailViewController") as! LessonPlanDetailViewController
            calenderWeekVC.mEntityId = mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getEntityId()
            calenderWeekVC.navTitle = mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getTitle()
            calenderWeekVC.studentImageUrl = imgURL
            calenderWeekVC.studentFullName = agendaTitle
            self.mCalenderWeekVC.navigationController!.pushViewController(calenderWeekVC, animated: true)
        } else {
            self.mCalenderWeekVC.mFabClick = false
            self.mCalenderWeekVC.mViewAppointment = true
            print("indexDidSelect = \(index)")
            self.mCalenderWeekVC.viewAppointmentAPI(id: self.mCalenderListing[mCalenderWeekVC.selectDay].getData()[indexPath.row].getEntityId())
        }
    }
    
    //Dropdown Function
    //Select_Lessons
    func openMenu(index: Int){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Select Item", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = arrayOfMenu.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(self.arrayOfMenu[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.selectedMenuItem = self.arrayOfMenu[index]
                    if self.selectedMenuItem == "Edit" {
                        self.mCalenderWeekVC.viewAssignmentAPI(id: self.mCalenderListing[self.mCalenderWeekVC.selectDay].getData()[index].getEntityId())
                        
//                        let homeViewController = self.mCalenderWeekVC.storyboard!.instantiateViewController(withIdentifier: "CreateSingleLessonPlanViewController") as! CreateSingleLessonPlanViewController
//                        homeViewController.mFabClick = true
//                        homeViewController.edAssignmentId = 0
//                        homeViewController.edNoteId = 0
//                        self.mCalenderWeekVC.navigationController!.pushViewController(homeViewController, animated: true)
                    } else {
                        let homeViewController = self.mCalenderWeekVC.storyboard!.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                        self.mCalenderWeekVC.navigationController!.pushViewController(homeViewController, animated: true)
                    }
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.mCalenderWeekVC.present(actionSheetController, animated: true, completion: nil)
    }
}
