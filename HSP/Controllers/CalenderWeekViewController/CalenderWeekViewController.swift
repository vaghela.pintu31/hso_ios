//
//  CalenderWeekViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 13/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import FSCalendar
import Alamofire

class CalenderWeekViewController: UIViewController, BaseView,FSCalendarDataSource, FSCalendarDelegate, UIGestureRecognizerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var tblCalenderWeek: UITableView!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnFabOl: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG : String = "CalenderWeekViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mCalenderWeekAdapter: CalenderWeekAdapter!
    public var mViewAppointment = true
    public var mFabClick = true
    public var edEntityId: Int?
    
    public var mAppointmentList: AppointmentData?
    private var mList = [User]()
    private var mCalenderListing = [ViewAppointment]()
    private var mSelectedImage: UIImage?
    private var mSelectedImageId: Int?
    private var fab = Floaty()
    
    var selectDay: Int!
    var arrayCalenderListing = [CalenderListingResponse]()
    var callApiFlag = false
    var firstCall = true
    
    //dateFormatter
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    //scopeGesture
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calender, action: #selector(self.calender.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //FabButton
        layoutFAB()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        callApiFlag = false
        firstCall = true
        
        // Do any additional setup after loading the view.
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }
        
        self.calender.select(Date())
        
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "dd"
        let dateString = dateFormatterString.string(from: Date())
        self.selectDay = Int(dateString)!
        
        self.view.addGestureRecognizer(self.scopeGesture)
        self.tblCalenderWeek.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calender.scope = .month
        
        // For UITest
        self.calender.accessibilityIdentifier = "calendar"
        
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("calender", comment: "")
        self.setNavigationBarItem()
        
        initialise()
    }
    
    
    // MARK:- UIGestureRecognizerDelegate
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.tblCalenderWeek.contentOffset.y <= -self.tblCalenderWeek.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calender.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        //   let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "dd"
        let dateString = dateFormatterString.string(from: date)
        let dayIndex = Int(dateString)
        
        self.selectDay = dayIndex!
        if arrayCalenderListing.count != 0{
            if arrayCalenderListing[selectDay].getData().count == 0 {
                UIStyle.showEmptyTableView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self.tblCalenderWeek)
                self.tblCalenderWeek.reloadData()
            }else {
                self.tblCalenderWeek.tableFooterView = nil
                self.tblCalenderWeek.backgroundView = nil
                self.tblCalenderWeek.separatorStyle = .none
                self.tblCalenderWeek.reloadData()
                
            }
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        //print("\(self.dateFormatter.string(from: calendar.currentPage))")
        self.arrayCalenderListing.removeAll()
        callCalenderListingAPI()
        
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        //Convert Date To String
        
        if callApiFlag == true{
            let dateNumberDate = self.dateFormatter.date(from: self.dateFormatter.string(from: calender.currentPage))
            
            let dateFormatterString = DateFormatter()
            dateFormatterString.dateFormat = "dd"
            let dateString = dateFormatterString.string(from: date)
            let dayIndex = Int(dateString)
            
            dateFormatterString.dateFormat = "MM"
            let month = Int(dateFormatterString.string(from: date))
            
            let currentMonth = Int(dateFormatterString.string(from: dateNumberDate!))
            if month == currentMonth{
                
                let isIndexValid = arrayCalenderListing.indices.contains(dayIndex!)
                if isIndexValid{
                    return arrayCalenderListing[dayIndex!].getData().count
                }else {
                    return 0
                }
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    
    //MARK: BaseView Method
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "LessonPlanListing-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        //setUI
        UIStyle.setAppButtonStyleRounded(view: btnFabOl)
        
        /** Calender_Listing_API_Data **/
        //TODO
        callCalenderListingAPI()
    }
    
    
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    func layoutFAB() {
    
        fab.addItem("Create Appointment", icon: UIImage(named: "ic_appointment1")) { item in
            let calenderViewController = self.storyboard!.instantiateViewController(withIdentifier: "CreateAppointmentViewController") as! CreateAppointmentViewController
            calenderViewController.mFabClick = true
            self.fab.close()
            self.navigationController!.pushViewController(calenderViewController, animated: true)
        }
        fab.addItem("Create Assignment", icon: UIImage(named: "ic_lessanplan")) { item in
            let calenderViewController = self.storyboard!.instantiateViewController(withIdentifier: "CreateSingleLessonPlanViewController") as! CreateSingleLessonPlanViewController
            self.navigationController!.pushViewController(calenderViewController, animated: true)
            self.fab.close()
        }
        
        fab.sticky = true
        self.view.addSubview(fab)
    }

    
    //MARK: CALL API
    @IBAction func btnFabOnClick(_ sender: Any) {
        let calenderViewController = self.storyboard!.instantiateViewController(withIdentifier: "CreateAppointmentViewController") as! CreateAppointmentViewController
        calenderViewController.mFabClick = true
        self.navigationController!.pushViewController(calenderViewController, animated: true)
    }
    
    //EditAppointment
    func callEditAppointmentAPI(id: Int) {
        let calenderweekViewController = self.storyboard!.instantiateViewController(withIdentifier: "CreateAppointmentViewController") as! CreateAppointmentViewController
        calenderweekViewController.mFabClick = mFabClick
        calenderweekViewController.mSelectedAppId = id
        self.navigationController!.pushViewController(calenderweekViewController, animated: true)
    }
    
    //ViewAppointment
    func viewAppointmentAPI(id: Int) {
        let calenderweekViewController = self.storyboard!.instantiateViewController(withIdentifier: "CreateAppointmentViewController") as! CreateAppointmentViewController
        calenderweekViewController.mFabClick = mFabClick
        calenderweekViewController.mSelectedAppId = id
        calenderweekViewController.mViewAppointment = true
        self.navigationController!.pushViewController(calenderweekViewController, animated: true)
    }
    
    //ViewAssignment
    func viewAssignmentAPI(id: Int) {
        let calenderweekViewController = self.storyboard!.instantiateViewController(withIdentifier: "CreateSingleLessonPlanViewController") as! CreateSingleLessonPlanViewController
        calenderweekViewController.mFabClick = true
        calenderweekViewController.edEntityId = id
        self.navigationController!.pushViewController(calenderweekViewController, animated: true)
    }
    
    //CalenderListing
    func callCalenderListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            let date = calender.currentPage
            let strStartDate = Utility.convertDateToString(date: date.startOfMonth()!)
            let strEndDate = Utility.convertDateToString(date: date.endOfMonth()!)
            
            let calenderListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)calendars/index?startDate=\(strStartDate)&endDate=\(strEndDate)")
            AppLog.debug(tag: self.TAG, msg: "URL = \(calenderListingRequest)")
            
            Alamofire.request(calenderListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    //debugPrint(response)
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let studentListData = CalenderListingResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setCalenderListing(data: value as! NSDictionary)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setCalenderListing(data: NSDictionary) {
        self.hideProgress()
        let serverResponse = CalenderListingResponse(dictionary: data)
        mCalenderListing = (serverResponse?.getData())!
        
        if mCalenderListing.count == 0 {
            UIStyle.showEmptyTableView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self.tblCalenderWeek)
            mCalenderWeekAdapter = CalenderWeekAdapter(viewController: self, data: arrayCalenderListing)
            self.tblCalenderWeek.delegate = self.mCalenderWeekAdapter
            self.tblCalenderWeek.dataSource = self.mCalenderWeekAdapter
            self.tblCalenderWeek.separatorStyle = .none
            self.tblCalenderWeek.tableFooterView = UIView()
            self.tblCalenderWeek.setNeedsLayout()
            self.tblCalenderWeek.layoutIfNeeded()
            self.tblCalenderWeek.reloadData()
        } else {
            setData()
        }
    }
    
    func setData()  {
        self.hideProgress()
        /** Shorting Data using date **/
        for i in 0..<mCalenderListing.count{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let returnDate = dateFormatter.date(from: mCalenderListing[i].getStartDate())
            mCalenderListing[i].setCreatedAtDate(createdAt: returnDate!)
        }
        mCalenderListing.sort(by: {$0.getCreatedAtDate() < $1.getCreatedAtDate()})
        
        /** Add Object in Array if CalenderListingResponse **/
        let dateNumberDate = self.dateFormatter.date(from: self.dateFormatter.string(from: calender.currentPage))
        let cal = Calendar(identifier: .gregorian)
        let monthRange = cal.range(of: .day, in: .month, for: dateNumberDate!)!
        let daysInMonth = monthRange.count
        
        arrayCalenderListing.removeAll()
        for _ in 0..<daysInMonth + 1 {
            arrayCalenderListing.append(CalenderListingResponse())
        }
        
        /** Set data using CalenderListingResponse Object **/
        for i in 0..<mCalenderListing.count{
            //Convert String To Date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let returnDate = dateFormatter.date(from: mCalenderListing[i].getStartDate())
            
            let dateFormatterString = DateFormatter()
            dateFormatterString.dateFormat = "dd"
            let dateString = dateFormatterString.string(from: returnDate!)
            let dayIndex = Int(dateString)
            
            dateFormatterString.dateFormat = "MM"
            let month = Int(dateFormatterString.string(from: returnDate!))
            
            let currentMonth: Int!
            if firstCall == true{
                firstCall = false
                currentMonth = Int(dateFormatterString.string(from: Date()))
            }else{
                let dateNumberDate = self.dateFormatter.date(from: self.dateFormatter.string(from: calender.currentPage))
                currentMonth = Int(dateFormatterString.string(from: dateNumberDate!))
            }
            
            if month == currentMonth{
                //Convert Date To String
                
                let temp = arrayCalenderListing[dayIndex!]
                var array = temp.getData()
                array.append(mCalenderListing[i])
                temp.setData(data: array)
                arrayCalenderListing[dayIndex!] = temp
            }
            
        }
        
        callApiFlag = true
        self.calender.reloadData()
        /** TableView Delegate Methods **/
        mCalenderWeekAdapter = CalenderWeekAdapter(viewController: self, data: arrayCalenderListing)
        self.tblCalenderWeek.delegate = self.mCalenderWeekAdapter
        self.tblCalenderWeek.dataSource = self.mCalenderWeekAdapter
        self.tblCalenderWeek.separatorStyle = .none
        self.tblCalenderWeek.tableFooterView = UIView()
        self.tblCalenderWeek.setNeedsLayout()
        self.tblCalenderWeek.layoutIfNeeded()
    }
    
    //DeleteAppointment
    func callAppointmentDeleteAPI(index: Int) {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            
            let myUrl = "\(mainURL)appointments/deleteAppointment/\(self.arrayCalenderListing[selectDay].getData()[index].getEntityId())"
            //Dynammic
            //let myUrl = "\(mainURL)appointments/deleteAppointment/\(self.mList[id].getId())"
            
            let appointmentDeleteRequest = ServerRequest.authenticatePostRequest(url: myUrl)
            AppLog.debug(tag: self.TAG, msg: "URL = \(appointmentDeleteRequest)")
            AppLog.debug(tag: self.TAG, msg: "Header = \(String(describing: appointmentDeleteRequest.allHTTPHeaderFields))")
            
            Alamofire.request(appointmentDeleteRequest).responseJSON { response in
                //debugPrint(response)
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let deleteServerResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setAppointmentDeleteData(data: value as! NSDictionary, index: index)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    
    func setAppointmentDeleteData(data: NSDictionary, index: Int) {
        self.hideProgress()
        //AppLog.debug(tag: TAG, msg: "ResponseDeleteApi = \(data)")
        
        //let serverResponse = LessonPlanListingResponse(dictionary: data)
        //Utility.showTost(strMsg: (serverResponse?.getMessage())!, view: self.view)
        //print("id :- \(index)")
        var array = self.arrayCalenderListing[selectDay].getData()
        array.remove(at: index)
        self.arrayCalenderListing[selectDay].setData(data: array)
        
        mCalenderWeekAdapter = CalenderWeekAdapter(viewController: self, data: arrayCalenderListing)
        self.tblCalenderWeek.delegate = self.mCalenderWeekAdapter
        self.tblCalenderWeek.dataSource = self.mCalenderWeekAdapter
        self.tblCalenderWeek.reloadData()
        
    }
    
    //CALL API MARK PRESENCE
    //MARK: CALLAPIS
    func callMarkPresenceAPI (attType: Int, index: Int) {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            
            var markPresenceRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/markPresence")
            
            let attendanceType: String!
            if attType == 0 {
                attendanceType = "P"
            } else {
                attendanceType = "A"
            }
            
            
            let serverParam = ServerRequestParam()
            serverParam.setStudentAssignmentId(studentAssignmentId:self.arrayCalenderListing[selectDay].getData()[index].getEntityId())
            serverParam.setAttendance(attendance: attendanceType)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            markPresenceRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(markPresenceRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(paramObject)")
            
            Alamofire.request(markPresenceRequest).responseJSON { response in
          
                switch response.result {
                case .success:
                    //debugPrint(response)
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setMarkPresenceAPIData(data: value as! NSDictionary, type: attType, index: index)
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "SocialApi_Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //setMarkPresenceAPIData
    func setMarkPresenceAPIData(data: NSDictionary, type: Int, index: Int) {
        //let markPresenceResponse = ServerResponse(dictionary: data)
        //Utility.showTost(strMsg: (markPresenceResponse?.getMessage())!, view: self.view)
        self.hideProgress()
        let attendanceType: String!
        if type == 0 {
            attendanceType = "P"
        } else {
            attendanceType = "A"
        }
        
        var array = self.arrayCalenderListing[selectDay].getData()
        array[index].getmetaData().setAttendance(attendance: attendanceType)
        self.arrayCalenderListing[selectDay].setData(data: array)
        
        mCalenderWeekAdapter = CalenderWeekAdapter(viewController: self, data: arrayCalenderListing)
        self.tblCalenderWeek.delegate = self.mCalenderWeekAdapter
        self.tblCalenderWeek.dataSource = self.mCalenderWeekAdapter
        self.tblCalenderWeek.reloadData()
    }
    
    //CAllGradeStudentAPI
    func callGradeStudentAPI (grade: String, index: Int) {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            
            var gradeStudentRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)lessonplans/scoreStudent")
            
            let serverParam = ServerRequestParam()
            serverParam.setStudentAssignmentId(studentAssignmentId: self.arrayCalenderListing[selectDay].getData()[index].getEntityId())
            serverParam.setScore(score: grade)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            gradeStudentRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(gradeStudentRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(paramObject)")
            
            Alamofire.request(gradeStudentRequest).responseJSON { response in
                switch response.result {
                case .success:
                    debugPrint(response)
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            //Utility.showTost(strMsg: (serverResponse?.getMessage())!, view: self.view)
                            var array = self.arrayCalenderListing[self.selectDay].getData()
                            array[index].getmetaData().setScore(score: grade)
                            self.arrayCalenderListing[self.selectDay].setData(data: array)
                            self.mCalenderWeekAdapter = CalenderWeekAdapter(viewController: self, data: self.arrayCalenderListing)
                            self.tblCalenderWeek.delegate = self.mCalenderWeekAdapter
                            self.tblCalenderWeek.dataSource = self.mCalenderWeekAdapter
                            self.tblCalenderWeek.reloadData()
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "SocialApi_Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //UPLOAD ATTACHMENT ICON
    func callUploadImage(index: Int) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        mSelectedImageId = index
        present(imagePickerController,animated: true,completion: nil)
    }
    
    //MARK: UIImagePickerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo  info: [String : Any]) {
        
        mSelectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        UploadImageApi()
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
    func UploadImageApi() {
        if Reachability.isConnectedToNetwork() == true {
            //self.showProgress()
            let headers: HTTPHeaders = ["X-Access-Token": Utility.getXAccessToken()]
            let uploadImageRequest = try! URLRequest(url: "\(mainURL)upload", method: .post, headers: headers)
            AppLog.debug(tag: self.TAG, msg: "URL = \(uploadImageRequest)")
            AppLog.debug(tag: self.TAG, msg: "headers = \(headers)")
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("studentassigment".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "model")
                AppLog.debug(tag: self.TAG, msg: "SelectedId = \(self.arrayCalenderListing[self.selectDay].getData()[self.mSelectedImageId!].getEntityId())")
                multipartFormData.append("\(self.arrayCalenderListing[self.selectDay].getData()[self.mSelectedImageId!].getEntityId())".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "entityId")
                multipartFormData.append(UIImagePNGRepresentation(self.mSelectedImage!)!, withName: "media", fileName: "picture.png", mimeType: "image/png")
            }, with: uploadImageRequest, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        //debugPrint("SUCCESS RESPONSE: \(response)")
                        // let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                        //AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                        
                        if (statusCode == 200) {
                            let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                            if response.result.value != nil {
                                //self.hideProgress()
                                AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: serverResponse?.getMessage(), action: { (CancelButton) -> Void in
                                    self.hideProgress()
                                    self.tblCalenderWeek.delegate = self.mCalenderWeekAdapter
                                    self.tblCalenderWeek.dataSource = self.mCalenderWeekAdapter
                                    self.tblCalenderWeek.reloadData()
                                })
                            }else{
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                //self.hideProgress()
                                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            }
                        }else{
                            let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            //self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                        
                    }
                case .failure(let encodingError):
                    //self.hideProgress()
                    AppLog.debug(tag: self.TAG, msg: "ERROR RESPONSE: \(encodingError)")
                    self.showAlert(msg: serverDown)
                }
            })
        } else {
            showAlert(msg: noInternet)
        }
    }

    
}



extension Date {
    
    func startOfMonth() -> Date? {
        
        let calendar = Calendar.current
        let currentDateComponents = calendar.dateComponents([.year, .month], from: self)
        let startOfMonth = calendar.date(from: currentDateComponents)
        
        return startOfMonth
    }
    
    func dateByAddingMonths(_ monthsToAdd: Int) -> Date? {
        
        let calendar = Calendar.current
        var months = DateComponents()
        months.month = monthsToAdd
        
        return calendar.date(byAdding: months, to: self)
    }
    
    func endOfMonth() -> Date? {
        
        guard let plusOneMonthDate = dateByAddingMonths(1) else { return nil }
        
        let calendar = Calendar.current
        let plusOneMonthDateComponents = calendar.dateComponents([.year, .month], from: plusOneMonthDate)
        let endOfMonth = calendar.date(from: plusOneMonthDateComponents)?.addingTimeInterval(-1)
        
        return endOfMonth
    }
    
}



