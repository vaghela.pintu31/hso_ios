//
//  DrawerAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 17/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class DrawerAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    private var mDrawerViewController: DrawerViewController!
    private var mySelectedDrawerRow: Int!
    
    private var nameListArray: [String] = [String]()
    private var imageListArray: [String] = [String]()
    
    // MARK: - Constructor
    init (viewController: DrawerViewController) {
        mDrawerViewController = viewController
        
        imageListArray = ["ic_dashboard","ic_lesson_planner","ic_calendar","ic_collaborate",
                          "ic_student_enrollment","ic_budget","ic_nearby","ic_about",
                          "ic_privacy","ic_terms","ic_logout"]
       
        nameListArray = [NSLocalizedString("dashboard", comment: ""),
                         NSLocalizedString("lesson_planner", comment: ""),
                         NSLocalizedString("calender", comment: ""),
                         NSLocalizedString("collaborate", comment: ""),
                         NSLocalizedString("student_enrollment", comment: ""),
                         NSLocalizedString("budget", comment: ""),
                         NSLocalizedString("nearby_parents", comment: ""),
                         NSLocalizedString("about_us", comment: ""),
                         NSLocalizedString("privacy_policy", comment: ""),
                         NSLocalizedString("terms_amp_conditions", comment: ""),
                         NSLocalizedString("logout", comment: "")]
    }
    
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerTableViewCell", for: indexPath) as! DrawerTableViewCell
        
        var iconName: String
        iconName = imageListArray[indexPath.row]
        cell.imgDashboard.image = UIImage(named: iconName)
        cell.lblDashboardTitle.text = nameListArray[indexPath.item]
        
        if(cell.isSelected){
            cell.reload()
        }else{
            cell.backgroundColor = UIColor.clear
        }
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mySelectedDrawerRow = indexPath.row
        if tableView == mDrawerViewController.tblDrawerView {
            //changeTabColor()
            if let menu = LeftMenu(rawValue: indexPath.row) {
                mDrawerViewController.changeViewController(menu)
            }
        }
    }
    
    func changeTabColor() {
        mDrawerViewController.tblDrawerView.reloadData()
    }
}

