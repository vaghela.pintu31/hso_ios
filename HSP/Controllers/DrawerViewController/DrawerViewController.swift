//
//  FirstViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 17/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import SDWebImage

enum LeftMenu: Int {
    case main = 0
    case LessonPlanner
    case Calender
    case Collaborate
    case StudentsEnrollment
    case Budget
    case NearbyParents
    case AboutUs
    case PrivacyPolicy
    case TermsAndConditions
    case Logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class DrawerViewController: UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var tblDrawerView: UITableView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmailId: UILabel!
    
    //MARK:- Variable Declaration
    private var TAG : String = "DrawerViewController"
    private var mDrawerAdapter: DrawerAdapter!
    
    private var menus = ["Main", "Dashboard", "LessonPlanner","Calender","Collaborate",
                         "StudentsEnrollment","Budget","NearbyParents","AboutUs",
                         "PrivacyPolicy","TermsAndConditions","Logout"]
    
    var mainViewController: UIViewController!
    var lessonPlanLandingViewController: UIViewController!
    var calenderLandingViewController: UIViewController!
    var collaborateLandingViewController: UIViewController!
    var studentsEnrollmentViewController: UIViewController!
    var budgetLandingViewController: UIViewController!
    var nearbyParentsViewController: UIViewController!
    var aboutUsViewController: UIViewController!
    var privacyPolicyController: UIViewController!
    var termsAndConditionsViewController: UIViewController!
    var logoutViewController: UIViewController!
    //ClickEvent
    var userProfileViewController: UIViewController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSettingOnClick(_ sender: Any) {
         self.drawerMenuController()?.changeMainViewController(self.userProfileViewController, close: true)
    }
    
    
    func setData() {
        imgUserProfile.layer.borderColor = Utility.hexStringToUIColor(hex: COLOR_DASHBOARDSELECTED, alpha: 1.0).cgColor
        imgUserProfile.layer.borderWidth = 2
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.width/2
        imgUserProfile.clipsToBounds = true
        
        AppLog.debug(tag: self.TAG, msg: "UserName-> \(Utility.getUserName())")
        AppLog.debug(tag: self.TAG, msg: "EmailID -> \(Utility.getUserEmail())")
        AppLog.debug(tag: self.TAG, msg: "ImageURl -> \(Utility.getUserImageUrl())")
        
        //   cell.imgStudent.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound), options: [.continueInBackground,.progressiveDownload])
        imgUserProfile.sd_setImage(with: URL(string: Utility.getUserImageUrl()), placeholderImage: UIImage(named: noImageFound))
        imgUserProfile.sd_setShowActivityIndicatorView(true)
        imgUserProfile.sd_setIndicatorStyle(.white)
        lblUserName.text = Utility.getUserName()
        lblUserEmailId.text = Utility.getUserEmail()
        setMyDrawer()
    }
    
    func setMyDrawer() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //Main(Dashboard)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        self.mainViewController = UINavigationController(rootViewController: mainViewController)
        
        //LessonPlanLandingVC
        let lessonPlanLandingViewController = storyboard.instantiateViewController(withIdentifier: "LessonPlanListingViewController") as! LessonPlanListingViewController
        self.lessonPlanLandingViewController = UINavigationController(rootViewController: lessonPlanLandingViewController)
        
        //CalenderLandingVC
        let calenderLandingViewController = storyboard.instantiateViewController(withIdentifier: "CalenderWeekViewController") as! CalenderWeekViewController
        self.calenderLandingViewController = UINavigationController(rootViewController: calenderLandingViewController)
        
        //CollaborateLandingVC
        let collaborateLandingViewController = storyboard.instantiateViewController(withIdentifier: "CollaborateLandingViewController") as! CollaborateLandingViewController
        self.collaborateLandingViewController = UINavigationController(rootViewController: collaborateLandingViewController)
        
        //StudentsEnrollmentVC
        let studentsEnrollmentViewController = storyboard.instantiateViewController(withIdentifier: "StudentLandingViewController") as! StudentLandingViewController
        self.studentsEnrollmentViewController = UINavigationController(rootViewController: studentsEnrollmentViewController)
        
        //BudgetVC
        let budgetLandingViewController = storyboard.instantiateViewController(withIdentifier: "BudgetLandingViewController") as! BudgetLandingViewController
        self.budgetLandingViewController = UINavigationController(rootViewController: budgetLandingViewController)
        
        //NearbyParentsVC
        let nearbyParentsViewController = storyboard.instantiateViewController(withIdentifier: "LastViewController") as! LastViewController
        self.nearbyParentsViewController = UINavigationController(rootViewController: nearbyParentsViewController)
        
        //AboutUsVC
        let aboutUsViewController = storyboard.instantiateViewController(withIdentifier: "LastViewController") as! LastViewController
        self.aboutUsViewController = UINavigationController(rootViewController: aboutUsViewController)
        
        //PrivacyPolicyVC
        let privacyPolicyController = storyboard.instantiateViewController(withIdentifier: "LastViewController") as! LastViewController
        self.privacyPolicyController = UINavigationController(rootViewController: privacyPolicyController)
        
        //TermsAndConditionsVC
        let termsAndConditionsViewController = storyboard.instantiateViewController(withIdentifier: "LastViewController") as! LastViewController
        self.termsAndConditionsViewController = UINavigationController(rootViewController: termsAndConditionsViewController)
        
        //LogoutVC
        let logoutViewController = storyboard.instantiateViewController(withIdentifier: "SocialSignUpViewController") as! SocialSignUpViewController
        self.logoutViewController = UINavigationController(rootViewController: logoutViewController)
        
        
        //SettingOnClick
        let userProfileViewController = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        self.userProfileViewController = UINavigationController(rootViewController: userProfileViewController)
        
        mDrawerAdapter = DrawerAdapter(viewController: self)
        self.tblDrawerView.delegate = self.mDrawerAdapter
        self.tblDrawerView.dataSource = self.mDrawerAdapter
        self.tblDrawerView.separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.drawerMenuController()?.changeMainViewController(self.mainViewController, close: true)
            
        case .LessonPlanner:
            self.drawerMenuController()?.changeMainViewController(self.lessonPlanLandingViewController, close: true)
            
        case .Calender:
            self.drawerMenuController()?.changeMainViewController(self.calenderLandingViewController, close: true)
            
        case .Collaborate:
            self.drawerMenuController()?.changeMainViewController(self.collaborateLandingViewController, close: true)
            
        case .StudentsEnrollment:
            self.drawerMenuController()?.changeMainViewController(self.studentsEnrollmentViewController, close: true)
            
        case .Budget:
            self.drawerMenuController()?.changeMainViewController(self.budgetLandingViewController, close: true)
            
        case .NearbyParents:
            self.drawerMenuController()?.changeMainViewController(self.nearbyParentsViewController, close: true)
            
        case .AboutUs:
            self.drawerMenuController()?.changeMainViewController(self.aboutUsViewController, close: true)
            
        case .PrivacyPolicy:
            self.drawerMenuController()?.changeMainViewController(self.privacyPolicyController, close: true)
            
        case .TermsAndConditions:
            self.drawerMenuController()?.changeMainViewController(self.termsAndConditionsViewController, close: true)
            
        case .Logout:
            Utility.setUserLogin(isLogin: false)
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.synchronize()
            self.drawerMenuController()?.changeMainViewController(self.logoutViewController, close: true)
        }
    }
}





