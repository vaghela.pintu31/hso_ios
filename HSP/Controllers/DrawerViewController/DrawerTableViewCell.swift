//
//  DrawerTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 17/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class DrawerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgDashboard: UIImageView!
    @IBOutlet weak var lblDashboardTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        reload()
    }
    
    func reload() {
        if isSelected {
            contentView.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
            lblDashboardTitle.textColor = Utility.hexStringToUIColor(hex: COLOR_DASHBOARDSELECTED, alpha: 1.0)
            imgDashboard.image = imgDashboard.image!.withRenderingMode(.alwaysTemplate)
            imgDashboard.tintColor = Utility.hexStringToUIColor(hex: COLOR_DASHBOARDSELECTED, alpha: 1.0)
        } else {
            contentView.backgroundColor = UIColor.clear
            lblDashboardTitle.textColor = UIColor.white
            imgDashboard.image = imgDashboard.image!.withRenderingMode(.alwaysTemplate)
            imgDashboard.tintColor = UIColor.white
        }
    }
}
