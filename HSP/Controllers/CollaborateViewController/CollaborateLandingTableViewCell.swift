//
//  CollaborateLandingTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateLandingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgCollaborateLanding: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserMessage: UILabel!
    @IBOutlet weak var lblMessageTime: UILabel!
    @IBOutlet weak var BadgeView: UIView!
    @IBOutlet weak var lblBadgeCount: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
