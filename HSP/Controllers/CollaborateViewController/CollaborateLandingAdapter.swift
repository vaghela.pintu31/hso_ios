//
//  CollaborateLandingAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateLandingAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    var mCollaborateLandingVC: CollaborateLandingViewController!
    //var mList = [MessageData]()
    public var mChatListingData = [ChatListingData]()
    
    
    // MARK: - Constructor
    init (viewController: CollaborateLandingViewController, data: [ChatListingData]) {
        mCollaborateLandingVC = viewController
        mChatListingData = data
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mChatListingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollaborateLandingTableViewCell", for: indexPath) as! CollaborateLandingTableViewCell
        //SetCellView
        UIStyle.setAppButtonStyleRounded(view: cell.btnStatus)
        cell.BadgeView.layer.cornerRadius = cell.BadgeView.frame.height/2
        cell.BadgeView.clipsToBounds = true
        cell.imgCollaborateLanding.layer.cornerRadius = cell.imgCollaborateLanding.frame.width/2
        cell.imgCollaborateLanding.clipsToBounds = true
        
        
        if self.mChatListingData[indexPath.row].getType() == "group" {
            cell.lblUserName.text = mChatListingData[indexPath.row].getName()
        } else {
            if mChatListingData[indexPath.row].getName() == ""{
                if self.mChatListingData[indexPath.row].getUserchats().count != 0 {
                    for i in 0..<self.mChatListingData[indexPath.row].getUserchats().count {
                        let id = mChatListingData[indexPath.row].getUserchats()[i].getUserId()
                        if id != Utility.getMyUserId() {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            for k in 0..<appDelegate.getFriendList().count{
                                if appDelegate.getFriendList()[k].getId() == id {
                                    let fullName = appDelegate.getFriendList()[k].getFirstName() + " " + appDelegate.getFriendList()[k].getLastName()
                                    mChatListingData[indexPath.row].setName(name: fullName)
                                    mChatListingData[indexPath.row].setImageUrl(imageUrl: appDelegate.getFriendList()[k].getImageUrl())
                                    cell.lblUserName.text = mChatListingData[indexPath.row].getName()
                                    break
                                }
                            }
                        }
                    }
                }
            }else {
                cell.lblUserName.text = mChatListingData[indexPath.row].getName()
            }
            
        }
        
        //cell.lblUserName.text = mChatListingData[indexPath.row].getName()
        
        if mChatListingData[indexPath.row].getMessages().count != 0 {
            
            cell.BadgeView.isHidden = true
            if mChatListingData[indexPath.row].getUnread() != 0  {
                cell.lblBadgeCount.text = "\(mChatListingData[indexPath.row].getUnread())"
                cell.BadgeView.isHidden = false
            }
            
            let iconName = mChatListingData[indexPath.row].getImageUrl()
            cell.imgCollaborateLanding.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
            cell.imgCollaborateLanding.sd_setShowActivityIndicatorView(true)
            cell.imgCollaborateLanding.sd_setIndicatorStyle(.gray)
            
            let array = mChatListingData[indexPath.row].getMessages()
            cell.lblUserMessage.text = mChatListingData[indexPath.row].getMessages()[array.endIndex - 1].getMessageBody()
            
            cell.lblMessageTime.text = Utility.convertDBDateToStringTime(dateString: mChatListingData[indexPath.row].getMessages()[0].getCreatedAt())
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utility.getUserCity() != "" && Utility.getUserDob() != "" && Utility.getUserGender() != "" && Utility.getUserState() != "" {
            if mChatListingData[indexPath.row].getCanSendMessage() == true {
                mCollaborateLandingVC.selectedIndex = indexPath.row
                let collaborateLandingVC = mCollaborateLandingVC.storyboard!.instantiateViewController(withIdentifier: "CollaborateConversationViewController") as! CollaborateConversationViewController
                collaborateLandingVC.mChatListingData = mChatListingData[indexPath.row]
                collaborateLandingVC.delegate = mCollaborateLandingVC
                
                mCollaborateLandingVC.navigationController!.pushViewController(collaborateLandingVC, animated: true)
            } else {
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                if appDelegate.getFriendList()[indexPath.row].getAcceptorId() == mChatListingData[indexPath.row].get
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hidePopUp))
                mCollaborateLandingVC.viewInviteFriends.isUserInteractionEnabled = true
                mCollaborateLandingVC.viewInviteFriends.addGestureRecognizer(tapRecognizer)
                
                let iconName = mChatListingData[indexPath.row].getImageUrl()
                mCollaborateLandingVC.imgFriends.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
                mCollaborateLandingVC.imgFriends.sd_setShowActivityIndicatorView(true)
                mCollaborateLandingVC.imgFriends.sd_setIndicatorStyle(.gray)
                mCollaborateLandingVC.lblFriendName.text = mChatListingData[indexPath.row].getName()
                mCollaborateLandingVC.lblLastMessage.text = mChatListingData[indexPath.row].getMessages()[0].getMessageBody()
                mCollaborateLandingVC.selecteFriend = mChatListingData[indexPath.row]
                mCollaborateLandingVC.viewInviteFriends.isHidden = false
            }
        } else {
            AlertDialog().showAlertWithYesNoButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: NSLocalizedString("profile_warning", comment: ""), buttonTitle: NSLocalizedString("alertCancel", comment: ""), otherButtonTitle: NSLocalizedString("alertbtnYes", comment: ""), action: {(OtherButton) -> Void in
                let collaborateLandingVC = self.mCollaborateLandingVC.storyboard!.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
                self.mCollaborateLandingVC.navigationController!.pushViewController(collaborateLandingVC, animated: true)
            })
        }
    }
    
    func hidePopUp(){
        mCollaborateLandingVC.viewInviteFriends.isHidden = true
    }

}
