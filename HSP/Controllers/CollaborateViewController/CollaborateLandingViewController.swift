//
//  CollaborateLandingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire


protocol writeValueBackDelegate {
    func writeValueBack(sChatListingData: ChatListingData, updateFlage: Bool)
}

class CollaborateLandingViewController: UIViewController, UISearchBarDelegate,BaseView, writeValueBackDelegate {
    
    @IBOutlet weak var viewInviteFriends: UIView!
    @IBOutlet weak var imgFriends: UIImageView!
    @IBOutlet weak var lblFriendName: UILabel!
    @IBOutlet weak var lblLastMessage: UILabel!
    @IBOutlet weak var bAccept: UIButton!
    @IBOutlet weak var bDecline: UIButton!
    @IBOutlet weak var tblCollaborateLanding: UITableView!
    @IBOutlet weak var btnFab: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG =  "CollaborateLandingViewController"
    private var mProgressIndicator:ProgressIndicator?
    var mCollaborateLandingAdapter: CollaborateLandingAdapter!
    private var mList = [MessageData]()
    var fab = Floaty()
    //var mChatListingResponse = ChatListingResponse?
    private var mChatListingData = [ChatListingData]()
    var selecteFriend = ChatListingData()
    var selectedIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //SET_FAB
        layoutFAB()
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("collaborate", comment: "")
        self.setNavigationBarItem()
        //SET NAVIGATION RIGHTBAR Search
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(CollaborateLandingViewController.didTapSearchButton(sender:)))
    }
    
    func didTapSearchButton(sender: AnyObject){
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "CollaborateSearchViewContoller") as! CollaborateSearchViewContoller
        self.navigationController!.pushViewController(homeViewController, animated: true)
    }
    
    @IBAction func btnFabOnClick(_ sender: Any) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "CollaborateSelectContactViewController") as! CollaborateSelectContactViewController
        self.navigationController!.pushViewController(homeViewController, animated: true)
    }
    
    @IBAction func btnAccept(_ sender: Any) {
        callFrindRequestAPI(strStatus: "A")
    }
    
    @IBAction func btnDecline(_ sender: Any) {
        callFrindRequestAPI(strStatus: "R")
    }
    
    //MARK: BaseView Method
    func initialise() {
        self.btnFab.layer.cornerRadius = self.btnFab.frame.width/2
        self.btnFab.clipsToBounds = true
        
        AppLog.debug(tag: self.TAG, msg: "CollaborateLandingVC-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        self.tblCollaborateLanding.tableFooterView = UIView()
        showProgress()
        /** Set Tableview Data **/
        //ReadChatJSON()
        setData()
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    
    func writeValueBack(sChatListingData: ChatListingData, updateFlage: Bool) {
        if updateFlage{
            self.mChatListingData.remove(at: selectedIndex)
            self.mChatListingData.insert(sChatListingData, at: 0)
            self.mCollaborateLandingAdapter = CollaborateLandingAdapter(viewController: self, data: self.mChatListingData)
            self.tblCollaborateLanding.delegate = self.mCollaborateLandingAdapter
            self.tblCollaborateLanding.dataSource = self.mCollaborateLandingAdapter
            self.tblCollaborateLanding.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblCollaborateLanding.tableFooterView = UIView()
            
        }
    }
    
    
    //    func ReadChatJSON(){
    //        if let path = Bundle.main.url(forResource: "ChatIndex", withExtension: "json") {
    //            do {
    //                let jsonData = try Data(contentsOf: path, options: .mappedIfSafe)
    //                do {
    //                    if let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? NSDictionary {
    //                        mChatListingResponse = ChatListingResponse(dictionary: jsonResult as NSDictionary)!
    //                    }
    //                } catch let error as NSError {
    //                    print("Error: \(error)")
    //                }
    //            } catch let error as NSError {
    //                print("Error: \(error)")
    //            }
    //        }
    //    }
    
    func layoutFAB() {
        fab.addItem("Invite Friends", icon: UIImage(named: "ic_invite")) { item in
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "LastViewController") as! LastViewController
            self.navigationController!.pushViewController(homeViewController, animated: true)
            self.fab.close()
        }
        fab.addItem("Find Nearby Homeschoolers", icon: UIImage(named: "ic_nearby1")) { item in
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "LastViewController") as! LastViewController
            self.navigationController!.pushViewController(homeViewController, animated: true)
            self.fab.close()
        }
        fab.addItem("Add New Contact", icon: UIImage(named: "ic_addnewcontact")) { item in
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "CollaborateSearchViewContoller") as! CollaborateSearchViewContoller
            self.navigationController!.pushViewController(homeViewController, animated: true)
            self.fab.close()
        }
        fab.addItem("Send Message", icon: UIImage(named: "ic_sendmessage")) { item in
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "CollaborateSelectContactViewController") as! CollaborateSelectContactViewController
            self.navigationController!.pushViewController(homeViewController, animated: true)
            self.fab.close()
        }
        
        fab.sticky = true
        self.view.addSubview(fab)
    }
    
    
    /** Set Tableview Data **/
    func setData() {
        //let temp = mChatListingResponse.getData()
        //mCollaborateLandingAdapter = CollaborateLandingAdapter(viewController: self, data: temp)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.appdelSocket.getChats { (data, statusCode) in
            AppLog.debug(tag: self.TAG, msg: "CollaborateLandingResponse: \(data!.getData())")
            AppLog.debug(tag: self.TAG, msg: "CollaborateLandingStatusCode: \(statusCode)")
            AppLog.debug(tag: self.TAG, msg: "CollaborateLandingMessage = \(data!.getMessage())")
            self.mChatListingData = data!.getData()
            
            if (statusCode == 200) {
                self.hideProgress()
                if self.mChatListingData.count == 0 {
                    UIStyle.showEmptyTableView(message: NO_DATA_FOUND, viewController: self.tblCollaborateLanding)
                } else {
                    self.mCollaborateLandingAdapter = CollaborateLandingAdapter(viewController: self, data: self.mChatListingData)
                    self.tblCollaborateLanding.delegate = self.mCollaborateLandingAdapter
                    self.tblCollaborateLanding.dataSource = self.mCollaborateLandingAdapter
                    self.tblCollaborateLanding.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
                    self.tblCollaborateLanding.tableFooterView = UIView()
                }
            }else{
                self.hideProgress()
                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (data!.getMessage()), statusCode: statusCode))
            }
        }
    }
    
    //MARK: API Calling Methods
    func callFrindRequestAPI(strStatus: String) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var assignStudentRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)friends/updateStatus")
            
            let sFrindRequestStatus = FrindRequestStatus()
            sFrindRequestStatus.setFriendId(friendId: selecteFriend.getId())
            sFrindRequestStatus.setStatus(status: strStatus)
            
            let paramObject = sFrindRequestStatus.toDictionary()
            print("paramObject = \(paramObject)")
            
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            
            assignStudentRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(assignStudentRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            Alamofire.request(assignStudentRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            self.viewInviteFriends.isHidden = true
                            let collaborateLandingVC = self.storyboard!.instantiateViewController(withIdentifier: "CollaborateConversationViewController") as! CollaborateConversationViewController
                            collaborateLandingVC.mChatListingData = self.selecteFriend
                            self.navigationController!.pushViewController(collaborateLandingVC, animated: true)
                            
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            //                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (lpAssignStudentResponse?.getMessage())!, statusCode: statusCode))
                            self.viewInviteFriends.isHidden = true
                            
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        //                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (lpAssignStudentResponse?.getMessage())!, statusCode: statusCode))
                        self.viewInviteFriends.isHidden = true
                        
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                    self.viewInviteFriends.isHidden = true
                    
                }
            }
        } else {
            showAlert(msg: noInternet)
            self.viewInviteFriends.isHidden = true
        }
    }
    
    
    
}
