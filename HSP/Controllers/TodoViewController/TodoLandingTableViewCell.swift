//
//  TodoLandingTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 13/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class TodoLandingTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var btnTodoCheckBox: UIButton!
    @IBOutlet weak var lblTodoTitle: UILabel!
    @IBOutlet weak var lblTodoDate: UILabel!
    @IBOutlet weak var btnEditTodo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //Convert String To Date
    func convertDateString(dateString: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        print(dateString)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let returnDate = dateFormatter.date(from: dateString)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "dd MMM"
        let dateString = dateFormatterString.string(from: returnDate!)
        print(dateString)
        return dateString
        
    }
}
