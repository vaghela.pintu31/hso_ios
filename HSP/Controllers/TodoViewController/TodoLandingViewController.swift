//
//  TodoLandingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 13/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class TodoLandingViewController: UIViewController, BaseView, UITextFieldDelegate {
    
    @IBOutlet weak var tblTodoLanding: UITableView!
    @IBOutlet weak var btnSendTodoText: UIButton!
    
    @IBOutlet weak var txtSendNote: UITextField!
    
    
    
    //MARK:- Variable Declaration
    var TAG : String = "TodoLandingViewController"
    private var mProgressIndicator:ProgressIndicator?
    var mTodoLandingAdapter: TodoLandingAdapter!
    var mListData: TodoListingResponce!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtSendNote.delegate = self
        initialise()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callToDoListingAPI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: BaseView Method
    func initialise() {
        UIStyle.setCustomStyle(view: btnSendTodoText, cornerRadius: 3.0, boderColor: COLOR_WHITE, borderWidth: 0)
        
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("todo", comment: "")
        //  self.setNavigationBarItem()
        
        AppLog.debug(tag: self.TAG, msg: "LessonPlanListing-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)       
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtSendNote.resignFirstResponder()
        return true
    }
    
    @IBAction func btnOnClickeSend(_ sender: Any) {
        txtSendNote.resignFirstResponder()
        if txtSendNote.text?.characters.count != 0{
            callAddToDoAPI()
        }else {
            AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: "Enter Note Message", action: { (OK) -> Void in
            })
        }
    }
    
    func tableViewDelegateMethod() {
        mTodoLandingAdapter = TodoLandingAdapter(todoList: mListData!, viewController: self)
        self.tblTodoLanding.delegate = self.mTodoLandingAdapter
        self.tblTodoLanding.dataSource = self.mTodoLandingAdapter
        self.tblTodoLanding.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
        self.tblTodoLanding.tableFooterView = UIView()
    }
    
    //    func getWidth(text: String) -> CGFloat {
    //        let txtField = UITextField(frame: .zero)
    //        txtField.text = text
    //        txtField.sizeToFit()
    //        return txtField.frame.size.width
    //    }
    //
    //
    //
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //        let width = getWidth(text : textField.text!)
    //        if width > widthConstraint.constant {
    //            widthConstraint.constant = width
    //        }
    //        self.view.layoutIfNeeded()
    //        return true
    //    }
    
    
    
    
    
        //MARK: API Calling Methods
        func callToDoListingAPI() {
            if Reachability.isConnectedToNetwork() == true {
                showProgress()
    
                let todoListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)todos/index")
                AppLog.debug(tag: self.TAG, msg: "URL = \(todoListingRequest)")
    
    
                Alamofire.request(todoListingRequest).responseJSON { response in
                    switch response.result {
                    case .success:
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                        AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                        let serverResponse = TodoListingResponce(dictionary: response.result.value as! NSDictionary)
    
                        if (statusCode == 200) {
                            if let value = response.result.value {
                                self.hideProgress()
                                self.setTodoListing(data: value as! NSDictionary)
                            } else {
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.hideProgress()
                                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            }
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    case .failure(let encodingError):
                        AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                        self.hideProgress()
                        self.showAlert(msg: serverDown)
                    }
                }
            } else {
                showAlert(msg: noInternet)
            }
        }
    
        func setTodoListing(data: NSDictionary) {
            hideProgress()
            let serverResponse = TodoListingResponce(dictionary: data)
            if serverResponse?.getData().count == 0 {
                UIStyle.showEmptyTableView(message: (serverResponse?.getMessage())!, viewController: self.tblTodoLanding)
            } else {
                mTodoLandingAdapter = TodoLandingAdapter(todoList: serverResponse!, viewController: self)
                self.tblTodoLanding.delegate = self.mTodoLandingAdapter
                self.tblTodoLanding.dataSource = self.mTodoLandingAdapter
                self.tblTodoLanding.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
                self.tblTodoLanding.tableFooterView = UIView()
            }
        }
    
    
    //MARK: API Calling Methods
    func callAddToDoAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var todoListingRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)todos/add")
            AppLog.debug(tag: self.TAG, msg: "URL = \(todoListingRequest)")
            
            let serverParam = CreateTodo()
            serverParam.getTodo().setTitle(title: txtSendNote.text!)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            todoListingRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            Alamofire.request(todoListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let serverResponse = TodoCreateReponce(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setAddTodo(data: value as! NSDictionary)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setAddTodo(data: NSDictionary) {
        hideProgress()
        let serverResponse = TodoCreateReponce(dictionary: data)
        let createTodo = TodoData()
        createTodo.setTitle(title: (serverResponse?.getData().getTitle())!)
        createTodo.setId(id: (serverResponse?.getData().getId())!)
        createTodo.setUserId(userId: (serverResponse?.getData().getUserId())!)
        createTodo.setCompleted(completed: (serverResponse?.getData().getCompleted())!)
        createTodo.setCreatedAt(createdAt: (serverResponse?.getData().getCreatedAt())!)
        createTodo.setUpdatedAt(updatedAt: (serverResponse?.getData().getUpdatedAt())!)
        
        let todoList = mTodoLandingAdapter.getTodoList()
        var array = todoList.getData()
        array.insert(createTodo, at: 0)
        todoList.setData(data: array)
        self.txtSendNote.text = ""
        tblTodoLanding.reloadData()
    }
    
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    
}
