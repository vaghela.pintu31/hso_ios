//
//  TodoLandingAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 13/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class TodoLandingAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    var mListData: TodoListingResponce!
    var todoViewController: TodoLandingViewController!
    
    
    // MARK: - Constructor
    init (todoList: TodoListingResponce, viewController: TodoLandingViewController) {
        mListData = todoList
        todoViewController = viewController
    }
    
    
    func getTodoList() -> TodoListingResponce {
        return mListData
    }
    
    func setTodoList(todoList: TodoListingResponce)  {
        self.mListData = todoList
    }
    
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mListData.getData().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoLandingTableViewCell", for: indexPath) as! TodoLandingTableViewCell
        cell.lblTodoTitle.text = mListData.getData()[indexPath.row].getTitle()
        cell.lblTodoDate.text = "\(cell.convertDateString(dateString: mListData.getData()[indexPath.row].getCreatedAt()))"
        
        cell.btnTodoCheckBox.tag = indexPath.row
        cell.btnTodoCheckBox.addTarget(self, action: #selector(self.btnCheckBoxOnClick), for: .touchUpInside)
        
        cell.btnEditTodo.tag = indexPath.row
        cell.btnEditTodo.addTarget(self, action: #selector(self.btnEditOnClick), for: .touchUpInside)
        
        cell.btnTodoCheckBox.isSelected = mListData.getData()[indexPath.row].getCompleted()
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 41
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let mTodoEditingViewController = todoViewController.storyboard!.instantiateViewController(withIdentifier: "TodoEditingViewController") as! TodoEditingViewController
//        mTodoEditingViewController.tododata = mListData.getData()[indexPath.row]
//        todoViewController.navigationController!.pushViewController(mTodoEditingViewController, animated: true)
//    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
        if editingStyle == .delete{
            callDeleteToDoAPI(index: indexPath.row)
        }
    }
    
    func btnEditOnClick(sender: UIButton) {
        let mTodoEditingViewController = todoViewController.storyboard!.instantiateViewController(withIdentifier: "TodoEditingViewController") as! TodoEditingViewController
        mTodoEditingViewController.tododata = mListData.getData()[sender.tag]
        todoViewController.navigationController!.pushViewController(mTodoEditingViewController, animated: true)
    }
    
    
    
    func btnCheckBoxOnClick(sender: UIButton) {
        if sender.isSelected == true{
            //Utility.showTost(strMsg: "Todo is already Completed.", view: todoViewController.view)
            sender.isSelected = false
        }else {
            callCompleteToDoAPI(sender: sender)
        }
    }
    
    
    
    //MARK: API Calling Methods
    func callDeleteToDoAPI(index: Int) {
        if Reachability.isConnectedToNetwork() == true {
            todoViewController.showProgress()
            
            let todoDeleteRequest = ServerRequest.authenticateDeleteRequest(url: "\(mainURL)todos/remove/\(self.mListData.getData()[index].getId())")
            AppLog.debug(tag: todoViewController.TAG, msg: "URL = \(todoDeleteRequest)")

            Alamofire.request(todoDeleteRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    let serverResponse = TodoCreateReponce(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.todoViewController.hideProgress()
                            var array = self.mListData.getData()
                            array.remove(at: index)
                            self.mListData.setData(data: array)
                            self.todoViewController.tblTodoLanding.reloadData()
                        } else {
                            self.todoViewController.hideProgress()
                            self.todoViewController.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        self.todoViewController.hideProgress()
                        self.todoViewController.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure( _):
                    self.todoViewController.hideProgress()
                    self.todoViewController.showAlert(msg: serverDown)
                }
            }
        } else {
            todoViewController.showAlert(msg: noInternet)
        }
    }
    
    
    //MARK: API Calling Methods
    func callCompleteToDoAPI(sender: UIButton) {
        if Reachability.isConnectedToNetwork() == true {
            todoViewController.showProgress()
            let todoDeleteRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)todos/markComplete/\(self.mListData.getData()[sender.tag].getId())")
            AppLog.debug(tag: self.todoViewController.TAG, msg: "URL = \(todoDeleteRequest)")

            Alamofire.request(todoDeleteRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    print("response.result.value :- \(String(describing: response.result.value))")
                    let serverResponse = TodoListingResponce(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.todoViewController.hideProgress()
                            var array = self.mListData.getData()
                            array[sender.tag] = (serverResponse?.getData()[0])!
                            self.mListData.setData(data: array)
                            sender.isSelected = true
                        } else {
                            self.todoViewController.hideProgress()
                            self.todoViewController.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        AppLog.debug(tag: self.todoViewController.TAG, msg: "URL = \(todoDeleteRequest)")

                        self.todoViewController.hideProgress()
                      //  self.todoViewController.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure( _):
                    self.todoViewController.hideProgress()
                    self.todoViewController.showAlert(msg: serverDown)
                }
            }
        } else {
            todoViewController.showAlert(msg: noInternet)
        }
    }
}
