//
//  TodoLandingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 13/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class TodoEditingViewController: UIViewController, BaseView, UITextViewDelegate {
    
    @IBOutlet weak var txtTodoNote: UITextView!

    @IBOutlet weak var tbnCompite: UIButton!
    
    
    @IBAction func btnOnclickeCompite(_ sender: Any) {
        if tododata.getCompleted(){
            Utility.showTost(strMsg: "Todo is already complited.", view: self.view)
        }else {
            callCompleteToDoAPI(sender: sender as! UIButton)
        }
    }
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBAction func btnOnClickeUpdate(_ sender: Any) {
        callEditToDoAPI()
    }
    
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBAction func btnOnClickeCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Variable Declaration
    var TAG : String = "TodoEditingViewController"
    private var mProgressIndicator:ProgressIndicator?
   // var mTodoLandingAdapter: TodoLandingAdapter!
    
    var tododata = TodoData()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtTodoNote.delegate = self 
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = "My Tasks"
        //self.setNavigationBarItem()
    }
    
    //MARK: BaseView Method
    func initialise() {
        UIStyle.setCustomStyle(view: btnCancel, cornerRadius: 3.0, boderColor: COLOR_WHITE, borderWidth: 0)
        UIStyle.setCustomStyle(view: btnUpdate, cornerRadius: 3.0, boderColor: COLOR_WHITE, borderWidth: 0)
        
        AppLog.debug(tag: self.TAG, msg: "LessonPlanListing-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        self.txtTodoNote.text = tododata.getTitle()
        self.tbnCompite.isSelected = tododata.getCompleted()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtTodoNote.resignFirstResponder()
        return true
    }
    

    

    
    
    //MARK: API Calling Methods
    func callEditToDoAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var todoListingRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)todos/edit/\(tododata.getId())")
            AppLog.debug(tag: self.TAG, msg: "URL = \(todoListingRequest)")
            
            let serverParam = CreateTodo()
            serverParam.getTodo().setTitle(title: txtTodoNote.text!)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            todoListingRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")
            
            Alamofire.request(todoListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let serverResponse = TodoListingResponce(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()
                            AlertDialog().showAlertWithYesNoButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: "Todo edit successfully.", buttonTitle: NSLocalizedString("alertCancel", comment: ""), otherButtonTitle: NSLocalizedString("alertbtnYes", comment: ""), action: {(OtherButton) -> Void in
                                self.navigationController?.popViewController(animated: true)
                            })
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
  
    
    //MARK: API Calling Methods
    func callCompleteToDoAPI(sender: UIButton) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            let todoDeleteRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)todos/markComplete/\(tododata.getId())")
            AppLog.debug(tag: self.TAG, msg: "URL = \(todoDeleteRequest)")
            
            Alamofire.request(todoDeleteRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    print("response.result.value :- \(String(describing: response.result.value))")
                    let serverResponse = TodoListingResponce(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if response.result.value != nil {
                            self.hideProgress()                            
                            sender.isSelected = true
                        } else {
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        AppLog.debug(tag: self.TAG, msg: "URL = \(todoDeleteRequest)")
                        
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure( _):
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }

    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    
}
