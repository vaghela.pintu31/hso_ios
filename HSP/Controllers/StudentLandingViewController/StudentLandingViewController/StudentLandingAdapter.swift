//
//  StudentLandingAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import SDWebImage

class StudentLandingAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    private var mStudentLandingVC: StudentLandingViewController!
    //private var mList: StudentListingResponse!
    public var mList = [StudentListingData]()
    public var mSelectedId: Int?
    
    // MARK: - Constructor
    init (viewController: StudentLandingViewController, data : Data) {
        mStudentLandingVC = viewController
        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
        let serverResponse = StudentListingResponse(dictionary: userDictionary! as NSDictionary)
        mList = (serverResponse?.getData())!
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (mList.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentLandingTableViewCell", for: indexPath) as! StudentLandingTableViewCell
        
        cell.imgStudent.layer.cornerRadius = cell.imgStudent.frame.width/2
        cell.imgStudent.clipsToBounds = true
        cell.lblStudentName.textColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
        cell.lblStudentGrade.textColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_REGULAR, alpha: 1.0)
        let iconName = mList[indexPath.row].getImageUrl()
        cell.imgStudent.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        cell.imgStudent.sd_setShowActivityIndicatorView(true)
        cell.imgStudent.sd_setIndicatorStyle(.gray)
       
        
        let fullname = mList[indexPath.row].getFirstName() + " " + mList[indexPath.row].getLastName()
        cell.lblStudentName.text = fullname
        cell.lblStudentGrade.text = "Grade - " + mList[indexPath.row].getGrade()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let studentVC = mStudentLandingVC.storyboard!.instantiateViewController(withIdentifier: "StudentViewController") as! StudentViewController
        studentVC.mList = mList
        studentVC.mSelectedId = indexPath.row
        mStudentLandingVC.navigationController!.pushViewController(studentVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            AlertDialog().showAlertWithYesNoButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: "Are you sure you want to delete this item", buttonTitle: NSLocalizedString("alertCancel", comment: ""), otherButtonTitle: NSLocalizedString("alertbtnYes", comment: ""), action: {(OtherButton) -> Void in
                self.mStudentLandingVC.callStudentDeleteAPI(id: indexPath.row)
            })
        } else if editingStyle == .insert {
            //Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
}
