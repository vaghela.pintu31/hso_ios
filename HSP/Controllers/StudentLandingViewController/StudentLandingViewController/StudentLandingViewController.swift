//
//  StudentLandingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class StudentLandingViewController: UIViewController, BaseView {
    
    @IBOutlet weak var tblStudentLanding: UITableView!
    @IBOutlet weak var btnFab: UIButton!
    
    //MARK:- Variable Declaration
    private var mStudentLandingAdapter: StudentLandingAdapter!
    private var TAG : String = "StudentLandingViewController"
    private var mHttpCodes = DataManager.getHttpCodeData()
    private var mProgressIndicator: ProgressIndicator?
    private var mList = [StudentListingData]()
    public var mFabClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialise()
    }
    
    @IBAction func btnFabOnClick(_ sender: Any) {
        let studentLandingVC = self.storyboard!.instantiateViewController(withIdentifier: "AddStudentViewController") as! AddStudentViewController
        studentLandingVC.mFabClick = mFabClick
        self.navigationController!.pushViewController(studentLandingVC, animated: true)
    }
    
    func initialise() {
        //NavigationTitle     
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("student", comment: "")
        setNavigationBarItem()
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        self.btnFab.layer.cornerRadius = self.btnFab.frame.width/2
        self.btnFab.clipsToBounds = true
        
        // API Call
        showProgress()
        callStudentListingAPI()
    }
    
    func showEmptyView() {
        self.tblStudentLanding.isHidden = true
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    func tableReload() {
        self.tblStudentLanding.delegate = self.mStudentLandingAdapter
        self.tblStudentLanding.dataSource = self.mStudentLandingAdapter
        self.tblStudentLanding.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
        self.tblStudentLanding.tableFooterView = UIView()
    }
    
    //MARK: API Calling Methods
    func callStudentListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let studentListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)students/index")
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentListingRequest)")
            
            /*
            Alamofire.request(studentListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let studentListData = StudentListingResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setStudentListing(data: value as! NSDictionary)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }*/
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: studentListingRequest, success: { (data) -> Void in
                self.hideProgress()
                AppLog.debug(tag: self.TAG, msg: "ResponseSessionManager: \(String(describing: data))")
               self.setStudentListing(data: data as! Data)
            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("DATA = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setStudentListing(data: Data) {
        hideProgress()
        mStudentLandingAdapter = StudentLandingAdapter(viewController: self, data: data)
        //print("ResponseDeleteApi = \(data)")
        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
        
        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
        let serverResponse = StudentListingResponse(dictionary: userDictionary! as NSDictionary)
        //let serverResponse = StudentListingResponse(dictionary: data)
        mList = (serverResponse?.getData())!
        self.tblStudentLanding.tableFooterView = nil
        self.tblStudentLanding.backgroundView = nil
        if mList.count == 0 {
            UIStyle.showEmptyTableView(message: NSLocalizedString("student_not_found", comment: ""), viewController: self.tblStudentLanding)
        } else {
            self.tblStudentLanding.delegate = self.mStudentLandingAdapter
            self.tblStudentLanding.dataSource = self.mStudentLandingAdapter
            self.tblStudentLanding.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblStudentLanding.tableFooterView = UIView()
        }
    }
    
    //CALL_STUDENT_DELETEAPI
    func callStudentDeleteAPI(id: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            print("id = \(id)")
            let myUrl = "\(mainURL)students/remove/\(self.mList[id].getId())"
            print("myUrl = \(myUrl)")
            let studentDeleteRequest = ServerRequest.authenticateDeleteRequest(url: myUrl)
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentDeleteRequest)")
            AppLog.debug(tag: self.TAG, msg: "Header = \(String(describing: studentDeleteRequest.allHTTPHeaderFields))")
            
            Alamofire.request(studentDeleteRequest).responseJSON { response in
                //debugPrint(response)
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let deleteServerResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setStudentDeleteData(data: value as! NSDictionary, id: id)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    
    func setStudentDeleteData(data: NSDictionary, id: Int) {
        hideProgress()
        AppLog.debug(tag: TAG, msg: "ResponseDeleteApi = \(data)")
        self.mList.remove(at: id)
        initialise()
    }
}
