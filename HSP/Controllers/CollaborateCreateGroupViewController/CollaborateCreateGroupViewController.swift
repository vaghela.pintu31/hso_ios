//
//  CollaborateCreateGroupViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateCreateGroupViewController: UIViewController, BaseView {
    
    @IBOutlet weak var tblCreateGroup: UITableView!
    @IBOutlet weak var collectionViewCCGroup: UICollectionView!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    
    //MARK:- Variable Declaration
    private var TAG : String = "CollaborateCreateGroupViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mCollaborateCreateGroupAdapter: CollaborateCreateGroupAdapter!
    public var mList = [GetFriendListData]()
    public var mSelectedList = [GetFriendListData]()
    var screenSize: CGRect = UIScreen.main.bounds
    var selectedIndexPaths = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCreateGroupOnClick(_ sender: Any) {
        let collaborateCreateGroupVC = self.storyboard!.instantiateViewController(withIdentifier: "CollaborateCreateGroupDetailViewController") as! CollaborateCreateGroupDetailViewController
        collaborateCreateGroupVC.mSelectedList = mSelectedList
        self.navigationController!.pushViewController(collaborateCreateGroupVC, animated: true)
    }
    
    //MARK: BaseView Method
    func initialise() {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("new_group", comment: "")
        
        AppLog.debug(tag: self.TAG, msg: "CollaborateCreateGroup-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** Set Tableview Data **/
        setData()
        //callStudentListingAPI()
    }
    
    
    /** Set Tableview Data **/
    func setData() {
        if mList.count == 0 {
            UIStyle.showEmptyTableView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self.tblCreateGroup)
        } else {
            mCollaborateCreateGroupAdapter = CollaborateCreateGroupAdapter(viewController: self, data: mList)
            self.tblCreateGroup.delegate = self.mCollaborateCreateGroupAdapter
            self.tblCreateGroup.dataSource = self.mCollaborateCreateGroupAdapter
            self.tblCreateGroup.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblCreateGroup.tableFooterView = UIView()
            reloadCollection()
        }
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    func reloadCollection(){
        if mSelectedList.count == 0 {
            self.collectionHeightConstraint.constant = 0
        } else {
            self.collectionHeightConstraint.constant = 90
        }
        self.collectionViewCCGroup.delegate = self
        self.collectionViewCCGroup.dataSource = self
        self.collectionViewCCGroup.reloadData()
        //self.collectionViewCCGroup.selectItem(at: indexPath, animated: false, scrollPosition: [])
    }
    
//    //MARK: API Calling Methods
//    func callStudentListingAPI() {
//        if Reachability.isConnectedToNetwork() == true {
//            showProgress()
//            
//            let studentListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)students/index")
//            AppLog.debug(tag: self.TAG, msg: "URL = \(studentListingRequest)")
//            
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.getManager().startRequest(request: studentListingRequest, success: { (data) -> Void in
//                self.hideProgress()
//                AppLog.debug(tag: self.TAG, msg: "ResponseSessionManager: \(String(describing: data))")
//                self.setStudentListing(data: data as! Data)
//            }, failure: { (response, data, error) -> Void in
//                self.hideProgress()
//                print("DATA = \(String(describing: data))")
//            })
//        } else {
//            showAlert(msg: noInternet)
//        }
//    }
//    
//    func setStudentListing(data: Data) {
//        hideProgress()
//        //print("ResponseDeleteApi = \(data)")
//        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
//        
//        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
//        let serverResponse = StudentListingResponse(dictionary: userDictionary! as NSDictionary)
//        mList = (serverResponse?.getData())!
//        if mList.count == 0 {
//            UIStyle.showEmptyTableView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self.tblCreateGroup)
//        } else {
//            mCollaborateCreateGroupAdapter = CollaborateCreateGroupAdapter(viewController: self, data: mList)
//            self.tblCreateGroup.delegate = self.mCollaborateCreateGroupAdapter
//            self.tblCreateGroup.dataSource = self.mCollaborateCreateGroupAdapter
//            self.tblCreateGroup.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
//            self.tblCreateGroup.tableFooterView = UIView()
//            reloadCollection()
//        }
//    }
    
    
}


extension CollaborateCreateGroupViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mSelectedList.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "CollaborateCreateGroupCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! CollaborateCreateGroupCollectionViewCell
        
        //RoundedImageSet
        UIStyle.setAppButtonStyleRounded(view: cell.imgMemberPic)
        
        let iconName = mSelectedList[indexPath.row].getImageUrl()
        cell.imgMemberPic.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        cell.imgMemberPic.sd_setShowActivityIndicatorView(true)
        cell.imgMemberPic.sd_setIndicatorStyle(.gray)
        let fullname = mSelectedList[indexPath.row].getFirstName() + mSelectedList[indexPath.row].getLastName()
        cell.lblMemberName.text = fullname
    
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //handle tap events
        let cell = tblCreateGroup.cellForRow(at: indexPath) as? CollaborateCreateGroupTableViewCell
        let id = mSelectedList[indexPath.row].getId()
        //cell?.imgUserSelect.isHidden = true
        for i in 0..<self.mList.count {
            if id == self.mList[i].getId(){
                mSelectedList.remove(at: indexPath.row)
                if indexPath.row == id  {
                    cell?.imgUserSelect.isHidden = true
                }
                self.reloadCollection()
                let indexing = IndexPath(row: i, section: 0)
                print("#index\(i) id: \(id) Sli: \(self.mList[i].getId())")
                print("indexing = \(indexing)")
                tblCreateGroup.reloadRows(at: [indexing], with: .none)
            } else {
                //cell?.imgUserSelect.isHidden = true
                //self.reloadCollection()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: collectionHeightConstraint.constant)
    }
}
