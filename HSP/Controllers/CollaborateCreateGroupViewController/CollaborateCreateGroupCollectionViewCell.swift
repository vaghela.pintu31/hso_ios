//
//  CollaborateCreateGroupCollectionViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateCreateGroupCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgMemberPic: UIImageView!
    @IBOutlet weak var lblMemberName: UILabel!
    @IBOutlet weak var imgCancelOl: UIImageView!
}
