//
//  CollaborateCreateGroupAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateCreateGroupAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    private var mCollaborateCreateGroupVC: CollaborateCreateGroupViewController!
    private var mList = [GetFriendListData]()

    
    // MARK: - Constructor
    init (viewController: CollaborateCreateGroupViewController,data : [GetFriendListData]) {
        mCollaborateCreateGroupVC = viewController
        mList = data
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberListCell", for: indexPath) as! CollaborateCreateGroupTableViewCell
        
        //RoundedImageSet
        UIStyle.setAppButtonStyleRounded(view: cell.imgMemberListPic)
        
        let iconName = mList[indexPath.row].getImageUrl()
        cell.imgMemberListPic.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        cell.imgMemberListPic.sd_setShowActivityIndicatorView(true)
        cell.imgMemberListPic.sd_setIndicatorStyle(.gray)
        let fullname = mList[indexPath.row].getFirstName() + " " + mList[indexPath.row].getLastName()
        cell.lblMemberListName.text = fullname

        cell.imgUserSelect.isHidden = true
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? CollaborateCreateGroupTableViewCell
        //print("namw = \(mList[indexPath.row].getName())")
        //print("indexPath = \(indexPath.row)")
        if cell?.imgUserSelect.isHidden == true {
            cell?.imgUserSelect.isHidden = false
            mCollaborateCreateGroupVC.mSelectedList.append(mList[indexPath.row])
        } else {
            let id = mList[indexPath.row].getId()
            cell?.imgUserSelect.isHidden = true
            for i in 0..<mCollaborateCreateGroupVC.mSelectedList.count {
                if id == mCollaborateCreateGroupVC.mSelectedList[i].getId() {
                    //print("Mlist_id = \(id) = mSelectListID : \(mCollaborateCreateGroupVC.mSelectedList[i].getId())")
                    mCollaborateCreateGroupVC.mSelectedList.remove(at: i)
                    cell?.imgUserSelect.isHidden = true
                    break
                }
            }
            //mCollaborateCreateGroupVC.reloadCollection()
        }
        mCollaborateCreateGroupVC.reloadCollection()
    }
    
}
