//
//  CollaborateCreateGroupTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateCreateGroupTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgMemberListPic: UIImageView!
    @IBOutlet weak var lblMemberListName: UILabel!
    @IBOutlet weak var imgUserSelect: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
