//
//  CollaborateSelectContactTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateSelectContactTableViewCell: UITableViewCell {
    
    //CreateGroupCell
    @IBOutlet weak var imgNewGroup: UIImageView!
    @IBOutlet weak var lblNewGroupName: UILabel!
    
    //SelectContactCell
    @IBOutlet weak var imgContactList: UIImageView!
    @IBOutlet weak var lblContactListName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
