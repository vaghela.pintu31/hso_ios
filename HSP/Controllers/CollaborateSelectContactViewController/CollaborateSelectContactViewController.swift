//
//  CollaborateSelectContactViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateSelectContactViewController: UIViewController, BaseView {
    
    
    @IBOutlet weak var tblCollaborateSelectContactView: UITableView!

    //MARK:- Variable Declaration
    public var TAG : String = "CollaborateSelectContactViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mCollaborateSelectContactAdapter: CollaborateSelectContactAdapter!
    private var mList = [GetFriendListData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("select_contacts", comment: "")
    }
    
    //MARK: BaseView Method
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "CollaborateSelectContact-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** callFriendsListingAPI **/
        callFriendsListingAPI()
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }

    
    //MARK: API Calling Methods
    func callFriendsListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var studentListingRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)friends/index")
            
            let getFListData = GetFriendListData()
            getFListData.setUserId(userId: "\(Utility.getMyUserId())")
            
            let serverParam = GetFriendListResponse()
            serverParam.setFriend(friend: getFListData)
            
            let paramObject = serverParam.toGetFrndDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            studentListingRequest.httpBody = jsonData
            
            //let paramObject:[String : Any] = ["friend": ["userId": "\(Utility.getMyUserId())"]]
            //studentListingRequest.httpBody = try! JSONSerialization.data(withJSONObject: params)
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentListingRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(String(describing: paramObject))")

            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getManager().startRequest(request: studentListingRequest, success: { (data) -> Void in
                self.hideProgress()
                //AppLog.debug(tag: self.TAG, msg: "ResponseSessionManager: \(String(describing: data))")
                self.setFriendListing(data: data as! Data)
            }, failure: { (response, data, error) -> Void in
                self.hideProgress()
                print("DATA = \(String(describing: data))")
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setFriendListing(data: Data) {
        hideProgress()
    
        let sResponceString = NSString(data: data ,encoding: String.Encoding.ascii.rawValue)
        let userDictionary = Utility.convertStringToDictionary(text: sResponceString!)
        let serverResponse = GetFriendListResponse(dictionary: userDictionary! as NSDictionary)
        print("ResponseFriendListing = \(serverResponse!)")
        mList = (serverResponse?.getData())!
        if mList.count == 0 {
            UIStyle.showEmptyTableView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self.tblCollaborateSelectContactView)
        } else {
            mCollaborateSelectContactAdapter = CollaborateSelectContactAdapter(viewController: self, data: mList)
            self.tblCollaborateSelectContactView.delegate = self.mCollaborateSelectContactAdapter
            self.tblCollaborateSelectContactView.dataSource = self.mCollaborateSelectContactAdapter
            self.tblCollaborateSelectContactView.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblCollaborateSelectContactView.tableFooterView = UIView()
        }
    }


}
