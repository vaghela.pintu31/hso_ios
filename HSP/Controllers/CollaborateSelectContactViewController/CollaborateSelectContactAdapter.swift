//
//  CollaborateSelectContactAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 06/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class CollaborateSelectContactAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Variable Declaration
    var mCollaborateSelectContactVC: CollaborateSelectContactViewController!
    private var mList = [GetFriendListData]()
    
    // MARK: - Constructor
    init (viewController: CollaborateSelectContactViewController,data : [GetFriendListData]) {
        mCollaborateSelectContactVC = viewController
        mList = data
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return mList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 { //CreateGroupCell
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "CreateGroupCell", for: indexPath) as! CollaborateSelectContactTableViewCell
            
            //RoundedImageSet
            UIStyle.setAppButtonStyleRounded(view: cell1.imgNewGroup)
            cell1.imgNewGroup.image = UIImage(named: "ic_group")
            cell1.lblNewGroupName.text = NSLocalizedString("new_group", comment: "")
            
            cell1.selectionStyle = .none
            return cell1

        } else { //SelectContactCell
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "SelectContactCell", for: indexPath) as! CollaborateSelectContactTableViewCell
            
            //RoundedImageSet
            UIStyle.setAppButtonStyleRounded(view: cell2.imgContactList)
            
            let iconName = mList[indexPath.row].getImageUrl()
            cell2.imgContactList.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
            cell2.imgContactList.sd_setShowActivityIndicatorView(true)
            cell2.imgContactList.sd_setIndicatorStyle(.gray)
            let fullname = mList[indexPath.row].getFirstName() + " " + mList[indexPath.row].getLastName()
            cell2.lblContactListName.text = fullname
            
            cell2.selectionStyle = .none
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 { //CreateGroupCell
            let collaborateSelectContactVC = mCollaborateSelectContactVC.storyboard!.instantiateViewController(withIdentifier: "CollaborateCreateGroupViewController") as! CollaborateCreateGroupViewController
            collaborateSelectContactVC.mList = mList
            mCollaborateSelectContactVC.navigationController!.pushViewController(collaborateSelectContactVC, animated: true)
        } else {
            createIndividualChat(index: indexPath.row)
        }
    }
    
    func createIndividualChat(index: Int) {
        let newChat = NewChat()
        let fullname = mList[index].getFirstName() + " " + mList[index].getLastName()
        newChat.setName(name: fullname)
        newChat.setType(type: "individual")
        newChat.setUserchats(userchats: [mList[index].getId()])
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.appdelSocket.add(newChat) { (data) in
            let json  = data[0] as! Dictionary<String, AnyObject>
            AppLog.debug(tag: self.mCollaborateSelectContactVC.TAG, msg: "IndividualChatResponse: \(json)")
            let message = json["body"]?["message"] as! String
            let statusCode = json["statusCode"] as! Int
            AppLog.debug(tag: self.mCollaborateSelectContactVC.TAG, msg: "IndividualChatResponseMESSAGE \(message)")
            
            if statusCode == 200 || statusCode == 201 {
                let collaborateSelectContactVC = self.mCollaborateSelectContactVC.storyboard!.instantiateViewController(withIdentifier: "CollaborateConversationViewController") as! CollaborateConversationViewController
                collaborateSelectContactVC.comeFromCSContact = true
                collaborateSelectContactVC.userNameHeader = fullname
                self.mCollaborateSelectContactVC.navigationController!.pushViewController(collaborateSelectContactVC, animated: true)
            } else {
                Utility.showTost(strMsg: message, view: self.mCollaborateSelectContactVC.view)
            }
        }
    }

}
