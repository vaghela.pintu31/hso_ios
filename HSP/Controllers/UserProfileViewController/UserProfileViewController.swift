//
//  UserProfileViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 22/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class UserProfileViewController: UIViewController, BaseView, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var curveView: UIView!
    @IBOutlet weak var txtFirstName: MKTextField!
    @IBOutlet weak var txtLastName: MKTextField!
    @IBOutlet weak var txtEmailId: MKTextField!
    @IBOutlet weak var txtDateOfBirth: MKTextField!
    @IBOutlet weak var txtGender: MKTextField!
    @IBOutlet weak var txtStreet: MKTextField!
    @IBOutlet weak var txtCountryCode: MKTextField!
    @IBOutlet weak var txtPhoneNo: MKTextField!
    @IBOutlet weak var txtCity: MKTextField!
    @IBOutlet weak var txtPincode: MKTextField!
    @IBOutlet weak var txtState: MKTextField!
    @IBOutlet weak var txtCountry: MKTextField!
    @IBOutlet weak var btnUpdateOl: UIButton!
   // @IBOutlet weak var btnDeactivateOl: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG : String = "UserProfileViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mSelectedImage: UIImage?
    var flage: Bool = true
    var currentUser: Bool = false
    var sUserGroupInformation = UserGroupInformation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialise()
    }
    
    //MARK: BaseView Method
    func initialise() {
        
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = NSLocalizedString("user_profile", comment: "")
        
        AppLog.debug(tag: self.TAG, msg: "UserProfile-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** Set Tableview Data **/
        setData()
    }
    
    /** Set Tableview Data **/
    func setData() {
        DataManager.readJson()

        UIStyle.setCustomStyle(view: imgProfilePic, cornerRadius: imgProfilePic.frame.height / 2, boderColor: COLOR_BACKGROUND, borderWidth: 2)
        UIStyle.setAppButtonStyleRounded(view: curveView)
        UIStyle.setAppButtonStyleRounded(view: btnUpdateOl)
        //UIStyle.setAppButtonStyleRounded(view: btnDeactivateOl)
        
        txtFirstName.layer.borderColor = UIColor.clear.cgColor
        txtLastName.layer.borderColor = UIColor.clear.cgColor
        txtEmailId.layer.borderColor = UIColor.clear.cgColor
        txtDateOfBirth.layer.borderColor = UIColor.clear.cgColor
        txtGender.layer.borderColor = UIColor.clear.cgColor
        txtStreet.layer.borderColor = UIColor.clear.cgColor
        txtCountryCode.layer.borderColor = UIColor.clear.cgColor
        txtPhoneNo.layer.borderColor = UIColor.clear.cgColor
        txtCity.layer.borderColor = UIColor.clear.cgColor
        txtPincode.layer.borderColor = UIColor.clear.cgColor
        txtState.layer.borderColor = UIColor.clear.cgColor
        txtCountry.layer.borderColor = UIColor.clear.cgColor
        
        //SetPlaceHolderText
        txtFirstName.placeholder = NSLocalizedString("first_name", comment: "")
        txtLastName.placeholder = NSLocalizedString("last_name", comment: "")
        txtEmailId.placeholder = NSLocalizedString("email", comment: "")
        txtGender.placeholder = NSLocalizedString("gender", comment: "")
        txtPhoneNo.placeholder = NSLocalizedString("phone_number", comment: "")
        txtStreet.placeholder = NSLocalizedString("street", comment: "")
        txtCity.placeholder = NSLocalizedString("city", comment: "")
        txtCountry.placeholder = NSLocalizedString("country", comment: "")
        txtPincode.placeholder = NSLocalizedString("zipcode", comment: "")
        
        btnUpdateOl.setTitle(NSLocalizedString("update_profile", comment: ""), for: .normal)
        //btnDeactivateOl.setTitle(NSLocalizedString("deactivate", comment: ""), for: .normal)
        
        if flage{
            self.setNavigationBarItem()
            self.view.isUserInteractionEnabled = true
            self.btnUpdateOl.isHidden = false
            callUsersViewAPI()
        }else{
            
            if currentUser {
                self.view.isUserInteractionEnabled = true
                self.btnUpdateOl.isHidden = false
            }else {
                self.btnUpdateOl.isHidden = true
                self.view.isUserInteractionEnabled = false
            }
            
            self.imgProfilePic.sd_setImage(with: URL(string: sUserGroupInformation.getImageUrl()), placeholderImage: UIImage(named: noImageFound))
            self.imgProfilePic.sd_setShowActivityIndicatorView(true)
            self.imgProfilePic.sd_setIndicatorStyle(.white)
            
            self.txtFirstName.text = sUserGroupInformation.getFirstName()
            self.txtLastName.text = sUserGroupInformation.getLastName()
            self.txtEmailId.text = sUserGroupInformation.getEmail()
            self.txtDateOfBirth.text = sUserGroupInformation.getDob()
            self.txtGender.text = sUserGroupInformation.getGender()
            self.txtStreet.text = sUserGroupInformation.getAddress()
            self.txtCountryCode.text = String(describing: sUserGroupInformation.getCountryCode())
            self.txtPhoneNo.text = String(describing: sUserGroupInformation.getContactNumber())
            self.txtCity.text = sUserGroupInformation.getCity()
            self.txtPincode.text = sUserGroupInformation.getZipCode()
            self.txtState.text = sUserGroupInformation.getState()
            self.txtCountry.text = ""
        }

    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtEmailId.resignFirstResponder()
        txtDateOfBirth.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtStreet.resignFirstResponder()
        txtCountryCode.resignFirstResponder()
        txtPhoneNo.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtPincode.resignFirstResponder()
        txtState.resignFirstResponder()
        txtCountry.resignFirstResponder()
        return true
    }
    
    func closeKeyboard() {
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtEmailId.resignFirstResponder()
        txtDateOfBirth.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtStreet.resignFirstResponder()
        txtCountryCode.resignFirstResponder()
        txtPhoneNo.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtPincode.resignFirstResponder()
        txtState.resignFirstResponder()
        txtCountry.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtGender {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        } else if textField == txtDateOfBirth {
            txtDateOfBirthOnClick(textField as! MKTextField)
        } else if textField == txtCountryCode {
            txtCountryCodeOnClick(textField)
        } else if textField == txtCountry {
            txtCountryOnClick(textField)
        }
    }
    
    //MARK: OpenGallaryAction
    @IBAction func openGallary(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: UIImagePickerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo  info: [String : Any]) {
        mSelectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgProfilePic.backgroundColor = UIColor.clear
        imgProfilePic.contentMode = UIViewContentMode.scaleAspectFit
        imgProfilePic.image = mSelectedImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func txtDateOfBirthOnClick(_ sender: MKTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    //DatePicker
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale!
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: sender.date)
        self.txtDateOfBirth.text = strDate
    }
    
    @IBAction func txtGenderOnClick(_ sender: Any) {
        selectGender()
    }
    
    @IBAction func txtCountryCodeOnClick(_ sender: Any) {
        self.closeKeyboard()
        showProgress()
        //selectCountryCode()
        selectCountryName()
    }
    
    @IBAction func txtCountryOnClick(_ sender: Any) {
        self.closeKeyboard()
        showProgress()
        selectCountryName()
    }

    //Dropdown Function
    //Gender
    func selectGender(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: NSLocalizedString("selectGender", comment: ""), preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = DataManager.genderArray.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(DataManager.genderArray[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtGender.text = DataManager.genderArray[index]
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //CountryCode
    func selectCountryCode(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: NSLocalizedString("Select Country Code", comment: ""), preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = DataManager.countryCode.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(DataManager.countryCode[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtCountryCode.text = DataManager.countryCode[index]
                    self.txtCountry.text = DataManager.countryName[index]
                    self.hideProgress()
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in
            self.hideProgress()
        }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //Country
    func selectCountryName(){
        let actionSheetController = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Select Country Name", preferredStyle: UIAlertControllerStyle.actionSheet)
        let count = DataManager.countryName.count
        for index in 0..<count {
            let myAction = UIAlertAction(title: "\(DataManager.countryName[index])", style: UIAlertActionStyle.default) { (action) -> Void in
                if actionSheetController.actions.index(of: action) != nil {
                    self.txtCountry.text = DataManager.countryName[index]
                    self.txtCountryCode.text = DataManager.countryCode[index]
                    self.hideProgress()
                }
            }
            actionSheetController.addAction(myAction)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("alertCancel", comment: ""), style: .cancel) { (_) in
            self.hideProgress()
        }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }

    
    //MARK: OnClickEvents
    @IBAction func btnUpdateUserProfileOnClick(_ sender: Any) {
        callEditUserAPI()
    }

    //MARK: API Calling Methods
    func callUsersViewAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let userProfileViewRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)users/view/\(Utility.getMyUserId())")
            AppLog.debug(tag: self.TAG, msg: "URL = \(userProfileViewRequest)")
            
            Alamofire.request(userProfileViewRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let signUpResponse = UsersViewResponce(dictionary: response.result.value as! NSDictionary)
                    
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setUsersViewData(data: value as! NSDictionary)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (signUpResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (signUpResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    //MARK: API Calling Methods
    func callEditUserAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            var editUserProfileRequest = ServerRequest.authenticatePostRequest(url: "\(mainURL)users/edit/\(Utility.getMyUserId())")
            
            let sEditUser = EditUser()
            sEditUser.setFirstName(firstName: (txtFirstName.text)!)
            sEditUser.setLastName(lastName: (txtLastName.text)!)
            sEditUser.setEmail(email: txtEmailId.text!)
            sEditUser.setContactNumber(contactNumber: txtPhoneNo.text!)
            sEditUser.setAddress(address: txtStreet.text!)
            sEditUser.setZipCode(zipCode: txtPincode.text!)
            sEditUser.setCity(city: txtCity.text!)
            sEditUser.setCountryCode(countryCode: Int(txtCountryCode.text!)!)
            sEditUser.setGender(gender: txtGender.text!)
            sEditUser.setSignupType(signupType: mUsersViewDat.getData().getUser().getSignupType())
            sEditUser.setPassword(password: Utility.getPassword())
            //sEditUser.setMediaId(mediaId: 0)
            sEditUser.setMediaId(mediaId: mUsersViewDat.getData().getUser().getMediaId())
            sEditUser.setState(state: mUsersViewDat.getData().getUser().getState())
            sEditUser.setDob(dob: mUsersViewDat.getData().getUser().getDob())
            sEditUser.setId(id: mUsersViewDat.getData().getUser().getId())
            sEditUser.setImageUrl(imageUrl: mUsersViewDat.getData().getUser().getImageUrl())
            
            let serverParam = UserEditProfile()
            serverParam.setUser(user: sEditUser)
            
            let paramObject = serverParam.toDictionary()
            let jsonData = try? JSONSerialization.data(withJSONObject: paramObject, options: [])
            editUserProfileRequest.httpBody = jsonData
            
            AppLog.debug(tag: self.TAG, msg: "URL = \(editUserProfileRequest)")
            AppLog.debug(tag: self.TAG, msg: "params = \(paramObject)")
            
            Alamofire.request(editUserProfileRequest).responseJSON { response in
                let statusCode = response.response!.statusCode
                AppLog.debug(tag: self.TAG, msg: "Status Code = \(statusCode)")
                let  sUsersViewDat = UsersViewResponce(dictionary: response.result.value as! NSDictionary)
                switch response.result {
                case .success:
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            AppLog.debug(tag: self.TAG, msg: "Response = \(value)")
                            self.hideProgress()
                            self.setUsersViewData(data: value as! NSDictionary)
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (sUsersViewDat?.getMessage())!, statusCode: statusCode))
                        }
                    }else{
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (sUsersViewDat?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    var mUsersViewDat: UsersViewResponce!
    func setUsersViewData(data: NSDictionary) {
        hideProgress()
        mUsersViewDat = UsersViewResponce(dictionary: data)
        
        let Fullname = (mUsersViewDat?.getData().getUser().getFirstName())! + " " + (mUsersViewDat?.getData().getUser().getLastName())!
        Utility.setUserLogin(isLogin: true)
        Utility.setUserName(username: Fullname)
        Utility.setUserEmail(EmailId: mUsersViewDat?.getData().getUser().getEmail())
        Utility.setUserImageUrl(ImageUrl: mUsersViewDat?.getData().getUser().getImageUrl())
        Utility.setMyUserId(myuserId: mUsersViewDat?.getData().getUser().getId())
        
        //StoreProfileData
        Utility.setUserGender(gender: mUsersViewDat?.getData().getUser().getGender())
        Utility.setUserDob(dob: mUsersViewDat?.getData().getUser().getDob())
        Utility.setUserCity(city: mUsersViewDat?.getData().getUser().getCity())
        Utility.setUserState(state: mUsersViewDat?.getData().getUser().getState())

        AppLog.debug(tag: self.TAG, msg: "Id = \(String(describing: mUsersViewDat?.getData().getUser().getId()))")
        
        self.imgProfilePic.sd_setImage(with: URL(string: Utility.getUserImageUrl()), placeholderImage: UIImage(named: noImageFound))
        self.imgProfilePic.sd_setShowActivityIndicatorView(true)
        self.imgProfilePic.sd_setIndicatorStyle(.white)
        
        self.txtFirstName.text = mUsersViewDat?.getData().getUser().getFirstName()
        self.txtLastName.text = mUsersViewDat?.getData().getUser().getLastName()
        self.txtEmailId.text = mUsersViewDat?.getData().getUser().getEmail()
        self.txtDateOfBirth.text = mUsersViewDat.getData().getUser().getDob()
        self.txtGender.text = mUsersViewDat?.getData().getUser().getGender()
        self.txtStreet.text = mUsersViewDat?.getData().getUser().getAddress()
        if mUsersViewDat.getData().getUser().getCountry() == "" {
            for i in 0..<DataManager.countryCode.count {
                let checkCode = "+" + String(mUsersViewDat!.getData().getUser().getCountryCode())
                if  checkCode == DataManager.countryCode[i] {
                    self.txtCountry.text = DataManager.countryName[i]
                }
            }
        } else {
            self.txtCountry.text = mUsersViewDat.getData().getUser().getCountry()
        }
        
        self.txtCountryCode.text = String(mUsersViewDat!.getData().getUser().getCountryCode())
        self.txtPhoneNo.text = mUsersViewDat?.getData().getUser().getContactNumber()
        self.txtCity.text = mUsersViewDat?.getData().getUser().getCity()
        self.txtPincode.text = mUsersViewDat?.getData().getUser().getZipCode()
        self.txtState.text = mUsersViewDat.getData().getUser().getState()
        
        
        self.btnUpdateOl.setTitle(NSLocalizedString("update_profile", comment: ""), for: .normal)
        if mSelectedImage != nil {
            UploadImageApi()
        }
        
        //self.btnDeactivateOl.setTitle(NSLocalizedString("deactivate", comment: ""), for: .normal)
    }
    
    //UPLOAD_IMAGE
    func UploadImageApi() {
        if Reachability.isConnectedToNetwork() == true {
            self.showProgress()
            let headers: HTTPHeaders = ["X-Access-Token": Utility.getXAccessToken()]
            let uploadImageRequest = try! URLRequest(url: "\(mainURL)upload", method: .post, headers: headers)
            AppLog.debug(tag: self.TAG, msg: "URL = \(uploadImageRequest)")
            AppLog.debug(tag: self.TAG, msg: "headers = \(headers)")
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("user".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "model")
                AppLog.debug(tag: self.TAG, msg: "SelectedId = \(self.mSelectedImage!)")
                multipartFormData.append("\(Utility.getMyUserId())".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "entityId")
                multipartFormData.append(UIImagePNGRepresentation(self.mSelectedImage!)!, withName: "media", fileName: "picture.png", mimeType: "image/png")
            }, with: uploadImageRequest, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint("SUCCESS RESPONSE: \(response)")
                        let serverResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                        let statusCode = response.response!.statusCode
                        AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                        AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                        
                        if (statusCode == 200) {
                            if response.result.value != nil {
                                self.hideProgress()
                                AlertDialog().showAlertWithOkButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: serverResponse?.getMessage(), action: { (CancelButton) -> Void in
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }else{
                                let value = response.result.value
                                AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                                self.hideProgress()
                                self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                            }
                        }else{
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (serverResponse?.getMessage())!, statusCode: statusCode))
                        }
                    }
                case .failure(let encodingError):
                    self.hideProgress()
                    AppLog.debug(tag: self.TAG, msg: "ERROR RESPONSE: \(encodingError)")
                    self.showAlert(msg: serverDown)
                }
            })
        } else {
            showAlert(msg: noInternet)
        }
    }
    //
}
