//
//  BudgetSetupAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 23/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class BudgetSetupAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    private var mBudgetSetupVC: BudgetSetupViewController!
    private var sTotalBudget = TotalBudget()
    
    // MARK: - Constructor
    init (viewController: BudgetSetupViewController, data : TotalBudget) {
        mBudgetSetupVC = viewController
        sTotalBudget = data
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sTotalBudget.getCategories().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetSetupTableViewCell", for: indexPath) as! BudgetSetupTableViewCell
        cell.lblCategoryName.text = sTotalBudget.getCategories()[indexPath.row].getName()
        cell.txtEnterAmount.text = String(sTotalBudget.getCategories()[indexPath.row].getRemainingAmount())
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    
}
