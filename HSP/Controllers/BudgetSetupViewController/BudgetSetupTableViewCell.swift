//
//  BudgetSetupTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 23/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class BudgetSetupTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var txtEnterAmount: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
