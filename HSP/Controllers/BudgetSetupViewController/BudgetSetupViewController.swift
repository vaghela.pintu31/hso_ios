//
//  BudgetSetupViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 23/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class BudgetSetupViewController: UIViewController, BaseView {
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var tblBudgetSetup: UITableView!
    @IBOutlet weak var btnSubmitOl: UIButton!
    
    //MARK:- Variable Declaration
    private var TAG : String = "BudgetSetupViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mBudgetSetupAdapter: BudgetSetupAdapter!

    var sTotalBudget = TotalBudget()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = "Name"
    }
    
    //MARK: BaseView Method
    func initialise() {
        
        
        AppLog.debug(tag: self.TAG, msg: "BudgetSetup-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** Set Tableview Data **/
        setData()

    }
    
    /** Set Tableview Data **/
    func setData() {
        mBudgetSetupAdapter = BudgetSetupAdapter(viewController: self, data: sTotalBudget)
        self.tblBudgetSetup.delegate = self.mBudgetSetupAdapter
        self.tblBudgetSetup.dataSource = self.mBudgetSetupAdapter
        self.tblBudgetSetup.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
        self.tblBudgetSetup.tableFooterView = UIView()
        
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }

}
