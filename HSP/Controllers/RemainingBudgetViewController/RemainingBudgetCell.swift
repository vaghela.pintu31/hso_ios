//
//  LessonPlanListingTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class RemainingBudgetCell: UITableViewCell {
    
    

    @IBOutlet weak var imgDate: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonthYear: UILabel!
    @IBOutlet weak var imgDateView: UIView!
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var bEdit: UIButton!   
    @IBOutlet weak var bDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func convertDateFormateDay(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        let dateObj = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd"
        return dateFormatter.string(from: dateObj!)
    }
    
    func convertDateFormateMonth(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        let dateObj = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM yyyy"
        let returnString = dateFormatter.string(from: dateObj!)
        return returnString
    }

}
