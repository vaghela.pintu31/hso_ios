//
//  StudentLandingViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Charts
import Alamofire

class RemainingBudgetViewController: UIViewController, BaseView {
    
    @IBOutlet weak var pieChartView: PieChartView!
    
    @IBOutlet weak var tblRemaningBudget: UITableView!
    
    //MARK:- Variable Declaration
    private var TAG : String = "RemainingBudgetViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mRemainingBudgetAdapter: RemainingBudgetAdapter!
    var userId: Int!
    private var sIndividualBudgetResponce = IndividualBudgetResponce()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //NavigationTitle
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationItem.title = "Remaining Budget"
        
        //RightSide Two Button in Navigation Set
        let budget = UIImage(named: "ic_budget")!
        let addImg   = UIImage(named: "ic_add")!

         let addButton: UIBarButtonItem = UIBarButtonItem(image: addImg, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.didTapBudgetButton(sender:)))
        
         let budgetButton: UIBarButtonItem = UIBarButtonItem(image: budget, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.didTapAddButton(sender:)))
         navigationItem.rightBarButtonItems = [addButton,budgetButton]
        
        callBudgetsViewAPI()

        
    }
    
    //MARK: BaseView Method
    func initialise() {
        
        
        
        AppLog.debug(tag: self.TAG, msg: "BudgetLanding-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
    }
    
    
    
    

    
    func didTapAddButton(sender: AnyObject){
        let sBudgetSetupViewController = self.storyboard!.instantiateViewController(withIdentifier: "BudgetSetupViewController") as! BudgetSetupViewController
        let totalBudget = TotalBudget()
        
        let categoryCount = sIndividualBudgetResponce.getIndividualBudgetData().getBudgetCategories().count
        if categoryCount == 0{
            
        }else {
            totalBudget.setSpentAmount(spentAmount: sIndividualBudgetResponce.getIndividualBudgetData().getRemainingAmount())
            totalBudget.setRemainingAmount(remainingAmount: sIndividualBudgetResponce.getIndividualBudgetData().getRemainingAmount())
            var arrayCategoty = [Categories]()

            for i in 0..<categoryCount{
                let sCategoty = Categories()
                sCategoty.setName(name: sIndividualBudgetResponce.getIndividualBudgetData().getBudgetCategories()[i].getCategoryId().getName())
                sCategoty.setRemainingAmount(remainingAmount: sIndividualBudgetResponce.getIndividualBudgetData().getBudgetCategories()[i].getRemainingAmount())
                sCategoty.setSpentAmount(spentAmount: sIndividualBudgetResponce.getIndividualBudgetData().getBudgetCategories()[i].getSpentAmount())
                arrayCategoty.append(sCategoty)
            }
            totalBudget.setCategories(categories: arrayCategoty)
        }
    
        sBudgetSetupViewController.sTotalBudget = totalBudget
        self.navigationController!.pushViewController(sBudgetSetupViewController, animated: true)
    }
    
    func didTapBudgetButton(sender: AnyObject){
        let sAddExpenseViewController = self.storyboard!.instantiateViewController(withIdentifier: "AddExpenseViewController") as! AddExpenseViewController
        sAddExpenseViewController.addFlage = true
        self.navigationController!.pushViewController(sAddExpenseViewController, animated: true)
    }
    
    
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    func setChart() {
        let dataPoints = ["Books", "Sports", "Misc", "Stationary", "Uniform"]
        let values = [10.0, 4.0, 6.0, 3.0, 12.0]
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry1 = PieChartDataEntry(value: values[i], label: dataPoints[i], data:  dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry1)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        pieChartView.noDataText = "No data available"
        // user interaction
        pieChartView.isUserInteractionEnabled = false
        
        let d = Description()
        d.text = "" //"iOSCharts.io"
        pieChartView.chartDescription = d
        pieChartView.centerText = "Expenses"
        pieChartView.holeRadiusPercent = 0.5
        pieChartView.transparentCircleColor = UIColor.clear        
        pieChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        let colors: [UIColor] = [Utility.hexStringToUIColor(hex: "#339933", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#ff9900", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#3366cc", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#dd3f11", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#990099", alpha: 1.0)]
        
        pieChartDataSet.colors = colors
        
    }
    
    
    
    func setTotalChatData(remainingAmount: Int, spentAmount: Int)  {
        let totalBudget = spentAmount + (-1 * remainingAmount)
        let remainingPercentage = ((-1 * remainingAmount) * 100) / totalBudget
        let spentPercentage = (spentAmount * 100) / totalBudget
        let dataPoints = ["Remaining Amount", "Spent Amount"]
        let values = [remainingPercentage,spentPercentage]
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry1 = PieChartDataEntry(value: Double(values[i]), label: dataPoints[i], data:  dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry1)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        pieChartView.noDataText = "No data available"
        // user interaction
        pieChartView.isUserInteractionEnabled = false
        
        let d = Description()
        d.text = "" //"iOSCharts.io"
        pieChartView.chartDescription = d
        pieChartView.centerText = "Total Budget"
        pieChartView.holeRadiusPercent = 0.5
        pieChartView.transparentCircleColor = UIColor.clear
        
        pieChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        let colors: [UIColor] = [Utility.hexStringToUIColor(hex: "#339933", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#ff9900", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#3366cc", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#dd3f11", alpha: 1.0),
                                 Utility.hexStringToUIColor(hex: "#990099", alpha: 1.0)]
        
        pieChartDataSet.colors = colors
        
    }

    
    //MARK: API Calling Methods
    func callBudgetsViewAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let todoListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)budgets/view/\(userId!)?transactions=1&budgetcategories=1")
            AppLog.debug(tag: self.TAG, msg: "URL = \(todoListingRequest)")
            
            
            Alamofire.request(todoListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let sIndividualBudgetResponce = IndividualBudgetResponce(dictionary: response.result.value as! NSDictionary)
                    
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setTodoListing(data: value as! NSDictionary)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (sIndividualBudgetResponce?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (sIndividualBudgetResponce?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setTodoListing(data: NSDictionary) {
        hideProgress()
        sIndividualBudgetResponce = IndividualBudgetResponce(dictionary: data)!
        
        setTotalChatData(remainingAmount: sIndividualBudgetResponce.getIndividualBudgetData().getRemainingAmount(), spentAmount: sIndividualBudgetResponce.getIndividualBudgetData().getSpentAmount())
        if sIndividualBudgetResponce.getIndividualBudgetData().getTransactions().count == 0 {
            UIStyle.showEmptyTableView(message: "No Data Found!", viewController: self.tblRemaningBudget)
        } else {           
            mRemainingBudgetAdapter = RemainingBudgetAdapter(viewController: self, data: (sIndividualBudgetResponce.getIndividualBudgetData().getTransactions()))
            self.tblRemaningBudget.delegate = self.mRemainingBudgetAdapter
            self.tblRemaningBudget.dataSource = self.mRemainingBudgetAdapter
            self.tblRemaningBudget.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblRemaningBudget.tableFooterView = UIView()
        }
    }

   
}
