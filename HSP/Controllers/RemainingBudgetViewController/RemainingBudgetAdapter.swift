//
//  StudentLandingAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 18/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class RemainingBudgetAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Variable Declaration
    var mTransactions = [Transactions]()
    var arrayColor = ["#ff6666", "#9aca40", "#4f8ef3", "#9900ff", "#ff9900"]
    private var mRemainingBudgetViewController: RemainingBudgetViewController!

    // MARK: - Constructor
    init (viewController: RemainingBudgetViewController, data: [Transactions]) {
        mTransactions = data
        mRemainingBudgetViewController = viewController
    }
    
    //MARK: TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RemainingBudgetCell", for: indexPath) as! RemainingBudgetCell
        
        cell.lblAmount.layer.borderWidth = 0.5
        cell.lblAmount.layer.borderColor = UIColor.lightGray.cgColor
        cell.lblAmount.text = "$" + String(mTransactions[indexPath.row].getAmount())
        
        cell.lblName.text = mTransactions[indexPath.row].getBudgetCategoryId().getCategoryId().getName()
        cell.lblNote.text = mTransactions[indexPath.row].getNote()
        if indexPath.row == 0{
            cell.lblDay.text = cell.convertDateFormateDay(date: mTransactions[indexPath.row].getDate())
            cell.lblMonthYear.text = cell.convertDateFormateMonth(date: mTransactions[indexPath.row].getDate())
            cell.imgDateView.isHidden = false
            cell.imgDate.isHidden = false
            cell.lblLine.isHidden = true
        }else {
            if mTransactions[indexPath.row].getDate() == mTransactions[indexPath.row - 1].getDate(){
                cell.imgDateView.isHidden = true
                cell.imgDate.isHidden = true
                cell.lblLine.isHidden = false
            }else {
                cell.lblDay.text = cell.convertDateFormateDay(date: mTransactions[indexPath.row].getDate())
                cell.lblMonthYear.text = cell.convertDateFormateMonth(date: mTransactions[indexPath.row].getDate())
                cell.imgDateView.isHidden = false
                cell.imgDate.isHidden = false
                cell.lblLine.isHidden = true
            }
        }
        
        let index = Int(arc4random_uniform(UInt32(arrayColor.count)))
        cell.imgDate.image = cell.imgDate.image!.withRenderingMode(.alwaysTemplate)
        cell.imgDate.tintColor = Utility.hexStringToUIColor(hex: arrayColor[index] , alpha: 1.0)
        
        cell.bEdit.tag = indexPath.row
        cell.bEdit.addTarget(self, action: #selector(self.btnEditOnClick), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Get Cell Label
        let homeViewController = mRemainingBudgetViewController.storyboard!.instantiateViewController(withIdentifier: "BudgetSetupViewController") as! BudgetSetupViewController
        mRemainingBudgetViewController.navigationController!.pushViewController(homeViewController, animated: true)
    }
    
    //EditOnClick
    func btnEditOnClick(sender: UIButton) {
        let sAddExpenseViewController = mRemainingBudgetViewController.storyboard!.instantiateViewController(withIdentifier: "AddExpenseViewController") as! AddExpenseViewController
        sAddExpenseViewController.addFlage = false
        sAddExpenseViewController.sTransactions = mTransactions[sender.tag]
        mRemainingBudgetViewController.navigationController!.pushViewController(sAddExpenseViewController, animated: true)
    }

}
