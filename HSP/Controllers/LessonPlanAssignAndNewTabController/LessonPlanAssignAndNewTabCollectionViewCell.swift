//
//  LessonPlanAssignAndNewTabCollectionViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanAssignAndNewTabCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnHeader: UIButton!
    @IBOutlet weak var lblChange: UILabel!
}
