//
//  LessonPlanAssignedViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class LessonPlanAssignedViewController: UIViewController, BaseView {
    
    @IBOutlet weak var tblLessonPlanAssigned: UITableView!
    
    //MARK:- Variable Declaration
    private var TAG : String = "LessonPlanAssignedViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mLessonPlanAssignedAdapter: LessonPlanAssignedAdapter!
    var mLessonPlanListingData: LessonPlanListingData?    
    private var mainVC: LessonPlanAssignAndNewTabController!
    private var mStudentListingData = [StudentListingData]()
    
    //TabSetup
    private var mPageIndex : Int = 0
    private var mPagerView: UIPageViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadView(pageIndex : Int,pagerView: UIPageViewController!,mainVC: LessonPlanAssignAndNewTabController) -> LessonPlanAssignedViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LessonPlanAssignedViewController") as! LessonPlanAssignedViewController
        vc.mPageIndex = mPageIndex
        vc.mPagerView = mPagerView
        vc.mLessonPlanListingData = mLessonPlanListingData
        vc.mainVC = mainVC
        return vc
    }
    
    //MARK: Getter Setter Methods
    public func getPageIndex() -> Int {
        return mPageIndex
    }
    
    //MARK: BaseView Method
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "LessonPlanAssignedVC-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
        /** Set Tableview Data **/
        callLessonAssignListingAPI()
    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //MARK: Call APIS.
    //MARK: API Calling Methods
    func callLessonAssignListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            print("LessonPlanId = \(String(describing: mLessonPlanListingData!.getId()))")
            let studentListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)lessonplans/getAssignedStudents/\(mLessonPlanListingData!.getId())")
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentListingRequest)")
            
            Alamofire.request(studentListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    debugPrint(response)
                    let statusCode = response.response!.statusCode
//                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
//                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let assignmentResponse = GetAssignmentResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        
                        if response.result.value != nil {
                            self.hideProgress()
                            self.setLessonPlanAssignListing(assignmentResponse: assignmentResponse!)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (assignmentResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (assignmentResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setLessonPlanAssignListing(assignmentResponse: GetAssignmentResponse) {
        hideProgress()
        
        if assignmentResponse.getData().count == 0 {
            UIStyle.showEmptyTableView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self.tblLessonPlanAssigned)
        } else {
            mStudentListingData.removeAll()
            for i in 0..<assignmentResponse.getData().count {
                for j in 0..<mainVC.mStudentListing.count {
                    if assignmentResponse.getData()[i].getStudentId() == mainVC.mStudentListing[j].getId() {
                        mainVC.mStudentListing[j].setStudentLessonPlanId(studentLessonPlanId: assignmentResponse.getData()[i].getStudentLessonPlanId())
                        mStudentListingData.append(mainVC.mStudentListing[j])
                        break
                    }
                }
            }
            
            mLessonPlanAssignedAdapter = LessonPlanAssignedAdapter(viewController: self, data: mStudentListingData)
            self.tblLessonPlanAssigned.delegate = self.mLessonPlanAssignedAdapter
            self.tblLessonPlanAssigned.dataSource = self.mLessonPlanAssignedAdapter
            self.tblLessonPlanAssigned.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblLessonPlanAssigned.tableFooterView = UIView()
            self.tblLessonPlanAssigned.reloadData()
        }
    }
    
    //CALL_STUDENT_DELETEAPI
    func callLessonPlanDeleteAPI(id: Int) {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            print("id = \(id)")
            //dy
            let myUrl = "\(mainURL)lessonplans/removeAssignment/\(self.mStudentListingData[id].getStudentLessonPlanId())"
            //stat
            //let myUrl = "\(mainURL)lessonplans/removeAssignment/33"
            let lessonPlanDeleteRequest = ServerRequest.authenticatePostRequest(url: myUrl)
            AppLog.debug(tag: self.TAG, msg: "URL = \(lessonPlanDeleteRequest)")
            AppLog.debug(tag: self.TAG, msg: "Header = \(String(describing: lessonPlanDeleteRequest.allHTTPHeaderFields))")
            
            Alamofire.request(lessonPlanDeleteRequest).responseJSON { response in
                debugPrint(response)
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    
                    let deleteServerResponse = ServerResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setLADeleteData(data: value as! NSDictionary, id: id)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (deleteServerResponse?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    
    func setLADeleteData(data: NSDictionary, id: Int) {
        hideProgress()
        AppLog.debug(tag: TAG, msg: "ResponseDeleteApi = \(data)")
        initialise()
       // self.mList.remove(at: id)
        //self.tblLessonPlan.reloadData()
        //let serverResponse = LessonPlanListingResponse(dictionary: data)
        //Utility.showTost(strMsg: (serverResponse?.getMessage())!, view: self.view)
        
    }

    
}
