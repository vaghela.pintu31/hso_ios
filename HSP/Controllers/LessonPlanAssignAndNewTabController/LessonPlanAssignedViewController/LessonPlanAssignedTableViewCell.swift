//
//  LessonPlanAssignedTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanAssignedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgLPlanAssigned: UIImageView!
    @IBOutlet weak var lblLPlanAssignName: UILabel!
    @IBOutlet weak var lblLPlanAssignGrade: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnDeleteAssLesson: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
