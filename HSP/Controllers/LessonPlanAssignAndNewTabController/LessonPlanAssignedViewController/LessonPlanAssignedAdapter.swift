//
//  LessonPlanAssignedAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanAssignedAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Variable Declaration
    private var mLessonPlanAssignedVC: LessonPlanAssignedViewController!
    private var mList = [StudentListingData]()
    
    // MARK: - Constructor
    init (viewController: LessonPlanAssignedViewController, data : [StudentListingData]) {
        mLessonPlanAssignedVC = viewController
        mList = data
    }
    
    //MARK: TableView Delegate Methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //Duration : 8 week - 4 days
        let frame = tableView.frame
        let headerView = UIView(frame: CGRect(x: 0,y: 0,width: frame.size.width,height: frame.size.height))
        headerView.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_EXTRALIGHT, alpha: 1.0)
        
        let headerLabel = UILabel(frame: CGRect(x:0, y: 0,width: tableView.frame.size.width,height: 30))
        
        headerLabel.textColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
        headerLabel.font = UIFont.init(name: "Nunito-Regular", size: 14)
        headerLabel.textAlignment = NSTextAlignment.center
        
        
        let strMessage =  "Duration : " + "\(String(describing: mLessonPlanAssignedVC.mLessonPlanListingData!.getWeeks())) week - \(String(describing: mLessonPlanAssignedVC.mLessonPlanListingData!.getDays())) days"
        let strWorkout = "Duration : "
        let range = (strMessage as NSString).range(of: strWorkout)
        let attributedString = NSMutableAttributedString(string:strMessage)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: Utility.hexStringToUIColor(hex: COLOR_PRIMARY_LIGHT, alpha: 1.0) , range: range)
        headerLabel.attributedText = attributedString
        headerView.addSubview(headerLabel)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LessonPlanAssignedTableViewCell", for: indexPath) as! LessonPlanAssignedTableViewCell
        
        cell.imgLPlanAssigned.layer.cornerRadius = cell.imgLPlanAssigned.frame.width/2
        cell.imgLPlanAssigned.clipsToBounds = true
        let iconName = mList[indexPath.row].getImageUrl()
        cell.imgLPlanAssigned.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        cell.imgLPlanAssigned.sd_setShowActivityIndicatorView(true)
        cell.imgLPlanAssigned.sd_setIndicatorStyle(.gray)
        
        //cell.imgLPlanAssigned.image = UIImage(named: iconName)
        cell.lblLPlanAssignName.text = mList[indexPath.row].getName()
        cell.lblLPlanAssignGrade.text = "Grade - " + mList[indexPath.row].getGrade()
        cell.btnDeleteAssLesson.tag = indexPath.row
        cell.btnDeleteAssLesson.addTarget(self, action: #selector(self.btnDeleteOnClick), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    func btnDeleteOnClick(sender: UIButton) {
        AlertDialog().showAlertWithYesNoButton(title: NSLocalizedString("alertTitle", comment: ""), subTitle: "Are you sure you want to delete this item", buttonTitle: NSLocalizedString("alertCancel", comment: ""), otherButtonTitle: NSLocalizedString("alertbtnYes", comment: ""), action: {(OtherButton) -> Void in
            print("IndexPath.row = \(sender.tag)")
            self.mLessonPlanAssignedVC.callLessonPlanDeleteAPI(id: sender.tag)
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}
