//
//  LessonPlanNewAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanNewAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK:- Variable Declaration
    private var mLessonPlanNewVC: LessonPlanNewViewController!
    private var mList = [StudentListingData]()
    public var mStudentId: Int?
    
    // MARK: - Constructor
    init (viewController: LessonPlanNewViewController, data : NSDictionary) {
        mLessonPlanNewVC = viewController
        let serverResponse = StudentListingResponse(dictionary: data)
        mList = (serverResponse?.getData())!
    }
    
    //MARK: TableView Delegate Methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //Duration : 8 week - 4 days
        let frame = tableView.frame
        let headerView = UIView(frame: CGRect(x: 0,y: 0,width: frame.size.width,height: frame.size.height))
        headerView.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_PRIMARY_EXTRALIGHT, alpha: 1.0)
        
        let headerLabel = UILabel(frame: CGRect(x:0, y: 0,width: tableView.frame.size.width,height: 30))
        
        headerLabel.textColor = Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0)
        headerLabel.font = UIFont.init(name: "Nunito-Regular", size: 14)
        headerLabel.textAlignment = NSTextAlignment.center
        
        let strMessage =  "Duration : " + "\(String(describing: mLessonPlanNewVC.mLessonPlanListingData!.getWeeks())) week - \(String(describing: mLessonPlanNewVC.mLessonPlanListingData!.getDays())) days"
        let strWorkout = "Duration : "
        let range = (strMessage as NSString).range(of: strWorkout)
        let attributedString = NSMutableAttributedString(string:strMessage)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: Utility.hexStringToUIColor(hex: COLOR_PRIMARY_LIGHT, alpha: 1.0) , range: range)
        headerLabel.attributedText = attributedString
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LessonPlanNewTableViewCell", for: indexPath) as! LessonPlanNewTableViewCell

        UIStyle.setAppButtonStyle(view: cell.btnLPlanNAssignLesson)
        UIStyle.setAppButtonStyleRounded(view: cell.imgLPlanNew)
        let iconName = mList[indexPath.row].getImageUrl()
        cell.imgLPlanNew.sd_setImage(with: URL(string: iconName), placeholderImage: UIImage(named: noImageFound))
        cell.imgLPlanNew.sd_setShowActivityIndicatorView(true)
        cell.imgLPlanNew.sd_setIndicatorStyle(.gray)
    
        cell.lblLPlanNName.text = mList[indexPath.row].getName()
        cell.lblLPlanNGrade.text = "Grade - " + mList[indexPath.row].getGrade()
        
        cell.btnLPlanNAssignLesson.tag = indexPath.row
        cell.btnLPlanNAssignLesson.addTarget(self, action: #selector(self.btnAssignLessonOnClick), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    
    func btnAssignLessonOnClick(sender: UIButton) {
        let lpSelectDaysVC = mLessonPlanNewVC.storyboard!.instantiateViewController(withIdentifier: "LessonPlanerSelectDaysViewController") as! LessonPlanerSelectDaysViewController
        lpSelectDaysVC.mLessonPlanListingData = mLessonPlanNewVC.mLessonPlanListingData
        lpSelectDaysVC.mStudentId = mList[sender.tag].getId()
        mLessonPlanNewVC.navigationController!.pushViewController(lpSelectDaysVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
