//
//  LessonPlanNewTableViewCell.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanNewTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgLPlanNew: UIImageView!
    @IBOutlet weak var lblLPlanNName: UILabel!
    @IBOutlet weak var lblLPlanNGrade: UILabel!
    @IBOutlet weak var btnLPlanNAssignLesson: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
