
//
//  LessonPlanViewController.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import Alamofire

class LessonPlanNewViewController: UIViewController, BaseView {
    
    @IBOutlet weak var tblLessonPlanNew: UITableView!

    //MARK:- Variable Declaration
    private var TAG : String = "LessonPlanNewViewController"
    private var mProgressIndicator:ProgressIndicator?
    private var mLessonPlanNewAdapter: LessonPlanNewAdapter!
    private var mList = [StudentListingData]()

    //TabSetup
    private var mPageIndex : Int = 1
    private var mPagerView: UIPageViewController!
    var mLessonPlanListingData: LessonPlanListingData?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadView(pageIndex : Int,pagerView: UIPageViewController!,mainVC: LessonPlanAssignAndNewTabController) -> LessonPlanNewViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LessonPlanNewViewController") as! LessonPlanNewViewController
        vc.mPageIndex = mPageIndex
        vc.mPagerView = mPagerView
        vc.mLessonPlanListingData = mLessonPlanListingData
        return vc
    }
    
    //MARK: Getter Setter Methods
    public func getPageIndex() -> Int {
        return mPageIndex
    }
    
    //MARK: BaseView Method
    func initialise() {
        AppLog.debug(tag: self.TAG, msg: "LessonPlanNewVC-Init")
        
        //ProgressIndicator init
        mProgressIndicator = ProgressIndicator(inview:self.view,loadingViewColor: UIColor.gray, indicatorColor: UIColor.black, msg: "")
        self.view.addSubview(mProgressIndicator!)
        
//        /** Set Tableview Data **/
//        setData()
        callStudentListingAPI()
    }
    
//    func setData() {
//        mList = User().setData()
//        mLessonPlanNewAdapter = LessonPlanNewAdapter(viewController: self, data: mList)
//        self.tblLessonPlanNew.delegate = self.mLessonPlanNewAdapter
//        self.tblLessonPlanNew.dataSource = self.mLessonPlanNewAdapter
//        self.tblLessonPlanNew.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
//        self.tblLessonPlanNew.tableFooterView = UIView()
    
//    }
    
    func showProgress() {
        mProgressIndicator?.start()
        self.view.addSubview(mProgressIndicator!)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideProgress() {
        mProgressIndicator?.stop()
        self.mProgressIndicator?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlert(msg: String) {
        Utility.showTost(strMsg: msg, view: self.view)
    }
    
    //MARK: API Calling Methods
    func callStudentListingAPI() {
        if Reachability.isConnectedToNetwork() == true {
            showProgress()
            
            let studentListingRequest = ServerRequest.authenticateGetRequest(url: "\(mainURL)students/index")
            AppLog.debug(tag: self.TAG, msg: "URL = \(studentListingRequest)")
            
            Alamofire.request(studentListingRequest).responseJSON { response in
                switch response.result {
                case .success:
                    let statusCode = response.response!.statusCode
                    AppLog.debug(tag: self.TAG, msg: "StatusCode = \(statusCode)")
                    AppLog.debug(tag: self.TAG, msg: "Response = \(String(describing: response.result.value))")
                    let studentListData = StudentListingResponse(dictionary: response.result.value as! NSDictionary)
                    if (statusCode == 200) {
                        if let value = response.result.value {
                            self.hideProgress()
                            self.setStudentListing(data: value as! NSDictionary)
                        } else {
                            let value = response.result.value
                            AppLog.debug(tag: self.TAG, msg: "Response_Other = \(String(describing: value))")
                            self.hideProgress()
                            self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                        }
                    } else {
                        let value = response.result.value
                        AppLog.debug(tag: self.TAG, msg: "Response_Se1 = \(String(describing: value))")
                        self.hideProgress()
                        self.showAlert(msg: HttpStatusCode().getErrorMessage(msg: (studentListData?.getMessage())!, statusCode: statusCode))
                    }
                case .failure(let encodingError):
                    AppLog.debug(tag: self.TAG, msg: "Failure:- \(encodingError.localizedDescription)")
                    self.hideProgress()
                    self.showAlert(msg: serverDown)
                }
            }
        } else {
            showAlert(msg: noInternet)
        }
    }
    
    func setStudentListing(data: NSDictionary) {
        hideProgress()
        mLessonPlanNewAdapter = LessonPlanNewAdapter(viewController: self, data: data)
        let serverResponse = StudentListingResponse(dictionary: data)
        mList = (serverResponse?.getData())!
        if mList.count == 0 {
            UIStyle.showEmptyTableView(message: NSLocalizedString("data_not_found", comment: ""), viewController: self.tblLessonPlanNew)
        } else {
            self.tblLessonPlanNew.delegate = self.mLessonPlanNewAdapter
            self.tblLessonPlanNew.dataSource = self.mLessonPlanNewAdapter
            self.tblLessonPlanNew.separatorColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)
            self.tblLessonPlanNew.tableFooterView = UIView()
        }
    }

    
}
