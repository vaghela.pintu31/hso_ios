//
//  LessonPlanAssignAndNewTabAdapter.swift
//  HSP
//
//  Created by Keyur Ashra on 28/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class LessonPlanAssignAndNewTabAdapter: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    var mLPAssignNewTabVC: LessonPlanAssignAndNewTabController!
    
    var pagerView: UIPageViewController!
    var tutorialViewController = [UIViewController]()
    var currentCotroller: UIViewController!
    var filterArray: NSArray!
    var animator: UIDynamicAnimator!
    var lessonPlanANCollectionView: UICollectionView!
    var screenSize: CGRect!
    var screenHeight: CGFloat!
    
    // MARK: - Constructor
    init (viewController: LessonPlanAssignAndNewTabController) {
        mLPAssignNewTabVC = viewController
    }
    
    var currentPageIndex : Int = 0 {
        didSet {
            currentPageIndex = cap(pageIndex: currentPageIndex)
        }
    }
    private var nextPageIndex : Int = 0

    //MARK:- CollectionView Delegate Methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tutorialViewController.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LessonPlanAssignAndNewTabCollectionViewCell", for: indexPath as IndexPath) as! LessonPlanAssignAndNewTabCollectionViewCell
        lessonPlanANCollectionView = collectionView
        if indexPath.row == currentPageIndex {
            cell.btnHeader.setTitle("\(filterArray[indexPath.row])", for: UIControlState.normal)
            cell.lblChange.isHidden = false
        }else {
            cell.btnHeader.setTitle("\(filterArray[indexPath.row])", for: UIControlState.normal)
        }
        cell.btnHeader.tag = indexPath.row
        cell.btnHeader.addTarget(self, action: #selector(LessonPlanAssignAndNewTabAdapter.buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        return cell

    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width: CGFloat((screenWidth / 2)), height: CGFloat(50))
    }
    
    //HeaderButtonClick
    func buttonClicked(sender: UIButton) {
        if currentPageIndex != sender.tag {
            if currentPageIndex < sender.tag {
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                lessonPlanANCollectionView.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.right, animated: true)
                pagerView.setViewControllers([pageForindex(pageIndex: sender.tag)!], direction: .forward, animated: true, completion: nil)
            } else if currentPageIndex > sender.tag {
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                lessonPlanANCollectionView.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.left, animated: true)
                pagerView.setViewControllers([pageForindex(pageIndex: sender.tag)!], direction: .reverse, animated: true, completion: nil)
            }
            currentPageIndex = sender.tag
            chageCollectionCell(index: currentPageIndex)
        }
    }
    
    //MARK:- PageViewSwip
    func cap(pageIndex : Int) -> Int {
        return pageIndex
    }
    
    func carrouselJump() {
        currentPageIndex += 1
        pagerView.setViewControllers([self.pageForindex(pageIndex: currentPageIndex)!], direction: .forward, animated: true, completion: nil)
    }
    
    func pageForindex(pageIndex : Int) -> UIViewController? {
        guard (pageIndex < tutorialViewController.count) && (pageIndex>=0) else {
            return nil
        }
        
        print("pageIndex :- \(pageIndex)")
        if pageIndex == 0{
            let currentViewController = mLPAssignNewTabVC!.storyboard!.instantiateViewController(withIdentifier: "AddStudentViewController") as! AddStudentViewController
            currentPageIndex = pageIndex
            return currentViewController.loadView(pageIndex: pageIndex,pagerView: pagerView!)
        }else {
            let currentViewController = mLPAssignNewTabVC!.storyboard!.instantiateViewController(withIdentifier: "StudentAttendenceViewController") as! StudentAttendenceViewController
            currentPageIndex = pageIndex
            return currentViewController.loadView(pageIndex: pageIndex,pagerView: pagerView!)
        }
    }
    
    func indexForPage(vc : UIViewController) -> Int {
        if let vc = vc as? StudentAttendenceViewController {
            return vc.pageIndex
        } else if let vc = vc as? AddStudentViewController {
            return vc.pageIndex
        } else {
            preconditionFailure("VCPagImageSlidesTutorial page is not a VCTutorialImagePage")
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,viewControllerAfter viewController: UIViewController) -> UIViewController?{
        return pageForindex(pageIndex: cap(pageIndex: indexForPage(vc: viewController)+1))
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        return pageForindex(pageIndex: cap(pageIndex: indexForPage(vc: viewController)-1))
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        nextPageIndex = indexForPage(vc: pendingViewControllers.first!)
        chageCollectionCell(index: nextPageIndex)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !finished { return }
        currentPageIndex = nextPageIndex
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func pageViewSetup() {
        let studentProfileViewController = mLPAssignNewTabVC!.storyboard!.instantiateViewController(withIdentifier: "AddStudentViewController") as! AddStudentViewController
        let vcObjectProfile = studentProfileViewController.loadView(pageIndex: 0,pagerView: UIPageViewController()) as UIViewController
        tutorialViewController.append(vcObjectProfile)
        
        let studentAttendenceViewController = mLPAssignNewTabVC!.storyboard!.instantiateViewController(withIdentifier: "StudentAttendenceViewController") as! StudentAttendenceViewController
        let vcObjectAttendence = studentAttendenceViewController.loadView(pageIndex: 1,pagerView: UIPageViewController()) as UIViewController
        tutorialViewController.append(vcObjectAttendence)
        
        pagerView = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pagerView.view.backgroundColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        pagerView.view.frame = CGRect(x: CGFloat(0), y: CGFloat(114), width: CGFloat(screenWidth), height: CGFloat(screenHeight - 114))
        pagerView.dataSource=self
        pagerView.delegate=self
        pagerView.setViewControllers([pageForindex(pageIndex: currentPageIndex)!], direction: .forward, animated: false, completion: nil)
        mLPAssignNewTabVC!.addChildViewController(pagerView)
        mLPAssignNewTabVC!.view.addSubview(pagerView!.view)
        pagerView.didMove(toParentViewController: mLPAssignNewTabVC!)
    }

    
    func chageCollectionCell(index: Int){
        for cell in lessonPlanANCollectionView.visibleCells as! [LessonPlanAssignAndNewTabCollectionViewCell] {
            cell.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_COLLECTIONVIEW, alpha: 1.0)
            cell.lblChange.isHidden = true
            cell.btnHeader.titleLabel?.textColor = UIColor.white.withAlphaComponent(0.5)
        }
        
        let indexPath = NSIndexPath(row: index, section: 0)
        let cell = lessonPlanANCollectionView.cellForItem(at: indexPath as IndexPath) as! LessonPlanAssignAndNewTabCollectionViewCell
        cell.lblChange.isHidden = false
        cell.btnHeader.titleLabel?.textColor = UIColor.white.withAlphaComponent(1.0)
        
        UIView.animate(withDuration: 1.0, animations: {
            let animation: CATransition = CATransition()
            animation.duration = 0.6
            animation.type = kCATransitionFromRight
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            cell.lblChange.layer.add(animation, forKey: "changeTextTransition")
        }, completion: nil)
        lessonPlanANCollectionView.scrollToItem(at: indexPath as IndexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
    }
}
