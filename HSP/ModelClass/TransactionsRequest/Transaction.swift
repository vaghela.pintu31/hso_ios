/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Transaction {
    private var amount : Int?
    private var note : String?
    private var budgetCategoryId : Int?
    
    
   
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let transaction_list = Transaction.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Transaction Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Transaction]
    {
        var models:[Transaction] = []
        for item in array
        {
            models.append(Transaction(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let transaction = Transaction(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Transaction Instance.
*/
	required public init?(dictionary: NSDictionary) {

		amount = dictionary["amount"] as? Int
		note = dictionary["note"] as? String
		budgetCategoryId = dictionary["budgetCategoryId"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    
    init() {
        
    }
    
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.amount, forKey: "amount")
		dictionary.setValue(self.note, forKey: "note")
		dictionary.setValue(self.budgetCategoryId, forKey: "budgetCategoryId")

		return dictionary
	}
    
    //Amount
    public func getAmount() -> Int {
        return amount!
    }
    
    public func setAmount(amount : Int) {
        self.amount = amount
    }
    
    //Note
    public func getNote() -> String {
        return note!
    }
    
    public func setNote(note : String) {
        self.note = note
    }
    
    //BudgetCategoryId
    public func getBudgetCategoryId() -> Int {
        return budgetCategoryId!
    }
    
    public func setBudgetCategoryId(budgetCategoryId : Int) {
        self.budgetCategoryId = budgetCategoryId
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        return dictionary
    }

}
