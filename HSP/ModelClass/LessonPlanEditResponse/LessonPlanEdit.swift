/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class LessonPlanEdit {    
    private var userId : Int?
    private var name : String?
    private var weeks : Int?
    private var days : Int?
    private var hours : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var lessonAssignmentView = [LessonAssignmentView]()
    private var lessonNoteView = [LessonNoteView]()

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Json4Swift_Base Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [LessonPlanEdit]
    {
        var models:[LessonPlanEdit] = []
        for item in array
        {
            models.append(LessonPlanEdit(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Json4Swift_Base Instance.
*/
    init() {
        
    }
    
	required public init?(dictionary: NSDictionary) {

		userId = dictionary["userId"] as? Int
		name = dictionary["name"] as? String
		weeks = dictionary["weeks"] as? Int
		days = dictionary["days"] as? Int
		hours = dictionary["hours"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
        lessonAssignmentView = LessonAssignmentView.modelsFromDictionaryArray(array:dictionary["lessonAssignment"] as! NSArray)
        lessonNoteView = LessonNoteView.modelsFromDictionaryArray(array: dictionary["lessonNote"] as! NSArray)
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.weeks, forKey: "weeks")
		dictionary.setValue(self.days, forKey: "days")
		dictionary.setValue(self.hours, forKey: "hours")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")

		return dictionary
	}
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                
                print("key :- \(key)")
                print("value :- \(child.value)")
                
                if key == "lessonAssignmentView" {
                    let lessonArray = child.value as! [LessonAssignmentView]
                    dictionary["lessonAssignment"] = lessonArray.map({
                        (value: LessonAssignmentView) -> NSDictionary in
                        return value.dictionaryRepresentation()
                    })
                } else if key == "lessonNoteView" {
                    let lessonNoteArray = child.value as! [LessonNoteView]
                    dictionary["lessonNote"] = lessonNoteArray.map({
                        (value: LessonNoteView) -> NSDictionary in
                        return value.dictionaryRepresentation()
                    })
                } else {
                    dictionary[key] = child.value
                }
            }
        }
        return dictionary
    }
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //Name
    public func getName() -> String {
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }
    
    //Weeks
    public func getWeeks() -> Int {
        return weeks!
    }
    
    public func setWeeks(weeks : Int) {
        self.weeks = weeks
    }
    
    //Days
    public func getDays() -> Int {
        return days!
    }
    
    public func setDays(days : Int) {
        self.days = days
    }
    
    //Hours
    public func getHours() -> Int {
        return hours!
    }
    
    public func setHours(hours : Int) {
        self.hours = hours
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //LessonAssignment
    public func getLessonAssignmentView() -> [LessonAssignmentView] {
        return lessonAssignmentView
    }
    
    public func setLessonAssignmentView(lessonAssignment : [LessonAssignmentView]) {
        self.lessonAssignmentView = lessonAssignment
    }
    
    //LessonNote
    public func getLessonNoteView() -> [LessonNoteView] {
        return lessonNoteView
    }
    
    public func setLessonNoteView(lessonNote : [LessonNoteView]) {
        self.lessonNoteView = lessonNote
    }

}
