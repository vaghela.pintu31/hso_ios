//
//  AutoComplete.swift
//  HSP
//
//  Created by Keyur Ashra on 27/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import Foundation

public class AutoComplete {
    private var placeId: String?
    private var primaryText: NSAttributedString?
    private var secondaryText: NSAttributedString?
    private var fullText: NSAttributedString?

    
    //PlaceId
    public func getPlaceId() -> String {
        return placeId!
    }
    
    public func setPlaceId(placeId : String) {
        self.placeId = placeId
    }
    
    //PrimaryText
    public func getPrimaryText() -> NSAttributedString {
        return primaryText!
    }
    
    public func setPrimaryText(primaryText : NSAttributedString) {
        self.primaryText = primaryText
    }
    
    //SecondaryText
    public func getSecondaryText() -> NSAttributedString {
        return secondaryText!
    }
    
    public func setSecondaryText(secondaryText : NSAttributedString) {
        self.secondaryText = secondaryText
    }
    
    //FullText
    public func getFullText() -> NSAttributedString {
        return fullText!
    }
    
    public func setFullText(fullText : NSAttributedString) {
        self.fullText = fullText
    }

    
}
