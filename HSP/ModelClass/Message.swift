//
//  Messages.swift
//  HSP
//
//  Created by Keyur Ashra on 11/05/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class Message: NSObject {
    private var chatId : String?
    private var messageBody : String?
    private var type : String?
    private var creatorId : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var id : String?
    
    
    //ChatId
    public func getChatId() -> String {
        return chatId!
    }
    
    public func setChatId(chatId : String) {
        self.chatId = chatId
    }
    
    //MessageBody
    public func getMessageBody() -> String {
        return messageBody!
    }
    
    public func setMessageBody(messageBody : String) {
        self.messageBody = messageBody
    }
    
    //Type
    public func getType() -> String {
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //CreatorId
    public func getCreatorId() -> Int {
        return creatorId!
    }
    
    public func setCreatorId(creatorId : Int) {
        self.creatorId = creatorId
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Id
    public func getId() -> String {
        return id!
    }
    
    public func setId(id : String) {
        self.id = id
    }
}
