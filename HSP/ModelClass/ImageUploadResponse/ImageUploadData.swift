/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ImageUploadData {
    private var name : String?
    private var model : String?
    private var entityId : Int?
    private var userId : Int?
    private var hash : String?
    private var meta : ImageUploadMeta?
    private var createdAt : String?
    private var updatedAt : String?
    private var id : Int?
    private var imageUrl : String?
    
    
    //Name
    public func getName() -> String {
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }
    
    //Model
    public func getModel() -> String {
        return model!
    }
    
    public func setModel(model : String) {
        self.model = model
    }
    
    //EntityId
    public func getEntityId() -> Int {
        return entityId!
    }
    
    public func setEntityId(entityId : Int) {
        self.entityId = entityId
    }
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //Hash
    public func getHash() -> String {
        return hash!
    }
    
    public func setHash(hash : String) {
        self.hash = hash
    }
    
    //Meta
    public func getMeta() -> ImageUploadMeta {
        return meta!
    }
    
    public func setMeta(meta : ImageUploadMeta) {
        self.meta = meta
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ImageUploadData]
    {
        var models:[ImageUploadData] = []
        for item in array
        {
            models.append(ImageUploadData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		name = dictionary["name"] as? String
		model = dictionary["model"] as? String
		entityId = dictionary["entityId"] as? Int
		userId = dictionary["userId"] as? Int
		hash = dictionary["hash"] as? String
		if (dictionary["meta"] != nil) { meta = ImageUploadMeta(dictionary: dictionary["meta"] as! NSDictionary) }
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		id = dictionary["id"] as? Int
		imageUrl = dictionary["imageUrl"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.model, forKey: "model")
		dictionary.setValue(self.entityId, forKey: "entityId")
		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.hash, forKey: "hash")
		dictionary.setValue(self.meta?.dictionaryRepresentation(), forKey: "meta")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.imageUrl, forKey: "imageUrl")

		return dictionary
	}

}
