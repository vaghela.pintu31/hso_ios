/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class UserView {
    private var mediaId : Int?
    private var email : String?
    private var firstName : String?
    private var lastName : String?
    private var signupType : String?
    private var countryCode : Int?
    private var country : String?
    private var contactNumber : String?
    private var zipCode : String?
    private var city : String?
    private var state : String?
    private var imageId : String?
    private var dob : String?
    private var verified : Bool?
    private var gender : String?
    private var address : String?
    private var id : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var imageUrl : String?
    private var name : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let user_list = User.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of User Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [UserView]
    {
        var models:[UserView] = []
        for item in array
        {
            models.append(UserView(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let user = User(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: User Instance.
*/
	required public init?(dictionary: NSDictionary) {

		mediaId = dictionary["mediaId"] as? Int
		email = dictionary["email"] as? String
		firstName = dictionary["firstName"] as? String
		lastName = dictionary["lastName"] as? String
		signupType = dictionary["signupType"] as? String
		countryCode = dictionary["countryCode"] as? Int
        country = dictionary["country"] as? String
		contactNumber = dictionary["contactNumber"] as? String
		zipCode = dictionary["zipCode"] as? String
		city = dictionary["city"] as? String
		state = dictionary["state"] as? String
		imageId = dictionary["imageId"] as? String
		dob = dictionary["dob"] as? String
		gender = dictionary["gender"] as? String
		address = dictionary["address"] as? String
		id = dictionary["id"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		imageUrl = dictionary["imageUrl"] as? String
		name = dictionary["name"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.mediaId, forKey: "mediaId")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.firstName, forKey: "firstName")
		dictionary.setValue(self.lastName, forKey: "lastName")
		dictionary.setValue(self.signupType, forKey: "signupType")
		dictionary.setValue(self.countryCode, forKey: "countryCode")
        dictionary.setValue(self.country, forKey: "country")
		dictionary.setValue(self.contactNumber, forKey: "contactNumber")
		dictionary.setValue(self.zipCode, forKey: "zipCode")
		dictionary.setValue(self.city, forKey: "city")
		dictionary.setValue(self.state, forKey: "state")
		dictionary.setValue(self.imageId, forKey: "imageId")
		dictionary.setValue(self.dob, forKey: "dob")
		dictionary.setValue(self.verified, forKey: "verified")
		dictionary.setValue(self.gender, forKey: "gender")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.imageUrl, forKey: "imageUrl")
		dictionary.setValue(self.name, forKey: "name")

		return dictionary
	}
    
    //MediaId
    public func getMediaId() -> Int {
        return mediaId!
    }
    
    public func setMediaId(mediaId : Int) {
        self.mediaId = mediaId
    }
    
    //Email
    public func getEmail() -> String {
        return email!
    }
    
    public func setEmail(email : String) {
        self.email = email
    }
    
    //FirstName
    public func getFirstName() -> String {
        if firstName == nil {
            firstName = ""
        }
        return firstName!
    }
    
    public func setFirstName(firstName : String) {
        self.firstName = firstName
    }
    
    //LastName
    public func getLastName() -> String {
        if lastName == nil {
            lastName = ""
        }
        return lastName!
    }
    
    public func setLastName(lastName : String) {
        self.lastName = lastName
    }
    
    //SignupType
    public func getSignupType() -> String {
        if signupType == nil {
            signupType = ""
        }
        return signupType!
    }
    
    public func setSignupType(signupType : String) {
        self.signupType = signupType
    }
    
    //CountryCode
    public func getCountryCode() -> Int {
        return countryCode!
    }
    
    public func setCountryCode(countryCode : Int) {
        self.countryCode = countryCode
    }
    
    //Country
    public func getCountry() -> String {
        if country == nil {
            country = ""
        }
        return country!
    }
    
    public func setCountry(country : String) {
        self.country = country
    }
    
    //ContactNumber
    public func getContactNumber() -> String {
        if contactNumber == nil {
            contactNumber = ""
        }
        return contactNumber!
    }
    
    public func setContactNumber(contactNumber : String) {
        self.contactNumber = contactNumber
    }
    
    //ZipCode
    public func getZipCode() -> String {
        if zipCode == nil {
            zipCode = ""
        }
        return zipCode!
    }
    
    public func setZipCode(zipCode : String) {
        self.zipCode = zipCode
    }
    
    //City
    public func getCity() -> String {
        if city == nil {
            city = ""
        }
        return city!
    }
    
    public func setCity(city : String) {
        self.city = city
    }
    
    //State
    public func getState() -> String {
        if state == nil {
            state = ""
        }
        return state!
    }
    
    public func setState(state : String) {
        self.state = state
    }
    
    //ImageId
    public func getImageId() -> String {
        if imageId == nil {
            imageId = ""
        }
        return imageId!
    }
    
    public func setImageId(imageId : String) {
        self.imageId = imageId
    }
    
    //Dob
    public func getDob() -> String {
        if dob == nil {
            dob = ""
        }
        return dob!
    }
    
    public func setDob(dob : String) {
        self.dob = dob
    }
    
    //Verified
    public func getVerified() -> Bool {
        return verified!
    }
    
    public func setVerified(verified : Bool) {
        self.verified = verified
    }
    
    //Gender
    public func getGender() -> String {
        if gender == nil {
            return ""
        } else if gender == "M" {
            return "Male"
        } else if gender == "F" {
            return "Female"
        } else {
            return gender!
        }
    }
    
    public func setGender(gender : String) {
        self.gender = gender
    }
    
    //Address
    public func getAddress() -> String {
        if address == nil {
            address = ""
        }
        return address!
    }
    
    public func setAddress(address : String) {
        self.address = address
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        if createdAt == nil {
            createdAt = ""
        }
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        if updatedAt == nil {
            updatedAt = ""
        }
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        if imageUrl == nil {
            imageUrl = ""
        }
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    
    //Name
    public func getName() -> String {
        if name == nil {
            name = ""
        }
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }

}
