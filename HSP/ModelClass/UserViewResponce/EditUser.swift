/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class EditUser {
    private var mediaId : Int?
    private var email : String?
    private var firstName : String?
    private var lastName : String?
    private var signupType : String?
    private var password : String?
    private var countryCode : Int?
    private var country : String?
    private var contactNumber : String?
    private var zipCode : String?
    private var city : String?
    private var state : String?
    private var dob : String?
    private var gender : String?
    private var address : String?
    private var id : Int?
    private var imageUrl : String?
    


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let user_list = User.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of User Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [EditUser]
    {
        var models:[EditUser] = []
        for item in array
        {
            models.append(EditUser(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let user = User(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: User Instance.
*/
    
    init() {
        
    }
    
	required public init?(dictionary: NSDictionary) {

		mediaId = dictionary["mediaId"] as? Int
		email = dictionary["email"] as? String
		firstName = dictionary["firstName"] as? String
		lastName = dictionary["lastName"] as? String
		signupType = dictionary["signupType"] as? String
		password = dictionary["password"] as? String
		countryCode = dictionary["countryCode"] as? Int
        country = dictionary["country"] as? String
		contactNumber = dictionary["contactNumber"] as? String
		zipCode = dictionary["zipCode"] as? String
		city = dictionary["city"] as? String
		state = dictionary["state"] as? String
		dob = dictionary["dob"] as? String
		gender = dictionary["gender"] as? String
		address = dictionary["address"] as? String
		id = dictionary["id"] as? Int
		imageUrl = dictionary["imageUrl"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.mediaId, forKey: "mediaId")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.firstName, forKey: "firstName")
		dictionary.setValue(self.lastName, forKey: "lastName")
		dictionary.setValue(self.signupType, forKey: "signupType")
		dictionary.setValue(self.password, forKey: "password")
		dictionary.setValue(self.countryCode, forKey: "countryCode")
        dictionary.setValue(self.country, forKey: "country")
		dictionary.setValue(self.contactNumber, forKey: "contactNumber")
		dictionary.setValue(self.zipCode, forKey: "zipCode")
		dictionary.setValue(self.city, forKey: "city")
		dictionary.setValue(self.state, forKey: "state")
		dictionary.setValue(self.dob, forKey: "dob")
		dictionary.setValue(self.gender, forKey: "gender")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.imageUrl, forKey: "imageUrl")

		return dictionary
	}

    
    //MediaId
    public func getMediaId() -> Int {
        return mediaId!
    }
    
    public func setMediaId(mediaId : Int) {
        self.mediaId = mediaId
    }
    
    //Email
    public func getEmail() -> String {
        return email!
    }
    
    public func setEmail(email : String) {
        self.email = email
    }
    
    //FirstName
    public func getFirstName() -> String {
        return firstName!
    }
    
    public func setFirstName(firstName : String) {
        self.firstName = firstName
    }
    
    //LastName
    public func getLastName() -> String {
        return lastName!
    }
    
    public func setLastName(lastName : String) {
        self.lastName = lastName
    }
    
    //SignupType
    public func getSignupType() -> String {
        return signupType!
    }
    
    public func setSignupType(signupType : String) {
        self.signupType = signupType
    }
    
    //Password
    public func getPassword() -> String {
        return password!
    }
    
    public func setPassword(password : String) {
        self.password = password
    }
    
    //CountryCode
    public func getCountryCode() -> Int {
        return countryCode!
    }
    
    public func setCountryCode(countryCode : Int) {
        self.countryCode = countryCode
    }
    
    //Country
    public func getCountry() -> String {
        if country == nil {
            country = ""
        }
        return country!
    }
    
    public func setCountry(country : String) {
        self.country = country
    }
    
    //ContactNumber
    public func getContactNumber() -> String {
        return contactNumber!
    }
    
    public func setContactNumber(contactNumber : String) {
        self.contactNumber = contactNumber
    }
    
    //ZipCode
    public func getZipCode() -> String {
        return zipCode!
    }
    
    public func setZipCode(zipCode : String) {
        self.zipCode = zipCode
    }
    
    //City
    public func getCity() -> String {
        return city!
    }
    
    public func setCity(city : String) {
        self.city = city
    }
    
    //State
    public func getState() -> String {
        return state!
    }
    
    public func setState(state : String) {
        self.state = state
    }
    
    //Dob
    public func getDob() -> String {
        return dob!
    }
    
    public func setDob(dob : String) {
        self.dob = dob
    }
    
    //Gender
    public func getGender() -> String {
        if gender == nil {
            return ""
        } else if gender == "M" {
            return "Male"
        } else if gender == "F" {
            return "Female"
        } else {
            return gender!
        }
    }
    
    public func setGender(gender : String) {
        self.gender = gender
    }
    
    //Address
    public func getAddress() -> String {
        return address!
    }
    
    public func setAddress(address : String) {
        self.address = address
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }

    
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        return dictionary
    }
}
