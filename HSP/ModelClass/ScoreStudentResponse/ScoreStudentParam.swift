//
//  ScoreStudentParam.swift
//  HSP
//
//  Created by Keyur Ashra on 01/05/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import Foundation

public class ScoreStudentParam {
    
    private var score : String?
    private var studentAssignmentId = [Int]()
    
    
    //Score
    public func getScore() -> String {
        return score!
    }
    
    public func setScore(score : String) {
        self.score = score
    }
    
    //StudentAssignmentId
    public func getStudentAssignmentId() -> [Int] {
        return studentAssignmentId
    }
    
    public func setStudentAssignmentId(studentAssignmentId : [Int]) {
        self.studentAssignmentId = studentAssignmentId
    }
    
    
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        return dictionary
    }
    
}
