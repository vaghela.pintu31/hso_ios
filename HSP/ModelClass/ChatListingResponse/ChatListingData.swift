/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ChatListingData {
    private var messages = [ChatListingMessages]()
    private var userchats = [ChatListingUserchats]()
    public var userGroupInformation = [UserGroupInformation]()

    private var type : String?
    private var name : String?
    private var creatorId : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var id : String?
    private var unread : Int?
    private var canSendMessage : Bool?
    
    //ADDNEW
    private var imageUrl : String?
    
    //ImageUrl
    public func getImageUrl() -> String {
        if imageUrl == nil {
            return ""
        }
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    
    
    //Messages
    public func getMessages() -> [ChatListingMessages] {
        return messages
    }
    
    public func setMessages(messages : [ChatListingMessages]) {
        self.messages = messages
    }
    
    //Userchats
    public func getUserchats() -> [ChatListingUserchats] {
        return userchats
    }
    
    public func setUserchats(userchats : [ChatListingUserchats]) {
        self.userchats = userchats
    }
    
    //Userchats
    public func getUserGroupInformation() -> [UserGroupInformation] {
        return userGroupInformation
    }
    
    public func setUserGroupInformation(userGroupInformation : [UserGroupInformation]) {
        self.userGroupInformation = userGroupInformation
    }
    
    //Type
    public func getType() -> String {
        if type == nil {
            return ""
        }
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //Name
    public func getName() -> String {
        if name == nil {
            return ""
        }
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }
    
    //CreatorId
    public func getCreatorId() -> Int {
        return creatorId!
    }
    
    public func setCreatorId(creatorId : Int) {
        self.creatorId = creatorId
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Id
    public func getId() -> String {
        return id!
    }
    
    public func setId(id : String) {
        self.id = id
    }
    
    //Unread
    public func getUnread() -> Int {
        return unread!
    }
    
    public func setUnread(unread : Int) {
        self.unread = unread
    }
    
    //CanSendMessage
    public func getCanSendMessage() -> Bool {
        return canSendMessage!
    }
    
    public func setCanSendMessage(canSendMessage : Bool) {
        self.canSendMessage = canSendMessage
    }


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ChatListingData]
    {
        var models:[ChatListingData] = []
        for item in array
        {
            models.append(ChatListingData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
    init() { }
    
	required public init?(dictionary: NSDictionary) {

		if (dictionary["messages"] != nil) { messages = ChatListingMessages.modelsFromDictionaryArray(array: dictionary["messages"] as! NSArray) }
		if (dictionary["userchats"] != nil) { userchats = ChatListingUserchats.modelsFromDictionaryArray(array: dictionary["userchats"] as! NSArray) }
        if (dictionary["users"] != nil) { userGroupInformation = UserGroupInformation.modelsFromDictionaryArray(array: dictionary["users"] as! NSArray) }
		type = dictionary["type"] as? String
		name = dictionary["name"] as? String
		creatorId = dictionary["creatorId"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		id = dictionary["id"] as? String
		unread = dictionary["unread"] as? Int
		canSendMessage = dictionary["canSendMessage"] as? Bool
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.creatorId, forKey: "creatorId")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.unread, forKey: "unread")
		dictionary.setValue(self.canSendMessage, forKey: "canSendMessage")

		return dictionary
	}

}
