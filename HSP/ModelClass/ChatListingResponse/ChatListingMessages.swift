/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ChatListingMessages {
    private var chatId : String?
    private var messageBody : String?
    private var type : String?
    private var creatorId : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var id : String?
    private var textHight : CGFloat = 0
    private var userId : Int?
    
    //Id
    public func getUserId() -> Int {
        if userId == nil{
            userId = 0
        }
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //ChatId
    public func getTextHight() -> CGFloat {
        return textHight
    }
    
    public func setTextHight(textHight : CGFloat) {
        self.textHight = textHight
    }
    
    //ChatId
    public func getChatId() -> String {
        return chatId!
    }
    
    public func setChatId(chatId : String) {
        self.chatId = chatId
    }
    
    //MessageBody
    public func getMessageBody() -> String {
        if messageBody == nil {
            return ""
        }
        return messageBody!
    }
    
    public func setMessageBody(messageBody : String) {
        self.messageBody = messageBody
    }
    
    //Type
    public func getType() -> String {
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //CreatorId
    public func getCreatorId() -> Int {
        return creatorId!
    }
    
    public func setCreatorId(creatorId : Int) {
        self.creatorId = creatorId
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Id
    public func getId() -> String {
        return id!
    }
    
    public func setId(id : String) {
        self.id = id
    }

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let messages_list = Messages.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Messages Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ChatListingMessages]
    {
        var models:[ChatListingMessages] = []
        for item in array
        {
            models.append(ChatListingMessages(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let messages = Messages(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Messages Instance.
*/
	required public init?(dictionary: NSDictionary) {

		chatId = dictionary["chatId"] as? String
		messageBody = dictionary["messageBody"] as? String
		type = dictionary["type"] as? String
		creatorId = dictionary["creatorId"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		id = dictionary["id"] as? String
        userId = dictionary["userId"] as? Int

	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.chatId, forKey: "chatId")
		dictionary.setValue(self.messageBody, forKey: "messageBody")
		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.creatorId, forKey: "creatorId")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.userId, forKey: "userId")

		return dictionary
	}

}
