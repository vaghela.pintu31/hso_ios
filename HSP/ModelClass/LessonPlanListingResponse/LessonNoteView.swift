/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class LessonNoteView {
    private var title : String?
    private var lessonId : Int?
    private var week : Int?
    private var day : Int?
    private var userId : Int?
    private var id : Int?
    private var createdAt : String?
    private var updatedAt : String?
    
    
    //Title
    public func getTitle() -> String {
        if title == nil {
            title = ""
        }
        return title!
    }
    
    public func setTitle(title : String) {
        self.title = title
    }
    
    //LessonId
    public func getLessonId() -> Int {
        return lessonId!
    }
    
    public func setLessonId(lessonId : Int) {
        self.lessonId = lessonId
    }
    
    //Week
    public func getWeek() -> Int {
        return week!
    }
    
    public func setWeek(week : Int) {
        self.week = week
    }
    
    //Day
    public func getDay() -> Int {
        return day!
    }
    
    public func setDay(day : Int) {
        self.day = day
    }
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let lessonNote_list = LessonNote.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of LessonNote Instances.
*/
    init() {
        
        
    }
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [LessonNoteView]
    {
        var models:[LessonNoteView] = []
        for item in array
        {
            models.append(LessonNoteView(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let lessonNote = LessonNote(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: LessonNote Instance.
*/
	required public init?(dictionary: NSDictionary) {

		title = dictionary["title"] as? String
		lessonId = dictionary["lessonId"] as? Int
		week = dictionary["week"] as? Int
		day = dictionary["day"] as? Int
		userId = dictionary["userId"] as? Int
		id = dictionary["id"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.lessonId, forKey: "lessonId")
		dictionary.setValue(self.week, forKey: "week")
		dictionary.setValue(self.day, forKey: "day")
		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")

		return dictionary
	}
    
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        return dictionary
    }

}
