/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class StudentBudgets {
//	public var userId : Int?
//	public var studentId : StudentId?
//	public var type : String?
//	public var remainingAmount : Int?
//	public var spentAmount : Int?
//	public var id : Int?
//	public var createdAt : String?
//	public var updatedAt : String?

    private var userId : Int?
    private var studentId : StudentId?
    private var type : String?
    private var remainingAmount : Int?
    private var spentAmount : Int?
    private var id : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var imgeUrl : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let studentBudgets_list = StudentBudgets.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of StudentBudgets Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [StudentBudgets]
    {
        var models:[StudentBudgets] = []
        for item in array
        {
            models.append(StudentBudgets(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let studentBudgets = StudentBudgets(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: StudentBudgets Instance.
*/
	required public init?(dictionary: NSDictionary) {

		userId = dictionary["userId"] as? Int
		if (dictionary["studentId"] != nil) { studentId = StudentId(dictionary: dictionary["studentId"] as! NSDictionary) }
		type = dictionary["type"] as? String
		remainingAmount = dictionary["remainingAmount"] as? Int
		spentAmount = dictionary["spentAmount"] as? Int
		id = dictionary["id"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
        imgeUrl = dictionary["imgurl"] as? String

	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    
    init() {
        
    }
    
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.studentId?.dictionaryRepresentation(), forKey: "studentId")
		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.remainingAmount, forKey: "remainingAmount")
		dictionary.setValue(self.spentAmount, forKey: "spentAmount")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
        dictionary.setValue(self.imgeUrl, forKey: "imgurl")


		return dictionary
	}
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //StudentId
    public func getStudentId() -> StudentId {
        return studentId!
    }
    
    public func setStudentId(studentId : StudentId) {
        self.studentId = studentId
    }
    
    //Type
    public func getType() -> String {
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //RemainingAmount
    public func getRemainingAmount() -> Int {
        return remainingAmount!
    }
    
    public func setRemainingAmount(remainingAmount : Int) {
        self.remainingAmount = remainingAmount
    }
    
    //SpentAmount
    public func getSpentAmount() -> Int {
        return spentAmount!
    }
    
    public func setSpentAmount(spentAmount : Int) {
        self.spentAmount = spentAmount
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Type
    public func getImageUrl() -> String {
        if imgeUrl == nil{
            imgeUrl = ""
        }
        return imgeUrl!
    }
    
    public func setImageUrl(imgeUrl : String) {
        self.imgeUrl = imgeUrl
    }


}
