/*
 Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import UIKit

/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class SignUpUser {
    
    private var id = 0
    private var firstName : String?
    private var lastName : String?
    private var city : String?
    private var email : String?
    private var address : String?
    private var countryCode : String?
    private var country : String?
    private var zipCode : String?
    private var contactNumber : String?
    private var dob : String?
    private var signupType : String?
    private var verified : String?
    private var emailTokenExpiration : String?
    private var updatedAt : String?
    private var createdAt : String?
    private var password : String?
    //SocialSignUpExtraFeild
    private var imageUrl : String?
    private var emailToken : String?
    private var passwordToken : String?
    private var passwordTokenExpiration : String?
    private var gender : String?
    private var state : String?
    
    //MARK: GetterSetterMethod
    //Gender
    public func getGender() -> String {
        if gender == nil {
            return ""
        } else if gender == "M" {
            return "Male"
        } else if gender == "F" {
            return "Female"
        } else {
            return gender!
        }
    }
    
    public func setGender(gender : String) {
        self.gender = gender
    }
    
    //State
    public func getState() -> String {
        if state == nil {
            state = ""
        }
        return state!
    }
    
    public func setState(state : String) {
        self.state = state
    }

    
    //ID
    public func getId() -> Int {
        if id == 0 {
            return 0
        } else {
            return id
        }
    }
    
    public func setId(id : Int) {
        self.id = id
    }

    //Firstname
    public func getFirstName() -> String {
        if firstName == nil {
            return ""
        }
        return firstName!
    }
    
    public func setFirstName(firstName : String) {
        self.firstName = firstName
    }
    
    //Lastname
    public func getLastName() -> String {
        if lastName == nil {
            return ""
        }
        return lastName!
    }
    
    public func setLastName(lastName : String) {
        self.lastName = lastName
    }
    
    //City
    public func getCity() -> String {
        if city == nil {
            return ""
        }
        return city!
    }
    
    public func setCity(city : String) {
        self.city = city
    }
    
    //Email
    public func getEmail() -> String {
        if email == nil {
            return ""
        }
        return email!
    }
    
    public func setEmail(email : String) {
        self.email = email
    }
    
    //Address
    public func getAddress() -> String {
        if address == nil {
            return ""
        }
        return address!
    }
    
    public func setAddress(address : String) {
        self.address = address
    }
    
    //CountryCode
    public func getCountryCode() -> String {
        if countryCode == nil {
            return ""
        }
        return countryCode!
    }
    
    public func setCountryCode(countryCode : String) {
        self.countryCode = countryCode
    }
    
    //Country
    public func getCountry() -> String {
        if country == nil {
            return ""
        }
        return country!
    }
    
    public func setCountry(country : String) {
        self.country = country
    }

    
    //ZipCode
    public func getZipCode() -> String {
        if zipCode == nil {
            return ""
        }
        return zipCode!
    }
    
    public func setZipCode(zipCode : String) {
        self.zipCode = zipCode
    }
    
    //ContactNumber
    public func getContactNumber() -> String {
        if contactNumber == nil {
            return ""
        }
        return contactNumber!
    }
    
    public func setContactNumber(contactNumber : String) {
        self.contactNumber = contactNumber
    }
    
    

    //Dob
    public func getDob() -> String {
        if dob == nil {
            return ""
        }
        return dob!
    }
    
    public func setDob(dob : String) {
        self.dob = dob
    }
   
    //signupType
    public func getSignupType() -> String {
        if signupType == nil {
            return ""
        }
        return signupType!
    }
    
    public func setSignupType(signupType : String) {
        self.signupType = signupType
    }

    //verified
    public func getVerified() -> String {
        if verified == nil {
            return ""
        }
        return verified!
    }
    
    public func setVerified(verified : String) {
        self.verified = verified
    }
    
    //EmailTokenExpiration
    public func getEmailTokenExpiration() -> String {
        if emailTokenExpiration == nil {
            return ""
        }
        return emailTokenExpiration!
    }
    
    public func setEmailTokenExpiration(emailTokenExpiration : String) {
        self.emailTokenExpiration = emailTokenExpiration
    }
    
    //updatedAt
    public func getUpdatedAt() -> String {
        if updatedAt == nil {
            return ""
        }
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }

    //createdAt
    public func getCreatedAt() -> String {
        if createdAt == nil {
            return ""
        }
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //Password
    public func getPassword() -> String {
        if password == nil {
            return ""
        } else {
            return password!
        }
    }
    
    public func setPassword(password : String) {
        self.password = password
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        if imageUrl == nil {
            return ""
        }
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    
    //EmailToken
    public func getEmailToken() -> String {
        if emailToken == nil {
            return ""
        }
        return emailToken!
    }
    
    public func setEmailToken(emailToken : String) {
        self.emailToken = emailToken
    }
    
    //PasswordToken
    public func getPasswordToken() -> String {
        if passwordToken == nil {
            return ""
        }
        return passwordToken!
    }
    
    public func setPasswordToken(passwordToken : String) {
        self.passwordToken = passwordToken
    }
    
    //PasswordTokenExpiration
    public func getPasswordTokenExpiration() -> String {
        if passwordTokenExpiration == nil {
            return ""
        }
        return passwordTokenExpiration!
    }
    
    public func setPasswordTokenExpiration(passwordTokenExpiration : String) {
        self.passwordTokenExpiration = passwordTokenExpiration
    }
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let user_list = User.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of User Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [SignUpUser]
    {
        var models:[SignUpUser] = []
        for item in array
        {
            models.append(SignUpUser(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let user = User(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: User Instance.
     
     */
    
    init?() {}
    
    
    required public init?(dictionary: NSDictionary) {
        id = (dictionary["id"] as? Int)!
        city = dictionary["city"] as? String
        state = dictionary["state"] as? String
        gender = dictionary["gender"] as? String
        email = dictionary["email"] as? String
        address = dictionary["address"] as? String
        lastName = dictionary["lastName"] as? String
        countryCode = dictionary["countryCode"] as? String
        country = dictionary["country"] as? String
        firstName = dictionary["firstName"] as? String
        zipCode = dictionary["zipCode"] as? String
        contactNumber = dictionary["contactNumber"] as? String
        dob = dictionary["dob"] as? String
        signupType = dictionary["signupType"] as? String
        verified = dictionary["verified"] as? String
        emailTokenExpiration = dictionary["emailTokenExpiration"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        createdAt = dictionary["createdAt"] as? String
        imageUrl = dictionary["imageUrl"] as? String
        emailToken = dictionary["emailToken"] as? String
        passwordToken = dictionary["passwordToken"] as? String
        passwordTokenExpiration = dictionary["passwordTokenExpiration"] as? String


    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.city, forKey: "city")
        dictionary.setValue(self.state, forKey: "state")
        dictionary.setValue(self.gender, forKey: "gender")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.address, forKey: "address")
        dictionary.setValue(self.lastName, forKey: "lastName")
        dictionary.setValue(self.countryCode, forKey: "countryCode")
        dictionary.setValue(self.country, forKey: "country")
        dictionary.setValue(self.firstName, forKey: "firstName")
        dictionary.setValue(self.zipCode, forKey: "zipCode")
        dictionary.setValue(self.contactNumber, forKey: "contactNumber")
        dictionary.setValue(self.dob, forKey: "dob")
        dictionary.setValue(self.signupType, forKey: "signupType")
        dictionary.setValue(self.verified, forKey: "verified")
        dictionary.setValue(self.emailTokenExpiration, forKey: "emailTokenExpiration")
        dictionary.setValue(self.updatedAt, forKey: "updatedAt")
        dictionary.setValue(self.createdAt, forKey: "createdAt")
        dictionary.setValue(self.imageUrl, forKey: "imageUrl")
        dictionary.setValue(self.emailToken, forKey: "emailToken")
        dictionary.setValue(self.passwordToken, forKey: "passwordToken")
        dictionary.setValue(self.passwordTokenExpiration, forKey: "passwordTokenExpiration")
        return dictionary
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        return dictionary
    }
    
    //SIGNUP_DICTIONARY
    func toSignupDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                if (key == "firstName") {
                    dictionary[key] = child.value
                } else if (key == "lastName") {
                    dictionary[key] = child.value
                } else if (key == "email") {
                    dictionary[key] = child.value
                } else if (key == "password") {
                    dictionary[key] = child.value
                } else {
                    print("...")
                }
            }
        }
        return dictionary
        
    }
    
}
