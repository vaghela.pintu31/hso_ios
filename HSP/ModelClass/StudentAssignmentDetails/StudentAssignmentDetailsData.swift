/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class StudentAssignmentDetailsData {
    private var studentLessonId : Int?
    private var studentId : Int?
    private var attendance : String?
    private var score : String?
    private var startDate : String?
    private var endDate : String?
    private var week : Int?
    private var day : Int?
    private var studentAssignment : StudentAssignment?
    private var studentNote : StudentNote?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [StudentAssignmentDetailsData]
    {
        var models:[StudentAssignmentDetailsData] = []
        for item in array
        {
            models.append(StudentAssignmentDetailsData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		studentLessonId = dictionary["studentLessonId"] as? Int
		studentId = dictionary["studentId"] as? Int
		attendance = dictionary["attendance"] as? String
		score = dictionary["score"] as? String
		startDate = dictionary["startDate"] as? String
		endDate = dictionary["endDate"] as? String
		week = dictionary["week"] as? Int
		day = dictionary["day"] as? Int
		if (dictionary["studentAssignment"] != nil) { studentAssignment = StudentAssignment(dictionary: dictionary["studentAssignment"] as! NSDictionary) }
		if (dictionary["studentNote"] != nil) { studentNote = StudentNote(dictionary: dictionary["studentNote"] as! NSDictionary) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.studentLessonId, forKey: "studentLessonId")
		dictionary.setValue(self.studentId, forKey: "studentId")
		dictionary.setValue(self.attendance, forKey: "attendance")
		dictionary.setValue(self.score, forKey: "score")
		dictionary.setValue(self.startDate, forKey: "startDate")
		dictionary.setValue(self.endDate, forKey: "endDate")
		dictionary.setValue(self.week, forKey: "week")
		dictionary.setValue(self.day, forKey: "day")
		dictionary.setValue(self.studentAssignment?.dictionaryRepresentation(), forKey: "studentAssignment")
		dictionary.setValue(self.studentNote?.dictionaryRepresentation(), forKey: "studentNote")

		return dictionary
	}
    
    //StudentLessonId
    public func getStudentLessonId() -> Int {
        return studentLessonId!
    }
    
    public func setStudentLessonId(studentLessonId : Int) {
        self.studentLessonId = studentLessonId
    }
    
    //StudentId
    public func getStudentId() -> Int {
        return studentId!
    }
    
    public func setStudentId(studentId : Int) {
        self.studentId = studentId
    }
    
    //Attendance
    public func getAttendance() -> String {
        return attendance!
    }
    
    public func setAttendance(attendance : String) {
        self.attendance = attendance
    }
    
    //Score
    public func getScore() -> String {
        
        if score != nil{
            return score!
 
        }else {
            return ""
        }
    }
    
    public func setScore(score : String) {
        self.score = score
    }
    
    //StartDate
    public func getStartDate() -> String {
        return startDate!
    }
    
    public func setStartDate(startDate : String) {
        self.startDate = startDate
    }
    
    //EndDate
    public func getEndDate() -> String {
        return endDate!
    }
    
    public func setEndDate(endDate : String) {
        self.endDate = endDate
    }
    
    //Week
    public func getWeek() -> Int {
        return week!
    }
    
    public func setWeek(week : Int) {
        self.week = week
    }
    
    //Day
    public func getDay() -> Int {
        return day!
    }
    
    public func setDay(day : Int) {
        self.day = day
    }
    
    //StudentAssignment
    public func getStudentAssignment() -> StudentAssignment {
        return studentAssignment!
    }
    
    public func setStudentAssignment(studentAssignment : StudentAssignment) {
        self.studentAssignment = studentAssignment
    }
    
    //StudentNote
    public func getStudentNote() -> StudentNote {
        return studentNote!
    }
    
    public func setStudentNote(StudentNote : StudentNote) {
        self.studentNote = StudentNote
    }

}
