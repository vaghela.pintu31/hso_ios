/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class StudentAddData {
    private var userId : Int?
    private var mediaId : Int?
    private var email : String?
    private var firstName : String?
    private var lastName : String?
    private var grade : String?
    private var academicYearStart : String?
    private var academicYearEnd : String?
    private var school : String?
    private var countryCode : Int?
    private var contactNumber : String?
    private var imageId : String?
    private var dob : String?
    private var gender : String?
   // private var sex : String?
    private var parentEmail : String?
    private var parentFirstName : String?
    private var parentLastName : String?
    private var parentCountryCode : Int?
    private var parentContactNumber : String?
    private var id : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var parentName : String?
    private var academicYear : String?
    private var name : String?
    private var imageUrl : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Data Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [StudentAddData]
    {
        var models:[StudentAddData] = []
        for item in array
        {
            models.append(StudentAddData(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let data = Data(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Data Instance.
     */
    init() { }
    
    required public init?(dictionary: NSDictionary) {
        
        userId = dictionary["userId"] as? Int
        mediaId = dictionary["mediaId"] as? Int
        email = dictionary["email"] as? String
        firstName = dictionary["firstName"] as? String
        lastName = dictionary["lastName"] as? String
        grade = dictionary["grade"] as? String
        academicYearStart = dictionary["academicYearStart"] as? String
        academicYearEnd = dictionary["academicYearEnd"] as? String
        school = dictionary["school"] as? String
        countryCode = dictionary["countryCode"] as? Int
        contactNumber = dictionary["contactNumber"] as? String
        imageId = dictionary["imageId"] as? String
        dob = dictionary["dob"] as? String
        gender = dictionary["gender"] as? String
        //sex = dictionary["sex"] as? String
        parentEmail = dictionary["parentEmail"] as? String
        parentFirstName = dictionary["parentFirstName"] as? String
        parentLastName = dictionary["parentLastName"] as? String
        parentCountryCode = dictionary["parentCountryCode"] as? Int
        parentContactNumber = dictionary["parentContactNumber"] as? String
        id = dictionary["id"] as? Int
        createdAt = dictionary["createdAt"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        parentName = dictionary["parentName"] as? String
        academicYear = dictionary["academicYear"] as? String
        name = dictionary["name"] as? String
        imageUrl = dictionary["imageUrl"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.userId, forKey: "userId")
        dictionary.setValue(self.mediaId, forKey: "mediaId")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.firstName, forKey: "firstName")
        dictionary.setValue(self.lastName, forKey: "lastName")
        dictionary.setValue(self.grade, forKey: "grade")
        dictionary.setValue(self.academicYearStart, forKey: "academicYearStart")
        dictionary.setValue(self.academicYearEnd, forKey: "academicYearEnd")
        dictionary.setValue(self.school, forKey: "school")
        dictionary.setValue(self.countryCode, forKey: "countryCode")
        dictionary.setValue(self.contactNumber, forKey: "contactNumber")
        dictionary.setValue(self.imageId, forKey: "imageId")
        dictionary.setValue(self.dob, forKey: "dob")
        dictionary.setValue(self.gender, forKey: "gender")
        //dictionary.setValue(self.sex, forKey: "sex")
        dictionary.setValue(self.parentEmail, forKey: "parentEmail")
        dictionary.setValue(self.parentFirstName, forKey: "parentFirstName")
        dictionary.setValue(self.parentLastName, forKey: "parentLastName")
        dictionary.setValue(self.parentCountryCode, forKey: "parentCountryCode")
        dictionary.setValue(self.parentContactNumber, forKey: "parentContactNumber")
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.createdAt, forKey: "createdAt")
        dictionary.setValue(self.updatedAt, forKey: "updatedAt")
        dictionary.setValue(self.parentName, forKey: "parentName")
        dictionary.setValue(self.academicYear, forKey: "academicYear")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.imageUrl, forKey: "imageUrl")
        return dictionary
    }
    
    //MARK: GetterSetterMethod
    //UserId
    public func getUserId() -> Int {
        if userId == 0 {
            return 0
        }
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //MediaId
    public func getMediaId() -> Int {
        if mediaId == 0 {
            return 0
        }
        return mediaId!
    }
    
    public func setMediaId(mediaId : Int) {
        self.mediaId = mediaId
    }
    
    //Email
    public func getEmail() -> String {
        if email == nil {
            return ""
        }
        return email!
    }
    
    public func setEmail(email : String) {
        self.email = email
    }
    
    //FirstName
    public func getFirstName() -> String {
        if firstName == nil {
            return ""
        }
        return firstName!
    }
    
    public func setFirstName(firstName : String) {
        self.firstName = firstName
    }
    
    //LastName
    public func getLastName() -> String {
        if lastName == nil {
            return ""
        }
        return lastName!
    }
    
    public func setLastName(lastName : String) {
        self.lastName = lastName
    }
    
    //Grade
    public func getGrade() -> String {
        if grade == nil {
            return ""
        }
        return grade!
    }
    
    public func setGrade(grade : String) {
        self.grade = grade
    }
    
    //AcademicYearStart
    public func getAcademicYearStart() -> String {
        if academicYearStart == nil {
            return ""
        }
        return academicYearStart!
    }
    
    public func setAcademicYearStart(academicYearStart : String) {
        self.academicYearStart = academicYearStart
    }
    
    //AcademicYearEnd
    public func getAcademicYearEnd() -> String {
        if academicYearEnd == nil {
            return ""
        }
        return academicYearEnd!
    }
    
    public func setAcademicYearEnd(academicYearEnd : String) {
        self.academicYearEnd = academicYearEnd
    }
    
    //School
    public func getSchool() -> String {
        if school == nil {
            return ""
        }
        return school!
    }
    
    public func setSchool(school : String) {
        self.school = school
    }
    
    //CountryCode
    public func getCountryCode() -> Int {
        if countryCode == 0 {
            return 0
        }
        return countryCode!
    }
    
    public func setCountryCode(countryCode : Int) {
        self.countryCode = countryCode
    }
    
    //ContactNumber
    public func getContactNumber() -> String {
        if contactNumber == nil {
            return ""
        }
        return contactNumber!
    }
    
    public func setContactNumber(contactNumber : String) {
        self.contactNumber = contactNumber
    }
    
    //ImageId
    public func getImageId() -> String {
        if imageId == nil {
            return ""
        }
        return imageId!
    }
    
    public func setImageId(imageId : String) {
        self.imageId = imageId
    }
    
    //Dob
    public func getDob() -> String {
        if dob == nil {
            return ""
        }
        return dob!
    }
    
    public func setDob(dob : String) {
        self.dob = dob
    }
    
    //Gender
    public func getGender() -> String {
        if gender == nil {
            return ""
        }
        return gender!
    }
    
    public func setGender(gender : String) {
        self.gender = gender
    }
    
//    //Sex
//    public func getSex() -> String {
//        return sex!
//    }
//    
//    public func setSex(sex : String) {
//        self.sex = sex
//    }
    
    //ParentEmail
    public func getParentEmail() -> String {
        if parentEmail == nil {
            return ""
        }
        return parentEmail!
    }
    
    public func setParentEmail(parentEmail : String) {
        self.parentEmail = parentEmail
    }
    
    //ParentFirstName
    public func getParentFirstName() -> String {
        if parentFirstName == nil {
            return ""
        }
        return parentFirstName!
    }
    
    public func setParentFirstName(parentFirstName : String) {
        self.parentFirstName = parentFirstName
    }
    
    //ParentLastName
    public func getParentLastName() -> String {
        if parentLastName == nil {
            return ""
        }
        return parentLastName!
    }
    
    public func setParentLastName(parentLastName : String) {
        self.parentLastName = parentLastName
    }
    
    //ParentCountryCode
    public func getParentCountryCode() -> Int {
        if parentCountryCode == 0 {
            return 0
        }
        return parentCountryCode!
    }
    
    public func setParentCountryCode(parentCountryCode : Int) {
        self.parentCountryCode = parentCountryCode
    }
    
    //ParentContactNumber
    public func getParentContactNumber() -> String {
        if parentContactNumber == nil {
            return ""
        }
        return parentContactNumber!
    }
    
    public func setParentContactNumber(parentContactNumber : String) {
        self.parentContactNumber = parentContactNumber
    }
    
    //Id
    public func getId() -> Int {
        if id == 0 {
            return 0
        }
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        if createdAt == nil {
            return ""
        }
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        if updatedAt == nil {
            return ""
        }
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //ParentName
    public func getParentName() -> String {
        if parentName == nil {
            return ""
        }
        return parentName!
    }
    
    public func setParentName(parentName : String) {
        self.parentName = parentName
    }
    
    //AcademicYear
    public func getAcademicYear() -> String {
        if academicYear == nil {
            return ""
        }
        return academicYear!
    }
    
    public func setAcademicYear(academicYear : String) {
        self.academicYear = academicYear
    }
    
    //Name
    public func getName() -> String {
        if name == nil {
            return ""
        }
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        if imageUrl == nil {
            return ""
        }
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }

    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        return dictionary
    }
}
