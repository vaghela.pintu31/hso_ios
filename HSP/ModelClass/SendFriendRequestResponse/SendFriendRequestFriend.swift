/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class SendFriendRequestFriend {
    private var userId : Int?
    private var friendId : Int?
    private var acceptorId : Int?
    private var status : String?
    private var createdAt : String?
    private var updatedAt : String?
    private var id : Int?
    
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //FriendId
    public func getFriendId() -> Int {
        return friendId!
    }
    
    public func setFriendId(friendId : Int) {
        self.friendId = friendId
    }
    
    //AcceptorId
    public func getAcceptorId() -> Int {
        return acceptorId!
    }
    
    public func setAcceptorId(acceptorId : Int) {
        self.acceptorId = acceptorId
    }
    
    //Status
    public func getStatus() -> String {
        return status!
    }
    
    public func setStatus(status : String) {
        self.status = status
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let friend_list = Friend.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Friend Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [SendFriendRequestFriend]
    {
        var models:[SendFriendRequestFriend] = []
        for item in array
        {
            models.append(SendFriendRequestFriend(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let friend = Friend(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Friend Instance.
*/
	required public init?(dictionary: NSDictionary) {

		userId = dictionary["userId"] as? Int
		friendId = dictionary["friendId"] as? Int
		acceptorId = dictionary["acceptorId"] as? Int
		status = dictionary["status"] as? String
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		id = dictionary["id"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.friendId, forKey: "friendId")
		dictionary.setValue(self.acceptorId, forKey: "acceptorId")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.id, forKey: "id")

		return dictionary
	}

}
