/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class SendFriendRequestUserchats {
    private var userId : Int?
    private var addedBy : Int?
    private var type : String?
    private var chatId : String?
    private var unreadCount : Int?
    private var removedBy : Int?
    private var lastSeenMessage : String?
    private var createdAt : String?
    private var updatedAt : String?
    private var id : String?
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //AddedBy
    public func getAddedBy() -> Int {
        return addedBy!
    }
    
    public func setAddedBy(addedBy : Int) {
        self.addedBy = addedBy
    }
    
    //Type
    public func getType() -> String {
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //ChatId
    public func getChatId() -> String {
        return chatId!
    }
    
    public func setChatId(chatId : String) {
        self.chatId = chatId
    }
    
    //UnreadCount
    public func getUnreadCount() -> Int {
        return unreadCount!
    }
    
    public func setUnreadCount(unreadCount : Int) {
        self.unreadCount = unreadCount
    }
    
    //RemovedBy
    public func getRemovedBy() -> Int {
        return removedBy!
    }
    
    public func setRemovedBy(removedBy : Int) {
        self.removedBy = removedBy
    }
    
    //LastSeenMessage
    public func getLastSeenMessage() -> String {
        return lastSeenMessage!
    }
    
    public func setLastSeenMessage(lastSeenMessage : String) {
        self.lastSeenMessage = lastSeenMessage
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Id
    public func getId() -> String {
        return id!
    }
    
    public func setId(id : String) {
        self.id = id
    }


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let userchats_list = Userchats.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Userchats Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [SendFriendRequestUserchats]
    {
        var models:[SendFriendRequestUserchats] = []
        for item in array
        {
            models.append(SendFriendRequestUserchats(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let userchats = Userchats(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Userchats Instance.
*/
	required public init?(dictionary: NSDictionary) {

		userId = dictionary["userId"] as? Int
		addedBy = dictionary["addedBy"] as? Int
		type = dictionary["type"] as? String
		chatId = dictionary["chatId"] as? String
		unreadCount = dictionary["unreadCount"] as? Int
		removedBy = dictionary["removedBy"] as? Int
		lastSeenMessage = dictionary["lastSeenMessage"] as? String
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		id = dictionary["id"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.addedBy, forKey: "addedBy")
		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.chatId, forKey: "chatId")
		dictionary.setValue(self.unreadCount, forKey: "unreadCount")
		dictionary.setValue(self.removedBy, forKey: "removedBy")
		dictionary.setValue(self.lastSeenMessage, forKey: "lastSeenMessage")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.id, forKey: "id")

		return dictionary
	}

}
