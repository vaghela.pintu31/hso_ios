/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class SendFriendRequestMessages {
    private var userId : Int?
    private var messageBody : String?
    private var type : String?
    private var metaData : SendFriendRequestMetaData?
    private var chatId : String?
    private var createdAt : String?
    private var updatedAt : String?
    private var id : String?
    
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //MessageBody
    public func getMessageBody() -> String {
        return messageBody!
    }
    
    public func setMessageBody(messageBody : String) {
        self.messageBody = messageBody
    }
    
    //Type
    public func getType() -> String {
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //MetaData
    public func getMetaData() -> SendFriendRequestMetaData {
        return metaData!
    }
    
    public func setMetaData(metaData : SendFriendRequestMetaData) {
        self.metaData = metaData
    }
    
    //ChatId
    public func getChatId() -> String {
        return chatId!
    }
    
    public func setChatId(chatId : String) {
        self.chatId = chatId
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Id
    public func getId() -> String {
        return id!
    }
    
    public func setId(id : String) {
        self.id = id
    }


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let messages_list = Messages.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Messages Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [SendFriendRequestMessages]
    {
        var models:[SendFriendRequestMessages] = []
        for item in array
        {
            models.append(SendFriendRequestMessages(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let messages = Messages(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Messages Instance.
*/
	required public init?(dictionary: NSDictionary) {

		userId = dictionary["userId"] as? Int
		messageBody = dictionary["messageBody"] as? String
		type = dictionary["type"] as? String
		if (dictionary["metaData"] != nil) { metaData = SendFriendRequestMetaData(dictionary: dictionary["metaData"] as! NSDictionary) }
		chatId = dictionary["chatId"] as? String
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		id = dictionary["id"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.messageBody, forKey: "messageBody")
		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.metaData?.dictionaryRepresentation(), forKey: "metaData")
		dictionary.setValue(self.chatId, forKey: "chatId")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.id, forKey: "id")

		return dictionary
	}

}
