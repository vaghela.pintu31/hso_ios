/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class CalenderListData {
    //Appointment - Model
    private var description : String?
    private var location : String?
    private var latitude : Double?
    private var longitude : Double?
    
    //StudentAssignment - Model
    private var attendance : String?
    private var title : String?
    private var score : String?
    private var studentId : Int?
    private var category : String?
    private var imageUrl : String?
    
    /** Appointment - Model **/
    //Description
    public func getDescription() -> String {
        if description == nil {
            description = ""
        }
        return description!
    }
    
    public func setDescription(description : String) {
        self.description = description
    }
    
    //Location
    public func getLocation() -> String {
        if location == nil {
            location = ""
        }
        return location!
    }
    
    public func setLocation(location : String) {
        self.location = location
    }
    
    //Latitude
    public func getLatitude() -> Double {
        return latitude!
    }
    
    public func setLatitude(latitude : Double) {
        self.latitude = latitude
    }
    
    //Longitude
    public func getLongitude() -> Double {
        return longitude!
    }
    
    public func setLongitude(longitude : Double) {
        self.longitude = longitude
    }
    
    
    /** StudentAssignment - Model **/
    public func getAttendance() -> String {
        if attendance == nil {
            attendance = ""
        }
        return attendance!
    }
    
    public func setAttendance(attendance : String) {
        self.attendance = attendance
    }
    
    //Title
    public func getTitle() -> String {
        if title == nil {
            title = ""
        }
        return title!
    }
    
    public func setTitle(title : String) {
        self.title = title
    }
    
    //Score
    public func getScore() -> String {
        if score == nil {
            return ""
        } else {
            return score!
        }
    }
    
    public func setScore(score : String) {
        self.score = score
    }
    
    //StudentId
    public func getStudentId() -> Int {
        if studentId == 0 {
            return 0
        }
        return studentId!
    }
    
    public func setStudentId(studentId : Int) {
        self.studentId = studentId
    }
    
    //Category
    public func getCategory() -> String {
        if category == nil {
            category = ""
        }
        return category!
    }
    
    public func setCategory(category : String) {
        self.category = category
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        if imageUrl == nil {
            imageUrl = ""
        }
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let metaData_list = MetaData.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of MetaData Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [CalenderListData]
    {
        var models:[CalenderListData] = []
        for item in array
        {
            models.append(CalenderListData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let metaData = MetaData(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: MetaData Instance.
*/
    init() {}
     
	required public init?(dictionary: NSDictionary) {

		description = dictionary["description"] as? String
		location = dictionary["location"] as? String
		latitude = dictionary["latitude"] as? Double
		longitude = dictionary["longitude"] as? Double
        
        //--//
        attendance = dictionary["attendance"] as? String
        title = dictionary["title"] as? String
        score = dictionary["score"] as? String
        studentId = dictionary["studentId"] as? Int
        category = dictionary["category"] as? String
        imageUrl = dictionary["imageUrl"] as? String

        
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.description, forKey: "description")
		dictionary.setValue(self.location, forKey: "location")
		dictionary.setValue(self.latitude, forKey: "latitude")
		dictionary.setValue(self.longitude, forKey: "longitude")
        
        //--//
        dictionary.setValue(self.attendance, forKey: "attendance")
        dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.score, forKey: "score")
        dictionary.setValue(self.studentId, forKey: "studentId")
        dictionary.setValue(self.category, forKey: "category")
        dictionary.setValue(self.imageUrl, forKey: "imageUrl")


		return dictionary
	}

}
