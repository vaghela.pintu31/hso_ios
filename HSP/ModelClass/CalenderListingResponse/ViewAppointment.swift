/*
 Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import Foundation

/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ViewAppointment {
    private var entityId : Int?
    private var model : String?
    private var title : String?
    private var userId : Int?
    private var startDate : String?
    private var endDate : String?
    private var imageUrl : String?
    private var createdAt : String?
    private var updatedAt : String?
    private var createdAtDate : Date?
    private var metaData : CalenderListData?
    
    //EntityId
    public func getEntityId() -> Int {
        return entityId!
    }
    
    public func setEntityId(entityId : Int) {
        self.entityId = entityId
    }
    
    //Model
    public func getModel() -> String {
        return model!
    }
    
    public func setModel(model : String) {
        self.model = model
    }
    
    //Title
    public func getTitle() -> String {
        if title == nil{
            title = ""
        }
        return title!
    }
    
    public func setTitle(title : String) {
        self.title = title
    }
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //StartDate
    public func getStartDate() -> String {
        return startDate!
    }
    
    public func setStartDate(startDate : String) {
        self.startDate = startDate
    }
    
    //EndDate
    public func getEndDate() -> String {
        return endDate!
    }
    
    public func setEndDate(endDate : String) {
        self.endDate = endDate
    }
    
    //Image
    public func getImageUrl() -> String {
        if imageUrl == nil {
            imageUrl = ""
        }
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //CreatedAt
    public func getCreatedAtDate() -> Date {
        return createdAtDate!
    }
    
    public func setCreatedAtDate(createdAt : Date) {
        self.createdAtDate = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    
    //CalenderListData
    public func getmetaData() -> CalenderListData {
        return metaData!
    }
    
    public func setCalenderListData(metaData : CalenderListData) {
        self.metaData = metaData
    }
    
    func setData() -> [ViewAppointment] {
        
        var mList = [ViewAppointment]()
        var user1 = ViewAppointment()
        user1.setEntityId(entityId: 1)
        user1.setModel(model: "appointment1")
        user1.setTitle(title: "AppointmentTitle_1")
        user1.setUserId(userId: 1)
        user1.setStartDate(startDate: "2017-04-26T06:56:03.000Z")
        user1.setEndDate(endDate: "2017-04-29T06:56:03.000Z")
        user1.setImageUrl(imageUrl: "woman")
        
        
        mList.append(user1)
        
        var calListData = CalenderListData()
        calListData.setDescription(description: "Hello_1")
        calListData.setLocation(location: "Sanand_1")
        calListData.setLatitude(latitude: 0.0)
        calListData.setLongitude(longitude: 0.0)
        calListData.setAttendance(attendance: "A")
        user1.setCalenderListData(metaData: calListData)
        mList.append(user1)
        
        //Second
        user1 = ViewAppointment()
        user1.setEntityId(entityId: 2)
        user1.setModel(model: "appointment2")
        user1.setTitle(title: "AppointmentTitle_2")
        user1.setUserId(userId: 2)
        user1.setStartDate(startDate: "2017-04-01T06:56:03.000Z")
        user1.setEndDate(endDate: "2017-04-29T06:56:03.000Z")
        user1.setImageUrl(imageUrl: "woman")
        mList.append(user1)
        
        calListData = CalenderListData()
        calListData.setDescription(description: "Hello_2")
        calListData.setLocation(location: "Sanand_2")
        calListData.setLatitude(latitude: 0.0)
        calListData.setLongitude(longitude: 0.0)
        calListData.setAttendance(attendance: "P")
        
        user1.setCalenderListData(metaData: calListData)
        mList.append(user1)
        
        //Third
        user1 = ViewAppointment()
        user1.setEntityId(entityId: 3)
        user1.setModel(model: "appointment3")
        user1.setTitle(title: "AppointmentTitle_3")
        user1.setUserId(userId: 3)
        user1.setStartDate(startDate: "2017-04-15T06:56:03.000Z")
        user1.setEndDate(endDate: "2017-04-29T06:56:03.000Z")
        user1.setImageUrl(imageUrl: "woman")
        mList.append(user1)
        
        calListData = CalenderListData()
        calListData.setDescription(description: "Hello_3")
        calListData.setLocation(location: "Sanand_3")
        calListData.setLatitude(latitude: 0.0)
        calListData.setLongitude(longitude: 0.0)
        calListData.setAttendance(attendance: "A")
        
        user1.setCalenderListData(metaData: calListData)
        mList.append(user1)
        
        //4
        user1 = ViewAppointment()
        user1.setEntityId(entityId: 4)
        user1.setModel(model: "appointment4")
        user1.setTitle(title: "AppointmentTitle_4")
        user1.setUserId(userId: 4)
        user1.setStartDate(startDate: "2017-04-10T06:56:03.000Z")
        user1.setEndDate(endDate: "2017-04-29T06:56:03.000Z")
        user1.setImageUrl(imageUrl: "woman")
        mList.append(user1)
        
        calListData = CalenderListData()
        calListData.setDescription(description: "Hello_4")
        calListData.setLocation(location: "Sanand_4")
        calListData.setLatitude(latitude: 0.0)
        calListData.setLongitude(longitude: 0.0)
        calListData.setAttendance(attendance: "A")
        
        user1.setCalenderListData(metaData: calListData)
        mList.append(user1)
        
        //5
        user1 = ViewAppointment()
        user1.setEntityId(entityId: 5)
        user1.setModel(model: "appointment5")
        user1.setTitle(title: "AppointmentTitle_5")
        user1.setUserId(userId: 5)
        user1.setStartDate(startDate: "2017-04-26T06:56:03.000Z")
        user1.setEndDate(endDate: "2017-04-29T06:56:03.000Z")
        user1.setImageUrl(imageUrl: "woman")
        mList.append(user1)
        
        calListData = CalenderListData()
        calListData.setDescription(description: "Hello_5")
        calListData.setLocation(location: "Sanand_5")
        calListData.setLatitude(latitude: 0.0)
        calListData.setLongitude(longitude: 0.0)
        calListData.setAttendance(attendance: "P")
        
        user1.setCalenderListData(metaData: calListData)
        mList.append(user1)
        
        return mList
    }
    
    //    func setDemoData() -> [ViewAppointment] {
    //        var mList = [ViewAppointment]()
    //        var user1 = ViewAppointment()
    //        for i in 0...5 {
    //            var mCalList = [ViewAppointment]()
    //            var mVPList = ViewAppointment()
    //            mVPList[i].setEntityId(entityId: 1)
    //            mVPList[i].setModel(model: "appointment\(i)")
    //            mVPList[i].setTitle(title: "AppointmentTitle \(i)")
    //            mVPList[i].setUserId(userId: 1)
    //            mVPList[i].setStartDate(startDate: "2017-04-29T06:56:03.000Z")
    //            mVPList[i].setEndDate(endDate: "2017-04-29T06:56:03.000Z")
    //
    //            let calListData = CalenderListData()
    //            calListData.setDescription(description: "Hello\(i)")
    //            calListData.setLocation(location: "Sanand\(i)")
    //            calListData.setLatitude(latitude: 0.0)
    //            calListData.setLongitude(longitude: 0.0)
    //            mVPList[i].setCalenderListData(metaData: calListData)
    //            mCalList.append(mVPList)
    //            return mVPList
    //        }
    //
    //    }
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let viewAppointment_list = ViewAppointment.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of ViewAppointment Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [ViewAppointment]
    {
        var models:[ViewAppointment] = []
        for item in array
        {
            models.append(ViewAppointment(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let viewAppointment = ViewAppointment(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: ViewAppointment Instance.
     */
    init() { }
    
    required public init?(dictionary: NSDictionary) {
        
        entityId = dictionary["entityId"] as? Int
        model = dictionary["model"] as? String
        title = dictionary["title"] as? String
        userId = dictionary["userId"] as? Int
        startDate = dictionary["startDate"] as? String
        endDate = dictionary["endDate"] as? String
        createdAt = dictionary["createdAt"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        if (dictionary["metaData"] != nil) { metaData = CalenderListData(dictionary: dictionary["metaData"] as! NSDictionary) }
    }
    /**
     Returns the dictionary representation for the current instance.
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.entityId, forKey: "entityId")
        dictionary.setValue(self.model, forKey: "model")
        dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.userId, forKey: "userId")
        dictionary.setValue(self.startDate, forKey: "startDate")
        dictionary.setValue(self.endDate, forKey: "endDate")
        
        dictionary.setValue(self.createdAt, forKey: "createdAt")
        dictionary.setValue(self.updatedAt, forKey: "updatedAt")
        dictionary.setValue(self.metaData?.dictionaryRepresentation(), forKey: "metaData")
        return dictionary
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                if key == "calenderListData" {
                    let lessonArray = child.value as! [CalenderListData]
                    dictionary[key] = lessonArray.map({
                        (value: CalenderListData) -> NSDictionary in
                        return value.dictionaryRepresentation()
                    })
                } else {
                    dictionary[key] = child.value
                }
            }
        }
        return dictionary
    }
    
}
