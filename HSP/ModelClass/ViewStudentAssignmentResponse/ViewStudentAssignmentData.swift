/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ViewStudentAssignmentData {
    private var attendance : String?
    private var studentId : Int?
    private var score : String?
    private var startDate : String?
    private var endDate : String?
    private var week : Int?
    private var day : Int?
    private var imageUrl : String?
    private var lessonId : Int?
    private var lesson : ViewLesson?
    private var studentAssignment : ViewStudentAssignment?
    
    
    //Attendance
    public func getAttendance() -> String {
        return attendance!
    }
    
    public func setAttendance(attendance : String) {
        self.attendance = attendance
    }
    
    //StudentId
    public func getStudentId() -> Int {
        return studentId!
    }
    
    public func setStudentId(studentId : Int) {
        self.studentId = studentId
    }
    
    //Score
    public func getScore() -> String {
        if score == nil {
            return ""
        }
        return score!
    }
    
    public func setScore(score : String) {
        self.score = score
    }
    
    //StartDate
    public func getStartDate() -> String {
        return startDate!
    }
    
    public func setStartDate(startDate : String) {
        self.startDate = startDate
    }
    
    //EndDate
    public func getEndDate() -> String {
        return endDate!
    }
    
    public func setEndDate(endDate : String) {
        self.endDate = endDate
    }
    
    //Week
    public func getWeek() -> Int {
        return week!
    }
    
    public func setWeek(week : Int) {
        self.week = week
    }
    
    //Day
    public func getDay() -> Int {
        return day!
    }
    
    public func setDay(day : Int) {
        self.day = day
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        if imageUrl == nil {
            return ""
        }
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    
    //LessonId
    public func getLessonId() -> Int {
        return lessonId!
    }
    
    public func setLessonId(lessonId : Int) {
        self.lessonId = lessonId
    }
    
    //Lesson
    public func getLesson() -> ViewLesson {
        return lesson!
    }
    
    public func setLesson(lesson : ViewLesson) {
        self.lesson = lesson
    }
    
    //StudentAssignment
    public func getStudentAssignment() -> ViewStudentAssignment {
        return studentAssignment!
    }
    
    public func setStudentAssignment(studentAssignment : ViewStudentAssignment) {
        self.studentAssignment = studentAssignment
    }


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ViewStudentAssignmentData]
    {
        var models:[ViewStudentAssignmentData] = []
        for item in array
        {
            models.append(ViewStudentAssignmentData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		attendance = dictionary["attendance"] as? String
		studentId = dictionary["studentId"] as? Int
		score = dictionary["score"] as? String
		startDate = dictionary["startDate"] as? String
		endDate = dictionary["endDate"] as? String
		week = dictionary["week"] as? Int
		day = dictionary["day"] as? Int
		imageUrl = dictionary["imageUrl"] as? String
		lessonId = dictionary["lessonId"] as? Int
		if (dictionary["lesson"] != nil) { lesson = ViewLesson(dictionary: dictionary["lesson"] as! NSDictionary) }
		if (dictionary["studentAssignment"] != nil) { studentAssignment = ViewStudentAssignment(dictionary: dictionary["studentAssignment"] as! NSDictionary) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.attendance, forKey: "attendance")
		dictionary.setValue(self.studentId, forKey: "studentId")
		dictionary.setValue(self.score, forKey: "score")
		dictionary.setValue(self.startDate, forKey: "startDate")
		dictionary.setValue(self.endDate, forKey: "endDate")
		dictionary.setValue(self.week, forKey: "week")
		dictionary.setValue(self.day, forKey: "day")
		dictionary.setValue(self.imageUrl, forKey: "imageUrl")
		dictionary.setValue(self.lessonId, forKey: "lessonId")
		dictionary.setValue(self.lesson?.dictionaryRepresentation(), forKey: "lesson")
		dictionary.setValue(self.studentAssignment?.dictionaryRepresentation(), forKey: "studentAssignment")

		return dictionary
	}

}
