/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ViewStudentAssignment {
    private var id : Int?
    private var title : String?
    private var noteTitle : String?
    private var displayTitle : String?
    private var category : String?
    private var createdAt : String?
    private var updatedAt : String?
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //Title
    public func getTitle() -> String {
        if title == nil {
            return ""
        }
        return title!
    }
    
    public func setTitle(title : String) {
        self.title = title
    }
    
    //NoteTitle
    public func getNoteTitle() -> String {
        if noteTitle == nil {
            return ""
        }
        return noteTitle!
    }
    
    public func setNoteTitle(noteTitle : String) {
        self.noteTitle = noteTitle
    }
    
    //DisplayTitle
    public func getDisplayTitle() -> String {
        return displayTitle!
    }
    
    public func setDisplayTitle(displayTitle : String) {
        self.displayTitle = displayTitle
    }
    
    //Category
    public func getCategory() -> String {
        return category!
    }
    
    public func setCategory(category : String) {
        self.category = category
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let studentAssignment_list = StudentAssignment.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of StudentAssignment Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ViewStudentAssignment]
    {
        var models:[ViewStudentAssignment] = []
        for item in array
        {
            models.append(ViewStudentAssignment(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let studentAssignment = StudentAssignment(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: StudentAssignment Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		title = dictionary["title"] as? String
		noteTitle = dictionary["noteTitle"] as? String
		displayTitle = dictionary["displayTitle"] as? String
		category = dictionary["category"] as? String
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.noteTitle, forKey: "noteTitle")
		dictionary.setValue(self.displayTitle, forKey: "displayTitle")
		dictionary.setValue(self.category, forKey: "category")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")

		return dictionary
	}

}
