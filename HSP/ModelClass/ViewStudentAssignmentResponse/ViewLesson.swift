/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ViewLesson {
    private var userId : Int?
    private var name : String?
    private var weeks : Int?
    private var days : Int?
    private var hours : Int?
    private var id : Int?
    private var createdAt : String?
    private var updatedAt : String?
    
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //Name
    public func getName() -> String {
        if name == nil {
            return ""
        }
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }
    
    //Weeks
    public func getWeeks() -> Int {
        if weeks == nil {
            return 0
        }
        return weeks!
    }
    
    public func setWeeks(weeks : Int) {
        self.weeks = weeks
    }
    
    //Days
    public func getDays() -> Int {
        if days == nil {
            return 0
        }
        return days!
    }
    
    public func setDays(days : Int) {
        self.days = days
    }
    
    //Hours
    public func getHours() -> Int {
        return hours!
    }
    
    public func setHours(hours : Int) {
        self.hours = hours
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let lesson_list = Lesson.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Lesson Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ViewLesson]
    {
        var models:[ViewLesson] = []
        for item in array
        {
            models.append(ViewLesson(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let lesson = Lesson(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Lesson Instance.
*/
	required public init?(dictionary: NSDictionary) {

		userId = dictionary["userId"] as? Int
		name = dictionary["name"] as? String
		weeks = dictionary["weeks"] as? Int
		days = dictionary["days"] as? Int
		hours = dictionary["hours"] as? Int
		id = dictionary["id"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.weeks, forKey: "weeks")
		dictionary.setValue(self.days, forKey: "days")
		dictionary.setValue(self.hours, forKey: "hours")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")

		return dictionary
	}

}
