/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class GetFriendListData {
    private var mediaId : Int?
    private var email : String?
    private var firstName : String?
    private var lastName : String?
    private var signupType : String?
    private var countryCode : Int?
    private var contactNumber : String?
    private var zipCode : String?
    private var city : String?
    private var state : String?
    private var imageId : String?
    private var dob : String?
    private var verified : Bool?
    private var gender : String?
    private var address : String?
    private var id : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var status : String?
    private var acceptorId : Int?
    private var imageUrl : String?
    private var name : String?
    
    //For SendRequest-CollaborateSearchViewContoller
    private var inviteMessage : String?
    
    //inviteMessage
    public func getInviteMessage() -> String {
        return inviteMessage!
    }
    
    public func setInviteMessage(inviteMessage : String) {
        self.inviteMessage = inviteMessage
    }
    
    //MediaId
    public func getMediaId() -> Int {
        return mediaId!
    }
    
    public func setMediaId(mediaId : Int) {
        self.mediaId = mediaId
    }
    
    //Email
    public func getEmail() -> String {
        return email!
    }
    
    public func setEmail(email : String) {
        self.email = email
    }
    
    //FirstName
    public func getFirstName() -> String {
        return firstName!
    }
    
    public func setFirstName(firstName : String) {
        self.firstName = firstName
    }
    
    //LastName
    public func getLastName() -> String {
        return lastName!
    }
    
    public func setLastName(lastName : String) {
        self.lastName = lastName
    }
    
    //SignupType
    public func getSignupType() -> String {
        return signupType!
    }
    
    public func setSignupType(signupType : String) {
        self.signupType = signupType
    }
    
    //CountryCode
    public func getCountryCode() -> Int {
        return countryCode!
    }
    
    public func setCountryCode(countryCode : Int) {
        self.countryCode = countryCode
    }
    
    //ContactNumber
    public func getContactNumber() -> String {
        return contactNumber!
    }
    
    public func setContactNumber(contactNumber : String) {
        self.contactNumber = contactNumber
    }
    
    //ZipCode
    public func getZipCode() -> String {
        return zipCode!
    }
    
    public func setZipCode(zipCode : String) {
        self.zipCode = zipCode
    }
    
    //City
    public func getCity() -> String {
        return city!
    }
    
    public func setCity(city : String) {
        self.city = city
    }
    
    //State
    public func getState() -> String {
        return state!
    }
    
    public func setState(state : String) {
        self.state = state
    }
    
    //ImageId
    public func getImageId() -> String {
        return imageId!
    }
    
    public func setImageId(imageId : String) {
        self.imageId = imageId
    }
    
    //Dob
    public func getDob() -> String {
        return dob!
    }
    
    public func setDob(dob : String) {
        self.dob = dob
    }
    
    //Verified
    public func getVerified() -> Bool {
        return verified!
    }
    
    public func setVerified(verified : Bool) {
        self.verified = verified
    }
    
    //Gender
    public func getGender() -> String {
        return gender!
    }
    
    public func setGender(gender : String) {
        self.gender = gender
    }
    
    //Address
    public func getAddress() -> String {
        return address!
    }
    
    public func setAddress(address : String) {
        self.address = address
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Status
    public func getStatus() -> String {
        return status!
    }
    
    public func setStatus(status : String) {
        self.status = status
    }
    
    //AcceptorId
    public func getAcceptorId() -> Int {
        return acceptorId!
    }
    
    public func setAcceptorId(acceptorId : Int) {
        self.acceptorId = acceptorId
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        if imageUrl == nil {
            return ""
        }
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    
    //Name
    public func getName() -> String {
        if name == nil {
            return ""
        }
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }

    
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [GetFriendListData]
    {
        var models:[GetFriendListData] = []
        for item in array
        {
            models.append(GetFriendListData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/  init() { }
    
	required public init?(dictionary: NSDictionary) {

		mediaId = dictionary["mediaId"] as? Int
		email = dictionary["email"] as? String
		firstName = dictionary["firstName"] as? String
		lastName = dictionary["lastName"] as? String
		signupType = dictionary["signupType"] as? String
		countryCode = dictionary["countryCode"] as? Int
		contactNumber = dictionary["contactNumber"] as? String
		zipCode = dictionary["zipCode"] as? String
		city = dictionary["city"] as? String
		state = dictionary["state"] as? String
		imageId = dictionary["imageId"] as? String
		dob = dictionary["dob"] as? String
		verified = dictionary["verified"] as? Bool
		gender = dictionary["gender"] as? String
		address = dictionary["address"] as? String
		id = dictionary["id"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		status = dictionary["status"] as? String
		acceptorId = dictionary["acceptorId"] as? Int
		imageUrl = dictionary["imageUrl"] as? String
		name = dictionary["name"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.mediaId, forKey: "mediaId")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.firstName, forKey: "firstName")
		dictionary.setValue(self.lastName, forKey: "lastName")
		dictionary.setValue(self.signupType, forKey: "signupType")
		dictionary.setValue(self.countryCode, forKey: "countryCode")
		dictionary.setValue(self.contactNumber, forKey: "contactNumber")
		dictionary.setValue(self.zipCode, forKey: "zipCode")
		dictionary.setValue(self.city, forKey: "city")
		dictionary.setValue(self.state, forKey: "state")
		dictionary.setValue(self.imageId, forKey: "imageId")
		dictionary.setValue(self.dob, forKey: "dob")
		dictionary.setValue(self.verified, forKey: "verified")
		dictionary.setValue(self.gender, forKey: "gender")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.acceptorId, forKey: "acceptorId")
		dictionary.setValue(self.imageUrl, forKey: "imageUrl")
		dictionary.setValue(self.name, forKey: "name")

		return dictionary
	}
    
    //PARAM
    //Friend
    private var userId : String?
    
    //Friend-userId
    public func getUserId() -> String {
        return userId!
    }
    
    public func setUserId(userId : String) {
        self.userId = userId
    }
    
    //FRIEND_PARAM
    func toGetFriendDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                if (key == "userId") {
                    dictionary[key] = child.value
                }
            }
        }
        return dictionary
    }
    
    //SEND_FRIEND_REQUEST_PARAM
    func toGetFriendRequestDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                if (key == "userId") {
                    dictionary[key] = child.value
                } else if (key == "inviteMessage") {
                    dictionary[key] = child.value
                }
            }
        }
        return dictionary
    }

}
