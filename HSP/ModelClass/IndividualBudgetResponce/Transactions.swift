/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Transactions {
    private var amount : Int?
    private var date : String?
    private var note : String?
    private var budgetCategoryId : BudgetCategoryId?
    private var type : String?
    private var id : Int?
    private var origin : String?
    private var studentId : StudentId?
    private var imageUrl : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let transactions_list = Transactions.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Transactions Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Transactions]
    {
        var models:[Transactions] = []
        for item in array
        {
            models.append(Transactions(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let transactions = Transactions(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Transactions Instance.
*/
	required public init?(dictionary: NSDictionary) {

		amount = dictionary["amount"] as? Int
		date = dictionary["date"] as? String
		note = dictionary["note"] as? String
		if (dictionary["budgetCategoryId"] != nil) { budgetCategoryId = BudgetCategoryId(dictionary: dictionary["budgetCategoryId"] as! NSDictionary) }
		type = dictionary["type"] as? String
		id = dictionary["id"] as? Int
		origin = dictionary["origin"] as? String
		if (dictionary["studentId"] != nil) { studentId = StudentId(dictionary: dictionary["studentId"] as! NSDictionary) }
		imageUrl = dictionary["imageUrl"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    
    init() {
        
    }
    
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.amount, forKey: "amount")
		dictionary.setValue(self.date, forKey: "date")
		dictionary.setValue(self.note, forKey: "note")
		dictionary.setValue(self.budgetCategoryId?.dictionaryRepresentation(), forKey: "budgetCategoryId")
		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.origin, forKey: "origin")
		dictionary.setValue(self.studentId?.dictionaryRepresentation(), forKey: "studentId")
		dictionary.setValue(self.imageUrl, forKey: "imageUrl")

		return dictionary
	}
    
   
    
    
    //Amount
    public func getAmount() -> Int {
        return amount!
    }
    
    public func setAmount(amount : Int) {
        self.amount = amount
    }
    
    //Date
    public func getDate() -> String {
        return date!
    }
    
    public func setDate(date : String) {
        self.date = date
    }
    
    //Note
    public func getNote() -> String {
        return note!
    }
    
    public func setNote(note : String) {
        self.note = note
    }
    
    //BudgetCategoryId
    public func getBudgetCategoryId() -> BudgetCategoryId {
        return budgetCategoryId!
    }
    
    public func setBudgetCategoryId(budgetCategoryId : BudgetCategoryId) {
        self.budgetCategoryId = budgetCategoryId
    }
    
    //Type
    public func getType() -> String {
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //Origin
    public func getOrigin() -> String {
        return origin!
    }
    
    public func setOrigin(origin : String) {
        self.origin = origin
    }
    
    //StudentId
    public func getStudentId() -> StudentId {
        return studentId!
    }
    
    public func setStudentId(studentId : StudentId) {
        self.studentId = studentId
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }

}
