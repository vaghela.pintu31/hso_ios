/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class IndividualBudgetData {
    
    private var budgetCategories = [BudgetCategories]()
    private var type : String?
    private var remainingAmount : Int?
    private var spentAmount : Int?
    private var id : Int?
    private var userId : Int?
    private var studentId : Int?
    private var transactions = [Transactions]()

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [IndividualBudgetData]
    {
        var models:[IndividualBudgetData] = []
        for item in array
        {
            models.append(IndividualBudgetData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		if (dictionary["budgetCategories"] != nil) { budgetCategories = BudgetCategories.modelsFromDictionaryArray(array: dictionary["budgetCategories"] as! NSArray) }
		type = dictionary["type"] as? String
		remainingAmount = dictionary["remainingAmount"] as? Int
		spentAmount = dictionary["spentAmount"] as? Int
		id = dictionary["id"] as? Int
		userId = dictionary["userId"] as? Int
		studentId = dictionary["studentId"] as? Int
		if (dictionary["transactions"] != nil) { transactions = Transactions.modelsFromDictionaryArray(array: dictionary["transactions"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.remainingAmount, forKey: "remainingAmount")
		dictionary.setValue(self.spentAmount, forKey: "spentAmount")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.studentId, forKey: "studentId")

		return dictionary
	}
    
    //BudgetCategories
    public func getBudgetCategories() -> [BudgetCategories] {
        return budgetCategories
    }
    
    public func setBudgetCategories(budgetCategories : [BudgetCategories]) {
        self.budgetCategories = budgetCategories
    }
    
    //Type
    public func getType() -> String {
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //RemainingAmount
    public func getRemainingAmount() -> Int {
        return remainingAmount!
    }
    
    public func setRemainingAmount(remainingAmount : Int) {
        self.remainingAmount = remainingAmount
    }
    
    //SpentAmount
    public func getSpentAmount() -> Int {
        return spentAmount!
    }
    
    public func setSpentAmount(spentAmount : Int) {
        self.spentAmount = spentAmount
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //UserId
    public func getUserId() -> Int {
        return userId!
    }
    
    public func setUserId(userId : Int) {
        self.userId = userId
    }
    
    //StudentId
    public func getStudentId() -> Int {
        return studentId!
    }
    
    public func setStudentId(studentId : Int) {
        self.studentId = studentId
    }
    
    //Transactions
    public func getTransactions() -> [Transactions] {
        return transactions
    }
    
    public func setTransactions(transactions : [Transactions]) {
        self.transactions = transactions
    }


}
