/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class LessonAssignment {
    
    private var title : String?
    private var noteTitle : String?
    private var category : String?
    private var week : Int?
    private var day : Int?
    
    //Title
    public func getTitle() -> String {
        return title!
    }
    
    public func setTitle(title : String) {
        self.title = title
    }
    
    //noteTitle
    public func getNoteTitle() -> String {
        if noteTitle == nil {
            return ""
        }
        return noteTitle!
    }
    
    public func setNoteTitle(noteTitle : String) {
        self.noteTitle = noteTitle
    }
    
    //Category
    public func getCategory() -> String {
        return category!
    }
    
    public func setCategory(category : String) {
        self.category = category
    }
    
    //Week
    public func getWeek() -> Int {
        return week!
    }
    
    public func setWeek(week : Int) {
        self.week = week
    }
    
    //Day
    public func getDay() -> Int {
        return day!
    }
    
    public func setDay(day : Int) {
        self.day = day
    }

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let lessonAssignment_list = LessonAssignment.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of LessonAssignment Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [LessonAssignment]
    {
        var models = [LessonAssignment]()
        for item in array {
            models.append(LessonAssignment(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let lessonAssignment = LessonAssignment(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: LessonAssignment Instance.
*/
     init() { }
    
	required public init?(dictionary: NSDictionary) {

		title = dictionary["title"] as? String
        noteTitle = dictionary["noteTitle"] as? String
		category = dictionary["category"] as? String
		week = dictionary["week"] as? Int
		day = dictionary["day"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.noteTitle, forKey: "noteTitle")
		dictionary.setValue(self.category, forKey: "category")
		dictionary.setValue(self.week, forKey: "week")
		dictionary.setValue(self.day, forKey: "day")

		return dictionary
	}
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        return dictionary
    }


}
