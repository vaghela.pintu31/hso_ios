/*
 Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import Foundation

/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class LessonPlan {
    private var name : String?
    private var weeks = 0
    private var days = 0
    private var hours = 0.0
    private var lessonAssignment = [LessonAssignment]()
    private var lessonNote = [LessonNote]()
    
    
    //Name
    public func getName() -> String {
        if name == nil {
            return ""
        } else {
            return name!
        }
    }
    
    public func setName(name : String) {
        self.name = name
    }
    
    //Weeks
    public func getWeeks() -> Int {
        if weeks == 0 {
            return 0
        } else {
            return weeks
        }
    }
    
    public func setWeeks(weeks : Int) {
        self.weeks = weeks
    }
    
    //Days
    public func getDays() -> Int {
        if days == 0 {
            return 0
        } else {
            return days
        }
    }
    
    public func setDays(days : Int) {
        self.days = days
    }
    
    //Hours
    public func getHours() -> Double {
        if hours == 0 {
            return 0
        } else {
            return Double(hours)
        }
    }
    
    public func setHours(hours : Double) {
        self.hours = hours
    }
    
    //LessonAssignment
    public func getLessonAssignment() -> [LessonAssignment] {
        return lessonAssignment
    }
    
    public func setLessonAssignment(lessonAssignment : [LessonAssignment]) {
        self.lessonAssignment = lessonAssignment
    }
    
    //LessonNote
    public func getLessonNote() -> [LessonNote] {
        return lessonNote
    }
    
    public func setLessonNote(lessonNote : [LessonNote]) {
        self.lessonNote = lessonNote
    }
    
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let lessonPlan_list = LessonPlan.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of LessonPlan Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [LessonPlan]
    {
        var models:[LessonPlan] = []
        for item in array
        {
            models.append(LessonPlan(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let lessonPlan = LessonPlan(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: LessonPlan Instance.
     */
    init() { }
    
    required public init?(dictionary: NSDictionary) {
        
        name = dictionary["name"] as? String
        weeks = (dictionary["weeks"] as? Int)!
        days = (dictionary["days"] as? Int)!
        hours = (dictionary["hours"] as? Double)!
        
        lessonAssignment = LessonAssignment.modelsFromDictionaryArray(array:dictionary["lessonAssignment"] as! NSArray)
        
        lessonNote = LessonNote.modelsFromDictionaryArray(array: dictionary["lessonNote"] as! NSArray)
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.weeks, forKey: "weeks")
        dictionary.setValue(self.days, forKey: "days")
        dictionary.setValue(self.hours, forKey: "hours")
        return dictionary
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                if key == "lessonAssignment" {
                    let lessonArray = child.value as! [LessonAssignment]
                    dictionary[key] = lessonArray.map({
                        (value: LessonAssignment) -> NSDictionary in
                        return value.dictionaryRepresentation()
                    })
                } else if key == "lessonNote" {
                    let lessonNoteArray = child.value as! [LessonNote]
                    dictionary[key] = lessonNoteArray.map({
                        (value: LessonNote) -> NSDictionary in
                        return value.dictionaryRepresentation()
                    })
                } else {
                    dictionary[key] = child.value
                }
            }
        }
        return dictionary
    }
}
