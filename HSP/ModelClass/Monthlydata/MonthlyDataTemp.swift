//
//  User.swift
//  HSP
//
//  Created by Keyur Ashra on 22/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class MonthlyDataTemp: NSObject {
    private var date : String?
    private var data = [DataTemp]()

    //Id
    public func getDate() -> String {
        return date!
    }
    
    public func setDate(date : String) {
        self.date = date
    }
    
    //Name
    public func getData() -> [DataTemp] {
        return data
    }
    
    public func setData(data : [DataTemp]) {
        self.data = data
    }
    
    
    
    func setData() -> [MonthlyDataTemp] {
        var mList = [MonthlyDataTemp]()
        
        let user1 = MonthlyDataTemp()
        user1.date = "08-03-2017"
        
        let dataTemp1 = DataTemp()
        dataTemp1.setSubject(subject: "Maths")
        dataTemp1.setAttendance(attendance: true)
        user1.data.append(dataTemp1)
        
        mList.append(user1)
        
       
        
        let user3 = MonthlyDataTemp()
        user3.date = "10-03-2017"
        let dataTemp4 = DataTemp()
        dataTemp4.setSubject(subject: "Chemistry")
        dataTemp4.setAttendance(attendance: true)
        user3.data.append(dataTemp4)
        
        let dataTemp5 = DataTemp()
        dataTemp5.setSubject(subject: "Science")
        dataTemp5.setAttendance(attendance: false)
        user3.data.append(dataTemp5)
        mList.append(user3)
        
        
        let user11 = MonthlyDataTemp()
        user11.date = "08-03-2017"
        
        let dataTemp11 = DataTemp()
        dataTemp11.setSubject(subject: "Sports")
        dataTemp11.setAttendance(attendance: true)
        user11.data.append(dataTemp11)
        
        mList.append(user11)
        
        let user4 = MonthlyDataTemp()
        user4.date = "11-03-2017"
        let dataTemp6 = DataTemp()
        dataTemp6.setSubject(subject: "Chemistry")
        dataTemp6.setAttendance(attendance: true)
        user4.data.append(dataTemp6)
        
        let dataTemp7 = DataTemp()
        dataTemp7.setSubject(subject: "Science")
        dataTemp7.setAttendance(attendance: false)
        user4.data.append(dataTemp7)
        mList.append(user4)
        
        
        let user12 = MonthlyDataTemp()
        user12.date = "08-03-2017"
        
        let dataTemp12 = DataTemp()
        dataTemp12.setSubject(subject: "Sports")
        dataTemp12.setAttendance(attendance: true)
        user12.data.append(dataTemp12)
        
        mList.append(user12)
        
        let user2 = MonthlyDataTemp()
        user2.date = "09-03-2017"
        let dataTemp2 = DataTemp()
        dataTemp2.setSubject(subject: "Maths")
        dataTemp2.setAttendance(attendance: true)
        user2.data.append(dataTemp2)
        
        let dataTemp3 = DataTemp()
        dataTemp3.setSubject(subject: "Science")
        dataTemp3.setAttendance(attendance: false)
        user2.data.append(dataTemp3)
        
        
        let dataTemp33 = DataTemp()
        dataTemp33.setSubject(subject: "chemistry")
        dataTemp33.setAttendance(attendance: false)
        user2.data.append(dataTemp33)
        
        mList.append(user2)
        
        let user8 = MonthlyDataTemp()
        user8.date = "11-03-2017"
        let dataTemp66 = DataTemp()
        dataTemp66.setSubject(subject: "English")
        dataTemp66.setAttendance(attendance: true)
        user8.data.append(dataTemp66)
        
        let dataTemp77 = DataTemp()
        dataTemp77.setSubject(subject: "Science")
        dataTemp77.setAttendance(attendance: false)
        user8.data.append(dataTemp77)
        mList.append(user8)
        
        return mList
    }

}
