//
//  User.swift
//  HSP
//
//  Created by Keyur Ashra on 22/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class ListMonthlyData : NSObject {
    
    private var date : String?
    private var isHiden : Bool?
    private var subject : String?
    private var attendance : Bool?
    
  
    
    public func getDate() -> String {
        return date!
    }
    
    public func setDate(date : String) {
        self.date = date
    }
    
    //Name
    public func getIsHiden() -> Bool {
        return attendance!
    }
    
    public func setIsHiden(isHiden : Bool) {
        self.isHiden = isHiden
    }
    
    
    
    public func getSubject() -> String {
        return subject!
    }
    
    public func setSubject(subject : String) {
        self.subject = subject
    }
    
    //Name
    public func getAttendance() -> Bool {
        return attendance!
    }
    
    public func setAttendance(attendance : Bool) {
        self.attendance = attendance
    }
    
    
}
