//
//  User.swift
//  HSP
//
//  Created by Keyur Ashra on 22/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class DataTemp : NSObject {
    private var subject : String?
    private var attendance : Bool?
    
    //Id
    public func getSubject() -> String {
        return subject!
    }
    
    public func setSubject(subject : String) {
        self.subject = subject
    }
    
    //Name
    public func getAttendance() -> Bool {
        return attendance!
    }
    
    public func setAttendance(attendance : Bool) {
        self.attendance = attendance
    }  
    
  
}
