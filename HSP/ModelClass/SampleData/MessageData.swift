//
//  MessageData.swift
//  HSP
//
//  Created by Keyur Ashra on 27/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class MessageData: NSObject {
    private var id : String?
    private var name : String?
    private var date : String?
    private var sendMsg : String?
    private var sendTime : String?
    private var receiveMsg : String?
    private var receiveTime : String?
    private var imageUrl : String?
    private var imageReceiveUrl : String?
    
    //Id
    public func getId() -> String {
        return id!
    }
    
    public func setId(id : String) {
        self.id = id
    }
    
    //Name
    public func getName() -> String {
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }
    
    //Date
    public func getdate() -> String {
        return date!
    }
    
    public func setdate(date : String) {
        self.date = date
    }
    
    //sendMsg
    public func getSendMsg() -> String {
        return sendMsg!
    }
    
    public func setSendMsg(sendMsg : String) {
        self.sendMsg = sendMsg
    }
    
    //sendTime
    public func getSendTime() -> String {
        return sendTime!
    }
    
    public func setSendTime(sendTime : String) {
        self.sendTime = sendTime
    }
    
    //receiveMsg
    public func getReceiveMsg() -> String {
        return receiveMsg!
    }
    
    public func setReceiveMsg(receiveMsg : String) {
        self.receiveMsg = receiveMsg
    }
    
    //receiveTime
    public func getReceiveTime() -> String {
        return receiveTime!
    }
    
    public func setReceiveTime(receiveTime : String) {
        self.receiveTime = receiveTime
    }
    
    //Image
    public func getImageUrl() -> String {
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    
    //imageReceiveUrl
    public func getImageReceiveUrl() -> String {
        return imageReceiveUrl!
    }
    
    public func setImageReceiveUrl(imageReceiveUrl : String) {
        self.imageReceiveUrl = imageReceiveUrl
    }

    func setData() -> [MessageData] {
        
        var mList = [MessageData]()
        var user1 = MessageData()
        user1.id = "1"
        user1.name = "Keyur"
        user1.date = "2017-02-25"
        user1.sendMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.sendTime = "03:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "woman"
        user1.imageReceiveUrl = "man"
        mList.append(user1)
        
        user1 = MessageData()
        user1.id = "2"
        user1.name = "Jignesh Parekh"
        user1.date = "2017-02-25"
        user1.sendMsg = "Hello how are you??"
        user1.sendTime = "04:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "man"
        user1.imageReceiveUrl = "woman"
        mList.append(user1)
        
        user1 = MessageData()
        user1.id = "3"
        user1.name = "Pintoo"
        user1.date = "2017-02-26"
        user1.sendMsg = "Hello how are you??"
        user1.sendTime = "04:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "woman1"
        user1.imageReceiveUrl = "woman"
        mList.append(user1)
        
        user1 = MessageData()
        user1.id = "4"
        user1.name = "Brijesh"
        user1.date = "2017-02-26"
        user1.sendMsg = "Hello how are you?? Hello how are you??"
        user1.sendTime = "08:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "man"
        user1.imageReceiveUrl = "woman1"
        mList.append(user1)
        
        user1 = MessageData()
        user1.id = "5"
        user1.name = "Vishal"
        user1.date = "2017-02-27"
        user1.sendMsg = "Hello how are you?? Hello how are you??"
        user1.sendTime = "08:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "woman"
        user1.imageReceiveUrl = "man"
        mList.append(user1)
        
        user1 = MessageData()
        user1.id = "6"
        user1.name = "Dhaval"
        user1.date = "2017-02-27"
        user1.sendMsg = "Hello how are you?? Hello how are you??"
        user1.sendTime = "08:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "woman1"
        user1.imageReceiveUrl = "man"
        mList.append(user1)
        
        user1 = MessageData()
        user1.id = "7"
        user1.name = "Paumil"
        user1.date = "2017-02-27"
        user1.sendMsg = "Hello how are you?? Hello how are you??"
        user1.sendTime = "08:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "man"
        user1.imageReceiveUrl = "woman"
        mList.append(user1)
        
        user1 = MessageData()
        user1.id = "8"
        user1.name = "Sameet"
        user1.date = "2017-02-28"
        user1.sendMsg = "Hello how are you?? Hello how are you??"
        user1.sendTime = "08:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "woman1"
        user1.imageReceiveUrl = "man"
        mList.append(user1)
        
        user1 = MessageData()
        user1.id = "9"
        user1.name = "Mithun"
        user1.date = "2017-02-29"
        user1.sendMsg = "Hello how are you?? Hello how are you??"
        user1.sendTime = "08:25 PM"
        user1.receiveMsg = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        user1.receiveTime = "05:25 PM"
        user1.imageUrl = "woman"
        user1.imageReceiveUrl = "man"
        mList.append(user1)
        
        return mList
    }


}
