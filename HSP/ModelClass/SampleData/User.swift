//
//  User.swift
//  HSP
//
//  Created by Keyur Ashra on 22/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class User: NSObject {
    private var id : String?
    private var name : String?
    private var grade : String?
    private var time : String?
    private var date : String?
    private var lastMsg : String?
    private var imageUrl : String?
    private var amount: String?
    private var subject: String?
    private var longDesc: String?

    //Id
    public func getId() -> String {
        return id!
    }
    
    public func setId(id : String) {
        self.id = id
    }
    
    //Name
    public func getName() -> String {
        return name!
    }
    
    public func setName(name : String) {
        self.name = name
    }
    
    //Grade
    public func getGrade() -> String {
        return grade!
    }
    
    public func setGrade(grade : String) {
        self.grade = grade
    }
    
    //Time
    public func getTime() -> String {
        return time!
    }
    
    public func setTime(time : String) {
        self.time = time
    }
    
    //Date
    public func getDate() -> String {
        return date!
    }
    
    public func setDate(date : String) {
        self.date = date
    }
    
    //lastMsg
    public func getlastMsg() -> String {
        return lastMsg!
    }
    
    public func setlastMsg(lastMsg : String) {
        self.lastMsg = lastMsg
    }

    //Image
    public func getImageUrl() -> String {
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }
    
    //Amount
    public func getAmount() -> String {
        return amount!
    }
    
    public func setAmount(amount : String) {
        self.amount = amount
    }
    
    //Subject
    public func getSubject() -> String {
        return subject!
    }
    
    public func setSubject(subject : String) {
        self.subject = subject
    }

    //LongDesc
    public func getLongDesc() -> String {
        return longDesc!
    }
    
    public func setLongDesc(longDesc : String) {
        self.longDesc = longDesc
    }

 
    func setData() -> [User] {
        
        var mList = [User]()
        var user1 = User()
        user1.id = "1"
        user1.name = "Keyur"
        user1.grade = "Grade-1"
        user1.time = "03:25 PM"
        user1.date = "03/08/2017"
        user1.lastMsg = "Hey there I am using..."
        user1.imageUrl = "woman"
        user1.subject = "Books"
        user1.amount = "$1000"
        user1.longDesc = "Lorem ipsum is not simply random text."
        mList.append(user1)
        
        user1 = User()
        user1.id = "2"
        user1.name = "Jignesh Parekh"
        user1.grade = "Grade-2"
        user1.time = "03:20 PM"
        user1.date = "04/08/2017"
        user1.lastMsg = "How are you?"
        user1.imageUrl = "man"
        user1.subject = "Sports"
        user1.amount = "$800"
        user1.longDesc = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        mList.append(user1)
        
        user1 = User()
        user1.id = "3"
        user1.name = "Pintoo"
        user1.grade = "Grade-3"
        user1.time = "02:25 PM"
        user1.date = "05/08/2017"
        user1.lastMsg = "Wassup"
        user1.imageUrl = "man"
        user1.subject = "Uniform"
        user1.amount = "$600"
        user1.longDesc = "Lorem ipsum is not simply dummy text of the printing and type setting industry."
        mList.append(user1)
        
        user1 = User()
        user1.id = "4"
        user1.name = "Brijesh"
        user1.grade = "Grade-4"
        user1.time = "01:11 PM"
        user1.date = "06/08/2017"
        user1.lastMsg = "I am doing very well.."
        user1.imageUrl = "woman1"
        user1.subject = "Stationary"
        user1.amount = "$400"
        user1.longDesc = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
        mList.append(user1)
        
        user1 = User()
        user1.id = "5"
        user1.name = "Vishal"
        user1.grade = "Grade-5"
        user1.time = "10:20 AM"
        user1.date = "07/08/2017"
        user1.lastMsg = "🙏🙏🙏Bye-Bye🙏🙏🙏"
        user1.imageUrl = "man"
        user1.subject = "Uniform"
        user1.amount = "$200"
        user1.longDesc = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        mList.append(user1)
        
        user1 = User()
        user1.id = "6"
        user1.name = "Dhaval"
        user1.grade = "Grade-5"
        user1.time = "10:20 AM"
        user1.date = "08/08/2017"
        user1.lastMsg = "🙏🙏🙏Bye-Bye🙏🙏🙏"
        user1.imageUrl = "man"
        user1.subject = "Uniform"
        user1.amount = "$200"
        user1.longDesc = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        mList.append(user1)
        
        user1 = User()
        user1.id = "7"
        user1.name = "Paumil"
        user1.grade = "Grade-5"
        user1.time = "10:20 AM"
        user1.date = "09/08/2017"
        user1.lastMsg = "🙏🙏🙏Bye-Bye🙏🙏🙏"
        user1.imageUrl = "man"
        user1.subject = "Uniform"
        user1.amount = "$200"
        user1.longDesc = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        mList.append(user1)
        
        user1 = User()
        user1.id = "8"
        user1.name = "Sameet"
        user1.grade = "Grade-5"
        user1.time = "10:20 AM"
        user1.date = "10/08/2017"
        user1.lastMsg = "🙏🙏🙏Bye-Bye🙏🙏🙏"
        user1.imageUrl = "man"
        user1.subject = "Uniform"
        user1.amount = "$200"
        user1.longDesc = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        mList.append(user1)

        user1 = User()
        user1.id = "9"
        user1.name = "Mithun"
        user1.grade = "Grade-5"
        user1.time = "10:20 AM"
        user1.date = "11/08/2017"
        user1.lastMsg = "🙏🙏🙏Bye-Bye🙏🙏🙏"
        user1.imageUrl = "man"
        user1.subject = "Uniform"
        user1.amount = "$200"
        user1.longDesc = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        mList.append(user1)
        
        return mList
    }

}
