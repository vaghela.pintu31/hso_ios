//
//  DataManager.swift
//  HSP
//
//  Created by Keyur Ashra on 20/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class DataManager {
    
    static var statusCodes = [Int:String]()
    
    static func getHttpCodeData() ->[Int:String]  {
        if statusCodes.count == 0 {
            let httpCodeList = HttpStatusCode().getStatusCodes()
            statusCodes = httpCodeList
            print("Status Code Not Available")
            return statusCodes
        } else {
            //print("Status Code Available")
            return statusCodes
        }
    }
    
    
    static var genderArray = [NSLocalizedString("Male", comment: ""),
                              NSLocalizedString("Female", comment: "")]
    
    static var studetGradeArray = ["Pre-school","Pre-Kindengarten","Kindengarten","1st Grade","2st Grade","3st Grade","4st Grade","5st Grade","6st Grade","7st Grade","8st Grade","9st Grade","10st Grade","11st Grade","12st Grade"]
    
    static var countryName = [String]()
    static var countryCode = [String]()
    /*
     In Adapter init Read Json
     ---CellForRowAtIndexPath---
     print("Name = \(DataManager.nameListArray[indexPath.row])")
     */
    
    static func readJson() {
        if let path = Bundle.main.url(forResource: "CountryList", withExtension: "json") {
            do {
                let jsonData = try Data(contentsOf: path, options: .mappedIfSafe)
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? NSDictionary {
                        if let personArray = jsonResult.value(forKey: "countryList") as? NSArray {
                            print("ResponseJsonArray = \(personArray)")
                            for (_, element) in personArray.enumerated() {
                                if let element = element as? NSDictionary {
                                    let name = element.value(forKey: "name") as! String
                                    let code = element.value(forKey: "code") as! String
                                    countryName.append(name)
                                    countryCode.append(code)
                                    //nameListArray.append(name)
                                    //messageListArray.append(message)
                                    //timeListArray.append(time)
                                    print("Name: \(name),  Code: \(code)")
                                }
                            }
                        }
                    }
                } catch let error as NSError {
                    print("Error: \(error)")
                }
            } catch let error as NSError {
                print("Error: \(error)")
            }
        }
    }
    
//    static func readChatIndexJson() {
//        if let path = Bundle.main.url(forResource: "ChatIndex", withExtension: "json") {
//            do {
//                let jsonData = try Data(contentsOf: path, options: .mappedIfSafe)
//                do {
//                    if let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? NSDictionary {
//                        if let personArray = jsonResult.value(forKey: "data") as? NSArray {
//                            ChatListingResponse.setData(personArray)
//                            print("ChatIndexDataArray = \(personArray)")
//                            for (_, element) in personArray.enumerated() {
//                                if let element = element as? NSDictionary {
//                                    let name = element.value(forKey: "name") as! String
//                                    let
//                                    print("Name: \(name)")
////                                    ChatListingData.setName(name)
////                                    ChatListingData.setMessages(<#T##ChatListingData#>)
//                                }
//                            }
//                        }
//                    }
//                } catch let error as NSError {
//                    print("Error: \(error)")
//                }
//            } catch let error as NSError {
//                print("Error: \(error)")
//            }
//        }
//    }
}

/*
 private var mHttpCodes = DataManager.getHttpCodeData()
 
 let result = self.mHttpCodes[statusCode]
 if result != nil {
 print("CodeIsAvailable = \(self.mHttpCodes[statusCode]!)")
 self.showAlert(msg: "\(self.mHttpCodes[statusCode]!)")
 } else {
 print("CodeNotAvailabele = \(String(statusCode))")
 }
 
*/
/*
 let dictionaryObject = serverParam.toDictionary()
 let jsonData = try? JSONSerialization.data(withJSONObject: dictionaryObject, options: [])
 signUpRequest.httpBody = jsonData
 ----
 var theJSONText: NSString!
 theJSONText = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
 AppLog.debug(tag: TAG, msg: "JSONConvert \(theJSONText!)")
 signUpRequest.httpBody = theJSONText!.data(using: String.Encoding.utf8.rawValue)
 ---
 //JP//
 let data = try! JSONSerialization.data(withJSONObject: dictionaryObject, options: JSONSerialization.WritingOptions.prettyPrinted)
 
 let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
 if let json = json {
 print("JSON-- \(json)")
 }
 signUpRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
 //JP//
 */
