/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class SaveStudentAssignment {
    private var title : String?
    private var noteTitle : String?
    private var category : String?
    private var startDate : String?
    private var endDate : String?
    private var studentId : Int?
    private var displayTitle : String?
    private var score : String?
    private var attendance : String?
    private var imageId : String?
    private var studentLessonPlanId : Int?
    private var mediaId : Int?
    private var createdAt : String?
    private var updatedAt : String?
    private var id : Int?
    private var imageUrl : String?
    
    
    //Title
    public func getTitle() -> String {
        return title!
    }
    
    public func setTitle(title : String) {
        self.title = title
    }
    
    //noteTitle
    public func getNoteTitle() -> String {
        return noteTitle!
    }
    
    public func setNoteTitle(noteTitle : String) {
        self.noteTitle = noteTitle
    }

    
    //Category
    public func getCategory() -> String {
        return category!
    }
    
    public func setCategory(category : String) {
        self.category = category
    }
    
    //StartDate
    public func getStartDate() -> String {
        return startDate!
    }
    
    public func setStartDate(startDate : String) {
        self.startDate = startDate
    }
    
    //EndDate
    public func getEndDate() -> String {
        return endDate!
    }
    
    public func setEndDate(endDate : String) {
        self.endDate = endDate
    }
    
    //StudentId
    public func getStudentId() -> Int {
        return studentId!
    }
    
    public func setStudentId(studentId : Int) {
        self.studentId = studentId
    }
    
    //DisplayTitle
    public func getDisplayTitle() -> String {
        return displayTitle!
    }
    
    public func setDisplayTitle(displayTitle : String) {
        self.displayTitle = displayTitle
    }
    
    //Score
    public func getScore() -> String {
        return score!
    }
    
    public func setScore(score : String) {
        self.score = score
    }
    
    //Attendance
    public func getAttendance() -> String {
        return attendance!
    }
    
    public func setAttendance(attendance : String) {
        self.attendance = attendance
    }
    
    //ImageId
    public func getImageId() -> String {
        return imageId!
    }
    
    public func setImageId(imageId : String) {
        self.imageId = imageId
    }
    
    //StudentLessonPlanId
    public func getStudentLessonPlanId() -> Int {
        return studentLessonPlanId!
    }
    
    public func setStudentLessonPlanId(studentLessonPlanId : Int) {
        self.studentLessonPlanId = studentLessonPlanId
    }
    
    //MediaId
    public func getMediaId() -> Int {
        return mediaId!
    }
    
    public func setMediaId(mediaId : Int) {
        self.mediaId = mediaId
    }
    
    //CreatedAt
    public func getCreatedAt() -> String {
        return createdAt!
    }
    
    public func setCreatedAt(createdAt : String) {
        self.createdAt = createdAt
    }
    
    //UpdatedAt
    public func getUpdatedAt() -> String {
        return updatedAt!
    }
    
    public func setUpdatedAt(updatedAt : String) {
        self.updatedAt = updatedAt
    }
    
    //Id
    public func getId() -> Int {
        return id!
    }
    
    public func setId(id : Int) {
        self.id = id
    }
    
    //ImageUrl
    public func getImageUrl() -> String {
        return imageUrl!
    }
    
    public func setImageUrl(imageUrl : String) {
        self.imageUrl = imageUrl
    }


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let studentAssignment_list = StudentAssignment.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of StudentAssignment Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [SaveStudentAssignment]
    {
        var models:[SaveStudentAssignment] = []
        for item in array
        {
            models.append(SaveStudentAssignment(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let studentAssignment = StudentAssignment(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: StudentAssignment Instance.
*/
    init() { }
    
	required public init?(dictionary: NSDictionary) {

		title = dictionary["title"] as? String
        noteTitle = dictionary["noteTitle"] as? String
		category = dictionary["category"] as? String
		startDate = dictionary["startDate"] as? String
		endDate = dictionary["endDate"] as? String
		studentId = dictionary["studentId"] as? Int
		displayTitle = dictionary["displayTitle"] as? String
		score = dictionary["score"] as? String
		attendance = dictionary["attendance"] as? String
		imageId = dictionary["imageId"] as? String
		studentLessonPlanId = dictionary["studentLessonPlanId"] as? Int
		mediaId = dictionary["mediaId"] as? Int
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		id = dictionary["id"] as? Int
		imageUrl = dictionary["imageUrl"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.noteTitle, forKey: "noteTitle")
		dictionary.setValue(self.category, forKey: "category")
		dictionary.setValue(self.startDate, forKey: "startDate")
		dictionary.setValue(self.endDate, forKey: "endDate")
		dictionary.setValue(self.studentId, forKey: "studentId")
		dictionary.setValue(self.displayTitle, forKey: "displayTitle")
		dictionary.setValue(self.score, forKey: "score")
		dictionary.setValue(self.attendance, forKey: "attendance")
		dictionary.setValue(self.imageId, forKey: "imageId")
		dictionary.setValue(self.studentLessonPlanId, forKey: "studentLessonPlanId")
		dictionary.setValue(self.mediaId, forKey: "mediaId")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.imageUrl, forKey: "imageUrl")

		return dictionary
	}
    
    func toAssignmentDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                if (key == "title") {
                    dictionary[key] = child.value
                } else if (key == "noteTitle") {
                    dictionary[key] = child.value
                } else if (key == "category") {
                    dictionary[key] = child.value
                } else if (key == "startDate") {
                    dictionary[key] = child.value
                } else if (key == "endDate") {
                    dictionary[key] = child.value
                } else if (key == "studentId") {
                    dictionary[key] = child.value
                } else if (key == "id") {
                    dictionary[key] = child.value
                }
            }
        }
        return dictionary
    }

}
