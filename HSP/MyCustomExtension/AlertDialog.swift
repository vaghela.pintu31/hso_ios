//
//  JPAlert.swift
//  JPAlert
//
//  Created by Keyur Ashra on 21/10/16.
//  Copyright © 2016 Jignesh Parekh. All rights reserved.
//

import UIKit

//GET SCREEN WIDTH HEIGHT
let SCREEN_WIDTH:CGFloat = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT:CGFloat = UIScreen.main.bounds.size.height
let MAIN_COLOR = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)

class AlertDialog: UIViewController {
    
    let kBakcgroundTansperancy: CGFloat = 0.7
    let kHeightMargin: CGFloat = 10.0
    let KTopMargin: CGFloat = 20.0
    let kWidthMargin: CGFloat = 10.0
    let kAnimatedViewHeight: CGFloat = 70.0
    let kMaxHeight: CGFloat = 300.0
    var kContentWidth: CGFloat = 300.0
    let kButtonHeight: CGFloat = 35.0
    var textViewHeight: CGFloat = 90.0
    let kTitleHeight:CGFloat = 30.0
    var contentView = UIView()
    var titleLabel: UILabel = UILabel()
    var subTitleTextView = UITextView()
    var buttons: [UIButton] = []
    var strongSelf:AlertDialog?
    var userAction:((_ button: UIButton) -> Void)? = nil
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.view.frame = UIScreen.main.bounds
        self.view.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.view.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.7)
        self.view.addSubview(contentView)
        
        //Strong reference otherwise click the button can not be executed
        strongSelf = self
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: -Initialization SetupContentView
    func setupContentView() {
        contentView.backgroundColor = UIColor(white: 1.0, alpha: 1.0)
        contentView.layer.cornerRadius = 5.0
        contentView.layer.masksToBounds = true
        contentView.layer.borderWidth = 0.5
        contentView.addSubview(titleLabel)
        contentView.addSubview(subTitleTextView)
        contentView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0xFFFFFF)
        contentView.layer.borderColor = UIColor.colorFromRGB(rgbValue: 0xCCCCCC).cgColor
        view.addSubview(contentView)
    }
    
    
    func setupTitleLabel() {
        titleLabel.text = ""
        titleLabel.backgroundColor = Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0)
        titleLabel.textColor = UIColor.white
        titleLabel.numberOfLines = 1
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont(name: "Nunito", size:20)
    }
    
    func setupSubtitleTextView() {
        subTitleTextView.text = ""
        subTitleTextView.textAlignment = .center
        subTitleTextView.font = UIFont(name: "Nunito", size:16)
        subTitleTextView.textColor = UIColor.colorFromRGB(rgbValue: 0x797979)
        subTitleTextView.isEditable = false
    }
    
    //MARK: -LayoutSet
    func resizeAndRelayout() {
        let mainScreenBounds = UIScreen.main.bounds
        self.view.frame.size = mainScreenBounds.size
        let x: CGFloat = kWidthMargin
        var y: CGFloat = KTopMargin
        let width: CGFloat = kContentWidth - (kWidthMargin*2)
        
        // Title
        if self.titleLabel.text != nil {
            titleLabel.frame = CGRect(x: 0, y: 0, width: kContentWidth, height: 40)
            contentView.addSubview(titleLabel)
            y += kTitleHeight + kHeightMargin
        }
        
        if self.subTitleTextView.text.isEmpty == false {
            let subtitleString = subTitleTextView.text! as NSString
            let rect = subtitleString.boundingRect(with: CGSize(width: width, height: 0.0), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName:subTitleTextView.font!], context: nil)
            textViewHeight = ceil(rect.size.height) + 10.0
            subTitleTextView.frame = CGRect(x: x, y: y, width: width, height: textViewHeight)
            contentView.addSubview(subTitleTextView)
            y += textViewHeight + kHeightMargin
        }
        
        var buttonRect:[CGRect] = []
        for button in buttons {
            button.titleLabel?.font = UIFont(name: "Nunito", size:16)
            let string = button.title(for: UIControlState.normal)! as NSString
            buttonRect.append(string.boundingRect(with: CGSize(width: width, height:0.0), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes:[NSFontAttributeName:button.titleLabel!.font], context:nil))
        }
        
        var totalWidth: CGFloat = 0.0
        if buttons.count == 2 {
            totalWidth = 150
            var buttonX = 0
            
            for i in 0 ..< buttons.count {
                buttons[i].frame = CGRect(x: CGFloat(buttonX), y: y + kHeightMargin, width: totalWidth, height: buttonRect[i].size.height + 20.0)
                buttons[i].layer.borderWidth = 0.5
                
                buttonX = Int(totalWidth)
                self.contentView.addSubview(buttons[i])
            }
        }
        else{
            totalWidth = 300
            buttons[0].frame = CGRect(x: 0, y: y + kHeightMargin, width: totalWidth, height: buttonRect[0].size.height + 20.0)
            
            buttons[0].layer.borderWidth = 0.5
            self.contentView.addSubview(buttons[0])
        }
        y += kHeightMargin
        
        y += kHeightMargin + buttonRect[0].size.height + 10.0
        if y > kMaxHeight {
            let diff = y - kMaxHeight
            let sFrame = subTitleTextView.frame
            subTitleTextView.frame = CGRect(x: sFrame.origin.x, y: sFrame.origin.y, width: sFrame.width, height: sFrame.height - diff)
            
            for button in buttons {
                let bFrame = button.frame
                button.frame = CGRect(x: bFrame.origin.x, y: bFrame.origin.y - diff, width: bFrame.width, height: bFrame.height)
            }
            
            y = kMaxHeight
        }
        
        contentView.frame = CGRect(x: (mainScreenBounds.size.width - kContentWidth) / 2.0, y: (mainScreenBounds.size.height - y) / 2.0, width: kContentWidth, height: y)
        contentView.clipsToBounds = true
        
        //Animation on Entry Of JPAlert
//        contentView.transform = CGAffineTransform(translationX: 0, y: -SCREEN_HEIGHT/2)
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
//            self.contentView.transform =  CGAffineTransform.identity
//        }, completion: nil)
    }
}

extension AlertDialog{
    
    //MARK: -JPAlert Method Body
    func showAlertWithOkButton(title: String, subTitle: String?,action:@escaping ((_ OtherButton: UIButton) -> Void)) {
        userAction = action
        let window: UIWindow = UIApplication.shared.keyWindow!
        window.addSubview(view)
        window.bringSubview(toFront: view)
        view.frame = window.bounds
        self.setupContentView()
        self.setupTitleLabel()
        self.setupSubtitleTextView()
        
        self.titleLabel.text = title
        if subTitle != nil {
            self.subTitleTextView.text = subTitle
        }
        buttons = []
        let button: UIButton = UIButton(type: UIButtonType.custom)
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.setTitleColor(  Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0) , for: .normal)
        button.setTitle(alertbtnOk, for: UIControlState.normal)
        button.isUserInteractionEnabled = true
        button.addTarget(self, action:#selector(AlertDialog.pressed(sender:)), for: .touchUpInside)
        button.tag = 1
        buttons.append(button)
        
        resizeAndRelayout()
    }
    
    
    func showAlertWithYesNoButton(title: String, subTitle: String?,buttonTitle: String ,otherButtonTitle:String?,action:@escaping ((_ OtherButton: UIButton) -> Void)) {
        userAction = action
        let window: UIWindow = UIApplication.shared.keyWindow!
        window.addSubview(view)
        window.bringSubview(toFront: view)
        view.frame = window.bounds
        self.setupContentView()
        self.setupTitleLabel()
        self.setupSubtitleTextView()
        
        self.titleLabel.text = title
        if subTitle != nil {
            self.subTitleTextView.text = subTitle
        }
        buttons = []
        if buttonTitle.isEmpty == false {
            let button: UIButton = UIButton()
            button.layer.borderColor = UIColor.lightGray.cgColor
            button.setTitleColor(Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0) , for: .normal)
            button.setTitle(buttonTitle, for: UIControlState.normal)
            button.isUserInteractionEnabled = true
            button.addTarget(self, action:#selector(AlertDialog.doCancel(sender:)), for: .touchUpInside)
            button.tag = 0
            buttons.append(button)
        }
        
        if otherButtonTitle != nil && otherButtonTitle!.isEmpty == false {
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.layer.borderColor = UIColor.lightGray.cgColor
            button.setTitleColor(Utility.hexStringToUIColor(hex: COLOR_BACKGROUND, alpha: 1.0) , for: .normal)
            button.setTitle(otherButtonTitle, for: UIControlState.normal)
            button.isUserInteractionEnabled = true
            button.addTarget(self, action:#selector(AlertDialog.pressed(sender:)), for: .touchUpInside)
            button.tag = 1
            buttons.append(button)
        }
        resizeAndRelayout()
    }
    
    //MARK: - CancelButtonClick
    func doCancel(sender:UIButton){
        self.view.removeFromSuperview()
        self.cleanUpAlert()
        self.strongSelf = nil
        
        
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
//            self.view.alpha = 0.0
//            self.contentView.transform = CGAffineTransform(translationX: 0, y: SCREEN_HEIGHT)
//            
//        }) { (Bool) -> Void in
//            self.view.removeFromSuperview()
//            self.cleanUpAlert()
//            self.strongSelf = nil
//        }
    }
    
    func cleanUpAlert() {
        self.contentView.removeFromSuperview()
        self.contentView = UIView()
    }
    func pressed(sender: UIButton!) {
        if userAction !=  nil {
            userAction!(sender)
            doCancel(sender: sender)
        }
    }
}

extension UIColor {
    class func colorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

