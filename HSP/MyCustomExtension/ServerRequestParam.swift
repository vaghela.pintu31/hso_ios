//
//  ServerRequestParam.swift
//  HSP
//
//  Created by Keyur Ashra on 04/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

public class ServerRequestParam {
    
    private var user : SignUpUser?
    private var type : String?
    private var token : String?
    private var studentAssignmentId : Int?
    private var attendance : String?
    private var score : String?
    
    //SIGN_UP
    private var firstName : String?
    private var lastName : String?
    private var email : String?
    private var password : String?
    
    //User
    public func getUser() -> SignUpUser {
        return user!
    }
    
    public func setUser(user : SignUpUser) {
        self.user = user
    }
    
    //Type
    public func getType() -> String {
        return type!
    }
    
    public func setType(type : String) {
        self.type = type
    }
    
    //token
    public func getToken() -> String {
        return token!
    }
    
    public func setToken(token : String) {
        self.token = token
    }
    
    //StudentAssignmentId
    public func getStudentAssignmentId() -> Int {
        return studentAssignmentId!
    }
    
    public func setStudentAssignmentId(studentAssignmentId : Int) {
        self.studentAssignmentId = studentAssignmentId
    }
    
    //Attendance
    public func getAttendance() -> String {
        return attendance!
    }
    
    public func setAttendance(attendance : String) {
        self.attendance = attendance
    }
    
    //Score
    public func getScore() -> String {
        return score!
    }
    
    public func setScore(score : String) {
        self.score = score
    }
    
    
    //SIGN_UP
    //Firstname
    public func getFirstName() -> String {
        return firstName!
    }
    
    public func setFirstName(firstName : String) {
        self.firstName = firstName
    }
    
    //Lastname
    public func getLastName() -> String {
        return lastName!
    }
    
    public func setLastName(lastName : String) {
        self.lastName = lastName
    }
    
    //Email
    public func getEmail() -> String {
        return email!
    }
    
    public func setEmail(email : String) {
        self.email = email
    }
    
    //Password
    public func getPassword() -> String {
        if password == nil {
            return ""
        } else {
            return password!
        }
    }
    
    public func setPassword(password : String) {
        self.password = password
    }
    
    //SIGNUP_DICTIONARY
    func toSignupDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                if (key == "type") {
                    dictionary[key] = child.value
                } else {
                    dictionary[key] = user?.toSignupDictionary()
                }
            }
        }
        return dictionary
        
    }
    

    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        
        for child in otherSelf.children {
            if let key = child.label {
                if (key == "type") {
                    dictionary[key] = child.value
                } else if (key == "token") {
                    dictionary[key] = child.value
                } else if (key == "studentAssignmentId") {
                    dictionary[key] = child.value
                } else if (key == "attendance") {
                    dictionary[key] = child.value
                } else if (key == "score") {
                    dictionary[key] = child.value
                } else {
                    dictionary[key] = user?.toDictionary()
                }
            }
        }
        return dictionary
    }
    
    
   
    
}
