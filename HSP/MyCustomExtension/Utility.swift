//
//  Utility.swift
//  HSP
//
//  Created by Keyur Ashra on 16/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import SDWebImage

class Utility: NSObject {
    
    class func convertStringToDictionary(text: NSString) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8.rawValue) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                //print("Something went wrong")
            }
        }
        return nil
    }
    
    class func hexStringToUIColor (hex:String,alpha:Double) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
    class func showTost(strMsg: String, view: UIView){
        let toastLabel = UILabel(frame: CGRect(x: 0.0, y: view.frame.size.height - 44, width: view.frame.size.width, height: 44))
        toastLabel.backgroundColor = UIColor.black
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = NSTextAlignment.center
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Nunito-Regular", size:14)
        view.addSubview(toastLabel)
        toastLabel.text = strMsg
        toastLabel.alpha = 1.0
        //toastLabel.layer.cornerRadius = 10;
        //toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 4.0,delay: 1.0, animations: {
            toastLabel.alpha = 0.0
             toastLabel.frame.origin.y = toastLabel.frame.origin.y + 44
        }, completion: nil)
    }
    
   
    
    class func stringIsNilOrEmpty(aString: String) -> Bool { return (aString).isEmpty }
    /*
     if Utility.stringIsNilOrEmpty(aString: msg) {
        print("here")
     }
     else {
        print("there \(msg)")
     }
     */
    
    //MARK:- Validation
    class func isValidEmail(urlString:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: urlString)
    }
    
    //MARK: ADD_BOLD_TEXT
    class func addBoldText(fullString: NSString, boldPartOfString: NSString, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSFontAttributeName:font!]
        let boldFontAttribute = [NSFontAttributeName:boldFont!]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartOfString as String))
        return boldString
    }
    
    /*
     //USE LIKE//
     let normalFont = UIFont(name: "Nunito-Regular", size: 17)!
     let boldFont = UIFont(name: "Nunito-Bold", size: 17)!
     let attributedString = Utility.addBoldText(fullString: "By creating account you agree to our Terms & Conditions.", boldPartOfString: " Terms & Conditions.", font: normalFont, boldFont: boldFont)

     */
    //ANOTHER WAY
    /*
     //1
     let normalFont = UIFont(name: "Nunito-Regular", size: 15)!
     let boldFont = UIFont(name: "Nunito-Bold", size: 15)!
     let string = "By creating account you agree to our Terms & Conditions." as NSString
     let attributedString = NSMutableAttributedString(string: string as String)
     
     // 2
     let firstAttributes = [NSForegroundColorAttributeName: Utility.hexStringToUIColor(hex: COLOR_PRIMARY_LIGHT, alpha: 1.0 ), NSFontAttributeName: normalFont]
     let secondAttributes = [NSForegroundColorAttributeName: Utility.hexStringToUIColor(hex: COLOR_PIMARY_DARK, alpha: 1.0), NSFontAttributeName: boldFont]
     
     // 3
     attributedString.addAttributes(firstAttributes, range: string.range(of: "By creating account you agree to our"))
     attributedString.addAttributes(secondAttributes, range: string.range(of: " Terms & Conditions."))
     
     //4
     btnTermsAndCondLabelOl.setAttributedTitle(attributedString, for: .normal)
     */
    
    
    
    //DATE_FORMAT_USE
    /*
     let now = NSDate()
     print("now = \(now)")
     let formatter = DateFormatter()
     formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
     formatter.timeZone = TimeZone(secondsFromGMT: 0)
     print(formatter.string(from: now as Date))
     let strDate = formatter.string(from: now as Date)
     
     //SecondWay
     let date = NSDate();
     let dateFormatter = DateFormatter()
     //To prevent displaying either date or time, set the desired style to NoStyle.
     dateFormatter.timeStyle = .medium //Set time style
     dateFormatter.dateStyle = .medium //Set date style
     dateFormatter.timeZone = TimeZone.current
     
     let localDate = dateFormatter.string(from: date as Date)
     
     print("UTC Time")
     print(date)
     print("Local Time")
     print(localDate)
     
     //Calender
     let date = Date()
     let calendar = Calendar.current
     let components = calendar.dateComponents([.year, .month, .day], from: date)
     
     let year =  components.year
     let month = components.month
     let day = components.day
     
     print(year)
     print(month)
     print(day)
     
     //Third
     // create dateFormatter with UTC time format
     let dateFormatter = NSDateFormatter()
     dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
     dateFormatter.timeZone = NSTimeZone(name: "UTC")
     let date = dateFormatter.dateFromString("2015-04-01T11:42:00")
     
     // change to a readable time format and change to local time zone
     dateFormatter.dateFormat = "EEE, MMM d, yyyy - h:mm a"
     dateFormatter.timeZone = NSTimeZone.localTimeZone()
     let timeStamp = dateFormatter.stringFromDate(date!)
     */
    
    /*
    class func addPrefix(name: String,range: Double,day: Int, week: Int) -> String {
        var counter = 0
        var nameIs: String?
        let totalDay:Double = (Double(day * week))
        let ratio:Double = (Double(range / totalDay))
        let roundedRatio = ratio.rounded()
        let remainingCount:Double = 0.0
        print("Name = \(name)\nRange = \(range)\nDay = \(day)\nWeek = \(week)\n")
        print("ratio = \(ratio)")
        print("roundedRatio = \(roundedRatio)")
        print("remainingCount = \(remainingCount)")
        nameIs = name

        for indu in 0..<Int(totalDay) {
            for _ in 0..<Int(roundedRatio) {
                counter += 1
                nameIs = ("\(String(describing: nameIs)) \(counter),")
                print("#\(indu)rounded = \(String(describing: nameIs))")
            }
        }
        return nameIs!
    }
    */
 
    //Convert String To Date
    class func convertDBDateToString(dateString: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        //print(dateString) 
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let returnDate = dateFormatter.date(from: dateString)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "MM-dd-yyyy"// =yyyy-MM-dd
        let dateString = dateFormatterString.string(from: returnDate!)
        //print(dateString)
        return dateString
    }
    
    //Convert String To Date
    class func convertLocalDateToServerDate(string: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        //print(dateString)
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let returnDate = dateFormatter.date(from: string)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "yyyy-MM-dd"// =yyyy-MM-dd
        let dateString = dateFormatterString.string(from: returnDate!)
        //print(dateString)
        return dateString
    }
    
    class func convertLocalTimeToServerTime(dateString: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        //print(dateString)
        dateFormatter.dateFormat = "hh:mm a"
        let returnDate = dateFormatter.date(from: dateString)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "HH:mm"
        let dateString = dateFormatterString.string(from: returnDate!)
        //print(dateString)
        return dateString
    }

    
    
    class func convertDBDateToStringDD_MMM(dateString: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        //print(dateString)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let returnDate = dateFormatter.date(from: dateString)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "dd-MMM"
        let dateString = dateFormatterString.string(from: returnDate!)
        //print(dateString)
        return dateString
    }
    
    class func convertDBDateToStringTime(dateString: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        //print(dateString)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let returnDate = dateFormatter.date(from: dateString)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "hh:mm a"
        let dateString = dateFormatterString.string(from: returnDate!)
        //print(dateString)
        return dateString
    }
    
    class func convertLocalDateTimeToDB(dateString: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        print(dateString)
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        let returnDate = dateFormatter.date(from: dateString)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let dateString = dateFormatterString.string(from: returnDate!)
        print(dateString)
        return dateString
    }
    
    class func convertOldDateToNewDate(string: String) -> String {
        //Convert String To Date
        let dateFormatter = DateFormatter()
        //print(dateString)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let returnDate = dateFormatter.date(from: string)
        
        //Convert Date To String
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "MM-dd-yyyy"// =yyyy-MM-dd
        let dateString = dateFormatterString.string(from: returnDate!)
        //print(dateString)
        return dateString
    }
    
    //Convert String To Date
    class func convertStringToDate(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let returnDate = dateFormatter.date(from: date)
        //print(returnDate!)
        return returnDate!
    }
    
    //Convert From NSDate to String
    class func convertDateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: date)
        //print(dateString)
        return dateString
    }
    
    
    //MARK: PrefrenceData
    //MARK:- StoreUserId
    class func getMyUserId() -> Int {
        if UserDefaults.standard.integer(forKey: strMyUserId) == 0 {
            return 0
        } else {
            let getMyUserId = UserDefaults.standard.integer(forKey: strMyUserId)
            return getMyUserId
        }
    }
    
    class func setMyUserId(myuserId: Int!) {
        UserDefaults.standard.set(myuserId, forKey: strMyUserId)
    }
    
    /* UserLogin */
    class func getUserLogin() -> Bool {
        if UserDefaults.standard.object(forKey: strUserLogin) == nil{
            return false
        }else {
            let isLogin = UserDefaults.standard.object(forKey: strUserLogin) as! Bool
            return isLogin
        }
    }
    
    class func setUserLogin(isLogin: Bool!) {
        UserDefaults.standard.set(isLogin, forKey: strUserLogin)
    }
    
    /* CurrentTimeStamp */
    class func getCurrentTimeStamp() -> String {
        if UserDefaults.standard.object(forKey: strCurrentTimestamp) == nil{
            return ""
        }else {
            let currentTimeStamp = UserDefaults.standard.object(forKey: strCurrentTimestamp) as! String
            return currentTimeStamp
        }
    }
    
    class func setCurrentTimeStamp(timestamp: String!) {
        UserDefaults.standard.set(timestamp, forKey: strCurrentTimestamp)
    }
    
    /* User Name */
    class func getUserName() -> String {
        if UserDefaults.standard.object(forKey: strUserName) == nil {
            return ""
        }else {
            let  userNameis = UserDefaults.standard.object(forKey: strUserName) as! String
            return userNameis
        }
    }
    class func setUserName(username: String!) {
        UserDefaults.standard.set(username, forKey: strUserName)
    }
    
    /* Password */
    class func getPassword() -> String {
        if UserDefaults.standard.object(forKey: strPassword) == nil {
            return ""
        }else {
            let  passwordis = UserDefaults.standard.object(forKey: strPassword) as! String
            return passwordis
        }
    }
    class func setPassword(password: String!) {
        UserDefaults.standard.set(password, forKey: strPassword)
    }
    
    /* User Email */
    class func getUserEmail() -> String {
        if UserDefaults.standard.object(forKey: strUserEmail) == nil {
            return ""
        }else {
            let  userEmailis = UserDefaults.standard.object(forKey: strUserEmail) as! String
            return userEmailis
        }
    }
    class func setUserEmail(EmailId: String!) {
        UserDefaults.standard.set(EmailId, forKey: strUserEmail)
    }
    
    /* User ImageUrl */
    class func getUserImageUrl() -> String {
        if UserDefaults.standard.object(forKey: strUserImageurl) == nil {
            return ""
        }else {
            let  userImageUrlis = UserDefaults.standard.object(forKey: strUserImageurl) as! String
            return userImageUrlis
        }
    }
    class func setUserImageUrl(ImageUrl: String!) {
        UserDefaults.standard.set(ImageUrl, forKey: strUserImageurl)
    }
    
    /* CurrentUserGender */
    class func getUserGender() -> String {
        if UserDefaults.standard.object(forKey: strUserGender) == nil {
            return ""
        }else {
            let  genderis = UserDefaults.standard.object(forKey: strUserGender) as! String
            return genderis
        }
    }
    class func setUserGender(gender: String!) {
        UserDefaults.standard.set(gender, forKey: strUserGender)
    }
    
    /* CurrentUserDob */
    class func getUserDob() -> String {
        if UserDefaults.standard.object(forKey: strUserDob) == nil {
            return ""
        }else {
            let  userdob = UserDefaults.standard.object(forKey: strUserDob) as! String
            return userdob
        }
    }
    class func setUserDob(dob: String!) {
        UserDefaults.standard.set(dob, forKey: strUserDob)
    }
    
    /* CurrentUserCity */
    class func getUserCity() -> String {
        if UserDefaults.standard.object(forKey: strUserCity) == nil {
            return ""
        }else {
            let  userCity = UserDefaults.standard.object(forKey: strUserCity) as! String
            return userCity
        }
    }
    class func setUserCity(city: String!) {
        UserDefaults.standard.set(city, forKey: strUserCity)
    }

    /* CurrentUserState */
    class func getUserState() -> String {
        if UserDefaults.standard.object(forKey: strUserState) == nil {
            return ""
        }else {
            let  userState = UserDefaults.standard.object(forKey: strUserState) as! String
            return userState
        }
    }
    class func setUserState(state: String!) {
        UserDefaults.standard.set(state, forKey: strUserState)
    }
    
    /* GetXAccessToken */
    class func getXAccessToken() -> String {
        if UserDefaults.standard.object(forKey: strXAccessToken) == nil {
            return ""
        }else {
            let  tokenis = UserDefaults.standard.object(forKey: strXAccessToken)
                as! String
            return tokenis
        }
    }
    class func setXAccessToken(token: String!) {
        UserDefaults.standard.set(token, forKey: strXAccessToken)
    }
    
    /* selectedLP_Name */
    class func getSelectedLpName() -> String {
        if UserDefaults.standard.object(forKey: strSelectedlpName) == nil{
            return ""
        }else {
            let selectedData = UserDefaults.standard.object(forKey: strSelectedlpName) as! String
            return selectedData
        }
    }
    
    class func setSelectedLpName(data: String!) {
        UserDefaults.standard.set(data, forKey: strSelectedlpName)
    }
    
    
    /* selectedLP_Name */
    class func getFcmToken() -> String {
        if UserDefaults.standard.object(forKey: strFcmToken) == nil{
            return ""
        }else {
            let selectedData = UserDefaults.standard.object(forKey: strFcmToken) as! String
            return selectedData
        }
    }
    
    class func setFcmToken(data: String!) {
        UserDefaults.standard.set(data, forKey: strFcmToken)
    }
    
    /* selectedLP_Name */
    class func getPushNotification() -> Bool {
        if UserDefaults.standard.object(forKey: strPushNotifiction) == nil{
            return false
        }else {
            let selectedData = UserDefaults.standard.object(forKey: strPushNotifiction) as! Bool
            return selectedData
        }
    }
    
    class func setPushNotification(data: Bool!) {
        UserDefaults.standard.set(data, forKey: strPushNotifiction)
    }
    
    
}


extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
}

extension UITextField {
    class func connectFields(fields:[UITextField]) -> Void {
        guard let last = fields.last else {
            return
        }
        for i in 0 ..< fields.count - 1 {
            fields[i].returnKeyType = .next
            fields[i].addTarget(fields[i+1], action: #selector(UIResponder.becomeFirstResponder), for: .editingDidEndOnExit)
        }
        last.returnKeyType = .go
        last.addTarget(last, action: #selector(UIResponder.resignFirstResponder), for: .editingDidEndOnExit)
    }
}

extension Collection where Iterator.Element == String {
    var initialsFirstChar: [String] {
        return map{String($0.characters.prefix(1))}
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return self[start..<end]
    }
    
    subscript (r: ClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return self[start...end]
    }
    
    /*
     let firstChar = someString[someString.startIndex]
     let lastChar = someString[someString.index(before: someString.endIndex)]
     let charAtIndex = someString[someString.index(someString.startIndex, offsetBy: 10)]
     
     let range = someString.startIndex..<someString.index(someString.startIndex, offsetBy: 10)
     let subtring = someString[range]
     */
}





