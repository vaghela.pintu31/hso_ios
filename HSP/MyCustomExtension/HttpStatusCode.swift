//
//  HttpStatusCode.swift
//  HSP
//
//  Created by Keyur Ashra on 24/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class HttpStatusCode: NSObject {
    private var code : String?
    private var name : String?
    
    //var mHttpCodeList = HttpStatusCode().setData()
    
    //TODO HTTP status code string

    func getStatusCodes() -> [Int:String] {
        var httpsCodeList = [Int:String]()
        //Defaults
        httpsCodeList[001] = "No internet connection"
        httpsCodeList[002] = "Something went wrong please try again."
        httpsCodeList[003] = "No Data Found"
        
        //httpsCodeList[400] = "Bad Request"
        httpsCodeList[401] = "Unauthorized"
        httpsCodeList[402] = "Payment Required"
        httpsCodeList[403] = "Forbidden, User not authorized to perform this action in current workspace"
        httpsCodeList[404] = "Not Found"
        httpsCodeList[405] = "Method Not Allowed"
        httpsCodeList[406] = "Not Acceptable"
        httpsCodeList[407] = "Proxy Authentication Required"
        httpsCodeList[408] = "Request Timeout"
        httpsCodeList[409] = "Conflict"
        httpsCodeList[410] = "Gone"
        httpsCodeList[411] = "Length Required"
        httpsCodeList[412] = "Precondition Failed"
        httpsCodeList[413] = "Request Entity Too Large"
        httpsCodeList[414] = "Request-URI Too Long"
        httpsCodeList[415] = "Unsupported Media Type"
        httpsCodeList[416] = "Requested Range Not Satisfiable"
        httpsCodeList[417] = "Expectation Failed"
        
        httpsCodeList[500] = "Internal Server Error"
        httpsCodeList[501] = "Not Implemented"
        httpsCodeList[502] = "Bad Gateway"
        httpsCodeList[503] = "Service Unavailable"
        httpsCodeList[504] = "Gateway Timeout"
        httpsCodeList[505] = "HTTP Version Not Supported"
        httpsCodeList[506] = "Server not responding"

        return httpsCodeList
    }
    
    func getErrorMessage(msg: String, statusCode: Int) -> String {
        let mHttpCodes = self.getStatusCodes()
        let errorMsg = mHttpCodes[statusCode]
        
        if msg.isEmpty == false {
            print("Not Empty")
        } else {
            print("empty")
        }
        
        if msg != "" {
            return msg
        } else if errorMsg != nil {
            return errorMsg!
        } else {
            return mHttpCodes[002]!
        }
    }
}
