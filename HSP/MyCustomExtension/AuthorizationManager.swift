import Foundation
import Alamofire

public class AuthorizationManager: SessionManager {
    
    //MARK:- Variable Declaration
    private var TAG : String = "AuthorizationManager"
    
    public typealias NetworkSuccessHandler = (AnyObject?) -> Void
    public typealias NetworkFailureHandler = (HTTPURLResponse?, AnyObject?, NSError) -> Void
    private typealias CachedTask = (HTTPURLResponse?, AnyObject?, NSError?) -> Void
    
    private var cachedTasks = Array<CachedTask>()
    private var isRefreshing = false
    private var mLastHitRequest: URLRequest?
    //    private var mLastHitURL: URLConvertible?
    //    private var mLastHitParameters: [String: AnyObject]?
    //    private var mLastHitMethod: HTTPMethod?
    
    var mResponce: AnyObject!
    
    public func startRequest(
        request: URLRequest,
        //method: HTTPMethod,
        //URLString: URLConvertible,
        //parameters: [String: AnyObject]?,
        //encoding: ParameterEncoding,
        success: NetworkSuccessHandler?,
        failure: NetworkFailureHandler?) -> Request?
    {
        /*// Required for last hit api
         self.mLastHitURL = URLString
         self.mLastHitParameters = parameters
         self.mLastHitMethod = method*/
        //self.mLastHitRequest = request
        
        let cachedTask: CachedTask = { [weak self] URLResponse, data, error in
            guard let strongSelf = self else { return }
            if let error = error {
                failure?(URLResponse, data, error)
            } else {
                strongSelf.startRequest(
                    request: request,
                    //                    method: method,
                    //                    URLString: URLString,
                    //                    parameters: parameters,
                    //                    encoding: encoding,
                    success: success,
                    failure: failure
                )
            }
        }
        
        if self.isRefreshing {
            self.cachedTasks.append(cachedTask)
            return nil
        }
        
        func returnStatusCode(status_code: Int){
            
        }
        
        // Append your auth tokens here to your parameters
        
        let request = self.request(request)
        request.response { [weak self] response in
            guard let strongSelf = self else { return }
            
            if let response = response.response, response.statusCode == 401  {
                strongSelf.cachedTasks.append(cachedTask)
                Utility.setUserLogin(isLogin: false)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.createJPMenuView()
                
                // strongSelf.refreshTokens(success: success, failure: failure)
                return
            }
            if let response = response.response, response.statusCode == 502 {
                strongSelf.cachedTasks.append(cachedTask)
                let statusCodeIs = response.statusCode
                returnStatusCode(status_code: statusCodeIs)
                return
            }
            if response.error != nil {
                failure?(response.response, response.data as AnyObject, response.error! as NSError)
            } else {
                success?(response.data as AnyObject)
                let cachedTaskCopy = self!.cachedTasks
                self!.cachedTasks.removeAll()
                self!.isRefreshing = false
                cachedTaskCopy.forEach { $0(nil, nil, nil) }
            }
        }
        return request
    }
    
    func refreshTokens(success: NetworkSuccessHandler?,
                       failure: NetworkFailureHandler?) {
        self.isRefreshing = true
        callAPILogin(success: success, failure: failure)
    }
    
    /*
     Token Is Expire and set Upadeted Token
     */
    
    
    func callAPILogin(success: NetworkSuccessHandler?,
                      failure: NetworkFailureHandler?) {
        
        var loginRequest = ServerRequest.getPostRequest(url: "\(mainURL)users/login")
        let params:[String : Any] = ["user": ["email": "\(Utility.getUserName())", "password": Utility.getPassword()], "type": "password"]
        loginRequest.httpBody = try! JSONSerialization.data(withJSONObject: params)
        
        _ = SessionManager.default.request(loginRequest).responseJSON { [weak self] response in
            guard self != nil else { return }
            if response.response?.statusCode == 200 {
                do {
                    let loginResponse = SignUpResponse(dictionary: response.result.value as! NSDictionary)
                    Utility.setXAccessToken(token: loginResponse?.getData().getAuthToken())
                    AppLog.debug(tag: (self?.TAG)!, msg: "RefreshTokenis = \(Utility.getXAccessToken())")
                    self!.lastHitAPI(success: success, failure: failure)
                }
            } else {
                print("NetworkFailureHandler...\(String(describing: response.response?.statusCode))")
            }
        }
    }
    
    /*
     This method call API and Return NSData
     */
    func lastHitAPI(success: NetworkSuccessHandler?,
                    failure: NetworkFailureHandler?){
        
        
        print("URLRequest = \(mLastHitRequest.debugDescription)")
        print("mLastHitRequestToken = \(String(describing: mLastHitRequest!.value(forHTTPHeaderField: "X-Access-Token")))")
        mLastHitRequest?.setValue(Utility.getXAccessToken(), forHTTPHeaderField: "X-Access-Token")
        print("Header = \(String(describing: mLastHitRequest?.allHTTPHeaderFields))")
        print("Utility.getXAccessToken()= \(Utility.getXAccessToken())")
        let request = self.request(mLastHitRequest!)
        request.response { [weak self] response in
            guard self != nil else { return }
            if response.response?.statusCode == 200 {
                if response.error != nil   {
                    failure?(response.response, response.data as AnyObject, response.error! as NSError)
                } else {
                    success?(response.data as AnyObject)
                    self!.cachedTasks.removeAll()
                    self!.isRefreshing = false
                    return
                }
            } else if response.response?.statusCode == 401 {
                if response.error != nil{
                    failure?(response.response, response.data as AnyObject, response.error! as NSError)
                }
            }else {
                if response.error != nil{
                    failure?(response.response, response.data as AnyObject, response.error! as NSError)
                } else {
                    success?(response.data as AnyObject)
                    self!.cachedTasks.removeAll()
                    self!.isRefreshing = false
                    return
                }
            }
        }
    }
}
