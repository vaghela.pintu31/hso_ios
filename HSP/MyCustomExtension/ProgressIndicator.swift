

import UIKit

class ProgressIndicator: UIView {
    
    var indicatorColor:UIColor
    var loadingViewColor:UIColor
    var loadingMessage:String
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    init(inview:UIView,loadingViewColor:UIColor,indicatorColor:UIColor,msg:String){
        
        self.indicatorColor = indicatorColor
        self.loadingViewColor = loadingViewColor
        self.loadingMessage = msg
        super.init(frame: CGRect(x: inview.frame.midX - 25, y: inview.frame.midY - 25 , width: 50, height: 50))
        initalizeCustomIndicator()
        
    }
    convenience init(inview:UIView) {
        self.init(inview: inview,loadingViewColor: UIColor.brown,indicatorColor:UIColor.black, msg: "Loading..")
    }
    convenience init(inview:UIView,messsage:String) {
        
        self.init(inview: inview,loadingViewColor: UIColor.brown,indicatorColor:UIColor.black, msg: messsage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    func initalizeCustomIndicator(){
        messageFrame.frame = self.bounds
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicator.color = UIColor.blue
        activityIndicator.hidesWhenStopped = true
        activityIndicator.frame = CGRect(x: self.bounds.origin.x + 15, y: 0, width: 20, height: 50)
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor.lightGray
        messageFrame.alpha = 0.8
        messageFrame.addSubview(activityIndicator)
    }
    
    func  start(){
        //check if view is already there or not..if again started
        if !self.subviews.contains(messageFrame){
            activityIndicator.startAnimating()
            self.addSubview(messageFrame)
        }
    }
    
    func stop(){
        if self.subviews.contains(messageFrame){
            activityIndicator.stopAnimating()
            messageFrame.removeFromSuperview()
            self.removeFromSuperview()
        }
    }
}
