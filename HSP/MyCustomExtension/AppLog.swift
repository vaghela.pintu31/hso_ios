//
//  AppLog.swift
//  HSP
//
//  Created by Keyur Ashra on 25/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit
import os


public class AppLog {
    private static var mPackageName : StaticString = "com.hsp.app"
    private static var mIsLogEnable: Bool! = true
     private static var currentDateTime = Date()
    
    public static func debug(tag : String , msg :String ){
        if(mIsLogEnable) {
            //print("Time:- \(currentDateTime) = \(tag)  : \(msg)")
            print("\(tag) : \(msg)")
           // print("\u{001B}[0;33]myellow")
            //debugPrint("\(currentDateTime) = \(tag)  : \n \(msg)")
           // let logger = OSLog(subsystem: "\(mPackageName)" , category: "\(tag)")
            //os_log(msg , log: logger, type: .debug)
           // NSLog(@"\e[1;31mRed text here\e[m normal text here");
        }
    }

    
    public static func error(tag : StaticString , msg :StaticString){
        if(mIsLogEnable) {
            print("Time:- \(currentDateTime) = \(tag)  : \(msg)")
//            let logger = OSLog(subsystem: "\(mPackageName)" , category: "\(tag)")
  //          os_log(msg , log: logger, type: .error)
        }
    }
    
    public static func defaultLog(tag : StaticString , msg :StaticString){
        if(mIsLogEnable) {
            print("Time:- \(currentDateTime) = \(tag)  : \(msg)")

//            let logger = OSLog(subsystem: "\(mPackageName)" , category: "\(tag)")
  //          os_log(msg, log: logger, type: .default)
        }
    }
    
    public static func info(tag : StaticString , msg :StaticString){
        if(mIsLogEnable) {
            print("Time:- \(currentDateTime) = \(tag)  : \(msg)")

//            let logger = OSLog(subsystem: "\(mPackageName)" , category: "\(tag)")
  //          os_log(msg , log: logger, type: .info)
        }
    }
    
}
