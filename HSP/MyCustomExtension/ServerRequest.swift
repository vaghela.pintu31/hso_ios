//
//  ServerRequest.swift
//  HSP
//
//  Created by Keyur Ashra on 03/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class ServerRequest: NSObject {
    
    static var CONTENT_TYPE_VALUE = "application/json"
    
    static func getGetRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "GET"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        return request
    }
    
    static func getPostRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "POST"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        return request
    }
    
    static func getDeleteRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "DELETE"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        return request
    }
    
    static func getPutRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "PUT"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        return request
    }
    
    static func authenticateGetRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "GET"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.getXAccessToken(), forHTTPHeaderField: "X-Access-Token")
        return request
    }
    
    static func authenticatePostRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "POST"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.getXAccessToken(), forHTTPHeaderField: "X-Access-Token")

        return request
    }
    
    static func authenticateDeleteRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "DELETE"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.getXAccessToken(), forHTTPHeaderField: "X-Access-Token")

        return request
    }
    
    static func authenticatePutRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "PUT"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.getXAccessToken(), forHTTPHeaderField: "X-Access-Token")

        return request
    }
    
    static func authenticatePatchRequest(url: String!)->URLRequest{
        let myUrl = URL(string: url)
        var request = URLRequest(url: myUrl!)
        request.httpMethod = "PATCH"
        request.setValue(CONTENT_TYPE_VALUE, forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.getXAccessToken(), forHTTPHeaderField: "X-Access-Token")
        return request
    }

}
