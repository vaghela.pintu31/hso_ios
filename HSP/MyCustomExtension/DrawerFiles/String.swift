//
//  String.swift
//  JPDrawer
//
//  Created by Keyur Ashra on 25/05/16.
//  Copyright © 2016 Jignesh Parekh. All rights reserved.
//

import Foundation

extension String {
    static func className(_ aClass: AnyClass) -> String {
        //return NSStringFromClass(aClass).componentsSeparatedByString(".").last! //Swift 2.3
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        //return self.substringFromIndex(self.startIndex.advancedBy(from)) //Swift 2.3
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    func chopPrefix(_ count: Int = 1) -> String {
        return substring(from: index(startIndex, offsetBy: count))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        return substring(to: index(endIndex, offsetBy: -count))
    }
    
    var length: Int {
        return self.characters.count
    }
}
