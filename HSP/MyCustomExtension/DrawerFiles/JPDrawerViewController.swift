//
//  JPDrawerViewController.swift
//  JPDrawer
//
//  Created by Keyur Ashra on 26/05/16.
//  Copyright © 2016 Jignesh Parekh. All rights reserved.
//

import UIKit

class JPDrawerViewController: DrawerMenuController
{
    override func isTagetViewController() -> Bool {
        if let vc = UIApplication.topViewController() {
            if vc is HomeViewController ||
                //vc is HomeViewController || //Dashboard--
                vc is LessonPlanListingViewController ||
                vc is LoginViewController || //Calender--
                vc is CollaborateLandingViewController ||
                vc is StudentLandingViewController ||
                vc is BudgetLandingViewController ||
                vc is LoginViewController || //NearbyParents--
                vc is LoginViewController || //AboutUs--
                vc is LoginViewController || //PrivacyPolicy--
                vc is LoginViewController || //TermsAndConditions-- LAST IS Logout--
                vc is LoginViewController {
                return true
            }
        }
        return false
    }

    override func track(_ trackAction: TrackAction) {
        switch trackAction {
        case .leftTapOpen: break
            //print("TrackAction: left tap open.")
        case .leftTapClose: break
            //print("TrackAction: left tap close.")
        case .leftFlickOpen: break
            //print("TrackAction: left flick open.")
        case .leftFlickClose: break
            //print("TrackAction: left flick close.")
        case .rightTapOpen: break
            //print("TrackAction: right tap open.")
        case .rightTapClose: break
            //print("TrackAction: right tap close.")
        case .rightFlickOpen: break
            //print("TrackAction: right flick open.")
        case .rightFlickClose: break
            //print("TrackAction: right flick close.")
        }
    }
}
