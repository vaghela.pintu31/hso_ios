//
//  UIViewController.swift
//  JPDrawer
//
//  Created by Keyur Ashra on 25/05/16.
//  Copyright © 2016 Jignesh Parekh. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setNavigationBarItem(){
        self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!)
        self.addRightBarButtonWithImage(UIImage(named: "ic_notifications_black_24dp")!)
        self.drawerMenuController()?.removeLeftGestures()
        self.drawerMenuController()?.removeRightGestures()
        self.drawerMenuController()?.addLeftGestures()
        self.drawerMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.drawerMenuController()?.removeLeftGestures()
        self.drawerMenuController()?.removeRightGestures()
    }
}
