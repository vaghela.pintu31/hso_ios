//
//  UIView.swift
//  JPDrawer
//
//  Created by Keyur Ashra on 25/05/16.
//  Copyright © 2016 Jignesh Parekh. All rights reserved.
//

import UIKit

extension UIView {
    class func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        let className = String.className(viewType)
        return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
    
    class func loadNib() -> Self {
        return loadNib(self)
    }
}
