//
//  UIStyle.swift
//  HSP
//
//  Created by Keyur Ashra on 10/04/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import UIKit

class UIStyle: NSObject {
    
    static func setAppButtonStyle(view: UIView) {
        let cornerRadius:CGFloat = 3.0
        let borderWidth:CGFloat = 0.5
        view.layer.cornerRadius = cornerRadius
        view.clipsToBounds = true
        view.layer.borderColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0).cgColor
        view.layer.borderWidth = borderWidth
    }
    
    static func setAppButtonStyleRounded(view: UIView) {
        view.layer.cornerRadius = view.frame.height / 2
        view.clipsToBounds = true
    }

    static func setCustomStyle(view: UIView,cornerRadius: CGFloat,boderColor: String,borderWidth: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.clipsToBounds = true
        view.layer.borderColor = Utility.hexStringToUIColor(hex: boderColor, alpha: 1.0).cgColor
        view.layer.borderWidth = borderWidth
    }
    
    static func showEmptyView(message:String, viewController:UIViewController) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: viewController.view.bounds.size.width, height: viewController.view.bounds.size.height))
        messageLabel.text = message
        let bubbleColor = UIColor(red: CGFloat(57)/255, green: CGFloat(81)/255, blue: CGFloat(104)/255, alpha :1)
        messageLabel.textColor = bubbleColor
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Nunito-Regular", size: 15)
        messageLabel.sizeToFit()
        
        viewController.view = messageLabel;
    }
    
    
    static func showEmptyTableView(message:String, viewController: UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: viewController.frame.width, height: viewController.frame.height))
        messageLabel.text = message
        let bubbleColor = UIColor(red: CGFloat(57)/255, green: CGFloat(81)/255, blue: CGFloat(104)/255, alpha :1)
        
        messageLabel.textColor = bubbleColor
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Nunito-Regular", size: 15)
        messageLabel.sizeToFit()
        
        viewController.tableFooterView = UIView()
        viewController.backgroundView = messageLabel;
        viewController.separatorStyle = .none;
        viewController.reloadData()
    }
    
    // MARK: - Draw Bottom Line
    //Navigation bar Bottom Line - Thick
    static func drawBottomLine(optionView:UIView)->UIView{
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: optionView.frame.height-1, width: optionView.frame.width, height: 0.7)
        bottomBorder.backgroundColor = UIColor.red.cgColor
        optionView.layer.addSublayer(bottomBorder)
        
        return optionView;
    }
    
    //SetShadow
    static func setShadow(view: UIView) {
        view.layer.backgroundColor = UIColor.white.cgColor
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 0.0
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 2.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 1.0
    }
    
    //SetTextViewTextFeildBottomBorder
    static func setBottomBorder(textView: UITextView) {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: textView.frame.size.height - width, width: textView.frame.size.width, height: textView.frame.size.height)
        border.borderWidth = width
        textView.layer.addSublayer(border)
        textView.layer.masksToBounds = true
    }
    
    //Adjust TextViewHeight
    static func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    //UIViewAnimation
    static func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: { _ in
            view.isHidden = hidden
        }, completion: nil)
    }
    
    //FlipViews
    static func flipViews(fromView: UIView, toView: UIView,mainView: UIViewController) {
        
        toView.frame.origin.y = 0
        
        mainView.view.isUserInteractionEnabled = false
        
        UIView.transition(from: fromView, to: toView, duration: 0.5, options: .transitionFlipFromLeft, completion: { finished in
            
            fromView.frame.origin.y = -900
            
            mainView.view.isUserInteractionEnabled = true
        })
    }
    
    
}

extension UIView {
    func borders(for edges:[UIRectEdge], width:CGFloat = 0.5, color: UIColor = Utility.hexStringToUIColor(hex: COLOR_SEPARATOR, alpha: 1.0)) {
        
        if edges.contains(.all) {
            layer.borderWidth = width
            layer.borderColor = color.cgColor
        } else {
            let allSpecificBorders:[UIRectEdge] = [.top, .bottom, .left, .right]
            
            for edge in allSpecificBorders {
                if let v = viewWithTag(Int(edge.rawValue)) {
                    v.removeFromSuperview()
                }
                
                if edges.contains(edge) {
                    let v = UIView()
                    v.tag = Int(edge.rawValue)
                    v.backgroundColor = color
                    v.translatesAutoresizingMaskIntoConstraints = false
                    addSubview(v)
                    
                    var horizontalVisualFormat = "H:"
                    var verticalVisualFormat = "V:"
                    
                    switch edge {
                    case UIRectEdge.bottom:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "[v(\(width))]-(0)-|"
                    case UIRectEdge.top:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v(\(width))]"
                    case UIRectEdge.left:
                        horizontalVisualFormat += "|-(0)-[v(\(width))]"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    case UIRectEdge.right:
                        horizontalVisualFormat += "[v(\(width))]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    default:
                        break
                    }
                    
                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: horizontalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: verticalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                }
            }
        }
    }
    
    
    func fadeIn() {
        // Move our fade out code from earlier
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0 // Instead of a specific instance of, say, birdTypeLabel, we simply set [thisInstance] (ie, self)'s alpha
        }, completion: nil)
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }, completion: nil)
    }
}


