//
//  Constant.swift
//  HSP
//
//  Created by Keyur Ashra on 16/03/17.
//  Copyright © 2017 Riontech. All rights reserved.
//

import Foundation

var mainURL : String = "http://hsp.pivotingsoftwares.com/api/"
var GOOGLE_PLACES_KEY: String = "AIzaSyA4EwscvGzMBQCINhV7n-TahFCHpapsAck"


//MARK: - Color Code
var COLOR_BACKGROUND: String = "#9ACA40" //Background(Button)
var COLOR_PIMARY_DARK: String = "#252A32" //Header & MainText
var COLOR_PRIMARY_REGULAR: String = "#444850" //Secondary
var COLOR_PRIMARY_LIGHT: String = "#919498" //Label & Icon
var COLOR_PRIMARY_EXTRALIGHT: String = "#E9EAEB" //Label(Header)
var COLOR_SEPARATOR: String = "#C8C9CB" //Separator(Box Border)
var COLOR_TOOLBAR: String = "#2E343E" //TOOLBAR(Navigation)
var COLOR_ABSENT: String = "#FF6666" //ABSENT(Cancel)
var COLOR_WHITE: String = "#FFFFFF"
var COLOR_DASHBOARDSELECTED: String = "#3CB21F"

//Fab Background
var COLOR_INVITE: String = "#F5B547"
var COLOR_NEARBY: String = "#489FEC"
var COLOR_ADD_CONTACT: String = "#FF6666"
var COLOR_SEND_MESSAGE: String = "#666699"

//MARK:- AlertDialog
var alertTitle: String = "HSP"
var alertbtnOk: String = "OK"
var alertCancel: String = "Cancel"
var alertYes: String = "Yes"
var alertNo: String = "No"
var serverDown: String = "Could not connect to the server."
var noInternet: String = "No internet connection!"
var unable_to_load_data: String = "Unable to load data!"
var NO_DATA_FOUND: String = "No Data Available."
var STATUS_CODE_ERROR: String = "Oops! Something went to wrong."

// MARK: - StringMessage
var strMinimumLengthUsername: String = "Username must be minimum 6 Characters"
var strMinimumLengthPassword: String = "Password must be minimum 6 Characters"
var alertEmailMsg: String = "Enter Valid Username"

//MARK:- ImagesNotFound
var noImageFound: String = "ic_placeholder"
var strLightAbsent: String = "ic_lightabsent"
var strLightPresent: String = "ic_lightpresent"
var strPresent: String = "ic_present"
var strAbsent: String = "ic_absent"
var ic_appointment: String = "ic_appointment"


//MARK: Prefrences.
var strCurrentTimestamp: String = "currentTimestamp"
var strXAccessToken: String = "xaccesstoken"
var strUserName: String = "userName"
var strPassword: String = "password"
var strUserLogin: String = "userLogin"
var strUserEmail: String = "userEmail"
var strUserImageurl: String = "userImageurl"
var strUserGender: String = "userGender"
var strUserDob: String = "userDob"
var strUserCity: String = "userCity"
var strUserState: String = "userState"

var strSelectedlpName: String = "selectedLpName"
var strFcmToken: String = "FCMToken"
var strPushNotifiction: String = "PushNotifiction"


var strSelectedWeeks: String = "selectedWeeks"
var strSelectedDays: String = "selectedDays"
var strSelectedHours: String = "selectedHours"
var strMyUserId: String = "strMyUserId"
var prefStudentListingResponse: String = "prefStudentListingResponse"








