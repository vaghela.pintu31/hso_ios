//
//  SocketEventUtils.swift
//  Gridle
//
//  Created by Bhavik Bansal on 1/25/17.
//  Copyright © 2017 Bhavik Bansal. All rights reserved.
//

import Foundation
struct SocketEventUtils{
    public static let EVENT_CONNECT = "connect"
    public static let EVENT_DISCONNECT = "disconnect"
    public static let EVENT_ERROR = "error"
    public static let EVENT_RECONNECT = "reconnect"
    public static let EVENT_RECONNECT_ATTEMPT = "reconnectAttempt"
    public static let EVENT_USER_STATUS = "userstatus"
    public static let EVENT_ONLINE = "useronline"
    public static let EVENT_OFFLINE = "useroffline"
    public static let EVENT_CHAT_CREATE = "chatcreate"
    public static let EVENT_MESSAGE_CREATE = "messagecreate"
    public static let EVENT_MESSAGE_CREATE_PROJECT = "messagecreateProject"
    public static let EVENT_MESSAGE_RECEIVED = "message_received"
    public static let EVENT_MESSAGE_DELETE = "messagedelete"
    
    public static let MESSAGE = "message"
    public static let EVENT_TYPE = "message_type"
    public static let EVENT_MESSAGE_CREATE_BOT = "botmessage"
    public static let SOCKET_EVENT_CHAT_CREATE = "chatcreate"
}
