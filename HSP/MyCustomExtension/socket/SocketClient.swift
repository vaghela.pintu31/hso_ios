//
//  SocketClient.swift
//  Gridle
//
//  Created by Bhavik Bansal on 1/19/17.
//  Copyright © 2017 Bhavik Bansal. All rights reserved.
//

import Foundation
import SocketIO
import ObjectMapper

var CHAT_BASE_URL: String = "http://hsp.pivotingsoftwares.com"
var CHAT_SERVER_URL: String = CHAT_BASE_URL + "/api/chats/"
var TAG: String = "SocketClient"

class SocketClient {
    
    var socket: SocketIOClient = SocketIOClient(socketURL: URL(string: CHAT_BASE_URL)!, config: [.log(false), .forcePolling(true),SocketIOClientOption.connectParams(["__sails_io_sdk_version":"0.13.5","__sails_io_sdk_platform":"ios","__sails_io_sdk_language":"swift"])])
    
    //var queue = Queue<Dictionary<String,Any>>()
    
    func connect(){
        if !self.connected() {
            socket.on(SocketEventUtils.EVENT_CONNECT,callback: onConnected)
            socket.on(SocketEventUtils.EVENT_RECONNECT){ (dataArray, socketAck) -> Void in
                AppLog.debug(tag: TAG, msg: "Socket onReconnect")
            }
            socket.on(SocketEventUtils.EVENT_DISCONNECT,callback: onDisconnect)
            socket.on(SocketEventUtils.EVENT_ERROR,callback: onConnectError)
            //socket.on(SocketEventUtils.EVENT_USER_STATUS, callback: onUserStatus)
            //socket.on(SocketEventUtils.EVENT_CHAT_CREATE,callback: onChatCreate)
            //socket.on(SocketEventUtils.EVENT_MESSAGE_CREATE,callback: onMessageCreate)
            
//            socket.on(SocketEventUtils.EVENT_ONLINE, callback: onUserOnline)
//            socket.on(SocketEventUtils.EVENT_OFFLINE, callback: onUserOffline)
//            socket.on(SocketEventUtils.EVENT_MESSAGE_CREATE,callback: onMessageCreate)
//            socket.on(SocketEventUtils.EVENT_MESSAGE_CREATE_PROJECT,callback: onMessageCreate)
//            socket.on(SocketEventUtils.EVENT_MESSAGE_CREATE_BOT,callback: onMessageCreate)
//            socket.on(SocketEventUtils.EVENT_CHAT_CREATE,callback: onChatCreate)
//            socket.on(SocketEventUtils.EVENT_MESSAGE_DELETE, callback: onMessageDelete)
            socket.connect()
        }else{
            AppLog.debug(tag: TAG, msg: "Socket already connected")
        }
    }
    
    func disconnect(){
        socket.off(SocketEventUtils.EVENT_CONNECT)
        socket.off(SocketEventUtils.EVENT_RECONNECT)
        socket.off(SocketEventUtils.EVENT_DISCONNECT)
        socket.off(SocketEventUtils.EVENT_ERROR)
        socket.off(SocketEventUtils.EVENT_ONLINE)
        socket.off(SocketEventUtils.EVENT_OFFLINE)
        socket.off(SocketEventUtils.EVENT_MESSAGE_CREATE)
        socket.off(SocketEventUtils.EVENT_MESSAGE_CREATE_BOT)
        socket.off(SocketEventUtils.EVENT_MESSAGE_CREATE_PROJECT)
        socket.off(SocketEventUtils.EVENT_CHAT_CREATE)
        socket.off(SocketEventUtils.EVENT_MESSAGE_DELETE)
        socket.disconnect()
    }
    
    var onConnected: NormalCallback { return { dataArray,socketAck in
        AppLog.debug(tag: TAG, msg: "Socket onConnected")
        self.subscribe()
        //        while let element = self.queue.dequeue(){
        //            self.request(element["method"] as! String, url: element["url"] as! String,data: element["data"] as! [String : Any]?, ack: element["ack"] as! AckCallback)
        //        }
        }
    }
    
    let onConnectError: NormalCallback = {dataArray,socketAck in
        AppLog.debug(tag: TAG, msg: "Socket onConnectError")
    }
    
    let onDisconnect: NormalCallback = {dataArray,socketAck in
        AppLog.debug(tag: TAG, msg: "Socket Disconnected")
    }
    
    func subscribe() {
        var url = "/api/chats/subscribe"
        url += "?" + getAuthorizationQuery()
        let param:[String: Any] = ["clientType": "IOS"]
        
        post(url,param){ data in
            //print("\nURL: \(url)\nParam: \(param)\nData: \(data)")
            let json  = data[0] as! Dictionary<String, AnyObject>
            //print("SUBSCRIBE = \(json)")
            let body = json["body"] as! Dictionary<String, AnyObject>
            print("JSON = \(body)")
            //            self.getChats(completionHandler: { (ChatListingResponse) in
            //                print("subscribe_GetChatsCALL")
            //            })//OnlyTestingUse
            //            let users = body["onlineUsers"] as! [Int]
            //            for user in users{
            //                //NotificationUtils.sendBroadcast(type: SocketEventUtils.EVENT_ONLINE, message: user)
            //                //Singleton.instance.addSubscriber(user, status: true)
            //            }
        }
    }
    
    
    
//    let onUserStatus: NormalCallback = {dataArray,socketAck in
//        let data = dataArray[0] as! Dictionary<String,Int>
//        AppLog.debug(tag: TAG, msg: "onChatCreate: \(dataArray[0])")
//        //NotificationUtils.sendBroadcast(type: SocketEventUtils.EVENT_USER_STATUS, message: data["user_id"]!)
//    }
//    
//    let onChatCreate: NormalCallback = {dataArray,socketAck in
//        AppLog.debug(tag: TAG, msg: "onChatCreate: \(dataArray[0])")
//        let chatResponse  = dataArray[0] as! Dictionary<String, AnyObject>
//        let statusCode = chatResponse["statusCode"]! as! Int
//        AppLog.debug(tag: TAG, msg: "statusCode: \(statusCode)")
//        //let chatListingResponse = ChatListingResponse(dictionary: dataArray["body"]! as! NSDictionary)
//
//        //let chat  =  Chat(JSONString:GridleUtils.getJsonString(fromObject: dataArray[0]))
//        //if chat != nil{
//            //ChatController().writeChat(chat: chat!)
//            //NotificationUtils.sendBroadcast(type: SocketEventUtils.EVENT_CHAT_CREATE, message: chat!)
//        //}
//    }
//    
//    let onMessageCreate: NormalCallback = {dataArray,socketAck in
//        AppLog.debug(tag: TAG, msg: "onMessageCreate: \(dataArray[0])")
//        /*
//         let message  =  Message(JSONString:GridleUtils.getJsonString(fromObject: dataArray[0]))
//         if(message != nil) {
//         if Singleton.instance.getChatId() == message?.chatId{
//         NotificationUtils.sendBroadcast(type: SocketEventUtils.EVENT_MESSAGE_CREATE, message: message!)
//         } else{
//         if message?.creatorId != GridlePreferenceUtils.getUserId(){
//         NotificationController().scheduleNotification(message!)
//         }
//         }
//         NotificationUtils.sendBroadcast(type: ChatUtils.NOTIFICATION_MESSAGE, message: message!)
//         }*/
//    }
    
    func viewChat(_ chatId: String, completionHandler: @escaping AckCallback){
        var url = mainURL + "/chat/view/" + chatId
        //url += "?messageLimit=" + String(ChatUtils.MESSAGE_LIMIT)
        url += "?" + getAuthorizationQuery()
        get(url,completionHandler)
    }
    
    func getChats(completionHandler: @escaping (_ chatResponse: ChatListingResponse?,_ statusCode: Int) -> Void){
        var url = "/api/chats/index" //?messageLimit=15
        //url += "workspace_id=" + String(GridlePreferenceUtils.getWorkspaceId()) + "&messageLimit=1"
        url += "?" + getAuthorizationQuery()
        self.get(url, { (data) in
            AppLog.debug(tag: TAG, msg: "GetChatsDATA[0]: \(data[0])")
            let chatResponse  = data[0] as! Dictionary<String, AnyObject>
            let statusCode = chatResponse["statusCode"]! as! Int
            let chatListingResponse = ChatListingResponse(dictionary: chatResponse["body"]! as! NSDictionary)
            //let jsonString = GridleUtils.getJsonString(fromObject: chatResponse["body"]!)
            //let chat = APIResponse<ChatListingResponse>(JSONString: jsonString)
            completionHandler(chatListingResponse!, statusCode)
        })
    }
    
    func add(_ chat: NewChat, ack: @escaping AckCallback) {
        var url = "/api/chats/add"
        url += "?" + getAuthorizationQuery()
        let data = Mapper<NewChat>().toJSON(chat)
        AppLog.debug(tag: TAG, msg: "AddNewChatURL: \(url)")
        AppLog.debug(tag: TAG, msg: "AddNewChatData: \(data)")
        AppLog.debug(tag: TAG, msg: "ACK: \(ack)")
        return post(url, data, ack);
    }
    
    
    
    
    
    
    
    /*
     let onUserOnline: NormalCallback = {dataArray,socketAck in
     let data = dataArray[0] as! Dictionary<String,Int>
     NotificationUtils.sendBroadcast(type: SocketEventUtils.EVENT_ONLINE, message: data["user_id"]!)
     }
     
     let onUserOffline: NormalCallback = {dataArray,socketAck in
     let data = dataArray[0] as! Dictionary<String,Int>
     NotificationUtils.sendBroadcast(type: SocketEventUtils.EVENT_OFFLINE, message: data["user_id"]!)
     }
     
     let onMessageCreate: NormalCallback = {dataArray,socketAck in
     //AppLog.debug(tag: TAG, msg: "onMessageCreate: \(dataArray[0]))
     let message  =  Message(JSONString:GridleUtils.getJsonString(fromObject: dataArray[0]))
     if(message != nil) {
     if Singleton.instance.getChatId() == message?.chatId{
     NotificationUtils.sendBroadcast(type: SocketEventUtils.EVENT_MESSAGE_CREATE, message: message!)
     } else{
     if message?.creatorId != GridlePreferenceUtils.getUserId(){
     NotificationController().scheduleNotification(message!)
     }
     }
     NotificationUtils.sendBroadcast(type: ChatUtils.NOTIFICATION_MESSAGE, message: message!)
     }
     }
     
     
     let onMessageDelete: NormalCallback = {dataArray,socketAck in
     //AppLog.debug(tag: TAG, msg: "onMessageDelete: \(dataArray[0]))
     let message  =  Message(JSONString:GridleUtils.getJsonString(fromObject: dataArray[0]))
     if message != nil{
     if Singleton.instance.getChatId() == message?.chatId {
     NotificationUtils.sendBroadcast(type: SocketEventUtils.EVENT_MESSAGE_DELETE, message: message!)
     } else{
     NotificationUtils.sendBroadcast(type: ChatUtils.NOTIFICATION_MESSAGE_DELETE, message: message!)
     }
     }
     }*/
    
    /*
     func getMessages(chatId: String, limit: Int, skip: Int,completionHandler: @escaping (_ messages: [Message]?) -> Void){
     var url =  mainURL + "/message?chat_id=" + chatId + "&limit=" + String(describing: limit) + "&skip=" + String(describing: skip)
     url += "?" + getAuthorizationQuery()
     self.get(url, { (data) in
     let chatResponse  = data[0] as! Dictionary<String, AnyObject>
     //  let jsonString = GridleUtils.getJsonString(fromObject: chatResponse["body"]!)
     // let chat = APIArrayResponse<Message>(JSONString: jsonString)
     completionHandler(chat?.data)
     //AppLog.debug(tag: TAG, msg: "getMessagesJSON: \(jsonString)")
     })
     }*/
    
    
//    func createMessage(_ message: Message, ack: @escaping AckCallback){
//        var url = "/api/message/add"
//        //        if (message.isBotMessage) {
//        //            url = mainURL + "/bot/messagetoBot";
//        //        }
//        url += "?" + getAuthorizationQuery()
//        let data: [String:Any]?
//        //  let data = Mapper<Message>().toJSON(message)
//        data = ["hello": 1]
//        self.post(url,data, ack)
//    }
    
    
    
    func setChatSeen(chatId: String, ack: @escaping AckCallback){
        var url = mainURL + "/chat/setChatSeen"
        url += "?" + getAuthorizationQuery()
        var data = Dictionary<String,String>()
        data["chat_id"] = chatId
        post(url,data,ack);
    }
    
    func post(_ url : String,_ data: [String:Any]?,_ ack: @escaping AckCallback)  {
        request("post", url: url, data:data, ack: ack)
    }
    
    func get(_ url : String,_ ack: @escaping AckCallback)  {
        request("get", url: url, ack: ack)
    }
    
    func request(_ method: String, url: String, data: [String:Any]? = nil, ack: @escaping AckCallback) {
        var payload:Dictionary<String,Any> = Dictionary<String,Any>()
        payload["url"] = url
        payload["data"] = data
        
        if (!self.connected()) {
            socket.reconnect()
            payload["method"] = method
            payload["ack"] = ack
            //queue.append(newElement: payload)
            return
        }
        AppLog.debug(tag: TAG, msg: "requestPAYLOAD: \(payload)")
        socket.emitWithAck(method, payload).timingOut(after: 0 ) { data in
            AppLog.debug(tag: TAG, msg: "requestdata: \(data)")
            //let json  = data[0] as! Dictionary<String, AnyObject>
           // let statusCode = json["statusCode"] as! Int
            //print("statusCode = \(statusCode)")
            ack(data)

//            if json["statusCode"] as! Int == 200 || json["statusCode"] as! Int == 201 {
//                ack(data)
//            } else {
//                ack(data)
//            }
        }
    }
    func connected() -> Bool{
        return socket.status == SocketIOClientStatus.connected;
    }
    func  getAuthorizationQuery() -> String{
        return "authToken=" + Utility.getXAccessToken();
    }
    
    class Payload:Mappable {
        
        required init?(map: Map) { }
        
        //var url: String?
        //var method: String?
        var data: Any?
        func mapping(map: Map) {
            //url <- map["url"]
            //method <- map["method"]
            data <- map["data"]
        }
        required init() { }
    }
    class SocketResponse: Mappable {
        
        required init?(map: Map) { }
        
        var url: String?
        var method: String?
        //Object data;
        func mapping(map: Map) {
            url <- map["url"]
            method <- map["method"]
        }
        
        required init() { }
    }
    
    func getMessages(chatId: String, limit: Int, skip: Int,completionHandler: @escaping (_ messages: ChatListingResponse?,_ statusCode: Int) -> Void){
        var url =  "/api/messages/index?chatId=" + chatId + "&limit=" + String(describing: limit) + "&skip=" + String(describing: skip)
        url += "?" + getAuthorizationQuery()
        self.get(url, { (data) in
            AppLog.debug(tag: TAG, msg: "GetChatsDATA[0]: \(data[0])")
            let chatResponse  = data[0] as! Dictionary<String, AnyObject>
            let statusCode = chatResponse["statusCode"]! as! Int
            let chatListingResponse = ChatListingResponse(dictionary: chatResponse["body"]! as! NSDictionary)
            
            //  let jsonString = GridleUtils.getJsonString(fromObject: chatResponse["body"]!)
            // let chat = APIArrayResponse<Message>(JSONString: jsonString)
            completionHandler(chatListingResponse!, statusCode)
            //AppLog.debug(tag: TAG, msg: "getMessagesJSON: \(jsonString)")
        })
    }
    
    
    func createMessage(_ message: Message, ack: @escaping AckCallback){
        var url = "/api/messages/add"
        url += "?" + getAuthorizationQuery()
        var data = Dictionary<String,String>()
        data["messageBody"] = message.getMessageBody()
        data["chatId"] = message.getChatId()
        self.post(url,data, ack)
    }
    
    func getGroupInfo(chatId: String,completionHandler: @escaping (_ messages: ChatGroupViewResponse?,_ statusCode: Int) -> Void){
        var url =  "/api/chats/view/" + chatId
        url += "?" + getAuthorizationQuery()
        self.get(url, { (data) in
            AppLog.debug(tag: TAG, msg: "GetChatsDATA[0]: \(data[0])")
            let chatResponse  = data[0] as! Dictionary<String, AnyObject>
            let statusCode = chatResponse["statusCode"]! as! Int
            let chatListingResponse = ChatGroupViewResponse(dictionary: chatResponse["body"]! as! NSDictionary)
            completionHandler(chatListingResponse!, statusCode)
        })
    }
    
    
    func getChatsLodeMore(completionHandler: @escaping (_ chatResponse: ChatListingResponse?,_ statusCode: Int) -> Void){
        var url = "/api/chats/index?messagesLimit=10"
        url += "?" + getAuthorizationQuery()
        self.get(url, { (data) in
            AppLog.debug(tag: TAG, msg: "GetChatsDATA[0]: \(data[0])")
            let chatResponse  = data[0] as! Dictionary<String, AnyObject>
            let statusCode = chatResponse["statusCode"]! as! Int
            let chatListingResponse = ChatListingResponse(dictionary: chatResponse["body"]! as! NSDictionary)
            completionHandler(chatListingResponse!, statusCode)
        })
    }
    
}
